import getpass
import os
from typing import Any
import psycopg
import psycopg.rows


def _get_connection_info():
    host = os.environ.get("REMARK_PG_HOST", "localhost")
    port = os.environ.get("REMARK_PG_PORT", "5432")
    db = os.environ.get("REMARK_PG_DB", "remark")
    user = os.environ.get("REMARK_PG_USER", "remark")
    password = os.environ.get("REMARK_PG_PASSWORD")
    schema = os.environ.get("REMARK_PG_SCHEMA", "remark")
    return host, port, db, user, password, schema


def connection_uri():
    host, port, db, user, password, _schema = _get_connection_info()
    return f"postgres://{user}:{password}/{db}"


def create_connection(password = None):
    host, port, db, user, password_, schema = _get_connection_info()
    password = password or password_

    if not password:
        password = getpass.getpass(f"Postgres password for user {user}: ")

    conn = psycopg.connect(
        f"postgres://{user}:{password}@/{db}",
        row_factory=psycopg.rows.dict_row,
    )
    # Open a cursor to perform database operations
    cur = conn.cursor()
    # Execute the set_config command to set the schema
    cur.execute("SELECT set_config('search_path', %s, false);", (schema,))
    return conn


def execute_statement(statement, params=None) -> list[dict[str, Any]]:
    conn = create_connection()
    cur = conn.cursor()
    try:
        if params:
            cur.execute(statement, params)
            conn.commit()
        else:
            cur.execute(statement)
            conn.commit()

        result = cur.fetchall()
        return result
    except Exception as e:
        print(f"An error occurred: {e}")
        conn.rollback()
        raise e
    finally:
        cur.close()
        conn.close()

from io import BytesIO
from loguru import logger
import xlsxwriter
import polars as pl
from psycopg import Connection, sql, errors

import db_connection


def export_run(conn: Connection, file: str | BytesIO, run_id: str | None):
    """Export information about run `run_id` into the file `filename`
    in Excel format. If `run_id` is `None`, the latest run will be used.
    """
    if not run_id:
        try:
            run_id = conn.execute(
                """
                SELECT run_id
                FROM latest_run;
                """
            ).fetchone()["run_id"]  # type: ignore  # this will be caught be the TypeError
        except (errors.UndefinedTable, TypeError):
            raise ValueError(
                "No run_id given and run_id could not be extracted from latest_run "
                "view."
            )
    assert isinstance(run_id, str)

    run_info = conn.execute(
        """
        SELECT *
        FROM runs
        WHERE id = %(run_id)s
        """,
        params={"run_id": run_id},
    ).fetchone()
    if run_info is None:
        raise ValueError(f"{run_id} is not a valid run id")

    logger.info(f"exporting info for {run_info}")

    tables = conn.execute(
        """
        SELECT table_name
        FROM information_schema.columns
        WHERE table_schema = 'remark'
        GROUP BY table_name;
        """,
    )

    # This should be extracted from the run somehow, but is fixed for
    # now. (Maybe add the simulated start time to the runs table?)
    EXPORT_START_DATE = "2023-07-02 00:00:00Z"
    logger.warning(f"Start date for export is fixed to {EXPORT_START_DATE} for now")

    with xlsxwriter.Workbook(file) as workbook:
        for table_row in tables:
            try:
                table = table_row["table_name"]
                if table in (
                    "latest_run",
                    "runs",
                    "changelog",
                    "users",
                    "scenarios",
                    "run_containers",
                    "agent_associations"
                ):
                    continue

                for baseline in (True, False):
                    extension = "_baseline" if baseline else ""
                    table_df = pl.read_database(
                        sql.SQL(
                            """
                            SELECT *
                            FROM {}
                            WHERE run_id = %(run_id)s
                            AND time >= %(export_start)s
                            ORDER BY time, src_id;
                            """
                        ).format(sql.Identifier(table)),
                        conn,
                        execute_options={
                            "params": {
                                "run_id": run_id + extension,
                                "export_start": EXPORT_START_DATE,
                            },
                        },
                        infer_schema_length=None,
                    )
                    table_df = table_df.drop("run_id")
                    table_df.write_excel(
                        workbook,
                        worksheet=table + extension,
                        table_name=table + extension,
                        autofit=True,
                    )
            except Exception as e:
                raise Exception(f"while exporting table {table}") from e
        association_df = pl.read_database(
            """
            SELECT *
            FROM agent_associations WHERE run_id = %(run_id)s
            ORDER BY trader;
            """,
            conn,
            execute_options={"params": {"run_id": run_id}},
        )
        association_df = association_df.drop("run_id")
        association_df.write_excel(
            workbook,
            worksheet="agent_associations",
            table_name="agent_associations",
            autofit=True,
        )


if __name__ == "__main__":
    conn = db_connection.create_connection(password="remark")
    export_run(conn, "test_export.xlsx", run_id=None)

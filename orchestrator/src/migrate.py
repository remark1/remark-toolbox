import hashlib
import os
import re
import sys

import psycopg.rows
from loguru import logger
from db_connection import create_connection


def migrate(migrations_path, conn):
    logger.info("Migrating database")
    conn.execute(
        """CREATE TABLE IF NOT EXISTS changelog(
            date TIMESTAMP,
            description TEXT,
            PRIMARY KEY (date, description),
            hash BYTEA NOT NULL,
            applied_at TIMESTAMPTZ DEFAULT NOW()
        );"""
    )
    with os.scandir(migrations_path) as migrations:
        mig_re = re.compile(r"(\d{4}\d{2}\d{2}T\d{2}\d{2})-(.*)\.sql")
        for mig in sorted(migrations, key=lambda entry: entry.name):
            if not mig.is_file:
                continue
            match = mig_re.fullmatch(mig.name)
            if not match:
                continue
            date, desc = match.groups()
            with open(mig.path, "rb") as f:
                migration_hash = hashlib.sha1(f.read()).digest()
            prev_migrate_cursor = conn.execute(
                """SELECT hash, applied_at
                    FROM changelog
                    WHERE date = %s AND description = %s;""",
                (date, desc),
            )
            prev_migrate_cursor.row_factory = psycopg.rows.tuple_row
            if prev_migrate := prev_migrate_cursor.fetchone():
                prev_hash, prev_applied = prev_migrate
                assert prev_hash == migration_hash, (
                    f"migration {desc} ({date}) was changed after being applied at "
                    f"{prev_applied}, hashes: old = {prev_hash.hex()}, new = "
                    f"{migration_hash.hex()}"
                )
                logger.info(
                    f"Migration {desc} ({date}) already applied at {prev_applied}"
                )
                continue
            with conn.transaction():
                conn.execute(
                    """INSERT INTO changelog(date, description, hash)
                        VALUES (%s, %s, %s);""",
                    (date, desc, migration_hash),
                )
                with open(mig.path, "r") as f:
                    conn.execute(f.read())
            logger.info(f"Migrated {desc} ({date})")
    conn.commit()


def get_connection_info():
    host = os.environ.get("REMARK_PG_HOST", "localhost")
    port = os.environ.get("REMARK_PG_PORT", "5432")
    db = os.environ.get("REMARK_PG_DB", "remark")
    user = os.environ.get("REMARK_PG_USER", "remark")
    password = os.environ.get("REMARK_PG_PASSWORD")
    schema = os.environ.get("REMARK_PG_SCHEMA", "remark")
    return host, port, db, user, password, schema


if __name__ == "__main__":
    conn = create_connection()
    migrate(sys.argv[1], conn)
    conn.close()

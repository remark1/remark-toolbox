from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
import io


timepoints = [
    "2024-06-03 00:00:00", "2024-06-03 01:00:00", "2024-06-03 02:00:00",
    "2024-06-04 00:00:00", "2024-06-04 01:00:00", "2024-06-04 02:00:00",
    "2024-06-05 00:00:00", "2024-06-05 01:00:00", "2024-06-05 02:00:00",
    "2024-06-06 00:00:00", "2024-06-06 01:00:00", "2024-06-06 02:00:00",
    "2024-06-07 00:00:00", "2024-06-07 01:00:00", "2024-06-07 02:00:00",
    "2024-06-08 00:00:00", "2024-06-08 01:00:00", "2024-06-08 02:00:00",
    "2024-06-09 00:00:00", "2024-06-09 01:00:00", "2024-06-09 02:00:00"
]
timepoints = [datetime.strptime(tp, "%Y-%m-%d %H:%M:%S") for tp in timepoints]

# EUR/MWh
bl_prices = [
    30, 35, 40,
    32, 38, 42,
    36, 39, 41,
    34, 37, 43,
    33, 36, 39,
    31, 34, 38,
    38, 40, 38
]

# EUR/MWh
bl_pay_as_bid_prices = [
    28, 32, 36,
    30, 35, 38,
    31, 34, 37,
    29, 33, 39,
    27, 31, 35,
    28, 32, 36,
    30, 34, 38
]

# EUR/MWh
bl_pay_as_cleared_prices = [
    29, 31, 35,
    31, 34, 36,
    30, 33, 37,
    29, 32, 38,
    28, 31, 36,
    29, 32, 35,
    30, 34, 37
]

# MWH
bl_value = [
    100, 110, 105,
    95, 100, 98,
    105, 102, 100,
    97, 99, 98,
    101, 104, 102,
    98, 95, 97,
    120, 115, 118
]

# EUR/MWh
spotmarket_prices = [
    30, 33, 38,
    35, 37, 40,
    32, 36, 39,
    34, 38, 41,
    31, 35, 39,
    33, 36, 40,
    32, 34, 37
]

#plots a graph with max, min, average prices of spotmarket, bl pay_as_bid and bl pay_as_cleared
def show_prices():
    data = {
        'timepoints': timepoints,
        'spotmarket_prices': spotmarket_prices,
        'bl_pay_as_bid_prices': bl_pay_as_bid_prices,
        'bl_pay_as_cleared_prices': bl_pay_as_cleared_prices
    }

    df = pd.DataFrame(data)
    df.set_index('timepoints', inplace=True)

    plt.figure(figsize=(10, 6))

    plt.plot(df.index, df['spotmarket_prices'], label='spotmarket prices', color='blue')

    plt.plot(df.index, df['bl_pay_as_bid_prices'], label='BL Market (Pay-as-bid)', color='green')

    plt.plot(df.index, df['bl_pay_as_cleared_prices'], label='BL Market (Pay-as-cleared)', color='red')

    plt.axhline(y=df['spotmarket_prices'].min(), color='blue', linestyle='--', label='Spotmarket Min')
    plt.axhline(y=df['spotmarket_prices'].max(), color='blue', linestyle='--', label='Spotmarket Max')
    plt.axhline(y=df['spotmarket_prices'].mean(), color='blue', linestyle='--', label='Spotmarket Average')

    plt.axhline(y=df['bl_pay_as_bid_prices'].min(), color='green', linestyle='--', label='BL Markt (Pay-as-bid) Min')
    plt.axhline(y=df['bl_pay_as_bid_prices'].max(), color='green', linestyle='--', label='BL Markt (Pay-as-bid) Max')
    plt.axhline(y=df['bl_pay_as_bid_prices'].mean(), color='green', linestyle='--', label='BL Markt (Pay-as-bid) Average')

    plt.axhline(y=df['bl_pay_as_cleared_prices'].min(), color='red', linestyle='--', label='BL Marktet (Pay-as-cleared) Min')
    plt.axhline(y=df['bl_pay_as_cleared_prices'].max(), color='red', linestyle='--', label='BL Marktet (Pay-as-cleared) Max')
    plt.axhline(y=df['bl_pay_as_cleared_prices'].mean(), color='red', linestyle='--', label='BL Marktet (Pay-as-cleared) Average')

    plt.xlabel('timepoints')
    plt.ylabel('Price (EUR/MWh)')
    plt.title('Prices at Spotmarket and BL-Market')
    plt.legend()
    plt.xticks(rotation=45)
    plt.tight_layout()

    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_bytes = buffer.read()
    return plot_bytes

def show_prices_pay_as_cleared():
    data = {
                'timepoints': timepoints,
                'bl_pay_as_bid_prices': bl_pay_as_bid_prices
            }

    df = pd.DataFrame(data)
    df.set_index('timepoints', inplace=True)

    plt.figure(figsize=(10, 6))
    plt.plot(df.index, df['bl_pay_as_bid_prices'], label='BL Market (Pay-as-Bid)', color='red')

    plt.axhline(y=df['bl_pay_as_bid_prices'].min(), color='blue', linestyle='--', label='BL Markt (Pay-as-bid) Min')
    plt.axhline(y=df['bl_pay_as_bid_prices'].max(), color='blue', linestyle='--', label='BL Markt (Pay-as-bid) Max')
    plt.axhline(y=df['bl_pay_as_bid_prices'].mean(), color='blue', linestyle='--', label='BL Markt (Pay-as-bid) Average')

    plt.xlabel('timepoints')
    plt.ylabel('Price (EUR/MWh)')
    plt.title('Prices BL-Market with pay as bid')
    plt.legend()
    plt.xticks(rotation=45)
    plt.tight_layout()

    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_bytes = buffer.read()
    return plot_bytes

def show_prices_pay_as_bid():
    data = {
                'timepoints': timepoints,
                'bl_pay_as_bid_prices': bl_pay_as_bid_prices
            }

    df = pd.DataFrame(data)
    df.set_index('timepoints', inplace=True)

    plt.figure(figsize=(10, 6))
    plt.plot(df.index, df['bl_pay_as_cleared_prices'], label='BL Market (Pay-as-cleared)', color='red')

    plt.axhline(y=df['bl_pay_as_cleared_prices'].min(), color='blue', linestyle='--', label='BL Marktet (Pay-as-cleared) Min')
    plt.axhline(y=df['bl_pay_as_cleared_prices'].max(), color='blue', linestyle='--', label='BL Marktet (Pay-as-cleared) Max')
    plt.axhline(y=df['bl_pay_as_cleared_prices'].mean(), color='blue', linestyle='--', label='BL Marktet (Pay-as-cleared) Average')

    plt.xlabel('timepoints')
    plt.ylabel('Price (EUR/MWh)')
    plt.title('Prices BL-Market with pay as cleared')
    plt.legend()
    plt.xticks(rotation=45)
    plt.tight_layout()

    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_bytes = buffer.read()
    return plot_bytes

def bl_prices_boxplot():
    bl_prices = [bl_pay_as_bid_prices, bl_pay_as_cleared_prices]

    plt.figure(figsize=(10, 6))
    plt.boxplot(bl_prices, labels=['Pay-as-bid', 'Pay-as-cleared'])
    plt.xlabel('BL Market Prices')
    plt.ylabel('Price (EUR/MWh)')
    plt.title('Price at BL-Market (Pay-as-bid vs. Pay-as-cleared)')
    plt.grid(True)

    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_bytes = buffer.read()
    return plot_bytes

def bl_prices_and_volume():
    plt.figure(figsize=(12, 6))

    plt.plot(timepoints, bl_prices, label='BL Market', color='green')

    plt.plot(timepoints, bl_volumes, label='BL Market Volumes', color='blue', linestyle='--')

    plt.xlabel('Timepoint')
    plt.ylabel('Prices (EUR/MWh) / Volume (MW)')
    plt.title('Prices and Volumes at BL-Market')
    plt.legend()
    plt.xticks(rotation=45)
    plt.tight_layout()
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_bytes = buffer.read()
    return plot_bytes

def prices_spotmarket_and_bl():
    plt.figure(figsize=(12, 6))

    plt.plot(timepoints, spotmarket_prices, label='Spotmarket Price', color='blue')

    plt.plot(timepoints, bl_prices, label='BL Market', color='green')

    plt.xlabel('Timepoints')
    plt.ylabel('Price (EUR/MWh)')
    plt.title('Prices at Spotmarket and at BL-Market')
    plt.legend()
    plt.xticks(rotation=45)
    plt.tight_layout()

    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_bytes = buffer.read()
    return plot_bytes
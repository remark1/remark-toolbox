import argparse
import hashlib
import json
import os
import sys
import threading
import uuid
from datetime import date, datetime
from io import BytesIO

import docker
import polars as pl
from db_connection import create_connection, execute_statement
from docker.models.containers import Container
from export import export_run
from flask import Flask, current_app, g, make_response, request, send_file
from flask.json.provider import DefaultJSONProvider
from flask_cors import CORS
from migrate import migrate
from psycopg import Connection, errors, sql

app = Flask(
    __name__,
    static_folder="../../frontend/build",
    static_url_path="",
)


# From this StackOverflow answer to provide dates in ISO format
# https://stackoverflow.com/a/74618781
class UpdatedJSONProvider(DefaultJSONProvider):
    def default(self, o):
        if isinstance(o, date) or isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


app.json = UpdatedJSONProvider(app)

"""Enabling CORS for the all paths via flask_cors"""
CORS(app)

docker_client = docker.from_env()


@app.get("/")
def index():
    return send_file(app.static_folder + "/index.html")


@app.post("/user/secret/validation")
def post_user_secret_validation():
    """Return a response code 200, if valid user_name and secret combination is given."""

    user_json = request.get_json()
    password_hasher = hashlib.sha256()
    password_hasher.update(user_json.get("password").encode("utf-8"))
    password_hash = password_hasher.hexdigest()

    secret_hasher = hashlib.sha256()
    secret_hasher.update(user_json.get("secret").encode("utf-8"))
    secret_hash = secret_hasher.hexdigest()

    sql_select_secret_statement = """
    SELECT user_name FROM users
    WHERE user_name = %s AND secret=%s;
    """

    sql_alter_statement = """
    UPDATE users
    SET password = %s
    WHERE user_name = %s AND secret = %s
    RETURNING user_name;
    """

    try:
        result = execute_statement(
            sql_select_secret_statement,
            params=(user_json.get("user_name"), secret_hash),
        )
        if not result:
            return {"error": "User not found or wrong secret."}, 404

        alter_result = execute_statement(
            sql_alter_statement,
            params=(password_hash, user_json.get("user_name"), secret_hash),
        )
        if not alter_result:
            return {"error": "Something went wrong."}, 400
        return {"detail": "Successfully changed password."}, 200
    except Exception as e:
        return {"error": str(e)}, 500


@app.post("/user/validation")
def post_user_validation():
    """Return a response code 200, if valid user_name and password combination is given."""

    user_json = request.get_json()
    password_hasher = hashlib.sha256()
    password_hasher.update(user_json.get("password").encode("utf-8"))
    password_hash = password_hasher.hexdigest()

    sql_statement = """
        SELECT user_name FROM users
        WHERE user_name = %s AND password = %s;
    """

    try:
        result = execute_statement(
            sql_statement,
            params=(user_json.get("user_name"), password_hash),
        )
        if not result:
            return {"error": "User not found or incorrect credentials."}, 404

        return {"detail": "Success"}, 200
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/user/<user_name>")
def get_user(user_name):
    """Return a response code 200, if valid user_name  is given."""

    sql_statement = """
        SELECT user_name FROM users
        WHERE user_name = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(user_name,))
        if not result:
            return {"error": "User not found."}, 404

        return {"detail": "Success"}, 200
    except Exception as e:
        return {"error": str(e)}, 500


@app.post("/user/registration")
def post_user_registration():
    """Create a new user. The payload of the POST request should be
    a JSON describing the user.

    The Code 200 will be returned.
    """

    user_json = request.get_json()
    password_hasher = hashlib.sha256()
    password_hasher.update(user_json.get("password").encode("utf-8"))
    password_hash = password_hasher.hexdigest()

    secret_hasher = hashlib.sha256()
    secret_hasher.update(user_json.get("secret").encode("utf-8"))
    secret_hash = secret_hasher.hexdigest()

    sql_select_statement = """
        SELECT user_name FROM users
        WHERE user_name = %s;
    """

    sql_post_statement = """
    INSERT INTO users (user_name, password, secret, creation_time)
    VALUES (%s, %s, %s, NOW())
    RETURNING user_name;
    """

    try:
        result = execute_statement(
            sql_select_statement, params=(user_json.get("user_name"),)
        )
        if result:
            return {"error": "User already exists."}, 400

        post_result = execute_statement(
            sql_post_statement,
            params=(user_json.get("user_name"), password_hash, secret_hash),
        )
        if post_result:
            return {"detail": "User successfully created"}, 200
        else:
            return {"error": "User could not be created."}, 400
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/scenarios/<user_name>")
def get_scenarios(user_name):
    """Return a list of all available scenarios."""

    sql_statement = """
        SELECT
            id,
            scenario -> 'name' as scenario_name
        FROM scenarios
        WHERE user_name = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(user_name,))
        if not result:
            return {"error": "Scenario not found."}, 404

        return [
            {"scenario_id": scenario["id"], "scenario_name": scenario["scenario_name"]}
            for scenario in result
        ]
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/scenario/<scenario_id>/runs")
def get_scenario_runs(scenario_id):
    runs = (
        get_db()
        .execute(
            """
            SELECT
                main.id AS id,
                baseline.id AS baseline_id,
                baseline.start_time AS baseline_start,
                main.start_time AS main_start,
                COALESCE(baseline.start_time, main.start_time) AS exp_start,
                baseline.successful AS base_successful,
                main.successful AS main_successful
            FROM (
                SELECT *
                FROM runs
                WHERE scenario_id = %(scenario_id)s
                AND baseline_for IS NULL
            ) AS main
            FULL OUTER JOIN (
                SELECT *
                FROM runs
                WHERE scenario_id = %(scenario_id)s
                AND baseline_for IS NOT NULL
            ) AS baseline
            ON main.id = baseline.baseline_for
            ORDER BY exp_start
            """,
            params={"scenario_id": scenario_id},
        )
        .fetchall()
    )
    return runs


@app.get("/scenario/<scenario_id>")
def get_scenario(scenario_id):
    """Return the scenario with id `scenario_id`."""
    try:
        scenario = (
            get_db()
            .execute(
                """
            SELECT scenario FROM scenarios
            WHERE id = %s;
            """,
                params=(scenario_id,),
            )
            .fetchone()
        )
        return scenario["scenario"]

    except Exception as e:
        return {"error": str(e)}, 500


@app.delete("/scenario/<scenario_id>")
def delete_scenario(scenario_id):
    """Delete the scenario with id `scenario_id`."""
    result = get_db().execute(
        """
        DELETE FROM scenarios
        WHERE id = %(scenario_id)s
        RETURNING id;
        """,
        params={"scenario_id": scenario_id},
    )
    if not result:
        return {"error": "Scenario not found."}, 404
    return {"detail": "Scenario deleted successfully"}, 200


@app.get("/scenario/results/<scenario_id>")
def scenario_results(scenario_id, user_name):
    """Get the results of the scenario with `scenario_id`  ."""
    sql_statement = """
        SELECT * FROM results
        WHERE id = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(scenario_id,))
        if not result:
            return {"error": "Scenario results not found."}, 404
        return {"scenario_id": result}
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/results/bids/<run_id>")
def get_results_bids(run_id):
    """Return a list of all available bid data."""

    sql_statement = """
        SELECT time, src_id, p_bid_price, p_bid_amount, q_bid_price, q_bid_amount, p_award_amount, q_award_amount,
         q_free_amount FROM bids
        WHERE run_id = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(run_id,))
        if not result:
            return {"error": "Results not found."}, 404
        return result
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/results/market/<run_id>")
def get_results_market(run_id):
    """Return a list of all available market data."""

    sql_statement = """
        SELECT time, src_id, p_price, marginal_q_price, efficiency_q_price FROM market
        WHERE run_id = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(run_id,))
        if not result:
            return {"error": "Results not found."}, 404
        return result
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/results/gens/<run_id>")
def get_results_gens(run_id):
    """Return a list of all available generation data."""

    sql_statement = """
        SELECT time, src_id, p_mw, q_mvar FROM gens
        WHERE run_id = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(run_id,))
        if not result:
            return {"error": "Results not found."}, 404
        return result
    except Exception as e:
        return {"error": str(e)}, 500


@app.get("/results/batteries/<run_id>")
def get_results_batteries(run_id):
    """Return a list of all available generation data."""

    sql_statement = """
        SELECT time, src_id, soc_percent FROM batteries
        WHERE run_id = %s;
    """

    try:
        result = execute_statement(sql_statement, params=(run_id,))
        if not result:
            return {"error": "Results not found."}, 404
        return result
    except Exception as e:
        return {"error": str(e)}, 500


@app.post("/scenarios/<user_name>")
def post_scenario(user_name):
    """Create a new scenario. The payload of the POST request should be
    a JSON describing the scenario.

    The `scenario_id` of the given scenario will be returned.
    No duplicates of scenario_id are allowed. This means the scenario
    must have a difference to other scenarios in the DB.
    """
    scenario_json = request.get_json()
    scenario_id = f"scenario_{uuid.uuid4()}"

    try:
        insert_result = execute_statement(
            """
            INSERT INTO scenarios
                (id, user_name, scenario_name, scenario, creation_time)
            VALUES (%s, %s, %s, %s, NOW())
            RETURNING id;
            """,
            params=(
                scenario_id,
                user_name,
                scenario_json["name"],
                json.dumps(scenario_json),
            ),
        )
        if not insert_result:
            return {"error": "Scenario could not be inserted."}, 400
        return {"scenario_id": insert_result[0]["id"]}
    except Exception as e:
        return {"error": str(e)}, 500


@app.post("/scenario/<scenario_id>")
def post_update_scenarios(scenario_id):
    """Updates a given scenario. The payload of the POST request should be
    a JSON describing the scenario.
    """
    new_scenario = request.get_json()
    old_scenario = (
        get_db()
        .execute(
            """
        SELECT scenario FROM scenarios
        WHERE id = %(scenario_id)s;
        """,
            params={"scenario_id": scenario_id},
        )
        .fetchone()["scenario"]
    )
    num_runs = (
        get_db()
        .execute(
            """
        SELECT count(*) as num_runs FROM runs
        WHERE scenario_id = %(scenario_id)s;
        """,
            params={"scenario_id": scenario_id},
        )
        .fetchone()["num_runs"]
    )
    new_name = new_scenario.pop("name")
    del old_scenario["name"]

    if num_runs > 0 and new_scenario != old_scenario:
        return {
            "error": (
                "Once a scenario has runs, only the name may be changed. "
                f"(This scenario has {num_runs} runs.)"
            )
        }, 400

    new_scenario["name"] = new_name

    alter_result = get_db().execute(
        """
        UPDATE scenarios
        SET scenario = %(scenario)s
        WHERE id = %(scenario_id)s;
        """,
        params={
            "scenario_id": scenario_id,
            "scenario": json.dumps(new_scenario),
        },
    )
    if not alter_result:
        return {"error": "Something went wrong."}, 400
    return {"detail": "Successfully updated scenario."}, 200


@app.get("/runs/<run_id>/results")
def get_run_results(run_id):
    run_info = (
        get_db()
        .execute(
            """
        SELECT *
        FROM runs
        WHERE id = %(run_id)s
        """,
            params={"run_id": run_id},
        )
        .fetchone()
    )
    if run_info is None:
        raise ValueError(f"{run_id} is not a valid run id")

    tables = get_db().execute(
        """
        SELECT table_name
        FROM information_schema.columns
        WHERE table_schema = 'remark'
        GROUP BY table_name;
        """,
    )

    # This should be extracted from the run somehow, but is fixed for
    # now. (Maybe add the simulated start time to the runs table?)
    EXPORT_START_DATE = "2023-07-02 00:00:00Z"

    output_data = {}
    for table_row in tables:
        try:
            table = table_row["table_name"]
            if table in (
                "latest_run",
                "runs",
                "changelog",
                "users",
                "scenarios",
                "run_containers",
                "agent_associations",
            ):
                continue

            for baseline in (True, False):
                extension = "_baseline" if baseline else ""
                table_df = pl.read_database(
                    sql.SQL(
                        """
                        SELECT *
                        FROM {}
                        WHERE run_id = %(run_id)s
                        AND time >= %(export_start)s
                        ORDER BY time, src_id;
                        """
                    ).format(sql.Identifier(table)),
                    get_db(),
                    execute_options={
                        "params": {
                            "run_id": run_id + extension,
                            "export_start": EXPORT_START_DATE,
                        },
                    },
                    infer_schema_length=None,
                )
                table_df = table_df.drop("run_id")
                output_data[table + extension] = (
                    table_df.to_dicts()
                )  # (as_series=False)
        except Exception as e:
            raise Exception(f"while exporting table {table}") from e

    association_df = pl.read_database(
        """
        SELECT *
        FROM agent_associations WHERE run_id = %(run_id)s
        ORDER BY trader;
        """,
        get_db(),
        execute_options={"params": {"run_id": run_id}},
    )
    association_df = association_df.drop("run_id")
    output_data["agent_associations"] = association_df.to_dicts()  # (as_series=False)
    return output_data


def container_run(scenario, *, run_id, scenario_id):
    """Performs the scenario run `run_id` and starts a thread that waits
    for it to finish.
    """
    local: bool = current_app.config["local"]
    container: Container = docker_client.containers.create(
        "remark_core",
        (run_id, scenario_id),
        network="remark-network",
        environment={
            "REMARK_PG_HOST": "remark-pg",
            "REMARK_PG_PASSWORD": os.environ["REMARK_PG_PASSWORD"],
        },
        stdin_open=True,
        extra_hosts={"remark-pg": "host-gateway"} if local else {},
    )  # type: ignore
    get_db().execute(
        """
        INSERT INTO run_containers (run_id, container_id)
        VALUES (%(run_id)s, %(container_id)s);
        """,
        params={"run_id": run_id, "container_id": container.id},
    )
    sock = container.attach_socket(params={"stdin": 1, "stream": 1})
    container.start()

    app.logger.info(scenario)
    try:
        sock._sock.send(json.dumps(scenario).encode("utf-8"))
    except Exception as e:
        app.logger.error(f"Error during scenario transmission: {e}")
        return {"error": str(e)}, 500
    finally:
        sock._sock.close()
        sock.close()

    app.logger.info(f"Started {run_id} (container {container.id})")
    threading.Thread(target=await_run_completion, args=(run_id, container)).start()
    return container


def await_run_completion(run_id, container):
    """Wait for the run in `container` to complete. (This is the target
    function for the container's waiting thread.)
    """
    status = container.wait()
    app.logger.info(
        f"Completed {run_id} (container {container.id}) with exit code {status}!"
    )


@app.post("/runs/<scenario_id>")
def post_run(scenario_id):
    """Create a new run of scenario `scenario_id` and start the
    corresponding docker container (in a separate thread).
    """

    run_id = f"run_{uuid.uuid4()}"

    result = (
        get_db()
        .execute(
            """
        SELECT scenario FROM scenarios
        WHERE id = %s;
        """,
            params=(scenario_id,),
        )
        .fetchone()
    )
    if not result:
        return {"error": "Scenario not found."}, 404

    scenario = result["scenario"]

    get_db().execute(
        """
        INSERT INTO runs (id, scenario_id) VALUES (%(run_id)s, %(scenario_id)s);
        """,
        params={"run_id": run_id, "scenario_id": scenario_id},
    )

    container = container_run(scenario, run_id=run_id, scenario_id=scenario_id)

    return {
        "run_id": run_id,
        "container": container.id,
    }


@app.get("/runs/<run_id>/export")
def get_run_export(run_id):
    conn = create_connection()
    output = BytesIO()
    export_run(conn, output, run_id)
    output.seek(0)
    return send_file(output, as_attachment=True, download_name=f"{run_id}.xlsx")


@app.get("/runs/<run_id>/logs")
def get_run_logs(run_id):
    try:
        container_id = (
            get_db()
            .execute(
                """
            SELECT container_id
            FROM run_containers
            WHERE run_id = %s
            """,
                params=(run_id,),
            )
            .fetchone()["container_id"]
        )
        container = docker_client.containers.get(container_id)
        response = make_response(container.logs(stream=True, follow=True))
        response.mimetype = "text/plain"
        return response
    except Exception as exc:
        return {"error": f"logs for {run_id} not available", "exception": str(exc)}, 404


def get_db():
    if "db" not in g:
        g.db = create_connection()
    return g.db


def close_db(e=None):
    conn = g.pop("db", None)
    if conn is not None:
        if e is None:
            conn.commit()
        else:
            conn.rollback()
        conn.close()


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description="run the REMARK orchestrator")
    argparser.add_argument(
        "migrations",
        type=str,
        nargs="?",
        default="../migrations",
        help="path to the folder containing the migrations",
    )
    argparser.add_argument(
        "--local", action="store_true", help="run in local development mode (no SSL)"
    )
    argparser.add_argument("--password", type=str, help="the password for the database")
    args = argparser.parse_args()

    conn = create_connection(password=args.password)
    migrate(args.migrations, conn)
    conn.close()
    app.teardown_appcontext(close_db)
    if args.local:
        app.config["local"] = True
        app.run(host="127.0.0.1", debug=True)
    else:
        app.config["local"] = False
        app.run(host="0.0.0.0", ssl_context=("local.crt", "local.key"))

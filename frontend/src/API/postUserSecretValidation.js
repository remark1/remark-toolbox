import {REST_API_Path} from "../components/commonComponents/environment_variables";



async function PostUserSecretValidation(user_name, password, secret) {
    let errorMsg = null;
    let res = false;
    const path = REST_API_Path() + "user/secret/validation" ;

    let body_value = {
        "user_name":user_name,
        "password":password,
        "secret":secret
    }
    try {
        const response = await fetch(path, {
            mode: 'cors',
            method: "POST",
            body: JSON.stringify(body_value),
            headers: {
                "Content-Type": "application/json"
            }
        });
        if (!response.ok) {
            if (response.status === 404) {
                errorMsg = "Wrong user or secret.";
                res = false;
            } else {
                res = false;
                throw new Error(`HTTP error! status: ${response.status}`);
            }
        } else {
            res = true;
        }
    } catch (error) {
        errorMsg = error.message;
        console.error("There was an error fetching the scenario:", errorMsg);
    }

    return {
        data: res,
        error: errorMsg,
    };
}

export default PostUserSecretValidation
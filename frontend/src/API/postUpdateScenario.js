import {REST_API_Path} from "../components/commonComponents/environment_variables";

async function PostUpdateScenario(scenarioId, scenario) {
    console.log(scenarioId, scenario)
    const path = `${REST_API_Path()}/scenario/${scenarioId}`

    const response = await fetch(path, {
        mode: 'cors',
        method: "POST",
        body: JSON.stringify(scenario),
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (!response.ok) {
        throw new Error(`${response.status} ${response.statusText}\n${await response.text()}`);
    }

    let result = await response.text()
    console.log("PostUpdateScenario:", result)
    return JSON.parse(result)
}

export default PostUpdateScenario
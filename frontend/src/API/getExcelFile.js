import { REST_API_Path } from "../components/commonComponents/environment_variables";

async function GetExcelFile(runId, name) {
    let errorMsg = null;
    let res = false;
    const path = `${REST_API_Path()}/runs/${runId}/export`;

    try {
        const response = await fetch(path, {
            mode: 'cors',
            method: "GET"
        });

        if (!response.ok) {
            if (response.status === 404) {
                errorMsg = "Run not found";
                res = false;
            } else {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
        } else {
            const blob = await response.blob();
            const url = window.URL.createObjectURL(new Blob([blob]));
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", `${name}.xlsx`);
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);
            res = true;
        }
    } catch (error) {
        errorMsg = error.message;
        console.error("There was an error downloading the file:", errorMsg);
    }

    return {
        success: res,
        error: errorMsg,
    };
}

export default GetExcelFile;
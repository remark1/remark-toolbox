import { REST_API_Path } from "../components/commonComponents/environment_variables";

async function GetRuns(scenarioId) {
    let response = await fetch(
        `${REST_API_Path()}/scenario/${scenarioId}/runs`,
        { method: "GET", mode: "cors", }
    );
    let body = await response.text();
    let runs = new Map();
    JSON.parse(body).forEach((run) => {
        runs.set(run.id, run)
    })
    return runs;
}

export default GetRuns

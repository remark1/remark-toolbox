import {REST_API_Path} from "../components/commonComponents/environment_variables";


async function GetBattery(runId) {
    let errorMsg = null;
    let res = null;
    const path = REST_API_Path() + "results/batteries" + "/" + runId;

    try {
        const response = await fetch(path, { mode: 'cors', method: "GET" });
        if (!response.ok) {
            if (response.status === 404) {
                errorMsg = "Run ID not found";
            } else {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
        } else {
            const result = await response.text();
            res = JSON.parse(result);
        }
    } catch (error) {
        errorMsg = error.message;
        console.error("There was an error fetching the data:", errorMsg);
    }

    return {
        data: res,
        error: errorMsg,
    };
}

export default GetBattery
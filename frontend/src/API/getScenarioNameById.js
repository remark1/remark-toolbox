import {REST_API_Path} from "../components/commonComponents/environment_variables";


async function GetScenarioNameById(scenarioName) {

    let errorMsg = null
    let res

    await fetch(REST_API_Path + "scenarios/name/" + scenarioName.toString(),
        {
            mode: 'cors',
            method: "GET",
        })
        .then(res => res.text())
        .then(
            (result) => {
                res = JSON.parse(result)
                console.log(res)
            },
            (error) => {
                errorMsg = error
            }
        )


    if (errorMsg !== null) {
        console.log("error: " + errorMsg)
    }
    return res
}

export default GetScenarioNameById
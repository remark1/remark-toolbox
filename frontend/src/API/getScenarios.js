import {REST_API_Path} from "../components/commonComponents/environment_variables";

async function GetScenarios(loggedInUser) {
    if (loggedInUser === null) {
        return [];
    }
    const path = `${REST_API_Path()}/scenarios/${loggedInUser}`;
    const response = await fetch(path, { mode: 'cors', method: "GET" });
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = JSON.parse(await response.text());
    
    let scenarioMap = new Map();
    result.forEach(scenario => {
        scenarioMap.set(scenario.scenario_id, scenario);
    });

    return scenarioMap;
}

export default GetScenarios

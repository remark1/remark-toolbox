import {REST_API_Path} from "../components/commonComponents/environment_variables";

async function PostRun(scenarioId) {
    const path = `${REST_API_Path()}/runs/${scenarioId}`;

    const response = await fetch(path, {
        mode: 'cors',
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }

    let res = await response.json();
    console.log("Response JSON: ", res)
    
    return res
}

export default PostRun
import {REST_API_Path} from "../components/commonComponents/environment_variables";

/**
 * Also start the simulation with the given scenario-id
 * @param scenarioId
 * @returns {Promise<*>}  A promise with the run-id or the error is given back.
 */
async function GetRun(scenarioId) {

    let errorMsg = null
    let res

    await fetch(REST_API_Path() + "runs/" + scenarioId.toString(),
        {
            mode: 'cors',
            method: "GET",
        })
        .then(res => res.text())
        .then(
            (result) => {
                res = JSON.parse(result)
                console.log(res)
            },
            (error) => {
                errorMsg = error
            }
        )


    if (errorMsg !== null) {
        console.log("error: " + errorMsg)
    }
    return res
}

export default GetRun
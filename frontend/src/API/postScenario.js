import {REST_API_Path} from "../components/commonComponents/environment_variables";

async function PostScenario(loggedInUser, scenario) {
    console.log(loggedInUser, scenario)
    const path = `${REST_API_Path()}/scenarios/${loggedInUser}`;

    const response = await fetch(path, {
        mode: 'cors',
        method: "POST",
        body: JSON.stringify(scenario),
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }

    let result = await response.text()
    return JSON.parse(result)["scenario_id"]
}

export default PostScenario
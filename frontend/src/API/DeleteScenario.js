import { REST_API_Path } from "../components/commonComponents/environment_variables";

export default async function DeleteScenario(scenarioId) {
    let response = await fetch(
        `${REST_API_Path()}/scenario/${scenarioId}`,
        {
            method: "DELETE",
            mode: "cors",
        }
    )

    if (!response.ok) {
        throw new Error(`HTTP error ${response.status}`)
    }

    return JSON.parse(await response.text())
}
import {REST_API_Path} from "../components/commonComponents/environment_variables";

//todo unfinished runs via orchestrator
async function GetRunsUnfinished() {
    let errorMsg = null
    let res

    await fetch(REST_API_Path() + "runs/unfinished", {
        method: "GET",
        headers: {
            "Content-Type": "text/plain",
            "Content-Length": ""

        }
    })
        .then(res => res.text())
        .then(
            (result) => {
                res = JSON.parse(result)
            },
            (error) => {
                //todo embed error component to display a generic errorMsg via modal
                errorMsg = error;
            }
        )

    if (errorMsg !== null) {
        console.log(errorMsg)
    }
    return res
}

export default GetRunsUnfinished
import {REST_API_Path} from "../components/commonComponents/environment_variables";


async function GetUser(userName) {
    let errorMsg = null;
    let res = false;
    const path = REST_API_Path() + "user/" + userName;

    try {
        const response = await fetch(path, { mode: 'cors', method: "GET" });
        if (!response.ok) {
            if (response.status === 404) {
                errorMsg = "User not found";
                res = false
            } else {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
        } else {
            res = true;
        }
    } catch (error) {
        errorMsg = error.message;
        console.error("There was an error fetching the scenario:", errorMsg);
    }

    return {
        data: res,
        error: errorMsg,
    };
}

export default GetUser
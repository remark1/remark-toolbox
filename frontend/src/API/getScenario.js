import {REST_API_Path} from "../components/commonComponents/environment_variables";


async function GetScenario(scenarioId) {
    const path = `${REST_API_Path()}/scenario/${scenarioId}`;

    const response = await fetch(path, { mode: 'cors', method: "GET" });
    if (!response.ok) {
        throw new Error(`HTTP error while fetching scenario; status code: ${response.status}`);
    }
    
    const result = await response.text();
    return JSON.parse(result);
}

export default GetScenario
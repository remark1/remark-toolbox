import { createContext } from "react";

/* Entries in the scenarioMap are one of:
- type: "LocalOnly", localScenario
- type: "ServerOnly", scenarioId, scenarioName, runs
- type: "Dual", localScenario, scenarioId, serverScenario, runs
*/
const AllScenarios = createContext({
    scenarios: {},
    scenariosDispatch: () => {
    }
})

/*
const defaultScenario = {
    name: "",
    id: null,
    until: 259200,
    grid: "",
    gridType: "",
    batteryStoragePenetration: 0,
    photoVoltaicPenetration: 10,
    windPenetration: 10,
    pvBatteryPenetration: 0,
    windBatteryPenetration: 0,
    pMarketPriceHistoric: "",
    maximumPrice: "",
    timeResolutionDA: 60,
    marketClearingDA: "pac",
    minimalProductSize: 0,
    forecastNoise: 10,
    minimalVolume: 0,
    providerSize: "",
}

export function useScenarioInfo(scenarioHandle) {
    const { scenarioInfoMap: scenarioMap } = useContext(AllScenarios)

    return scenarioMap.get(scenarioHandle.id);
}


export function useScenario(scenarioHandle, setScenario) {
    const { scenarioInfoMap: scenarioMap } = useContext(AllScenarios)

    useEffect(() => {
        if (typeof(scenarioHandle) === "string") {
            setScenario(null);
            GetScenario(scenarioHandle).then(setScenario, console.error)
        } else if (typeof(scenarioHandle) === "number") {
            setScenario(defaultScenario)
        }
    }, [scenarioHandle]);
}


export function useIsPristine(scenarioHandle, scenario) {
    const { scenarioMap } = useContext(AllScenarios)

    if (typeof(scenarioHandle) === "string" && scenarioMap.has(scenarioHandle)) {
        return scenario === scenarioMap.get(scenarioHandle)
    }
    return false
}*/


export default AllScenarios
import React, {useState, useEffect, useReducer} from "react";
import RefreshContext from "./refreshContext";
import AllScenarios from "./scenarioList";
import GetScenarios from "../../API/getScenarios";
import { DEFAULT_SCENARIO } from "../commonComponents/environment_variables";
import LoggedInUser from "./loggedInUser"

function scenariosReducer(scenarios, action) {
    console.debug("scenarioReducer", action)
    var newScenarios = Array.from(scenarios);
    let oldScenario = scenarios[action.scenarioHandle];
    switch (action.type) {
        case "Clear":
            return [];
        case "SetServerList":
            newScenarios = [];
            scenarios.forEach((sce, sceHandle) => {
                if (sce.type === "ServerOnly" || sce.type === "Dual") {
                    if (action.serverScenarios instanceof Map) {
                        action.serverScenarios.delete(sce.scenarioId);
                    }
                }
                newScenarios[sceHandle] = sce;
            });
            for (let [sceId, sce] of action.serverScenarios) {
                newScenarios.push({
                    type: "ServerOnly",
                    scenarioId: sceId,
                    scenarioName: sce.scenario_name,
                    runs: null,
                });
            }
            return newScenarios;
        case "DownloadScenario":
            newScenarios[action.scenarioHandle] = {
                type: "Dual",
                scenarioId: oldScenario.scenarioId,
                serverScenario: action.serverScenario,
                localScenario: action.serverScenario,
                runs: oldScenario.runs,
            }
            return newScenarios
        case "SetScenarioRuns":
            newScenarios[action.scenarioHandle] = {
                ...oldScenario,
                runs: action.runs
            }
            return newScenarios;
        case "UpdateLocal":
            if (typeof(action.updater) === "function") {
                newScenarios[action.scenarioHandle] = {
                    ...oldScenario,
                    localScenario: action.updater(oldScenario.localScenario),
                }
            } else {
                newScenarios[action.scenarioHandle] = {
                    ...oldScenario,
                    localScenario: action.updater,
                }
            }
            return newScenarios;
        case "CreateNewScenario":
            newScenarios.push({
                type: "LocalOnly",
                localScenario: DEFAULT_SCENARIO,
            })
            return newScenarios;
        case "PromoteToDual":
            newScenarios[action.scenarioHandle] = {
                type: "Dual",
                scenarioId: action.scenarioId,
                localScenario: oldScenario.localScenario,
                serverScenario: oldScenario.localScenario,
                runs: new Map(),
            }
            return newScenarios
        case "UpdateServer":
            newScenarios[action.scenarioHandle] = {
                ...oldScenario,
                serverScenario: action.localScenario,
            }
            return newScenarios
        default:
            console.error("scenariosDispatch received unknown action:", action.type);
            console.log("(Did you forget to return at the end of the newest branch of scenariosReducer?")
            return scenarios;
    }
}

const ContextWrapper = ({ children, setMainState, }) => {
    const [refresh, setRefresh] = useState(false);
    const [scenarios, scenariosDispatch] = useReducer(scenariosReducer, []);
    const refreshValue = { refresh, setRefresh };

    const [loggedInUser, setLoggedInUser] = useState(() => {
        const storedUser = sessionStorage.getItem("loggedInUser");
        return storedUser ? JSON.parse(storedUser) : null;
    });

    // Wenn der Benutzer geändert wird, speichern wir ihn in sessionStorage
    const setLoggedInUserHandler = (user) => {
        setLoggedInUser(user);
        if (user) {
            sessionStorage.setItem("loggedInUser", JSON.stringify(user));
        } else {
            sessionStorage.removeItem("loggedInUser");
        }
    };


    useEffect(() => {
        setMainState({ state: "NotLoggedIn" });
        scenariosDispatch({ type: "Clear" });
        GetScenarios(loggedInUser).then(
            (sces) => {
                setMainState({ state: "NothingSelected" });
                scenariosDispatch({ type: "SetServerList", serverScenarios: sces });
            },
            (err) => {
                console.error(err);
                setMainState({ state: "NotLoggedIn" });
                scenariosDispatch({ type: "Clear" });
            }
        );
    }, [loggedInUser, setMainState,]);

    return (
        <AllScenarios.Provider value={{ scenarios, scenariosDispatch }}>
            <LoggedInUser.Provider value={{ loggedInUser, setLoggedInUser: setLoggedInUserHandler }}>
                <RefreshContext.Provider value={refreshValue}>
                    {typeof children === 'function' ? children() : children}
                </RefreshContext.Provider>
            </LoggedInUser.Provider>
        </AllScenarios.Provider>
    );
};

export default ContextWrapper;

import {createContext} from "react";

const LoggedInUser = createContext({
    user: null,
    setLoggedInUser: () => {
    }
})


export default LoggedInUser
import React, { useContext, useState, useEffect } from "react";
import { Dropdown, TabPane } from "semantic-ui-react";
import HerfindalHirschmanIndex from "./plotComponents/HerfindalHirschmanIndex";
import ConcentrationRatio from "./plotComponents/concentrationRatio";
import { Logs } from "../results/Logs";
import { Tab } from "semantic-ui-react";
import { RunOverview } from "../results/RunOverview";
import AllScenarios from "../context/scenarioList";
import DateView from "../commonComponents/DateView";
import MarketOutcomePlot from "./plotComponents/MarketsOverTime";
import CompareResultsPlot from "./plotComponents/CompareResultsPlot";
import { REST_API_Path } from "../commonComponents/environment_variables";
import LineLoadingPlot from "./plotComponents/LineLoadingPlot";
import BusVoltagePlot from "./plotComponents/BusVoltagePlot";

function Results({ scenarioHandle, runId }) {
    const { scenarios } = useContext(AllScenarios);
    const scenario = scenarios[scenarioHandle];
    const run = scenario.runs.get(runId);
    const [results, setResults] = useState(null);

    useEffect(() => {
        (async () => {
            const response = await fetch(
                `${REST_API_Path()}/runs/${runId}/results`,
                { method: "GET", mode: "cors" },
            );
            setResults(await response.json());
        })();
    }, [setResults, runId]);

    const [isMobile, setIsMobile] = useState(window.innerWidth < 940);
    const [activeIndex, setActiveIndex] = useState(0);
    const panes = [
        {
            menuItem: "Overview",
            render: () => (
                <TabPane id="tab-pane">
                    <RunOverview run={run} />
                </TabPane>
            ),
        },
        {
            menuItem: "Logs",
            render: () => (
                <Tab.Pane id="tab-pane">
                    <Logs runId={runId} />
                </Tab.Pane>
            ),
        },
        {
            menuItem: "Markets over Time",
            render: () => (
                <Tab.Pane id="tab-pane">
                    <MarketOutcomePlot results={results} />
                </Tab.Pane>
            ),
        },
        {
            menuItem: "Trader View",
            render: () => (
                <Tab.Pane id="tab-pane">
                    <CompareResultsPlot results={results} />
                </Tab.Pane>
            ),
        },
        {
            menuItem: "Herfindal-Hirschman-Index",
            render: () => (
                <TabPane id="tab-pane">
                    <HerfindalHirschmanIndex results={results} />
                </TabPane>
            ),
        },
        {
            menuItem: "Concentration Ratio",
            render: () => (
                <TabPane id="tab-pane">
                    <ConcentrationRatio results={results} />
                </TabPane>
            ),
        },
        {
            menuItem: "Line Loadings",
            render: () => (
                <Tab.Pane id="tab-pane">
                    <LineLoadingPlot results={results} />
                </Tab.Pane>
            ),
        },
        {
            menuItem: "Bus Voltages",
            render: () => (
                <Tab.Pane id="tab-pane">
                    <BusVoltagePlot results={results} />
                </Tab.Pane>
            ),
        },
    ];

    const updateMedia = () => {
        setIsMobile(window.innerWidth < 940);
    };

    useEffect(() => {
        window.addEventListener("resize", updateMedia);
        return () => window.removeEventListener("resize", updateMedia);
    });

    const handleDropdownChange = (e, { value }) => {
        setActiveIndex(value);
    };

    return isMobile ? (
        <div className="results">
            <h2>
                Scenario <i>{scenario.localScenario.name}</i> &middot; Run
                started <DateView dateStr={run.exp_start} />
            </h2>
            <Dropdown
                id="result-dropdown"
                selection
                options={panes.map((pane, index) => ({
                    key: index,
                    text: pane.menuItem,
                    value: index,
                }))}
                value={activeIndex}
                onChange={handleDropdownChange}
            />
            <div className="tab-content">{panes[activeIndex].render()}</div>
        </div>
    ) : (
        <div className="results">
            <h2>
                Scenario <i>{scenario.localScenario.name}</i> &middot; Run
                started <DateView dateStr={run.exp_start} />
            </h2>
            <Tab id="tabs" panes={panes} />
        </div>
    );
}

export default Results;

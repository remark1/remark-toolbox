import { useContext, useRef, useState } from "react";
import LoggedInUser from "../context/loggedInUser";
import PostScenario from "../../API/postScenario";
import { Popup } from "semantic-ui-react";
import ScenarioList from "../context/scenarioList";
import PostUpdateScenario from "../../API/postUpdateScenario";

function SaveButton({ scenarioHandle, dualScenario }) {
    const { loggedInUser } = useContext(LoggedInUser)
    const timeoutLength = 2500
    const [msg, setMsg] = useState("")
    const timeoutRef = useRef(null);
    const [state, setState] = useState({ isOpen: false })
    const { scenariosDispatch } = useContext(ScenarioList)

    function handleOpen() {
        setState({ isOpen: true })

        timeoutRef.current = setTimeout(() => {
            setState({ isOpen: false })
        }, timeoutLength)
    }

    function handleClose() {
        setState({ isOpen: false })
        clearTimeout(timeoutRef.current)
        setMsg("")
    }

    const handleOnClick = () => {
        if (loggedInUser === null) {
            console.error("Cannot save scenario while no user is logged in.")
            return;
        }

        setMsg("Saving ...")
        console.log(dualScenario);
        if (dualScenario.type === "LocalOnly") {
            (async () => {
                try {
                    let newScenarioId = await PostScenario(loggedInUser, dualScenario.localScenario)
                    scenariosDispatch({
                        type: "PromoteToDual",
                        scenarioHandle,
                        scenarioId: newScenarioId, 
                    })
                    setMsg("Scenario saved!")
                } catch (e) {
                    console.error(e);
                    setMsg("Saving scenario failed")
                }
            })()
        } else if (dualScenario.type === "Dual") {
            (async () => {
                try {
                    await PostUpdateScenario(dualScenario.scenarioId, dualScenario.localScenario)
                    scenariosDispatch({
                        type: "UpdateServer",
                        scenarioHandle,
                        localScenario: dualScenario.localScenario,
                    })                    
                    setMsg("Scenario updated!")
                } catch (e) {
                    console.error(e)
                    setMsg("Saving scenario failed")
                }
            })()
        }
    }

    return (
        <Popup
            trigger={<button
                className="ui primary button"
                id="saveButton"
                onClick={handleOnClick}>
                Save
            </button>}
            content={msg}
            on='click'
            open={state.isOpen}
            onClose={handleClose}
            onOpen={handleOpen}
            position='top right'
        />

    )
}

export default SaveButton
/*
import {Button, Form, Header, Icon, Modal, Popup, Portal, Segment, TextArea} from "semantic-ui-react";
import React, {useContext, useEffect, useState} from "react";
import {adjustScenarioBasedOnMarketType, checkScenario} from "../commonComponents/helperFunctions";

function UploadButton() {
    const [open, setOpen] = useState(false);
    const [text, setText] = useState("");
    const [portalOpen, setPortalOpen] = useState(false)

    const {setScenario} = useContext(ScenarioContext)

    useEffect(()=> {
        setText("")
    },[open])

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const handleInputChange = (e) => {
        setText(e.target.value);
    };
    const handleFileUpload = (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = (event) => {
                let res = event.target.result
                res = JSON.parse(res)
                setText(res["text"]);
            };
            reader.readAsText(file);
        }
    };

    function togglePortal(){
        setPortalOpen(!portalOpen);
    }

    const handleImport = () => {
        if (checkScenario(adjustScenarioBasedOnMarketType(JSON.parse(text)))){
            setScenario(JSON.parse(text));
            handleClose()
        } else {
            togglePortal()
        }
    };

    return(
        <div>
            <Popup content='Upload' trigger={
                <Button icon onClick={handleOpen} id="uploadButton">
                    <Icon name='upload' />
                </Button>} />

            <Modal id="modal" open={open} onClose={handleClose}>
                <Modal.Header id="modal">Import Scenario</Modal.Header>
                <Modal.Content id="modal">
                    <Form>
                        <Form.Field>
                            <label>Enter Scenario In JSON Text Format</label>
                            <TextArea placeholder='Enter your JSON text here...' value={text} onChange={handleInputChange} />
                        </Form.Field>
                        <Form.Field>
                            <label>Upload Scenario-File</label>
                            <input type="file" accept=".json" onChange={handleFileUpload} />
                        </Form.Field>
                    </Form>
                </Modal.Content>
                <Modal.Actions id="modal">
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button positive onClick={handleImport}>Import</Button>
                </Modal.Actions>
                <Portal open={portalOpen}>
                    <Segment style={{
                        left: '30%',
                        position: "fixed",
                        top: '30%',
                        zIndex: 1000,
                    }}>
                        <Header>Wrong Input</Header>
                        <p>Please make sure that the given string is in JSON format and
                            that all attributes contain valid values.</p>

                        <Button
                            content='Close'
                            negative
                            onClick={togglePortal}
                        />
                    </Segment>
                </Portal>
            </Modal>
        </div>
    )
}
export default UploadButton
*/
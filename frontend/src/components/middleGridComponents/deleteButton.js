import DeleteScenario from "../../API/DeleteScenario"

function DeleteButton({ dualScenario }) {
    const onClick = () => {
        (async () => {
            try {
                await DeleteScenario(dualScenario.scenarioId)
            } catch (e) {
                console.error(e)
            }
        })()
    }

    return(
        <button className="ui negative button" id="deleteButton" onClick={onClick}>
            Delete
        </button>
    )
}

export default DeleteButton
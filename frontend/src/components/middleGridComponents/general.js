import {
    Card, Form, FormGroup, FormInput, FormSelect,
    Table, TableHeader, TableRow, TableHeaderCell, TableBody, TableCell,
} from "semantic-ui-react";
import { useState } from "react";
import Slider from 'rc-slider';
import { ASSET_DISTRIBUTIONS } from "../commonComponents/environment_variables";

function AssetCountView({ assetCounts, setAssetCounts, assetChoice, traderSize }) {
    let currentValue = assetCounts?.[assetChoice]?.[traderSize] || 0

    function onSliderChange(value) {
        setAssetCounts((prevCounts) => ({
            ...prevCounts,
            [assetChoice]: {
                ...(prevCounts?.[assetChoice] || {}),
                [traderSize]: value,
            },
        }))
    }

    return <>
        <div id="table-slider">
            <Slider value={currentValue} min={0} max={20} step={1} onChange={onSliderChange} />
            {currentValue}
        </div>
    </>
}


function General({ scenario, setScenario }) {
    const [gridOptions] = useState([
        { key: 'urban', text: 'Urban', value: 'urban' },
        { key: 'subUrban', text: 'Semi-urban', value: '1-MVLV-semiurb-3.202-1-sw' },
        { key: 'rural', text: 'Rural', value: 'rural' },
    ])

    const assetOptions = [
        { key: "many_small", value: "many_small", "text": "Many small assets" },
        { key: "many_large", value: "many_large", "text": "Many large assets" },
    ]

    function setAssetCounts(updater) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            assetCounts: updater(prevScenario.assetCounts || {})
        }))
    }

    var assetPreset = null
    for (let preset in ASSET_DISTRIBUTIONS) {
        if (scenario.assetCounts === ASSET_DISTRIBUTIONS[preset]) {
            assetPreset = preset
        }
    }

    function selectAssetPreset(_, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            assetCounts: ASSET_DISTRIBUTIONS[value],
        }))
    }

    function setDays(value) {
        setScenario((prevScenario) => {
            return {
                ...prevScenario,
                days: (value),
            }
        }
        )
    }

    function onChangeScenarioName(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            name: value,
        }));
    }

    function onChangeGrid(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            grid: value,
        }));
    }

    function setForecastNoise(value) {
        setScenario((prevScenario) => {
            return {
                ...prevScenario,
                forecastNoise: value,
            }
        }
        )
    }

    return (
        <>
            <Card>
                <h3>Basic Settings</h3>
                <Form>
                    <FormGroup widths='equal'>
                        <FormInput label="Scenario name" value={scenario.name} placeholder="Name..."
                            onChange={onChangeScenarioName} />
                        <FormSelect label="Grid type" options={gridOptions}
                            value={scenario.grid} placeholder="Grid Scenario" onChange={onChangeGrid} />
                    </FormGroup>
                </Form>

                <h5 id="slider-header">Duration: {scenario.days} days</h5>
                <Slider value={scenario.days}
                    min={0}
                    max={30}
                    step={1}
                    onChange={setDays}
                />
            </Card>
            <Card>
                <h3>Digitalization</h3>
                <h5 id="slider-header">Forecast inaccuracy: {scenario.forecastNoise} %</h5>
                <Slider
                        value={scenario.forecastNoise}
                        min={0}
                        max={30}
                        step={1}
                        onChange={setForecastNoise}
                />
            </Card>
            <Card>
                <h3>Agent Setup</h3>
                <Form>
                    <FormGroup>
                        <FormSelect label="Use preset" value={assetPreset} options={assetOptions}
                            onChange={selectAssetPreset}
                            placeholder="Custom"
                        />
                    </FormGroup>
                </Form>
                <Table id="table-general">
                    <TableHeader>
                        <TableRow>
                            <TableHeaderCell id="table-header" />
                            <TableHeaderCell id="table-header" colSpan="2">without Battery</TableHeaderCell>
                            <TableHeaderCell id="table-header" colSpan="2">with Battery</TableHeaderCell>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        <TableRow>
                            <TableHeaderCell rowSpan="2">Photo-voltaic</TableHeaderCell>
                            <TableCell>small</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="pv" traderSize="small"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                            <TableCell>small</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="pv-battery" traderSize="small"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>large</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="pv" traderSize="big"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                            <TableCell>large</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="pv-battery" traderSize="big"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableHeaderCell rowSpan="2">Wind</TableHeaderCell>
                            <TableCell>small</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="wind" traderSize="small"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                            <TableCell>small</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="wind-battery" traderSize="small"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>large</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="wind" traderSize="big"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                            <TableCell>large</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="wind-battery" traderSize="big"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableHeaderCell rowSpan="2">no asset</TableHeaderCell>
                            <TableCell></TableCell>
                            <TableCell></TableCell>
                            <TableCell>small</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="battery" traderSize="small"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell></TableCell>
                            <TableCell>large</TableCell>
                            <TableCell>
                                <AssetCountView
                                    assetChoice="battery" traderSize="big"
                                    assetCounts={scenario.assetCounts} setAssetCounts={setAssetCounts}
                                />
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Card>
        </>
    )
}

export default General
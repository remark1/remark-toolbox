import { Card, Form, FormGroup, FormInput, FormRadio, FormSelect } from "semantic-ui-react";

function Market({ scenario, setScenario }) {
    const pMarketOptions = [
        { key: '2023', text: '2023', value: 'p_prices_2023' },
        { key: '2018', text: '2022', value: 'p_prices_2022' },
        { key: '2019', text: '2021', value: 'p_prices_2021' },
    ]

    function setSequencingMode(e, { value }) {
        if (value === "NAIVE") {
            setScenario((prevScenario) => ({
                ...prevScenario,
                sequencingMode: value,
                qClearingRule: "pay_as_cleared",
            }));
        } else {
            setScenario((prevScenario) => ({
                ...prevScenario,
                sequencingMode: value,
            }));
        }
    }

    function setQTimeResolution(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            qTimeResolution_s: value,
        }));
    }

    function setQClearingRule(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            qClearingRule: value,
        }));
    }

    function onChangeMaxPrice(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            maximumPrice: value,
        }));
    }

    function onChangePMarketPrice(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            pMarketPriceHistoric: value,
        }));
    }

    function setQPriceTick(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            qPriceTick_EUR_Mvarh: value,
        }));
    }

    function setQVolumeTick(e, { value }) {
        setScenario((prevScenario) => ({
            ...prevScenario,
            qVolumeTick_Mvar: value,
        }));
    }


    return (
        <>
            <Card>
                <h3>Active Power Market</h3>
                <Form>
                    <FormGroup widths="equal">
                        <FormSelect label="Price data year" value={scenario.pMarketPriceHistoric} options={pMarketOptions}
                            placeholder="Select P-Market Price ..." onChange={onChangePMarketPrice} />
                    </FormGroup>
                </Form>

                <Form>
                    <FormGroup widths="equal">
                        <FormInput type="number" label="Maximum Price [EUR/MWh]" value={scenario.maximumPrice}
                            placeholder="Set max price ..."
                            onChange={onChangeMaxPrice} />
                    </FormGroup>

                </Form>

            </Card>

            <div>
                <Card>
                    <h3>Reactive Power Market</h3>
                    <Form>
                        <FormGroup inline={true}>
                            <label>Time resolution</label>
                            <FormRadio label="15 min" value={900} checked={scenario.qTimeResolution_s === 900}
                                onChange={setQTimeResolution} />
                            <FormRadio label="1 h" value={3600} checked={scenario.qTimeResolution_s === 3600}
                                onChange={setQTimeResolution} />
                            <FormRadio label="4 h" value={1400} checked={scenario.qTimeResolution_s === 14400}
                                onChange={setQTimeResolution} />
                        </FormGroup>
                        <FormGroup inline={true}>
                            <label>Sequencing with active power</label>
                            <FormRadio label="Simultaneous" value="SIMULTANEOUS"
                                checked={scenario.sequencingMode === "SIMULTANEOUS"}
                                onChange={setSequencingMode} />
                            <FormRadio label="Sequential" value="SEQUENTIAL" checked={scenario.sequencingMode === "SEQUENTIAL"}
                                onChange={setSequencingMode} />
                            <FormRadio label="Naive" value="NAIVE" checked={scenario.sequencingMode === "NAIVE"}
                                onChange={setSequencingMode} />                        
                        </FormGroup>
                        <FormGroup inline={true}>
                            <label>Clearing rule</label>
                            <FormRadio label="Pay-as-bid" value="pay_as_bid" checked={scenario.qClearingRule === "pay_as_bid"}
                                onChange={setQClearingRule} disabled={scenario.sequencingMode === "NAIVE"} />
                            <FormRadio label="Pay-as-cleared" value="pay_as_cleared" checked={scenario.qClearingRule === "pay_as_cleared"}
                                onChange={setQClearingRule} />
                        </FormGroup>
                        <FormInput style={{ width: "50%" }} type="number" label="Volume tick size [Mvar]"
                            value={scenario.qVolumeTick_Mvar}
                            placeholder="Set minimum product size ..."
                            onChange={setQVolumeTick}
                            step="0.0001" />
                        <FormInput style={{ width: "50%" }} type="number" label="Price tick size [EUR/Mvarh]"
                            value={scenario.qPriceTick_EUR_Mvarh}
                            placeholder="Set minimum product size ..."
                            onChange={setQPriceTick} />
                    </Form>
                </Card>
            </div>
        </>

    )
}

export default Market
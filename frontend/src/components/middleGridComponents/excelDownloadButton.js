import React from "react";
import GetExcelFile from "../../API/getExcelFile";
import {Button} from "semantic-ui-react";


const ExcelDownloadButton = ({ runId, name }) => {
    console.log(runId)

    const handleDownload = async () => {
        const result = await GetExcelFile(runId, name);
        if (!result.success) {
            console.error("Error downloading the file:", result.error);
        }
    };

    return (
            <Button secondary onClick={handleDownload} id="excelDownloadButton">Export Results</Button>
    );
};

export default ExcelDownloadButton;
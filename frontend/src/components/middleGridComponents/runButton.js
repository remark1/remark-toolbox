import { useContext, useEffect, useRef, useState } from "react";
import { Popup } from "semantic-ui-react";
import PostRun from "../../API/postRun";
import AllScenarios from "../context/scenarioList";
import GetRuns from "../../API/getRuns";



function RunButton({ scenarioHandle }) {
    const { scenarios, scenariosDispatch } = useContext(AllScenarios)
    const dualScenario = scenarios[scenarioHandle]
    const [, setStartingSimulation] = useState(false)
    const timeoutLength = 2500
    const [msg, setMsg] = useState("")
    const timeoutRef = useRef(null);
    const [state, setState] = useState({ isOpen: false })

    function handleOpen() {
        setState({ isOpen: true })

        timeoutRef.current = setTimeout(() => {
            setState({ isOpen: false })
        }, timeoutLength)
    }

    function handleClose() {
        setState({ isOpen: false })
        clearTimeout(timeoutRef.current)
        setMsg("")
    }


    useEffect(() => {

    }, [msg]);

    function onClickStartSimulation() {
        setStartingSimulation(true)
        setMsg("Starting run ...");

        (async () => {
            try {
                await PostRun(dualScenario.scenarioId)
                setMsg("Scenario run started")
                scenariosDispatch({
                    type: "SetScenarioRuns",
                    scenarioHandle: scenarioHandle,
                    runs: await GetRuns(scenarios[scenarioHandle].scenarioId),
                });
            } catch (e) {
                setMsg("Something went wrong")
            }
        })()
        timeoutRef.current = setTimeout(() => {
            setState({ isOpen: false })
        }, 2500)
    }

    let active = dualScenario.type === "Dual" && dualScenario.localScenario === dualScenario.serverScenario
    let title = (active
        ? "Start a run of the current scenario."
        : "You must save the scenario before you can start a run."
    )

    return (
        <Popup
            trigger={
                <button
                    className="ui positive button"
                    id="runButton"
                    onClick={onClickStartSimulation}
                    disabled={!active}
                    title={title}>
                    Run
                </button>}
            content={msg}
            on='click'
            open={state.isOpen}
            onClose={handleClose}
            onOpen={handleOpen}
            position='top right'
        />
    )
}

export default RunButton
import {Button, Form, Icon, Message, Modal, Popup} from "semantic-ui-react";
import {useContext, useEffect, useState} from "react";

function DownLoadButton() {
    return <></>;
/*
    const {scenario} = useContext(ScenarioContext)
    const [open, setOpen] = useState(false);
    const [text, setText] = useState(JSON.stringify(scenario));
    const [message, setMessage] = useState("");

    useEffect(() => {
        setText(JSON.stringify(scenario))
    },[scenario])

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    const handleCopyToClipboard = () => {
        navigator.clipboard.writeText(text)
            .then(() => {
                setMessage("Text copied to clipboard!");
                setTimeout(() => setMessage(""), 3000);
            })
            .catch(() => {
                setMessage("Failed to copy text.");
                setTimeout(() => setMessage(""), 3000);
            });
    };
    const handleDownloadJson = () => {
        const blob = new Blob([JSON.stringify({ text })], { type: "application/json" });
        const url = URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = url;
        a.download = scenario.name + ".json";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    };
    return (
        <div>
            <Popup content='Download' trigger={
                <Button icon onClick={handleOpen} id="downloadButton">
                    <Icon name="download"/>
                </Button>
            }/>


            <Modal id="modal" open={open} onClose={handleClose}>
                <Modal.Header id="modal">Copy or Download Text</Modal.Header>
                <Modal.Content id="modal">
                    <Form>
                        <Form.TextArea
                            label="Text"
                            value={text}
                        />
                    </Form>
                    {message && <Message>{message}</Message>}
                </Modal.Content>
                <Modal.Actions id="modal">
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button primary onClick={handleCopyToClipboard}>Copy to Clipboard</Button>
                    <Button secondary onClick={handleDownloadJson}>Download as JSON</Button>
                </Modal.Actions>
            </Modal>
        </div>
    )
        */
}

export default DownLoadButton
import Plot from "react-plotly.js";
import {updateSrcIds} from "./util";


function HerfindalHirschmanIndex({results}) {
    const dfs = results;

    const bid_df = dfs["bids"];

    const p_totalAmounts = bid_df.reduce((acc, entry) => {
        if (!acc[entry.src_id]) {
            acc[entry.src_id] = 0;
        }
        acc[entry.src_id] += entry.p_award_amount_mw;
        return acc;
    }, {});

    const p_totalMarket = Object.values(p_totalAmounts).reduce((sum, value) => sum + value, 0);

    let p_marketShares = Object.entries(p_totalAmounts).map(([src_id, amount]) => {
        const share = (amount / p_totalMarket) * 100; // Anteil in Prozent
        return { src_id, amount, share: share.toFixed(2) }; // Prozentanteil auf zwei Dezimalstellen runden
    });
    p_marketShares = updateSrcIds(p_marketShares)


    const p_hhi = p_marketShares.reduce((sum, { share }) => {
        const shareFraction = share / 100; // Anteil in Dezimal umwandeln
        return sum + Math.pow(shareFraction, 2); // Quadrate der Marktanteile summieren
    }, 0);

    const q_totalAmounts = bid_df.reduce((acc, entry) => {
        if (!acc[entry.src_id]) {
            acc[entry.src_id] = 0;
        }
        acc[entry.src_id] += entry.q_award_amount_mvar;
        return acc;
    }, {});

    const q_totalMarket = Object.values(q_totalAmounts).reduce((sum, value) => sum + value, 0);

    let q_marketShares = Object.entries(q_totalAmounts).map(([src_id, amount]) => {
        const share = (amount / q_totalMarket) * 100; // Anteil in Prozent
        return { src_id, amount, share: share.toFixed(2) }; // Prozentanteil auf zwei Dezimalstellen runden
    });

    q_marketShares = updateSrcIds(q_marketShares)

    const q_hhi = q_marketShares.reduce((sum, { share }) => {
        const shareFraction = share / 100; // Anteil in Dezimal umwandeln
        return sum + Math.pow(shareFraction, 2); // Quadrate der Marktanteile summieren
    }, 0);



    return (
        <div>
            <div>
                <h4 className="resultHeadline" style={{ color: '#FFFFFF' }}>Herfindal-Hirschman-Index for P-Market:
                    {(p_hhi * 10000).toFixed(2)}</h4>
                <h4 className="resultHeadline" style={{ color: '#FFFFFF' }}>Herfindal-Hirschman-Index for Q-Market:
                    {(q_hhi * 10000).toFixed(2)}</h4>
                <p/>
                <p className="interpretHl" style={{ color: '#FFFFFF' }}>0-1500: Low Concentration, 1500-2500: Moderate Concentration, 2500-10000: High Concentration</p>
            </div>
            <p/>
            <Plot
                className="plot"
                data={[
                    {
                        x: p_marketShares.map(entry => entry.src_id), // Provider-Namen aus `src_id`
                        y: p_marketShares.map(entry => parseFloat(entry.share)), // Marktanteile (P-Market)
                        type: 'bar',
                        name: 'P-Market Shares', // Name für die Legende
                        marker: { color: '#003A6F' } // Farbe für P-Market
                    },
                    {
                        x: q_marketShares.map(entry => entry.src_id), // Provider-Namen aus `src_id`
                        y: q_marketShares.map(entry => parseFloat(entry.share)), // Marktanteile (Q-Market)
                        type: 'bar',
                        name: 'Q-Market Shares', // Name für die Legende
                        marker: { color: '#FFA500' } // Farbe für Q-Market
                    }
                ]}
                layout={{
                    title: 'Market Shares Comparison',
                    barmode: 'group', // Gruppierte Balken
                    xaxis: {
                        title: 'Agents',
                        gridcolor: '#444444',
                        tickangle: 45, // Provider-Namen schräg stellen
                        showticklabels: false,
                    },
                    yaxis: {
                        title: 'Market Share (%)',
                        gridcolor: '#444444'
                    },
                    autosize: true,
                    paper_bgcolor: '#333333', // Hintergrundfarbe des gesamten Diagramms
                    plot_bgcolor: '#333333', // Hintergrundfarbe des Plots
                    font: {
                        color: '#FFFFFF' // Schriftfarbe auf Weiß setzen
                    },
                    height: 800,
                    legend: {
                        orientation: 'h', // Horizontale Ausrichtung der Legende
                        y: -0.2, // Position der Legende (abwärts verschieben)
                        x: 0.5, // Zentrale Ausrichtung der Legende
                        xanchor: 'center', // Legende horizontal zentrieren
                        yanchor: 'top' // Legende vertikal nach oben anpassen
                    }
                }}
                config={{ responsive: true }}
            />
        </div>
    );

}

export default HerfindalHirschmanIndex
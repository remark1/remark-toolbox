/* eslint no-lone-blocks: off */
import React, { useEffect, useState } from "react";
import Plot from "react-plotly.js";
import { getCol, add, mul as multiply, useWindowSize } from "./util";
import { FormSelect } from "semantic-ui-react";

const COLORS = [
    "#6EC5E9",
    "#F6A03D",
    "#6E0592",
    "#099b9c",
    "#9f9e9e",
    "#F4733D",
    "#003A6F",
    "#E39183",
    "#a64027",
    "#d8a909",
    "#25A5D0",
    "#d67410",
    "#ce2480",
    "#099b9c",
    "#9f9e9e",
    "#513FD7",
];
const LINE_WIDTH = 2;

function toAxes(row, col) {
    let axis = row * 2 + col;
    return { xaxis: `x${axis}`, yaxis: `y${axis}` };
}

function filterSrc(list, src) {
    return list.filter((row) => row.src_id === src);
}

function createRunColumn(agent, { bids, gens, gen_forecast, batteries }, col) {
    let trader = filterSrc(bids, agent.trader);
    let time = getCol(trader, "time");
    var battery = filterSrc(batteries, agent.battery_asset);
    if (battery.length === 0) {
        battery = null;
    }
    let gen = filterSrc(gens, agent.wind_asset).concat(
        filterSrc(gens, agent.pv_asset),
    );
    let genForecast = filterSrc(gen_forecast, agent.wind_agent).concat(
        filterSrc(gen_forecast, agent.pv_agent),
    );
    //console.log("gen", gen);

    let traces = [];
    let titles = [];
    var row = 0;

    {
        titles.push("Price forecasts<br>€/MVAh");
        traces.push({
            x: time,
            y: getCol(trader, "p_clear_price_fc_eur_mwh"),
            name: "Price Forecast P-market",
            legendgroup: "p_forecast",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: "#ce2480", width: LINE_WIDTH },
            ...toAxes(row, col),
        });
        traces.push({
            x: time,
            y: getCol(trader, "q_clear_price_fc_eur_mvarh"),
            name: "Price Forecast Q-market",
            legendgroup: "q_forecast",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: COLORS[1], width: LINE_WIDTH },
            ...toAxes(row, col),
        });
        row++;
    }

    {
        titles.push("Revenues<br>€");
        traces.push({
            x: time,
            y: multiply(
                getCol(trader, "p_award_amount_mw"),
                getCol(trader, "p_award_price_eur_mwh"),
            ),
            name: "Revenue P-market",
            legendgroup: "p_revenue",
            showlegend: col === 1,
            type: "bar",
            marker: { color: "#d8a909" },
            ...toAxes(row, col),
        });
        traces.push({
            x: time,
            y: multiply(
                getCol(trader, "q_award_amount_mvar"),
                getCol(trader, "q_award_price_eur_mwh"),
            ),
            name: "Revenue Q-market",
            legendgroup: "q_revenue",
            showlegend: col === 1,
            type: "bar",
            marker: { color: "#d67410" },
            ...toAxes(row, col),
        });
        row++;
    }

    if (battery !== null) {
        titles.push("Battery SOC<br>%");
        traces.push({
            x: time, // dfs1.bat_soc.index,
            y: getCol(battery, "soc_percent"),
            name: "SOC",
            legendgroup: "battery_soc",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: "#513FD7", width: LINE_WIDTH },
            ...toAxes(row, col),
        });

        row++;
    }

    {
        titles.push("P-market<br>MW");
        let pResGen = getCol(gen, "p_mw");
        traces.push({
            x: time, //dfs1.trader_df.index,
            y: pResGen,
            name: "Renewable generation",
            legendgroup: "ren_gen",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: COLORS[2], width: LINE_WIDTH },
            ...toAxes(row, col),
        });
        traces.push({
            x: time,
            y: getCol(genForecast, "p_fc_mw"),
            name: "Renewable forecast",
            legendgroup: "ren_fc",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: COLORS[3], width: LINE_WIDTH },
            ...toAxes(row, col),
        });
        let pRes = getCol(trader, "p_resource_amount");
        traces.push({
            x: time,
            y: pRes,
            name: "Renewable schedule",
            legendgroup: "ren_sche",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: "#ce2480", width: LINE_WIDTH },
            ...toAxes(row, col),
        });
        if (battery !== null) {
            let pBat = getCol(trader, "p_battery_amount_mw");
            traces.push({
                x: time,
                y: pBat,
                name: "Battery schedule",
                legendgroup: "bat_sche",
                showlegend: col === 1,
                type: "scatter",
                line: { color: "#E39183", width: LINE_WIDTH },
                ...toAxes(row, col),
            });
            traces.push({
                x: time,
                y: add(pResGen, pBat),
                name: "Total schedule",
                legendgroup: "total_sche",
                showlegend: col === 1,
                type: "scatter",
                line: { color: "#a64027", width: LINE_WIDTH },
                ...toAxes(row, col),
            });
        }
        traces.push({
            x: time,
            y: getCol(trader, "p_bid_amount_mw"),
            name: "Bid amount at P-market",
            legendgroup: "bid_p",
            showlegend: col === 1,
            type: "bar",
            marker: { color: "#003a6f" },
            ...toAxes(row, col),
        });
        row++;
    }

    {
        titles.push("Q-market<br>Mvar");
        traces.push({
            x: time,
            y: getCol(trader, "q_free_amount_mvar"),
            name: "Free Amount of Q",
            legendgroup: "free_q",
            showlegend: col === 1,
            type: "scatter",
            mode: "lines",
            line: { color: COLORS[5], width: LINE_WIDTH },
            ...toAxes(row, col),
        });
        traces.push({
            x: time,
            y: getCol(trader, "q_bid_amount_mvar"),
            name: "Bid Amount Q-market",
            legendgroup: "bid_q",
            showlegend: col === 1,
            type: "bar",
            marker: { color: "#6EC5E9" },
            ...toAxes(row, col),
        });
        row++;
    }

    return { rows: row, traces, titles };
}

const CompareResultsPlot = ({ results }) => {
    const [data, setData] = useState([]);
    const [layout, setLayout] = useState({});
    const [traderId, setTraderId] = useState(null);
    const { width } = useWindowSize();

    useEffect(() => {
        if (results === null) {
            return;
        }
        if (traderId === null) {
            setTraderId(results.agent_associations[0].trader);
            return;
        }

        //console.log(results);
        const { bids, gen_forecast, gens, batteries, agent_associations } =
            results;
        const agent_association = agent_associations.filter(
            (a) => a.trader === traderId,
        )[0];

        var { rows, traces, titles } = createRunColumn(
            agent_association,
            { bids, gens, batteries, gen_forecast },
            1,
        );
        traces = traces.concat(
            createRunColumn(
                agent_association,
                {
                    bids: results.bids_baseline,
                    gens: results.gens_baseline,
                    batteries: results.batteries_baseline,
                    gen_forecast: results.gen_forecast_baseline,
                },
                2,
            ).traces,
        );

        // Layout mit Subplot-Titeln und Stilen
        const axisStyle = { tickfont: { size: 11 }, gridcolor: "#444" };
        const layout = {
            grid: { rows, columns: 2, pattern: "independent" },
            barmode: "stack",
            autosize: true,
            paper_bgcolor: "#333333", // Hintergrundfarbe des gesamten Diagramms
            plot_bgcolor: "#333333", // Hintergrundfarbe des Plots
            font: { color: "#ffffff" },
            xaxis: { ...axisStyle },
            xaxis2: { matches: "x", ...axisStyle },
            xaxis3: { matches: "x", ...axisStyle },
            xaxis4: { matches: "x", ...axisStyle },
            xaxis5: { matches: "x", ...axisStyle },
            xaxis6: { matches: "x", ...axisStyle },
            xaxis7: { matches: "x", ...axisStyle },
            xaxis8: { matches: "x", ...axisStyle },
            xaxis9: { matches: "x", ...axisStyle },
            xaxis10: { matches: "x", ...axisStyle },
            yaxis: { title: titles[0], ...axisStyle },
            yaxis2: { matches: "y", ...axisStyle },
            yaxis3: { title: titles[1], ...axisStyle },
            yaxis4: { matches: "y3", ...axisStyle },
            yaxis5: { title: titles[2], ...axisStyle },
            yaxis6: { matches: "y5", ...axisStyle },
            yaxis7: { title: titles[3], ...axisStyle },
            yaxis8: { matches: "y7", ...axisStyle },
            yaxis9: { title: titles[4], ...axisStyle },
            yaxis10: { matches: "y9", ...axisStyle },
            legend: {
                orientation: "h",
                y: -0.2,
                x: 0.5,
                xanchor: "center",
                font: { size: 12, color: "#ffffff" },
            },
            annotations: [
                {
                    text: "<b>Main run</b>",
                    x: 0.18,
                    xref: "paper",
                    y: 1.08,
                    yref: "paper",
                    font: { size: 14, color: "#ffffff" },
                    showarrow: false,
                },
                {
                    text: "<b>Baseline run</b>",
                    x: 0.83,
                    xref: "paper",
                    y: 1.08,
                    yref: "paper",
                    font: { size: 14, color: "#ffffff" },
                    showarrow: false,
                },
            ],
            /*annotations: subplotTitles.map((title, i) => ({
                text: title,
                x: i % 2 === 0 ? 0.25 : 0.75,
                y: 1.05 - Math.floor(i / 2) * 0.25,
                xref: 'paper',
                yref: 'paper',
                showarrow: false,
                font: { size: 14, color: '#ffffff' }
            })),*/
            width: width * 0.77, // Dynamische Breite (z. B. 90% der Fensterbreite)
            height: 1000,
        };

        setData(traces);
        setLayout(layout);
    }, [results, traderId, setTraderId, width]);

    if (results === null) {
        return <>Loading &hellip;</>;
    }

    const traders = results.agent_associations?.map((ass) => ({
        key: ass.trader,
        value: ass.trader,
        text: ass.trader,
    }));

    function setTrader(e, { value }) {
        setTraderId(value);
    }

    return (
        <div className="plot-container">
            <FormSelect
                options={traders}
                placeholder="Select trader"
                value={traderId}
                onChange={setTrader}
            />
            <Plot data={data} layout={layout} config={{ responsive: true }} />
        </div>
    );
};

export default CompareResultsPlot;

import React from 'react';
import Plot from 'react-plotly.js';

/// not longer in use

const BoxplotBlMarket = ({ bl_pay_as_bid_prices, bl_pay_as_cleared_prices }) => {
    const data = [
        {
            type: 'violin',
            y: bl_pay_as_bid_prices,
            name: 'Pay-As-Bid',
            marker: { color: '#FF5959' },
            box: { visible: true },
            meanline: { visible: true }
        },
        {
            type: 'violin',
            y: bl_pay_as_cleared_prices,
            name: 'Pay-As-Cleared',
            marker: { color: '#FFC56C' },
            box: { visible: true },
            meanline: { visible: true }
        }
    ];

    const layout = {
        title: 'Price at BL-Market (Pay-as-bid vs. Pay-as-cleared)',
        xaxis: {
            title: 'BL Market Prices',
            color: '#FFFFFF'  // Schriftfarbe für Achsenbeschriftung auf Weiß setzen
        },
        yaxis: {
            title: 'Price (EUR/Var)',
            color: '#FFFFFF'  // Schriftfarbe für Achsenbeschriftung auf Weiß setzen
        },
        legend: {
            orientation: 'h',
            x: 0,
            y: -0.2,
            font: {
                color: '#FFFFFF'  // Schriftfarbe für die Legende auf Weiß setzen
            }
        },
        plot_bgcolor: '#2f2f2f',  // Hintergrundfarbe des Plots auf Grau setzen
        paper_bgcolor: '#2f2f2f', // Hintergrundfarbe des gesamten "Papiers" auf Grau setzen
        font: {
            color: '#FFFFFF'  // Allgemeine Schriftfarbe auf Weiß setzen
        },
        autosize: true,
    };

    return <Plot className="plot" data={data} layout={layout} config={{responsive: true}} />;
};


export default BoxplotBlMarket;
import React from 'react';
import Plot from 'react-plotly.js';

/// not longer in use

const SpotmarketBlMarketPrices = ({timepoints, spotmarket_prices, bl_prices}) => {
    return (
        <Plot className="plot"
              data={[
                  {
                      x: timepoints,
                      y: spotmarket_prices,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'Spotmarket Price',
                      line: {color: '#FF5959'}
                  },
                  {
                      x: timepoints,
                      y: bl_prices,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'BL Market (Pay-as-bid)',
                      line: {color: '#FFC56C'}
                  }
              ]}
              layout={{
                  title: 'Prices at Spotmarket and BL-Market',
                  xaxis: {
                      title: 'Timepoints',
                  gridcolor: '#444444'
                  },
                  yaxis: {
                      title: 'Price (EUR/MWh)',
                      gridcolor: '#444444'
                  },
                  legend: {orientation: 'h', x: 0, y: -0.2},
                  margin: {l: 50, r: 50, t: 50, b: 50},
                  autosize: true,
                  paper_bgcolor: '#333333', // Hintergrundfarbe des gesamten Diagramms auf Grau setzen
                  plot_bgcolor: '#333333',  // Hintergrundfarbe des Plot-Bereichs auf Grau setzen
                  font: {
                      color: '#FFFFFF' // Schriftfarbe auf Weiß setzen
                  }
              }}
              config={{responsive: true}}
        />
    )
};

export default SpotmarketBlMarketPrices;
import { useEffect, useState } from "react";
import Plot from "react-plotly.js";
import { prepareHeatmapData, srcGroups } from "./util";

const BUSES_HEATMAP = {
    type: "heatmap",
    zmin: 0.5,
    zmax: 1.5,
    colorscale: [
        [0, "#A02F6F"],
        [0.4, "#D14D41"],
        [0.42, "#F2F0E5"],
        [0.58, "#F2F0E5"],
        [0.6, "#CE5D97"],
        [1, "#AF3029"],
    ],
};

const BusVoltagePlot = ({ results }) => {
    const [dataBuses, setDataBuses] = useState(null);
    const [layoutBuses, setLayoutBuses] = useState({});

    useEffect(() => {
        if (results === null) {
            return;
        }

        let mainBusData = prepareHeatmapData(results.nodes, "vm_pu");
        let baseBusData = prepareHeatmapData(results.nodes_baseline, "vm_pu");

        setDataBuses([
            {
                ...BUSES_HEATMAP,
                ...mainBusData,
                xaxis: "x1",
                yaxis: "y1",
            },
            {
                ...BUSES_HEATMAP,
                ...baseBusData,
                xaxis: "x2",
                yaxis: "y2",
            },
        ]);

        setLayoutBuses({
            grid: { rows: 1, columns: 1, pattern: "independent" },
            height: mainBusData.y.length * 2 + 300,
            width: 700,
        });
    }, [results, setLayoutBuses, setDataBuses]);

    if (dataBuses === null) {
        return <div>Plots are loading &hellip;</div>;
    }

    return (
        <div style={{ overflow: "scroll", height: "80vh" }}>
            <Plot
                data={[dataBuses[0]]}
                layout={{ title: "Main run", ...layoutBuses }}
                config={{ responsive: true }}
            />
            <Plot
                data={[dataBuses[1]]}
                layout={{ title: "Baseline run", ...layoutBuses }}
                config={{ responsive: true }}
            />
        </div>
    );
};

export default BusVoltagePlot;

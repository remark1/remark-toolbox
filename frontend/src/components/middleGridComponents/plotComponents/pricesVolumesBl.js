import Plot from 'react-plotly.js';

/// not longer in use

const PricesVolumesBl = ({ timepoints, bl_pay_as_bid_prices, bl_pay_as_cleared_prices, volumes }) => {
    return (
        <Plot className="plot"
              data={[
                  {
                      x: timepoints,
                      y: bl_pay_as_bid_prices,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'BL Market (Pay-as-bid)',
                      line: {color: '#003A6F'},
                  },
                  {
                      x: timepoints,
                      y: bl_pay_as_cleared_prices,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'BL Market (Pay-as-cleared)',
                      line: {color: '#FF5959'},
                  },
                  {
                      x: timepoints,
                      y: volumes,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'BL Market Volume',
                      line: {color: '#FFC56C', dash: 'dash'},
                  },
              ]}
              layout={{
                  title: 'Price and Volumes at BL-Market',
                  xaxis: {title: 'Timepoints'},
                  yaxis: {title: 'Price (EUR/MWh) / Volume (MW)'},
                  legend: {orientation: 'h', x: 0, y: -0.2},
                  margin: {l: 50, r: 50, t: 50, b: 50},
                  autosize: true,
                  paper_bgcolor: '#333333', // Set background color of the entire chart to gray
                  plot_bgcolor: '#333333',  // Set background color of the plotting area to gray
                  font: {
                      color: '#FFFFFF' // Set the font color to white
                  }
              }}
              config={{responsive: true}}
        />
    );
}

export default PricesVolumesBl;
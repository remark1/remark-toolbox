import React, { useEffect, useState } from "react";
import Plot from "react-plotly.js";
import {ffill, useWindowSize} from "./util";

const LINE_WIDTH = 3;
const COLORS = [
    "#1f77b4",
    "#17becf",
    "#2ca02c",
    "#4daf4a",
    "#66c2a5",
    "#6baed6",
    "#3182bd",
    "#31a354",
    "#56b4e9",
    "#2b8cbe",
    "#41ab5d",
    "#99d8c9",
    "#74c476",
    "#addd8e",
    "#08519c",
    "#4292c6",
    "#9ecae1",
    "#005a32",
    "#0072b2",
    "#00441b",
];

const MarketOutcomePlot = ({ results }) => {
    const [data, setData] = useState([]);
    const [layout, setLayout] = useState({});
    const { width} = useWindowSize();

    useEffect(() => {
        const dfs = results;

        const bid_df = dfs["bids"];
        const market_df = dfs["market"];
        const grid_df = dfs["grid_operator"];
        const finc_df = dfs["finc"];
        const agent_associations = dfs["agent_associations"];
        const traders = agent_associations.map((d) => d["trader"]);

        const time = market_df.map((d) => d.time);
        const prices_p = market_df.map((d) => d.p_price_eur_mwh);
        const clearing_pr_q = ffill(
            market_df.map((d) => d.q_clear_price_eur_mvarh),
        );
        const efficiency_pr_q = ffill(
            market_df.map((d) => d.q_eff_price_eur_mvarh),
        );
        const q_demand = ffill(grid_df.map((d) => d.q_demand_mvar));
        const finc_amount_mvar = finc_df.map((d) => d["q_award_amount_mvar"]);

        const traces = [
            {
                x: time,
                y: prices_p,
                name: "Clearing Prices P-market",
                type: "scatter",
                mode: "lines",
                line: { color: "#ce2480", width: LINE_WIDTH },
                xaxis: "x1",
                yaxis: "y1",
            },
            {
                x: time,
                y: clearing_pr_q,
                name: "Clearing Prices Q-market",
                type: "scatter",
                mode: "lines",
                line: { color: "#ce2480", width: LINE_WIDTH },
                xaxis: "x4",
                yaxis: "y4",
            },
            {
                x: time,
                y: efficiency_pr_q,
                name: "Efficiency Prices Q-market",
                type: "scatter",
                mode: "lines",
                line: { color: "#F4733D", width: LINE_WIDTH },
                xaxis: "x4",
                yaxis: "y4",
            },
            {
                x: time,
                y: q_demand,
                name: "Q-demand",
                type: "scatter",
                mode: "lines",
                line: { color: "#F4733D", width: LINE_WIDTH },
                xaxis: "x6",
                yaxis: "y6",
            },
        ];

        // Hinzufügen der Händler-Traces
        traders.forEach((trader, i) => {
            const t_bids = bid_df.filter((b) => b.src_id === trader);
            const traderColor = COLORS[i % COLORS.length];
            const shortTraderName = trader
                .replace("Agents.", "")
                .split("-at")[0];

            traces.push({
                x: t_bids.map((b) => b.time),
                y: t_bids.map((b) => b.p_bid_amount_mw),
                type: "bar",
                name: "p_bid_amount_mw",
                legendgroup: "p_bid_amount_mw",
                showlegend: i === 0,
                marker: { color: traderColor },
                hovertext: shortTraderName,
                xaxis: "x2",
                yaxis: "y2",
            });
            traces.push({
                x: t_bids.map((b) => b.time),
                y: t_bids.map((b) => b.p_award_amount_mw),
                type: "bar",
                name: "p_award_amount_mw",
                legendgroup: "p_award_amount_mw",
                showlegend: i === 0,
                marker: { color: traderColor },
                hovertext: shortTraderName,
                xaxis: "x3",
                yaxis: "y3",
            });
            traces.push({
                x: t_bids.map((b) => b.time),
                y: t_bids.map((b) => b.q_bid_amount_mvar),
                type: "bar",
                name: "q_bid_amount_mvar",
                legendgroup: "q_bid_amount_mvar",
                showlegend: i === 0,
                marker: { color: traderColor },
                hovertext: shortTraderName,
                xaxis: "x5",
                yaxis: "y5",
            });
            traces.push({
                x: t_bids.map((b) => b.time),
                y: t_bids.map((b) => b.q_award_amount_mvar),
                type: "bar",
                name: "q_award_amount_mvar",
                legendgroup: "q_award_amount_mvar",
                showlegend: i === 0,
                marker: { color: traderColor },
                hovertext: shortTraderName,
                xaxis: "x6",
                yaxis: "y6",
            });
        });

        traces.push({
            x: time,
            y: finc_amount_mvar,
            name: "FINC Q-amount",
            type: "bar",
            mode: "lines",
            legendgroup: "FINC Q-amount",
            marker: { color: "#d8a909" },
            xaxis: "x6",
            yaxis: "y6",
        });

        let xaxisStyle = {
            tickfont: { size: 11, color: "#ffffff" },
            gridcolor: "#444",
        };
        let yaxisStyle = {
            tickfont: { size: 14, color: "#ffffff" },
            gridcolor: "#444",
        };
        const layout = {
            grid: { rows: 6, columns: 1, pattern: "independent" },
            barmode: "stack",
            xaxis: xaxisStyle,
            xaxis2: { matches: "x", ...xaxisStyle },
            xaxis3: { matches: "x", ...xaxisStyle },
            xaxis4: { matches: "x", ...xaxisStyle },
            xaxis5: { matches: "x", ...xaxisStyle },
            xaxis6: { matches: "x", ...xaxisStyle },
            yaxis: { title: "P-market prices<br>€/MWh", ...yaxisStyle },
            yaxis2: { title: "P-market bids<br>MW", ...yaxisStyle },
            yaxis3: { title: "P-market awards<br>MW", ...yaxisStyle },
            yaxis4: { title: "Q-market prices<br>€/Mvarh", ...yaxisStyle },
            yaxis5: { title: "Q-market bids<br>Mvar", ...yaxisStyle },
            yaxis6: { title: "Q-market awards<br>Mvar", ...yaxisStyle },
            legend: {
                orientation: "h", // Legende horizontal ausrichten
                yanchor: "top", // Vertikale Verankerung auf 'top' setzen
                y: -0.2, // Position unterhalb des Plots festlegen
                xanchor: "center", // Horizontale Verankerung auf 'center' setzen
                x: 0.5, // Legende zentrieren
                font: { color: "#ffffff" },
            },
            autosize: true,
            paper_bgcolor: '#333333', // Hintergrundfarbe des gesamten Diagramms
            plot_bgcolor: '#333333', // Hintergrundfarbe des Plots
            font: {
                color: "#ffffff", // Helle Schriftfarbe für Titel und Labels
            },
            width: width * 0.77, // Dynamische Breite (z. B. 90% der Fensterbreite)
            height: 1200
        };
        setData(traces);
        setLayout(layout);
    }, [results, width]);

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <Plot
                data={data}
                layout={layout}
                config={{ responsive: true }}
            />
        </div>


    );
};

export default MarketOutcomePlot;

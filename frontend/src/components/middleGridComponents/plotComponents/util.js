import { useEffect, useState } from "react";

export function ffill(list) {
    var latest;
    let result = [];
    for (let x of list) {
        if (x === null) {
            result.push(latest);
        } else {
            result.push(x);
            latest = x;
        }
    }
    return result;
}

export function getCol(df, col) {
    return df.map((r) => r[col]);
}

export function add(series1, series2) {
    return series1.map((val, i) => val + series2[i]);
}

export function mul(series1, series2) {
    return series1.map((val, i) => val * series2[i]);
}

export function useWindowSize() {
    const [width, setWidth] = useState({
        width: window.innerWidth,
    });

    useEffect(() => {
        const handleResize = () => {
            setWidth({
                width: window.innerWidth,
            });
        };

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return width;
}

export function updateSrcIds(data) {
    return data.map(function (entry) {
        // src_id mit replace anpassen, um den Teil nach "-at-" zu entfernen
        const updatedSrcId = entry.src_id.replace(/-at-\d+/, "");

        return {
            ...entry,
            src_id: updatedSrcId, // Rückgabe des Objekts mit der angepassten src_id
        };
    });
}

export function srcGroups(df) {
    let result = {};
    df.forEach((row) => {
        let src = row.src_id;
        if (!(src in result)) {
            result[src] = [];
        }
        result[src].push(row);
    });
    return result;
}

export function prepareHeatmapData(data, attr) {
    let lines = srcGroups(data);
    let srcs = [];
    let times = [];
    let z = [];
    let firstLine = true;
    for (let src in lines) {
        srcs.push(src);
        if (firstLine) {
            lines[src].forEach((line, i) => {
                times.push(line.time);
                z[i] = [line[attr]];
            });
            firstLine = false;
        } else {
            lines[src].forEach((line, i) => {
                z[i].push(line[attr]);
            });
        }
    }
    return { x: times, y: srcs, z };
}

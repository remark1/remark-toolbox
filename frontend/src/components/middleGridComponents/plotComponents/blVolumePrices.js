import React from 'react';
import Plot from 'react-plotly.js';

/// not longer in use


const BlVolumePrices = ({timepoints, bl_prices, bl_volumes}) => {

    return (
        <Plot className="plot"
              data={[
                  {
                      x: timepoints,
                      y: bl_prices,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'BL Market Prices',
                      line: {color: '#FF5959'}
                  },
                  {
                      x: timepoints,
                      y: bl_volumes,
                      type: 'scatter',
                      mode: 'lines',
                      name: 'BL Market Volumes',
                      line: {color: '#FFC56C', dash: 'dash'}
                  }]
              }
              layout={{
                  title: 'Prices and Volumes at BL-Market',
                  xaxis: {
                      title: 'Timepoint',
                      color: '#FFFFFF',  // Schriftfarbe für Achsenbeschriftung auf Weiß setzen
                      gridcolor: '#444444'
                  },
                  yaxis: {
                      title: 'Prices (EUR/MWh) / Volume (MW)',
                      color: '#FFFFFF',  // Schriftfarbe für Achsenbeschriftung auf Weiß setzen
                      gridcolor: '#444444'
                  },
                  legend: {
                      orientation: 'h',
                      x: 0,
                      y: -0.2,
                      font: {
                          color: '#FFFFFF'  // Schriftfarbe für die Legende auf Weiß setzen
                      }
                  },
                  plot_bgcolor: '#2f2f2f',  // Hintergrundfarbe des Plots auf Grau setzen
                  paper_bgcolor: '#2f2f2f', // Hintergrundfarbe des gesamten "Papiers" auf Grau setzen
                  font: {
                      color: '#FFFFFF'  // Allgemeine Schriftfarbe auf Weiß setzen
                  },
                  //margin: {l: 50, r: 50, t: 50, b: 50},
                  autosize: true,
              }}
              var config = {{responsive: true}}
        />
    )
};

export default BlVolumePrices;
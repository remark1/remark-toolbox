import Plot from "react-plotly.js";
import {useState} from "react";
import {FormInput} from "semantic-ui-react";
import {updateSrcIds} from "./util";


function ConcentrationRatio({results}) {
    const [pconcentrationNumber, setPconcentrationNumber] = useState(5);  // Default value: CR5
    const [qconcentrationNumber, setQconcentrationNumber] = useState(5);  // Default value: CR5

    const dfs = results;
    const bid_df = dfs["bids"];

    // Berechne die Marktanteile
    const p_totalAmounts = bid_df.reduce((acc, entry) => {
        if (!acc[entry.src_id]) acc[entry.src_id] = 0;
        acc[entry.src_id] += entry.p_award_amount_mw;
        return acc;
    }, {});

    const p_totalMarket = Object.values(p_totalAmounts).reduce((sum, value) => sum + value, 0);
    let p_marketShares = Object.entries(p_totalAmounts).map(([src_id, amount]) => {
        const share = (amount / p_totalMarket) * 100; // Anteil in Prozent
        return {src_id, amount, share: parseFloat(share.toFixed(2))};
    });

    p_marketShares = updateSrcIds(p_marketShares);

    // Erstelle die sortierte Liste der Marktanteile
    const p_sortedShares = p_marketShares.map(entry => entry.share).sort((a, b) => b - a);

    // Berechne die Konzentrationsraten
    const p_concentrationRatios = p_sortedShares.map((_, index) => {
        const topN = p_sortedShares.slice(0, index + 1);
        const totalShare = topN.reduce((sum, share) => sum + share, 0);
        return totalShare;
    });

    // Wähle die Top N Marktanteile
    const p_topNShares = p_sortedShares.slice(0, pconcentrationNumber);

    // Berechne den "Rest"-Anteil
    const p_restSum = p_sortedShares.slice(pconcentrationNumber).reduce((sum, share) => sum + share, 0);


    const p_data = {
        values: [...p_topNShares, p_restSum],
        labels: [
            ...p_topNShares.map((_, index) => `Agent ${index + 1}`),
            'Others'
        ],
        type: 'pie',
        textinfo: 'label+percent',
        hoverinfo: 'label+percent',
    };

    const q_totalAmounts = bid_df.reduce((acc, entry) => {
        if (!acc[entry.src_id]) acc[entry.src_id] = 0;
        acc[entry.src_id] += entry.q_award_amount_mvar;
        return acc;
    }, {});

    const q_totalMarket = Object.values(q_totalAmounts).reduce((sum, value) => sum + value, 0);
    let q_marketShares = Object.entries(q_totalAmounts).map(([src_id, amount]) => {
        const share = (amount / q_totalMarket) * 100; // Anteil in Prozent
        return {src_id, amount, share: parseFloat(share.toFixed(2))};
    });

    q_marketShares = updateSrcIds(q_marketShares);

    // Erstelle die sortierte Liste der Marktanteile
    const q_sortedShares = q_marketShares.map(entry => entry.share).sort((a, b) => b - a);

    // Berechne die Konzentrationsraten
    const q_concentrationRatios = q_sortedShares.map((_, index) => {
        const topN = q_sortedShares.slice(0, index + 1);
        const totalShare = topN.reduce((sum, share) => sum + share, 0);
        return totalShare;
    });

    // Wähle die Top N Marktanteile
    const q_topNShares = q_sortedShares.slice(0, qconcentrationNumber);

    // Berechne den "Rest"-Anteil
    const q_restSum = q_sortedShares.slice(qconcentrationNumber).reduce((sum, share) => sum + share, 0);

    // Die Daten für den Q-Market Plot
    const q_data = {
        values: [...q_topNShares, q_restSum],
        labels: [
            ...q_topNShares.map((_, index) => `Agent ${index + 1}`),
            'Others'
        ],
        type: 'pie',
        textinfo: 'label+percent',
        hoverinfo: 'label+percent',
    };

    return (
        <div>
            <div>
                <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                    {/* P-Market Section */}
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', width: '48%' }}>
                        <h4 className="resultHeadline" style={{ width: '100%' }}>
                            Concentration Ratio P-Market (CR{pconcentrationNumber}):
                            {p_concentrationRatios[pconcentrationNumber - 1]}%
                        </h4>
                        <FormInput
                            className="resultHeadline"
                            type="Number"
                            label="Concentration Ratio Number: "
                            value={pconcentrationNumber}
                            placeholder="Set concentration ratio number ..."
                            onChange={(e) => setPconcentrationNumber(Number(e.target.value))}
                            step={1}
                            min={1}
                            max={p_sortedShares.length}
                            style={{ width: '100%' }}
                        />
                    </div>

                    {/* Q-Market Section */}
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', width: '48%' }}>
                        <h4 className="resultHeadline" style={{ width: '100%' }}>
                            Concentration Ratio Q-Market (CR{qconcentrationNumber}):
                            {q_concentrationRatios[qconcentrationNumber - 1]}%
                        </h4>
                        <FormInput
                            className="resultHeadline"
                            type="Number"
                            label="Concentration Ratio Number: "
                            value={qconcentrationNumber}
                            placeholder="Set concentration ratio number ..."
                            onChange={(e) => setQconcentrationNumber(Number(e.target.value))}
                            step={1}
                            min={1}
                            max={q_sortedShares.length}
                            style={{ width: '100%' }}
                        />
                    </div>
                </div>
            </div>

            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                <div style={{ width: '48%' }}>
                    <Plot
                        className="plot"
                        data={[p_data]} // Nur P-Market Daten hier
                        layout={{
                            title: 'Concentration Ratio Of P-Market Share',
                            autosize: true,
                            paper_bgcolor: '#333333',
                            plot_bgcolor: '#333333',
                            font: {
                                color: '#FFFFFF',
                            },
                            showlegend: false, // Legende ausblenden
                            height: 500
                        }}
                        config={{ responsive: true }}
                    />
                </div>

                {/* Q-Market Plot */}
                <div style={{ width: '48%' }}>
                    <Plot
                        className="plot"
                        data={[q_data]} // Nur Q-Market Daten hier
                        layout={{
                            title: 'Concentration Ratio Of Q-Market Share',
                            autosize: true,
                            paper_bgcolor: '#333333',
                            plot_bgcolor: '#333333',
                            font: {
                                color: '#FFFFFF',
                            },
                            showlegend: false, // Legende ausblenden
                            height: 500
                        }}
                        config={{ responsive: true }}
                    />
                </div>
            </div>
        </div>
    );
}


export default ConcentrationRatio
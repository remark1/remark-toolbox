import { useEffect, useState } from "react";
import Plot from "react-plotly.js";
import { prepareHeatmapData } from "./util";

const LINE_HEATMAP = {
    type: "heatmap",
    zmin: 0,
    zmax: 150,
    colorscale: [
        [0, "#F2F0E5"],
        [2 / 3, "#CECDC3"],
        [2 / 3, "#D0A215"],
        [1, "#AF3029"],
    ],
};

const LineLoadingPlot = ({ results }) => {
    const [dataLines, setDataLines] = useState(null);
    const [layoutLines, setLayoutLines] = useState({});

    useEffect(() => {
        if (results === null) {
            return;
        }

        let mainLineData = prepareHeatmapData(results.lines, "loading_percent");
        let baseLineData = prepareHeatmapData(
            results.lines_baseline,
            "loading_percent",
        );

        setDataLines([
            {
                ...LINE_HEATMAP,
                ...mainLineData,
                xaxis: "x1",
                yaxis: "y1",
            },
            {
                ...LINE_HEATMAP,
                ...baseLineData,
                xaxis: "x2",
                yaxis: "y2",
            },
        ]);

        setLayoutLines({
            grid: { rows: 1, columns: 1, pattern: "independent" },
            height: mainLineData.y.length * 3 + 300,
            width: 700,
        });
    }, [results, setDataLines, setLayoutLines]);

    if (dataLines === null) {
        return <div>Plots are loading &hellip;</div>;
    }

    return (
        <div style={{ overflow: "scroll", height: "80vh" }}>
            <Plot
                data={[dataLines[0]]}
                layout={{ title: "Main run", ...layoutLines }}
                config={{ responsive: true }}
            />
            <Plot
                data={[dataLines[1]]}
                layout={{ title: "Baseline run", ...layoutLines }}
                config={{ responsive: true }}
            />
        </div>
    );
};

export default LineLoadingPlot;

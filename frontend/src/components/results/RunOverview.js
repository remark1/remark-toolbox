import { Table, TableBody, TableCell, TableHeader, TableHeaderCell, TableRow } from "semantic-ui-react";
import ExcelDownloadButton from "../middleGridComponents/excelDownloadButton";

export function RunOverview({ run }) {
    let values = [];
    for (const key in run) {
        values.push(<TableRow key={key}>
            <TableCell>{key}</TableCell>
            <TableCell>{`${run[key]}`}</TableCell>
        </TableRow>)
    }

    return <>

        <Table id="table-general" celled>
            <TableHeader>
                <TableRow>
                    <TableHeaderCell id="table-header-result-overview-left">Field</TableHeaderCell>
                    <TableHeaderCell id="table-header-result-overview"><text id="table-header-text">Value</text>
                        <ExcelDownloadButton style={{padding: "1px"}} runId={run.id} name={run.id} /></TableHeaderCell>
                </TableRow>
            </TableHeader>
            <TableBody>{values}</TableBody>
        </Table>
    </>
}
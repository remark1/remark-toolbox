import { useEffect, useState } from "react";
import { REST_API_Path } from "../commonComponents/environment_variables";

const decoder = new TextDecoder();

export function Logs({ runId }) {
    const [log, setLog] = useState("");

    useEffect(() => {
        setLog("");
        (async () => {
            const path = `${REST_API_Path()}/runs/${runId}/logs`;
            const response = await fetch(path, { mode: "cors", method: "GET"});
            if (!response.ok) {
                if (response.status === 404) {
                    setLog("Logs for this run are not (or no longer) available.");
                    return;
                }
                throw new Error(`HTTP error ${response.status}!`);
            }
            for await (const chunk of response.body) {
                setLog(oldLog => oldLog += decoder.decode(chunk));
            }
        })().catch(console.error);
    }, [runId])

    return <pre>{log}</pre>
}
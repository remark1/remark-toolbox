import {Accordion} from "semantic-ui-react";
import ScenarioDescription from "./rightGridComponents/scenarioDescription";
import ScenarioResults from "./rightGridComponents/results";
import Login from "./rightGridComponents/login";


function RightColumn({ scenarioHandle }) {
    let panels = []
    if (scenarioHandle !== undefined) {
        panels.push({
            key: "description",
            title: 'Description',
            content: [ <ScenarioDescription key="description" scenarioHandle={scenarioHandle}/> ],
        })
    }

    panels.push({
            key: "results",
            title: 'Results',
            content: [ <ScenarioResults key="results" /> ],
    })

    return (
        <div className="rightColumn" id="rightColumnGrid">
            <div className="outerUpperRow" id="rightColumnGridUpperRow">
                <Login/>
            </div>
            <Accordion id="accordion" styled defaultActiveIndex={2} panels={panels}/>
        </div>
    )
}

export default RightColumn
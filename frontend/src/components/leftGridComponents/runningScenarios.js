import React, {useState} from "react";

function RunningScenarios() {

    const [unfinishedRuns] = useState([]);

    // useEffect(() => {
    //     getUnfinishedRunsFromServer();
    // }, []);
    //
    //
    // async function getUnfinishedRunsFromServer() {
    //     const scenariosFromServer = await getRunsUnfinished();
    //     setUnfinishedRuns(scenariosFromServer);
    // }


    return (
        <div>
                {unfinishedRuns.map((scenario, index) => (
                    <li key={index} className="panelList">{scenario}</li>
                ))}
        </div>
    )

}

export default RunningScenarios;
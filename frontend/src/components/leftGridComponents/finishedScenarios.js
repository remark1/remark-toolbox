import React, {useState} from "react";

function FinishedScenarios() {

    const [finishedScenarios] = useState([]);

    // useEffect(() => {
    //     getUnfinishedRunsFromServer();
    // }, []);
    //
    //
    // async function getUnfinishedRunsFromServer() {
    //     const scenariosFromServer = await getRunsUnfinished();
    //     setUnfinishedRuns(scenariosFromServer);
    // }


    return (
        <div>
            {finishedScenarios.map((scenario, index) => (
                <li key={index} className="panelList">{scenario}</li>
            ))}
        </div>
    )

}

export default FinishedScenarios;
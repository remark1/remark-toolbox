import React, {useContext, useEffect} from "react";
import {Button} from "semantic-ui-react";
import GetScenarios from "../../API/getScenarios";
import LoggedInUser from "../context/loggedInUser";
import GetScenario from "../../API/getScenario";
import scenarioContext from "../context/scenarioContext";
import refreshContext from "../context/refreshContext";
import ScenarioList from "../context/scenarioList";

function Scenarios() {
    const { scenarioInfoMap: scenarioList, setScenarioList } = useContext(ScenarioList)
    const {loggedInUser} = useContext(LoggedInUser)
    const {setScenario} = useContext(scenarioContext)
    const {refresh} = useContext(refreshContext)


    useEffect(() => {
        if (loggedInUser === ""){
            setScenarioList([])
        } else {
            const getScenariosFromServer = async () => {
                const { data, error } = await GetScenarios(loggedInUser);
                if (data) {
                    setScenarioList(data);
                } else {
                    console.error(error || "Failed to fetch scenarios");
                }
            };
            getScenariosFromServer();
        }
    }, [loggedInUser, refresh, setScenarioList]);


    async function handleItemClick(sce) {
        try {
            let scenarioFromServer = await GetScenario(loggedInUser, sce);
            setScenario(scenarioFromServer.scenario)
        } catch (error) {
            console.error("Error fetching scenario from server:", error);
        }
    }

    if (scenarioList.length === 0) {
        return <div></div>;
    }

    return (
        <div>
            {scenarioList.map((scenario, index) => (
                <li key={index} className="panelList">
                    <Button id="scenarioListItem"
                            onClick={() => handleItemClick(scenario["scenario_id"])}>
                        {scenario["scenario_name"]}
                    </Button>
                </li>
            ))}
        </div>
    );

}

export default Scenarios;
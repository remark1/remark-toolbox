import { useContext, useEffect } from "react";
import { Grid, Tab } from "semantic-ui-react";

import RunButton from "../middleGridComponents/runButton";
import SaveButton from "../middleGridComponents/saveButton";
import DeleteButton from "../middleGridComponents/deleteButton";
import General from "../middleGridComponents/general";
import Market from "../middleGridComponents/market";
import AllScenarios from "../context/scenarioList";
import GetScenario from "../../API/getScenario";

export function Configurator({ scenarioHandle }) {
    const { scenarios, scenariosDispatch } = useContext(AllScenarios)
    let dualScenario = scenarios[scenarioHandle]
    useEffect(() => {
        if (dualScenario.type === "ServerOnly") {
            (async () => {
                try {
                    scenariosDispatch({
                        type: "DownloadScenario",
                        scenarioHandle: scenarioHandle,
                        serverScenario: await GetScenario(dualScenario.scenarioId),
                    });
                } catch (e) {
                    console.error(e);
                }
            })();
        }
    }, [dualScenario, scenariosDispatch, scenarioHandle])

    if (dualScenario.type === "ServerOnly") {
        return <h2>Loading scenario <i>{dualScenario.scenarioName}</i> from server &hellip;</h2>
    }

    function setLocalScenario(updater) {
        scenariosDispatch({ type: "UpdateLocal", scenarioHandle, updater })
    }

    const menu = [
        {
            menuItem: "General",
            render: () => <Tab.Pane id="tab-pane">
                <General scenario={dualScenario.localScenario} setScenario={setLocalScenario} />
            </Tab.Pane>,
        },
        {
            menuItem: "Market",
            render: () => <Tab.Pane id="tab-pane">
                <Market scenario={dualScenario.localScenario} setScenario={setLocalScenario} />
            </Tab.Pane>,
        },
        {
            menuItem: "Debug",
            render: () => <Tab.Pane id="tab-pane">
                Scenario ID: {dualScenario.scenarioId}
                <pre>{JSON.stringify(dualScenario.localScenario, null, 2)}</pre>
            </Tab.Pane>
        },
    ];

    return (
        <>
            <h2>
                Scenario <i>{dualScenario.localScenario.name}</i>
                {dualScenario.localScenario === dualScenario.serverScenario ? "" : " (unsaved)"}
            </h2>
            <Tab id="tabs" panes={menu} />
            <Grid container columns={4}>
                <Grid.Column>
                    <DeleteButton dualScenario={dualScenario} />
                </Grid.Column>
                <Grid.Column />
                <Grid.Column>
                </Grid.Column>
                <Grid.Column>
                    <div>
                        <RunButton scenarioHandle={scenarioHandle} />
                        <SaveButton dualScenario={dualScenario} scenarioHandle={scenarioHandle} />
                    </div>
                </Grid.Column>
            </Grid>
        </>
    )
}
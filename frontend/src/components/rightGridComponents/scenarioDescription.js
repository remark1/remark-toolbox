import { useContext } from "react";
import AllScenarios from "../context/scenarioList";

function ScenarioDescription({ scenarioHandle }) {
    const { scenarios } = useContext(AllScenarios);
    const scenario = scenarios[scenarioHandle].localScenario;

    if (scenario === undefined) {
        return <div>Loading &hellip;</div>
    }

    return <div>
        <p>Name: {scenario.name}</p>
        <p>Duration: {scenario.duration}</p>
        <p>Scenario: {scenario.gridScenario}</p>
        <p>Market Type: {scenario.marketType}</p>
        <p>Maximum Price: {scenario.maxPrice}</p>
    </div>
}

export default ScenarioDescription;
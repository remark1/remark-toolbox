import React, {useEffect, useState} from 'react';
import { Button, Modal, Form, Message } from 'semantic-ui-react';
import PostUserSecretValidation from "../../API/postUserSecretValidation";

function ForgotPasswordModal({ open, onClose }) {
    const [userName, setUserName] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [secret, setSecret] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("");
    const [error, setError] = useState("");
    const [message] = useState("");


    function reset_params() {
        setUserName("")
        setNewPassword("")
        setConfirmPassword("")
        setSecret("")
    }

    const handleReset = () => {
        if (newPassword !== confirmPassword) {
            setError('Passwords do not match');
        } else if (newPassword === "" || userName === "" || confirmPassword === "" || secret === "") {
            setError('Please fill all fields.');
        } else {
            const validating = async () => {
                const {data} = await PostUserSecretValidation(userName, newPassword, secret);

                if (data) {
                    setError('');
                    alert('Password reset successful!');
                    onClose()
                } else {
                    setError("Username or secret are wrong. Please try again.");
                }
            };
            validating();
        }
    };
    useEffect(()=> {
        setError("")
        reset_params()
    },[])

    return (
        <Modal closeIcon open={open} onClose={onClose}>
            <Modal.Header id="modal">Reset Password</Modal.Header>
            <Modal.Content id="modal">
                <Form error={!!error}>
                    <Form.Input
                        label="Username"
                        type="text"
                        placeholder="Enter your username"
                        value={userName}
                        onChange={(e) => setUserName(e.target.value)}
                    />
                    <Form.Input
                        label="Secret"
                        type="text"
                        placeholder="Enter your most loved dish"
                        value={secret}
                        onChange={(e) => setSecret(e.target.value)}
                    />
                    <Form.Input
                        label="New Password"
                        type="password"
                        placeholder="Enter your new password"
                        value={newPassword}
                        onChange={(e) => setNewPassword(e.target.value)}
                    />
                    <Form.Input
                        label="Confirm New Password"
                        type="password"
                        placeholder="Confirm your new password"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                    />
                    {error && <Message error content={error} />}
                    {message && <Message info content={message} />}
                </Form>
            </Modal.Content>
            <Modal.Actions id="modal">
                <Button onClick={onClose}>Cancel</Button>
                <Button primary onClick={handleReset}>Reset Password</Button>
            </Modal.Actions>
        </Modal>
    );
}

export default ForgotPasswordModal;
import React, {useEffect, useState} from 'react';
import {Button, Modal, Form, Message} from 'semantic-ui-react';
import PostRegistration from "../../API/postRegistration";

function RegistrationModal() {
    const [open, setOpen] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [secretAnswer, setSecretAnswer] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [error, setError] = useState('');

    function reset_params() {
        setUsername("")
        setPassword("")
        setConfirmPassword("")
        setSecretAnswer("")
    }

    const handleRegister = () => {
        if (password !== confirmPassword) {
            setError('Passwords do not match');
        } else if (password === "" || username === "" || confirmPassword === "" || secretAnswer === "") {
            setError('Please fill all fields.');
        } else {
            const registering = async () => {
                const {data} = await PostRegistration(username, confirmPassword, secretAnswer);

                if (data) {
                    setError('');
                    setOpen(false);
                    alert('Registration successful!');
                } else {
                    setError("Username already taken. Please try another.");
                }
            };
            registering();
        }
    }
    useEffect(()=> {
        setError("")
        reset_params()
    },[open])

return (
    <Modal
        closeIcon
        open={open}
        trigger={<Button id="registrationButton">Register</Button>}
        onClose={() => {
            setOpen(false)
        }}
        id="modal"
        onOpen={() => setOpen(true)}
    >
        <Modal.Header id="modal">Register</Modal.Header>
        <Modal.Content id="modal">
            <Form error={!!error}>
                <Form.Input
                    label="Username"
                    type="text"
                    placeholder="Enter your username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
                <Form.Input
                    label="Secret"
                    type="text"
                    placeholder="Enter your favorite meal"
                    value={secretAnswer}
                    onChange={(e) => setSecretAnswer(e.target.value)}
                />
                <Form.Input
                    label="Password"
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <Form.Input
                    label="Confirm Password"
                    type="password"
                    placeholder="Confirm your password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                />
                {error && <Message error content={error}/>}
            </Form>
        </Modal.Content>
        <Modal.Actions id="modal">
            <Button onClick={() => setOpen(false)}>Cancel</Button>
            <Button primary onClick={handleRegister}>Register</Button>
        </Modal.Actions>
    </Modal>
);
}

export default RegistrationModal;
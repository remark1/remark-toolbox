import React, { useContext, useEffect, useState } from 'react';
import LoginModal from './loginModal';
import RegistrationModal from './registrationModal';
import LoggedInUser from "../context/loggedInUser";
import { Button } from "semantic-ui-react";

function Login() {
    const { loggedInUser, setLoggedInUser } = useContext(LoggedInUser);
    const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);

    useEffect(() => {
        setIsUserLoggedIn(loggedInUser !== null);
    }, [loggedInUser]);

    return (
        <div>
            {!isUserLoggedIn && <LoginModal />}
            {!isUserLoggedIn && <RegistrationModal />}
        </div>
    );
}

export default Login;
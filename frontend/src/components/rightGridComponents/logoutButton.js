import React, {useContext, useEffect, useState} from "react";
import LoggedInUser from "../context/loggedInUser";
import {Button} from "semantic-ui-react";

function LogoutButton() {
    const { loggedInUser, setLoggedInUser } = useContext(LoggedInUser);
    const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);

    useEffect(() => {
        setIsUserLoggedIn(loggedInUser !== null);
    }, [loggedInUser]);

    function onClickLogout() {
        setLoggedInUser(null);
        console.log(loggedInUser)
    }

    return (
        <div>
            <div id="logoutDiv">
                <div>
                    <h4 id="headerLoggedIn">
                        {isUserLoggedIn ? `Logged in as:` : ''}
                    </h4>
                    <h3 id="headerUserLoggedIn">{loggedInUser}</h3>
                </div>
                <div>
                    {isUserLoggedIn && (
                        <Button negative onClick={onClickLogout}>Logout</Button>
                    )}
                </div>
            </div>
        </div>
    );
}

export default LogoutButton;
import React, {useContext, useState} from 'react';
import { Button, Modal, Form, Message } from 'semantic-ui-react';
import ForgotPasswordModal from './forgotPasswordModal';
import LoggedInUser from "../context/loggedInUser";
import PostValidation from "../../API/postValidation";

function LoginModal() {
    const { setLoggedInUser } = useContext(LoggedInUser);
    const [open, setOpen] = useState(false);
    const [forgotPasswordOpen, setForgotPasswordOpen] = useState(false);
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');


    const handleLogin = () => {
        const validation = async () => {
            const { data, error } = await PostValidation(userName, password);
            if (data) {
                setError('');
                setOpen(false);
                setLoggedInUser(userName);
            } else {
                console.log(error)
                setError(error || "Failed to fetch scenarios");
            }
        };
        validation();
    };


    return (
        <>
            <Modal
                closeIcon
                trigger={<Button primary id="loginButton">Login</Button>}
                open={open}
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
            >
                <Modal.Header id="modal">Login</Modal.Header>
                <Modal.Content id="modal">
                    <Form error={!!error}>
                        <Form.Input
                            label="Username"
                            type="text"
                            placeholder="Enter your username"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                        />
                        <Form.Input
                            label="Password"
                            type="password"
                            placeholder="Enter your password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        {error && <Message error content={error} />}
                    </Form>
                </Modal.Content>
                <Modal.Actions id="modal">
                    <Button onClick={() => setOpen(false)}>Cancel</Button>
                    <Button primary onClick={handleLogin}>Login</Button>
                    <Button onClick={() => setForgotPasswordOpen(true)} id="forgotPasswordButton">Forgot Password?</Button>
                </Modal.Actions>
            </Modal>

            <ForgotPasswordModal
                open={forgotPasswordOpen}
                onClose={() => setForgotPasswordOpen(false)}
            />
        </>
    );
}

export default LoginModal;
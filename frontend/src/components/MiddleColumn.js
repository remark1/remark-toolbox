import {Grid, Tab} from "semantic-ui-react";
import RunButton from "./middleGridComponents/runButton";
import SaveButton from "./middleGridComponents/saveButton";
import UploadButton from "./middleGridComponents/uploadButton";
import DeleteButton from "./middleGridComponents/deleteButton";
import General from "./middleGridComponents/general";
import Market from "./middleGridComponents/market";
import Results from "./middleGridComponents/results";
import DownLoadButton from "./middleGridComponents/downLoadButton";

function MiddleColumn() {
    const menu = [
        {
            menuItem: "General",
            render: () => <Tab.Pane id="tab-pane">
                <General/>
            </Tab.Pane>,
        },
        {
            menuItem: "Market",
            render: () => <Tab.Pane id="tab-pane">
                <Market/>
            </Tab.Pane>,
        },
        {
            menuItem: "Results",
            render: () => <Tab.Pane id="tab-pane-results">
                <Results/>
            </Tab.Pane>,
        },
    ];

    return (
        <div id="middle-col">
            <Tab id="tabs" panes={menu}/>
            <Grid container columns={4} id="bottom-middlegrid">
                <Grid.Column>
                    <DeleteButton/>
                </Grid.Column>
                <Grid.Column/>
                <Grid.Column>
                    <div>
                        <UploadButton/>
                        <DownLoadButton/>
                    </div>
                </Grid.Column>
                <Grid.Column>
                    <div>
                        <RunButton/>
                        <SaveButton/>
                    </div>
                </Grid.Column>
            </Grid>
        </div>

    )
}

export default MiddleColumn

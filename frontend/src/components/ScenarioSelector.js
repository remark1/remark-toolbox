import { Fragment, useContext, useEffect, useState } from "react";
import AllScenarios from "./context/scenarioList";
import { Accordion, AccordionContent, AccordionTitle, Icon } from 'semantic-ui-react';
import GetRuns from "../API/getRuns";
import DateView from "./commonComponents/DateView";
import LoggedInUser from "./context/loggedInUser";

function Success({ missing, value, name = "Run" }) {
    if (missing) {
        return <span title={`${name} does not exist`}>⛔</span>
    }
    switch (value) {
        case true:
            return <span title={`${name} successful`}>✅</span>
        case false:
            return <span title={`${name} failed`}>❌</span>
        default:
            return <span title={`${name} is running or status is unknown`}>{"\u2754"}</span>
    }
}

function ScenarioSelector({ mainState, setMainState }) {
    const { loggedInUser } = useContext(LoggedInUser)
    const { scenarios, scenariosDispatch } = useContext(AllScenarios);
    const [openHandle, setOpenHandle] = useState(null);

    const handleScenarioClick = (e, titleProps) => {
        const clickedHandle = titleProps.index;
        if (openHandle === clickedHandle && mainState.state === "ShowScenario") {
            setOpenHandle(null);
        } else {
            setOpenHandle(clickedHandle);
        }
        setMainState({ state: "ShowScenario", scenarioHandle: clickedHandle });
    }

    useEffect(() => {
        if (mainState.state === "ChangeToNewest") {
            setMainState({ state: "ShowScenario", scenarioHandle: scenarios.length - 1 });
            setOpenHandle(scenarios.length - 1)
        }
    }, [mainState, setMainState, scenarios])

    useEffect(() => {
        if (mainState.state === "ShowScenario") {
            setOpenHandle(mainState.scenarioHandle)
        }
    }, [mainState, setOpenHandle]);

    useEffect(() => {
        if (
            openHandle === null ||
            openHandle === undefined ||
            !scenarios?.[openHandle] ||
            scenarios[openHandle]?.runs !== undefined
        ) {
            return;
        }
        (async () => {
            try {
                scenariosDispatch({
                    type: "SetScenarioRuns",
                    scenarioHandle: openHandle,
                    runs: await GetRuns(scenarios[openHandle].scenarioId),
                });
            } catch (e) {
                scenariosDispatch({
                    type: "SetScenarioRuns",
                    scenarioHandle: openHandle,
                    runs: false,
                });
                console.log("japa")
                console.error(e);
            }
        })()
    }, [openHandle, scenarios, scenariosDispatch])

    const handleRunClick = (runId) => (e) => {
        setMainState({ state: "ShowRun", scenarioHandle: openHandle, runId: runId });
    }

    const createNewScenario = () => {
        scenariosDispatch({ type: "CreateNewScenario" })
        setMainState({ state: "ChangeToNewest" })
    }

    function reloadRuns() {
        (async () => {
            try {
                scenariosDispatch({
                    type: "SetScenarioRuns",
                    scenarioHandle: openHandle,
                    runs: await GetRuns(scenarios[openHandle].scenarioId),
                });
            } catch (e) {
                scenariosDispatch({
                    type: "SetScenarioRuns",
                    scenarioHandle: openHandle,
                    runs: false,
                });
                console.error(e);
            }
        })()        
    }

    if (scenarios === null) {
        return <>Loading your scenarios &hellip;</>
    }

    if (scenarios === false) {
        return <>Loading scenarios failed. Is the orchestrator running? (Check the console for more information.)</>
    }

    let scenarioSegments = [];
    scenarios.forEach((sce, sceHandle) => {
        const active = sceHandle === openHandle;

        let scenarioName;
        switch (sce.type) {
            case "LocalOnly":
                scenarioName = `(L) ${sce.localScenario.name}`
                break
            case "ServerOnly":
                scenarioName = `(S) ${sce.scenarioName}`
                break
            case "Dual":
                scenarioName = `(D) ${sce.localScenario.name}`
                break
            default:
                throw new Error(`Unknown type of dual scenario ${sce.type}`)
        }

        let runsList;
        if (sce.type === "LocalOnly") {
            runsList = <>Local scenario without runs.</>
        } else if (sce.runs === null) {
            runsList = <>Loading scenario runs &hellip;</>
        } else if (sce.runs === false) {
            runsList = <>Loading runs failed.</>
        } else if (sce.runs.size === 0) {
            runsList = <>No runs yet.</>
        } else {
            let runItems = [];
            for (let [runId, run] of sce.runs) {
                runItems.push(
                    <li id="accordion-list-item-runs-li" key={runId} onClick={handleRunClick(runId)} >
                        <Success name="Baseline run" missing={run.baseline_id === null} value={run.base_successful} />
                        <Success name="Main run" missing={run.id === null} value={run.main_successful} />
                        &nbsp;
                        <DateView dateStr={run.exp_start} />
                    </li>
                )
            }
            runsList = <ul id="accordion-list-item-runs">{runItems}</ul>
        }

        scenarioSegments.push(
            <Fragment key={sceHandle}>
                <AccordionTitle active={active} onClick={handleScenarioClick} index={sceHandle}>
                    <Icon name="dropdown" />
                    {scenarioName}
                </AccordionTitle>
                <AccordionContent active={active}>
                    Runs (<a style={{cursor: "pointer"}} onClick={reloadRuns}>reload</a>):
                    {runsList}
                </AccordionContent>
            </Fragment>
        );
    });

    return <>
        { loggedInUser !== null ? <button className="ui button" id="button_new_scenario"
                                          onClick={createNewScenario}>New Scenario</button> : <></> }
        <Accordion id="accordion-left" styled>
            {scenarioSegments}
        </Accordion>
    </>
}

export default ScenarioSelector
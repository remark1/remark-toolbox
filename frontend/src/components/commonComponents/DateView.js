    const format = Intl.DateTimeFormat('en-GB', {
        weekday: "short",
        day: "numeric",
        month: "short",
        year: "numeric",
        hour: "numeric",
        minute: "numeric",
    })

export default function DateView({ dateStr }) {
    if (dateStr === null) {
        return <>(date missing)</>
    }
    let date = new Date(dateStr)

    return <time dateTime={date.toISOString()}>{format.format(date)}</time>
}
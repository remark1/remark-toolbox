// eslint-disable-next-line
const PATH = "";
// eslint-disable-next-line
const DEV_PATH = "http://127.0.0.1:5000";

// TODO: It would be nice to download these from the orchestrator somehow
export const ASSET_DISTRIBUTIONS = {
    many_small: {
        pv: {
            small: 5,
            big: 3,
        },
        "pv-battery": {
            small: 3,
            big: 2,
        },
        wind: {
            small: 3,
        },
        "wind-battery": {
            small: 1,
        },
        battery: {
            small: 2,
            big: 1,
        },
    },
    many_large: {
        pv: {
            small: 3,
            big: 6,
        },
        "pv-battery": {
            small: 2,
            big: 4,
        },
        wind: {
            big: 2,
        },
        "wind-battery": {
            small: 1,
        },
        battery: {
            big: 2,
        },
    },
};

export const DEFAULT_SCENARIO = {
    name: "Untitled scenario",
    days: 7,
    grid: "1-MVLV-semiurb-3.202-1-sw",
    pMarketPriceHistoric: "p_prices_2023",
    maximumPrice: null,
    qTimeResolution_s: 3600,
    qClearingRule: "pay_as_cleared",
    sequencingMode: "SEQUENTIAL",
    forecastNoise: 10,
    qVolumeTick_Mvar: 0.0001,
    qPriceTick_EUR_Mvarh: 1,
    assetCounts: ASSET_DISTRIBUTIONS.many_small,
};

export function REST_API_Path() {
    if (process.env.NODE_ENV === "development") {
        return DEV_PATH;
    }

    return PATH;
}

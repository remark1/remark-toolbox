
export function adjustScenarioBasedOnMarketType(scenario) {
    let marketType = scenario["marketType"].toLowerCase();
    if (marketType === "dayahead") {
        delete scenario.timeResolutionID;
        delete scenario.leadTime;
        delete scenario.marketClearingID;
        delete scenario.minimalVolume;
    } else if (marketType === "intraday") {
        delete scenario.timeResolutionDA;
        delete scenario.closingTime;
        delete scenario.marketClearingDA;
        delete scenario.minimalProductSize;
    }
    return scenario;
}

export function checkScenario(scenario) {
    for (let key in scenario) {
        if (typeof scenario[key] === 'string' && scenario[key].trim() === '') {
            return false;
        }
    }
    return true;
}


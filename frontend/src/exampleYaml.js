export default function ExampleYaml() {
    const scenario = "\n" +
        "remark:\n" +
        "  modules: [store, timesim, sndata, powergrid, der, weather]\n" +
        "  custom_modules:\n" +
        "    - [remark_agents, mas.midas_module:RemarkAgentsModule]\n" +
        "  start_date: 2017-01-01 00:00:00+0100\n" +
        "  end: 24*60*60\n" +
        "  powergrid_params:\n" +
        "    remark_scope:\n" +
        "      gridfile: simplest_grid.json\n" +
        "  remark_agents_params:\n" +
        "    remark_scope:\n" +
        "      load_agents:\n" +
        "        - unit: [1, 1]\n" +
        "          trader: 0\n" +
        "        - unit: [1, 0]\n" +
        "          trader: 1\n" +
        "      battery_agents:\n" +
        "        - unit: [4, 1]\n" +
        "          trader: 0\n" +
        "      pv_agents:\n" +
        "        - 3\n" +
        "      traders:\n" +
        "        - {}\n" +
        "        - {}\n" +
        "  sndata_params:\n" +
        "    remark_scope:\n" +
        "      household_mapping:\n" +
        "        1: [[2, 1.0], [4, 2.0], [3, 2.0]]\n" +
        "        2: [[4, 2.0], [2, 1.0], [6, 1.0]]\n" +
        "      land_mapping: {}\n" +
        "  weather_params:\n" +
        "    my_weather_station:\n" +
        "      weather_mapping:\n" +
        "        WeatherCurrent: [\"interpolate\": true]\n" +
        "  der_params:\n" +
        "    remark_scope:\n" +
        "      grid_name: remark_scope\n" +
        "      sim_name: PysimmodsBAT\n" +
        "      mapping:\n" +
        "        4: [[BAT, 0.4], [BAT, 0.4]]\n" +
        "        13: [[BAT, 0.4], [BAT, 0.4], [BAT, 0.4]]\n" +
        "      weather_provider_mapping:\n" +
        "        BAT: [my_weather_station, 0]\n" +
        "\n"

    return scenario
}

/*
{
    "name": "test",
    "remark": {
    "modules": [
        "store",
        "timesim",
        "sndata",
        "powergrid",
        "der",
        "weather"
    ],
        "custom_modules": [
        [
            "remark_agents",
            "mas.midas_module:RemarkAgentsModule"
        ]
    ],
        "start_date": "2017-01-01 00:00:00 +0100",
        "end": "24*60*60",
        "powergrid_params": {
        "remark_scope": {
            "gridfile": "simplest_grid.json"
        }
    },
    "remark_agents_params": {
        "remark_scope": {
            "load_agents": [
                {
                    "unit": [
                        1,
                        1
                    ],
                    "trader": 0
                },
                {
                    "unit": [
                        1,
                        0
                    ],
                    "trader": 1
                }
            ],
                "battery_agents": [
                {
                    "unit": [
                        4,
                        1
                    ],
                    "trader": 0
                }
            ],
                "pv_agents": [
                3
            ],
                "traders": [
                {
                },
                {
                }
            ]
        }
    },
    "sndata_params": {
        "remark_scope": {
            "household_mapping": {
                "1": [
                    [
                        2,
                        1.0
                    ],
                    [
                        4,
                        2.0
                    ],
                    [
                        3,
                        2.0
                    ]
                ],
                    "2": [
                    [
                        4,
                        2.0
                    ],
                    [
                        2,
                        1.0
                    ],
                    [
                        6,
                        1.0
                    ]
                ]
            },
            "land_mapping": {
            }
        }
    },
    "weather_params": {
        "my_weather_station": {
            "weather_mapping": {
                "WeatherCurrent": [
                    {
                        "interpolate": true
                    }
                ]
            }
        }
    },
    "der_params": {
        "remark_scope": {
            "grid_name": "remark_scope",
                "sim_name": "PysimmodsBAT",
                "mapping": {
                "4": [
                    [
                        "BAT",
                        0.4
                    ],
                    [
                        "BAT",
                        0.4
                    ]
                ],
                    "13": [
                    [
                        "BAT",
                        0.4
                    ],
                    [
                        "BAT",
                        0.4
                    ],
                    [
                        "BAT",
                        0.4
                    ]
                ]
            },
            "weather_provider_mapping": {
                "BAT": [
                    "my_weather_station",
                    0
                ]
            }
        }
    }
}
}
*/
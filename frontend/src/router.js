import {createBrowserRouter} from "react-router-dom";
import React from "react";
import App from "./App";
import Errorpage from "./components/Errorpage";

function Router() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <App/>,
            errorElement: <Errorpage/>,
            children: [],
        },
    ]);

    return router
}

export default Router()
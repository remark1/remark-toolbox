import ContextWrapper from "./components/context/ContextWrapper";
import React, { Fragment, useEffect, useState } from "react";
import ScenarioSelector from "./components/ScenarioSelector";
import RightColumn from "./components/RightColumn";
import { Configurator } from "./components/scenario/Configurator";
import { Link } from "react-router-dom";
import Results from "./components/middleGridComponents/results";
import GetScenario from "./API/getScenario";
import Login from "./components/rightGridComponents/login";
import LogoutButton from "./components/rightGridComponents/logoutButton";

function App() {
    /* Main App State. Valid values are:
    - state: "NotLoggedIn"
    - state: "NothingSelected"
    - state: "ShowScenario", dualScenario
    - state: "ShowRun", scenarioId, runId
    */
    //const [mainState, setMainState] = useState({ state: "ShowRun", scenarioHandle: 2, runId: "run_79c81776-ca16-41de-b1ec-27ddc3a873e2" });
    const [mainState, setMainState] = useState({ state: "NotLoggedIn" });
    //const [mainState, setMainState] = useState({
    //    state: "DelayTo",
    //    followUps: [
    //        { state: "ShowScenario", scenarioHandle: 3 },
    //        { state: "ShowRun", scenarioHandle: 3, runId: "run_1eb41681-325f-4f9a-bc15-cf2f037be114" },
    //    ]
    //})
    const [loggedInUser, setLoggedInUser] = useState(null);

    useEffect(() => {
        console.log(loggedInUser);
        if (!loggedInUser) {
            setMainState({ state: "NotLoggedIn" });
        }
    }, [loggedInUser]);

    useEffect(() => {
        switch (mainState.state) {
            case "LoadScenario":
                (async () => {
                    try {
                        setMainState({
                            state: "ShowScenario",
                            scenarioHandle: mainState.scenarioId,
                            scenario: await GetScenario(mainState.scenarioId),
                        });
                    } catch (e) {
                        console.error(e);
                    }
                })();
                break;
            case "DelayTo":
                mainState.followUps.forEach((followUp, index) => {
                    setTimeout(
                        () => {
                            setMainState(followUp);
                        },
                        500 * (index + 1),
                    );
                });
                break;
            default:
                break;
        }
    }, [mainState]);

    let mainView;
    switch (mainState.state) {
        case "NotLoggedIn":
            mainView = (
                <>
                    <h2>Welcome to the REMARK Toolbox</h2>
                    <p>Please log in in the top right.</p>
                </>
            );
            break;
        case "NothingSelected":
            mainView = (
                <>
                    <h2>Welcome, {loggedInUser}</h2>
                    <p>Please select a scenario or run in the left sidebar.</p>
                </>
            );
            break;
        case "ShowScenario":
            mainView = (
                <Configurator scenarioHandle={mainState.scenarioHandle} />
            );
            break;
        case "ShowRun":
            mainView = (
                <Results
                    scenarioHandle={mainState.scenarioHandle}
                    runId={mainState.runId}
                />
            );
            break;
        case "ChangeToNewest":
            mainView = <h2>Opening new scenario &hellip;</h2>;
            break;
        case "DelayTo":
            mainView = <h2>Loading requested view &hellip;</h2>;
            break;
        default:
            throw new Error(
                `Invalid main state: ${JSON.stringify(mainState)}.`,
            );
    }

    return (
        <React.Fragment>
            <ContextWrapper
                setMainState={setMainState}
                loggedInUser={loggedInUser}
                setLoggedInUser={setLoggedInUser}
            >
                {(children) => (
                    <div id="app-main">
                        <div className="ui grid" id="main-grid">
                            <div className="three wide column" id="left-col">
                                <div
                                    className="outerUpperRow"
                                    id="leftColumnGridUpperRow"
                                >
                                    <div
                                        className="leftColumn"
                                        id="leftColumnGrid"
                                    >
                                        <Link to="home">
                                            <img
                                                className="logo"
                                                src={require("./resources/images/REMARKT_Toolbox_RGB_v2.png")}
                                                alt="REMARK Logo"
                                            />
                                        </Link>
                                        <ScenarioSelector
                                            mainState={mainState}
                                            setMainState={setMainState}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div
                                className="thirteen wide column"
                                id="middle-col-hl"
                            >
                                <div id="middle-col">
                                    <LogoutButton />
                                    {mainView}
                                </div>
                            </div>
                            {/*<div className="three wide column" id="right-col">*/}
                            {/*    <RightColumn scenarioHandle={mainState.scenarioHandle} />*/}
                            {/*</div>*/}
                        </div>
                        <div className="ten wide column" id="middle-col">
                            <div className="middle-col">{mainView}</div>
                        </div>
                        <div className="three wide column" id="right-col">
                            <RightColumn
                                scenarioHandle={mainState.scenarioHandle}
                            />
                        </div>
                    </div>
                )}
            </ContextWrapper>
        </React.Fragment>
    );
}

export default App;

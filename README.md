# Remark Toolbox

The **REMARK Toolbox** is a tool to analyze reactive-power market designs.
It integrates a configurable market simulation consisting of a real- and reactive-power market, agents bidding on those market and a grid operator procuring reactive power from that market with a grid simulation to study the impact on the grid state.

It comes with predefined smart-grid scenarios and configurable parameters for scenario creation, so you can create the use case you want to simulate.

The the **core** of the toolbox is based on the following pieces of software:

- the smart-grid co-simulation framework [mosaik] developed by OFFIS - Institute for Information Technology in Oldenburg, Germany
- the multi-agent simulation framework [mango], also developed by OFFIS
- the grid simulation [pandapower]

It is loosely based on the Mango Mosaik Electricity Market, [Mamoem].

For the **orchestrator** which provides the frontend to the toolbox, it also uses

- a PostgreSQL database
- Docker, for containerizing the core simulation
- the Python framework Flask as a backend for the UI
- a web UI written in React

[mosaik]: https://gitlab.com/mosaik
[MIDAS]: https://gitlab.com/midas-mosaik/midas
[Mamoem]: https://gitlab.com/digitalized-energy-systems/models/mamoem

## Installation

There are a couple of ways of running the toolbox, depending on your needs:

1. You can run the entire thing as a Docker application consisting of several Docker containers. This is maybe the easiest way, but it is also the least convenient for extending the toolbox.
2. You can run the orchestrator outside of the toolbox, with just the simulation runs happening in containers.
3. You can run the simulation independent from the orchestrator.

The easiest way to do stuff with the toolbox is to install [invoke].
This is a task runner written in Python.
As we have several virtual environments for different parts of the project, invoke should be installed globally.

We also use [Poetry] for dependency management.
If you want to run the tasks completely, it is required.

The tasks.py file currently defines the following tasks, which you can call using `inv <taskname>` from anywhere in the repository:

| task       | purpose |
|------------|---------|
| build.core | Build the core container which runs the actual simulation. |
| build.app  | Build the core container, then build the app. |
| build      | Same as build.app. |
| run.app    | Start the core container. The below on how to run a simulation within it. After starting this, go to <http://localhost:8080> for the interface. [Necessary setup](#running-using-docker). |
| run        | Perform a free run of the core without using docker. See below [on what to set up for this](#running-without-docker).
| migrate    | Run the migrations to prepare free runs. |

[invoke]: https://www.pyinvoke.org
[Poetry]: https://python-poetry.org/docs/master/#installing-with-the-official-installer

### Running using Docker

The orchestrator will spawn Docker containers running the actual simulation as its siblings, which you need to build. This requires the following files:

- The SCIPOptSuite which you can download on their [website](https://www.scipopt.org/). You need the Debian package *SCIPOptSuite-9.1.0-Linux-debian12.deb*, which should be placed in the *core* folder. If you get a different version, adapt lines 7 and 9 in *core/Dockerfile*.
- Price data for the active power market. This should be a CSV file starting like
    ```csv
    PPrices
    "MTU (UTC)","Price[EUR/MWh]","Currency"
    2023-01-01 00:00:00,"-1.07","EUR"
    ```
    This data should include the simulation interval, which starts at 2023-07-01 by default. This should be placed as *p_prices_2023.csv* in *core/data*.

You can now build the REMARK core image, using the following commands (make sure that the image is tagged `remark_core`.)

```bash
$ poetry lock --no-update
$ docker build -t remark_core core
```
or
```
$ inv build.core
```

You also need to build the web frontend by running
```bash
$ npm run build
```
in the *frontend* folder.

In the *orchestrator* folder, supply an SSL certificate via the two files *local.crt* and *local.key*.

Then, you can run the simulation using `docker-compose`, i.e.
```bash
$ docker compose up --build
```
or
```
$ inv run.app
```
(the latter will also build the core container.)

You should be able to see the frontend by going to <https://localhost:5000>.


### Running the orchestrator outside of docker

To run the orchestrator outside of Docker, you need to set up a PostgreSQL database with schema and a user with privileges for that schema. See the *storage/init-remark-db.sh* script an example of how to do this. (You can also run the storage Docker container from the compose.yaml file to run PostgreSQL in a container.)

Second, you need to build the core Docker container as in the [Running using Docker](#running-using-docker).

Finally, you need to have [Poetry] installed.

Set environment variables according to the following table and based on the schema and user name you selected when setting up the database.

| Variable | Value |
| - | - |
| `REMARK_PG_HOST` | The URL of the database. (Default: localhost) |
| `REMARK_PG_PORT` | The port of the database. (Default: 5432) |
| `REMARK_PG_DB`   | The name of the database. (Default: remark) |
| `REMARK_PG_USER` | The user for the database. (Default: remark) |
| `REMARK_PG_PASSWORD` | The user’s password. |
| `REMARK_PG_SCHEMA` | A schema to use within the database (Default: remark) |

In the *orchestrator* folder, make sure to deactivate any active Python environments and run `poetry install` and then `poetry run python -m orchestrator --local`. (The `--local` flag turns off https.)

Finally, either build the frontend using `npm run build` in the *frontend* folder, in which case you can access the frontend by going to the orchestrator's address, or run `npm start` in the *frontend* folder, in which case you need to instead supply the orchestrator's address in *frontend/src/components/commonComponents/environment_variables.js* as `dev_path` and you can access the frontend using the server started by npm. (Using `npm start` is useful for development, as it features live reload.)

If you want to run the simulation outside of Docker, for example while
developing, you can install it like this:


### Running scenarios only (no GUI)

Set up a PostgreSQL database as in [Running the orchestrator outside of docker](#running-the-orchestrator-outside-of-docker), and still install [Poetry]. You also need a working installation of SCIPOpt which Python needs to be able to find.

Now, in the *core* folder, you can `poetry run python -m scenario.suite --help` to use the scenario suite, or `poetry run python -m scenario` to run a scenario directly, providing the scenario description via standard input.

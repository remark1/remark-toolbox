Currently, there are a couple of steps to deploy this on our VMs:

0. Install docker.

1. Install the invoke task runner globally on the machine. This should provide the `inv` command, used to run the tasks in `tasks.py`.

2. Create self-signed certificate for the HTTPS connection. These should give you two files `local.crt` and `local.key` that need to be placed in the `orchestrator` folder. (These will only be used for the connection between our VM and OFFIS' webproxy -- the proxy will present an non-self-signed certificate to the outside world, instead.)

3. Build the frontend. For this, run `npm run build` in the `frontend` folder.

4. Build the core. For this, download
   - SCIPOptSuite-9.1.0-Linux-debian12.deb from the SCIOpt website and place it in the `core` folder. (If you get a newer version, update `core/Dockerfile` to use that, instead.)
   - The historical price data from our NextCloud. Name it `p_prices_2023.csv` and place it in `core/data`.
   Run `inv build.core`.

5. Build the orchestrator. This should just be `inv build.app`, given you have performed steps in steps 1, 2 and 3.

6. Run `inv run.app` or `docker compose up`.

CREATE TABLE gens(
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    p_mw DOUBLE PRECISION NULL,
    q_mvar DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)    
);

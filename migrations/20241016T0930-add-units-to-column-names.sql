ALTER TABLE bids
    RENAME COLUMN p_bid_price TO P_bid_price_EUR_MWh;
ALTER TABLE bids
    RENAME COLUMN p_bid_amount TO P_bid_amount_MW;
ALTER TABLE bids
    RENAME COLUMN q_bid_price TO Q_bid_price_EUR_Mvarh;
ALTER TABLE bids
    RENAME COLUMN q_bid_amount TO Q_bid_amount_Mvar;
ALTER TABLE bids
    RENAME COLUMN p_award_amount TO P_award_amount_MW;
ALTER TABLE bids
    RENAME COLUMN q_award_amount TO Q_award_amount_Mvar;
ALTER TABLE bids
    RENAME COLUMN q_free_amount TO Q_free_amount_Mvar;
ALTER TABLE bids
    RENAME COLUMN p_clear_price_fc TO P_clear_price_fc_EUR_MWh;
ALTER TABLE bids
    RENAME COLUMN q_demand_fc TO Q_demand_fc_Mvar;
ALTER TABLE bids
    RENAME COLUMN q_clear_price_fc TO Q_clear_price_fc_EUR_Mvarh;
ALTER TABLE bids
    RENAME COLUMN q_eff_price_fc TO Q_eff_price_fc_EUR_Mvarh;

ALTER TABLE gen_forecast
    RENAME COLUMN p_mw_fc TO P_fc_MW;

-- Use a temporary table name as Postgres does not allow RENAME COLUMN
-- to just change the capitalization.
ALTER TABLE gens
    RENAME COLUMN p_mw TO P_MW_temp;
ALTER TABLE gens
    RENAME COLUMN P_MW_temp TO P_MW;
ALTER TABLE gens
    RENAME COLUMN q_mvar TO Q_Mvar_temp;
ALTER TABLE gens
    RENAME COLUMN Q_Mvar_temp TO Q_Mvar;

ALTER TABLE market
    RENAME COLUMN p_price TO P_price_EUR_MWh;
ALTER TABLE market
    RENAME COLUMN marginal_q_price TO Q_clear_price_EUR_Mvarh;
ALTER TABLE market
    RENAME COLUMN efficiency_q_price TO Q_eff_price_EUR_Mvarh;

ALTER TABLE runs
    RENAME COLUMN run_id TO id;
ALTER TABLE runs
    ADD COLUMN baseline_for TEXT NULL REFERENCES runs (id);
UPDATE runs
SET scenario_id = (
    SELECT id
    FROM scenarios
    WHERE runs.id = any(string_to_array(scenarios.run_id, ','))
);
ALTER TABLE scenarios
    DROP COLUMN run_id;

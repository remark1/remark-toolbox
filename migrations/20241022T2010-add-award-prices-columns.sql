ALTER TABLE bids
    ADD COLUMN p_award_price_eur_mwh DOUBLE PRECISION NULL,
    ADD COLUMN q_award_price_eur_mwh DOUBLE PRECISION NULL;

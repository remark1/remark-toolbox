CREATE TABLE market(
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    p_price DOUBLE PRECISION NULL,
    marginal_q_price DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)
);

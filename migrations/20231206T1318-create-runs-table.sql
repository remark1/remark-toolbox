CREATE TABLE runs(
    run_id TEXT PRIMARY KEY,
    start_time TIMESTAMPTZ,
    end_time TIMESTAMPTZ
);

CREATE VIEW latest_run AS(
    SELECT run_id FROM runs ORDER BY start_time DESC LIMIT 1
);

CREATE TABLE scenarios (
    id TEXT NOT NULL,
    run_id TEXT,
    user_name TEXT NOT NULL,
    scenario_name TEXT NOT NULL,
    scenario JSONB,
    creation_time TIMESTAMPTZ,
    PRIMARY KEY (id, user_name)
);
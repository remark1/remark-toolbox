This directory contains update scripts for the database schema.

To change the database schema, add a corresponding script in this directory,
named

    <current datetime>-<description>.sql

where the current datetime is the time at which the migration is created (not
applied) and is given in the form `2023-12-01T14:09`.

When applying the upgrade script, a corresponding row should be added to the
*changelog* table in the database, filling in the *migration_date* and
*migration_desc* fields with the corresponding parts of the filename. By
checking whether this row already exists, we can avoid running migrations
multiple times. **This also means that a migration should not be changed
anymore after having been published.** Create a new migration undoing or
amending the change, instead.

The `tasks.py` file contains an invoke task *migrate* which performs the
migration automatically (in ascending order of datetime). It will only run
migrations that have not been performed on the database already.

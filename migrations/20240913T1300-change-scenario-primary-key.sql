ALTER TABLE runs
  DROP CONSTRAINT runs_scenario_id_scenario_user_fkey;
ALTER TABLE scenarios
  DROP CONSTRAINT scenarios_pkey,
  ADD PRIMARY KEY (id);
ALTER TABLE runs
  DROP COLUMN scenario_user,
  ADD FOREIGN KEY (scenario_id) REFERENCES scenarios ON DELETE CASCADE;
ALTER TABLE runs
ADD COLUMN scenario_id TEXT NULL,
ADD COLUMN scenario_user TEXT NULL,
ADD FOREIGN KEY (scenario_id, scenario_user) REFERENCES scenarios,
ADD COLUMN successful BOOLEAN NULL;
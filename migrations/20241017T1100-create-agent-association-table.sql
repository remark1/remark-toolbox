CREATE TABLE agent_associations (
    run_id TEXT,
    trader TEXT,
    pv_agent TEXT,
    wind_agent TEXT,
    battery_agent TEXT,
    pv_asset TEXT,
    wind_asset TEXT,
    battery_asset TEXT,
    PRIMARY KEY (run_id, trader)
);
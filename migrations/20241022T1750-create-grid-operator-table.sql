CREATE TABLE grid_operator (
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    q_demand_mvar DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)
);
CREATE TABLE bids(
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    p_bid_price DOUBLE PRECISION NULL,
    p_bid_amount DOUBLE PRECISION NULL,
    q_bid_price DOUBLE PRECISION NULL,
    q_bid_amount DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)
);

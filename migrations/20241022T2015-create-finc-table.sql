CREATE TABLE finc (
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    q_award_amount_mvar DOUBLE PRECISION NULL,
    q_award_price_eur_mvarh DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)
);
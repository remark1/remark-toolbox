CREATE TABLE run_containers (
    run_id TEXT PRIMARY KEY REFERENCES runs (id),
    container_id TEXT
);
ALTER TABLE runs
    DROP COLUMN container_id;
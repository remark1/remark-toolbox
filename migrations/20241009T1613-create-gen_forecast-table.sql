CREATE TABLE gen_forecast(
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    p_MW_fc DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)    
);

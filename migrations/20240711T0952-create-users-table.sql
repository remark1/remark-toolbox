CREATE TABLE users (
    user_name TEXT NOT NULL,
    password TEXT NOT NULL,
    secret TEXT NOT NULL,
    creation_time TIMESTAMPTZ,
    PRIMARY KEY (user_name)
);

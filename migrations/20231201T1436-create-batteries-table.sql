CREATE TABLE batteries (
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    PRIMARY KEY (run_id, time, src_id),
    soc_percent DOUBLE PRECISION
);

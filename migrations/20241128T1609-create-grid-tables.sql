CREATE TABLE lines (
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    i_ka DOUBLE PRECISION NULL,
    loading_percent DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)
);
CREATE TABLE nodes (
    run_id TEXT,
    time TIMESTAMPTZ,
    src_id TEXT,
    vm_pu DOUBLE PRECISION NULL,
    va_deg DOUBLE PRECISION NULL,
    p_mw DOUBLE PRECISION NULL,
    q_mvar DOUBLE PRECISION NULL,
    PRIMARY KEY (run_id, time, src_id)
);

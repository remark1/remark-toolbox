import os
import uuid

from invoke import Collection, task


remark_root, _ = os.path.split(__file__)
core_path = os.path.join(remark_root, "core")
orchestrator_path = os.path.join(remark_root, "orchestrator")
migrations_path = os.path.join(remark_root, "migrations")
frontend_path = os.path.join(remark_root, "frontend")


@task
def build_core(ctx):
    with ctx.cd(core_path):
        ctx.run("poetry lock --no-update")
        ctx.run("docker build -t remark_core .")


@task(build_core)
def build_app(ctx):
    # This takes too long to do it each time
    # with ctx.cd(frontend_path):
    #    ctx.run("npm run build")
    with ctx.cd(orchestrator_path):
        ctx.run("poetry lock --no-update")
    with ctx.cd(remark_root):
        ctx.run("docker compose build")


@task(build_app)
def run_app(ctx):
    with ctx.cd(remark_root):
        ctx.run("docker compose up")


@task
def run(ctx):
    run_id = f"inv_{uuid.uuid4().hex[0:8]}"
    print(f"Starting {run_id}")
    with ctx.cd(core_path):
        ctx.run(
            f"poetry run python src/scenario.py {run_id} < ../example_scenario.json",
            pty=True,
        )
    print(f"Completed {run_id}")


@task
def migrate(ctx):
    with ctx.cd(orchestrator_path):
        ctx.run(f"poetry run python -m migrate {migrations_path}", pty=True)


ns = Collection()

build_ns = Collection()
build_ns.add_task(build_core, "core")
build_ns.add_task(build_app, "app", default=True)
ns.add_collection(build_ns, "build")

run_ns = Collection()
run_ns.add_task(run_app, "app")
run_ns.add_task(run, "free", default=True)
ns.add_collection(run_ns, "run")

ns.add_task(migrate, "migrate")

## Unit conventions

We try to follow naming conventions similar to pandapower. If a variable contains values in some unit, the variable name is extended by the unit. We follow the standard capitalization of units (choosing to violate the Python standard of lower-case variable names, instead). Units with a negative exponent are placed after an additional underscore instead of a slash. For example, `speed_m_s` would be a variable holding a speed in m/s, `volume_Wh` would be a variable holding the volume of some product measured in Wh (watt-hours).

In addition to standard units, we also use the following:

- EUR is currency in euros
- VA is used whenever the actual unit could be W or VAr
- vt is the *volume tick*; its relation to Wh or VArh is given in some variable `vt_in_VAh`
- pt is the *price tick*; it size is given by some variable `pt_in_EUR_vt` (i.e. "price tick in euros per volume tick")

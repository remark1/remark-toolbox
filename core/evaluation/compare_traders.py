import math

import pandas as pd
import plotly.graph_objects as go
from eval_util import get_dfs_and_agent_associations
from plotly.subplots import make_subplots

COLORS = [
    "#6EC5E9",  # light blue
    "#F6A03D",  # orange
    "#6E0592",  # purple
    "#099b9c",  # turquase
    "#9f9e9e",  # grey
    # '#022336',  # black
    # '#149406',  # green
    # '#ce2480', # pink
    "#F4733D",  # dark orange
    "#003A6F",  # dark blue
    "#E39183",
    "#a64027",  # redbrown
    "#d8a909",  # yellow
    "#25A5D0",  # blue
    "#d67410",  # orange
    "#ce2480",  # pink
    "#099b9c",  # green
    "#9f9e9e",  # grey
    "#513FD7",
]
LINE_WIDTH = 3


def compare_results(
    f_path, asset_types, m_setup1, m_setup2=None, file_name1=None, file_name2=None
):
    dfs1, tr_dict1 = get_dfs_and_agent_associations(
        f_path, asset_types, m_setup1, file_name=file_name1
    )
    setup1_name = f"{m_setup1['trader_setup']}-{m_setup1['mode']}-{m_setup1['clearing']}-{m_setup1['noise']}"
    if m_setup2 is not None:
        dfs2, tr_dict2 = get_dfs_and_agent_associations(
            f_path, asset_types, m_setup2, file_name=file_name2 or file_name1
        )
        setup2_name = f"{m_setup2['trader_setup']}-{m_setup2['mode']}-{m_setup2['clearing']}-{m_setup2['noise']}"
    else:
        tr_dict2 = tr_dict1
        setup2_name = f"{setup1_name} Benchmark"

    for tr_name, tr_setup in tr_dict1.items():
        if tr_name in tr_dict2.keys():
            if isinstance(tr_setup["battery_asset"], str):
                rows = 5
                subplot_titles = (
                    f"{setup1_name}<b><br>Price Forecasts</b>",
                    f"{setup2_name} <b><br>Price Forecasts</b>",
                    f"{setup1_name}<b><br>Revenues</b>",
                    f"{setup2_name} <b><br>Revenues</b>",
                    # f"{setup1_name}<b><br> Battery SOC</b>", f"{setup2_name}<b><br>Battery SOC</b>",
                    # f"{setup1_name}<b><br>P-Market</b>", f"{setup2_name}<b><br>P-Market</b>",
                    # f"{setup1_name}<b><br> Q-Market</b>", f"{setup2_name}<b><br>Q-Market</b>")
                    f"<b>Battery SOC</b>",
                    f"<b>Battery SOC</b>",
                    f"<b>P-Market</b>",
                    f"<b>P-Market</b>",
                    f"<b>Q-Market</b>",
                    f"<b>Q-Market</b>",
                )
                row_heights = [0.225, 0.225, 0.1, 0.225, 0.225]
                p_market_row = 4
                q_market_row = 5
                bat_included = True
            else:
                rows = 4
                subplot_titles = (
                    f"{setup1_name}<b><br>Price Forecasts</b>",
                    f"{setup2_name} <b><br>Price Forecasts</b>",
                    f"{setup1_name}<b><br>Revenues</b>",
                    f"{setup2_name} <b><br>Revenues</b>",
                    f"<b>P-Market</b>",
                    f"<b>P-Market</b>",
                    f"<b>Q-Market</b>",
                    f"<b>Q-Market</b>",
                )
                row_heights = [0.25, 0.25, 0.25, 0.25]
                p_market_row = 3
                q_market_row = 4
                bat_included = False
            fig = make_subplots(
                rows=rows,
                cols=2,
                subplot_titles=subplot_titles,
                row_heights=row_heights,
                shared_yaxes=True,
                vertical_spacing=0.08,
                horizontal_spacing=0.05,
            )
            add_data_from_run(
                fig,
                dfs1,
                setup1_name,
                tr_setup,
                bat_included,
                1,
                p_market_row,
                q_market_row,
            )
            if m_setup2 is not None:
                add_data_from_run(
                    fig,
                    dfs2,
                    setup2_name,
                    tr_dict2[tr_name],
                    bat_included,
                    2,
                    p_market_row,
                    q_market_row,
                    show_legend=False,
                )
            else:
                add_data_from_run(
                    fig,
                    dfs1,
                    setup2_name,
                    tr_dict2[tr_name],
                    bat_included,
                    2,
                    p_market_row,
                    q_market_row,
                    use_benchmark=True,
                    show_legend=False,
                )
            font_size = 14
            x_font_size = 11
            title = f"{setup1_name} - {setup2_name} <br> {tr_name}"

            fig.update_layout(
                margin=dict(t=160),
                template="seaborn",
                title=dict(
                    text=title,
                    font_size=font_size + 3,
                    y=0.93,
                    x=0.5,
                    xanchor="center",
                    yanchor="top",
                ),
                legend=dict(
                    orientation="h",
                    yanchor="bottom",
                    y=-0.1,
                    # y=1.1,
                    xanchor="center",
                    x=0.5,
                    font={"size": font_size},
                ),
                yaxis=dict(
                    tickfont=dict(size=font_size),
                    title=dict(font_size=font_size, text="€/MW - €/MVar"),
                ),
                yaxis2=dict(
                    tickfont=dict(size=font_size),
                    title=dict(font_size=font_size, text="€/MW - €/MVar"),
                ),
                yaxis3=dict(
                    tickfont=dict(size=font_size),
                    title=dict(font_size=font_size, text="€"),
                ),
                yaxis4=dict(
                    tickfont=dict(size=font_size),
                    title=dict(font_size=font_size, text="€"),
                ),
                xaxis=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis2=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis3=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis4=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis5=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis6=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis7=dict(
                    tickfont=dict(size=x_font_size),
                ),
                xaxis8=dict(
                    tickfont=dict(size=x_font_size),
                ),
            )
            if bat_included:
                fig.update_layout(
                    yaxis5=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="SOC %"),
                    ),
                    yaxis6=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="SOC %"),
                    ),
                    yaxis7=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MW"),
                    ),
                    yaxis8=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MW"),
                    ),
                    yaxis9=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MVar"),
                    ),
                    yaxis10=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MVar"),
                    ),
                )
            else:
                fig.update_layout(
                    yaxis5=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MW"),
                    ),
                    yaxis6=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MW"),
                    ),
                    yaxis7=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MVar"),
                    ),
                    yaxis8=dict(
                        tickfont=dict(size=font_size),
                        title=dict(font_size=font_size, text="MVar"),
                    ),
                )
            fig.update_annotations(font_size=font_size)
            fig.update_xaxes(matches=f"x1")
            for i in range(0, rows):
                col = i * 2 + 1
                fig.update_yaxes(matches=f"y{col}", row=i + 1)
            fig.show()
        else:
            print(f"{tr_name} in {setup1_name} but not in {setup2_name}")


def add_data_from_run(
    fig,
    dfs,
    s_name,
    trader_setup,
    bat_included,
    column,
    p_market_row,
    q_market_row,
    show_legend=True,
    use_benchmark=False,
):
    suffix = ""
    if use_benchmark:
        suffix = "_baseline"
    bid_df = dfs[f"bids{suffix}"]
    gen_forecast_df = dfs[f"gen_forecast{suffix}"]
    gen_df = dfs[f"gens{suffix}"]
    trader = trader_setup["trader"]
    wind_gen_forecast_mw = gen_forecast_df[
        gen_forecast_df["src_id"] == trader_setup["wind_agent"]
    ].set_index("time")["p_fc_mw"]
    pv_gen_forecast_mw = gen_forecast_df[
        gen_forecast_df["src_id"] == trader_setup["pv_agent"]
    ].set_index("time")["p_fc_mw"]
    wind_gen = gen_df[gen_df["src_id"] == trader_setup["wind_asset"]].set_index("time")[
        "p_mw"
    ]
    pv_gen = gen_df[gen_df["src_id"] == trader_setup["pv_asset"]].set_index("time")[
        "p_mw"
    ]
    trader_df = bid_df[bid_df["src_id"] == trader].set_index("time")

    trader_df_df_hourly = trader_df.resample("h").first()
    revenue_p = (
        trader_df_df_hourly.p_award_amount_mw
        * trader_df_df_hourly.p_award_price_eur_mwh
    )
    revenue_q = (
        trader_df_df_hourly.q_award_amount_mvar
        * trader_df_df_hourly.q_award_price_eur_mwh
    )
    sum_p = round(sum(revenue_p), 2)
    sum_q = round(sum(revenue_q), 2)

    bids_p = trader_df["p_bid_amount_mw"]
    bids_q = trader_df["q_bid_amount_mvar"]
    price_fc_p = trader_df["p_clear_price_fc_eur_mwh"]
    price_fc_q = trader_df["q_clear_price_fc_eur_mvarh"]
    q_free_amount = trader_df["q_free_amount_mvar"]
    p_bat = trader_df["p_battery_amount_mw"]
    if bat_included:
        soc = dfs[f"batteries{suffix}"]
        bat = trader_setup["battery_asset"]
        first_soc = soc[soc["src_id"] == bat]["soc_percent"].iloc[0] / 100
        last_soc = soc[soc["src_id"] == bat]["soc_percent"].iloc[-1] / 100
        avg_p_price = price_fc_p.mean()
        if "small" in trader:
            capa_kw = 500
        else:
            capa_kw = 4000
        soc_value = round((last_soc - first_soc) * capa_kw / 1000 * avg_p_price, 2)
        if not math.isnan(sum_q):
            sum_total = sum_p + sum_q + soc_value
        else:
            sum_total = sum_p + soc_value
        rev_title = f"<b>Revenues</b><br>P-market: {sum_p} € -- Q-market: {sum_q} € -- SOC Value: {soc_value} total: {round(sum_total, 2)} €"
    else:
        if not math.isnan(sum_q):
            sum_total = sum_p + sum_q
        else:
            sum_total = sum_p
        rev_title = f"<b>Revenues</b><br>P-market: {sum_p} € -- Q-market: {sum_q} € -- total: {round(sum_total, 2)} €"
    fig.layout.annotations[1 + column].text = rev_title

    time = trader_df.index

    p_res = trader_df["p_resource_amount_mw"]
    p_inv = p_res + p_bat
    bat_included = "Bat" in trader

    fig.add_trace(
        go.Scatter(
            x=time,
            y=price_fc_p,
            name="price forecast P-market",
            line=dict(color="#ce2480", width=LINE_WIDTH),
            legendgroup="price_fc_p",
            showlegend=show_legend,
        ),
        row=1,
        col=column,
    )

    fig.add_trace(
        go.Scatter(
            x=time,
            y=price_fc_q,
            name="price forecast Q-market",
            line=dict(color=COLORS[1], width=LINE_WIDTH),
            legendgroup="price_fc_q",
            showlegend=show_legend,
        ),
        row=1,
        col=column,
    )

    fig.add_trace(
        go.Bar(
            x=trader_df_df_hourly.index,
            y=revenue_p,
            name="revenue P-market",
            showlegend=show_legend,
            legendgroup="revenue_p",
            # legendgroup='finc q_award_amount_mvar',
            marker_color="#d8a909",  # yellow
        ),
        row=2,
        col=column,
    )
    fig.add_trace(
        go.Bar(
            x=trader_df_df_hourly.index,
            y=revenue_q,
            name="revenue Q-market",
            showlegend=show_legend,
            legendgroup="revenue_q",
            marker_color="#d67410",  # orange
        ),
        row=2,
        col=column,
    )

    if bat_included:
        bat_df = dfs[f"batteries{suffix}"]
        bat_soc = bat_df[bat_df["src_id"] == trader_setup["battery_asset"]].set_index(
            "time"
        )["soc_percent"]
        fig.add_trace(
            go.Scatter(
                x=time,
                y=bat_soc,
                name="SOC",
                line=dict(color="#513FD7", width=LINE_WIDTH),
                showlegend=show_legend,
                legendgroup="soc",
            ),
            row=3,
            col=column,
        )

    fig.add_trace(
        go.Scatter(
            x=time,
            y=pv_gen,
            name="PV generation",
            line=dict(color=COLORS[2], width=LINE_WIDTH),
            legendgroup="pv_gen",
            showlegend=show_legend,
        ),
        row=p_market_row,
        col=column,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=wind_gen,
            name="Wind generation",
            line=dict(color=COLORS[2], width=LINE_WIDTH),
            legendgroup="wind_gen",
            showlegend=show_legend,
        ),
        row=p_market_row,
        col=column,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=pv_gen_forecast_mw,
            name="PV generation forecast",
            line=dict(color=COLORS[3], width=LINE_WIDTH),
            legendgroup="pv_fc",
            showlegend=show_legend,
        ),
        row=p_market_row,
        col=column,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=wind_gen_forecast_mw,
            name="Wind generation forecast",
            line=dict(color=COLORS[3], width=LINE_WIDTH),
            legendgroup="wind_fc",
            showlegend=show_legend,
        ),
        row=p_market_row,
        col=column,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=p_res,
            name="schedule for RES",
            showlegend=show_legend,
            legendgroup="p_res",
            line=dict(
                color="#ce2480",  # pink
                width=LINE_WIDTH,
            ),
        ),
        row=p_market_row,
        col=column,
    )

    if bat_included:
        fig.add_trace(
            go.Scatter(
                x=time,
                y=p_bat,
                name="battery power",
                line=dict(color="#E39183", width=LINE_WIDTH),
                showlegend=show_legend,
                legendgroup="p_bat",
            ),
            row=p_market_row,
            col=column,
        )
        fig.add_trace(
            go.Scatter(
                x=time,
                y=p_inv,
                name="traded power",
                line=dict(color="#a64027", width=LINE_WIDTH),
                showlegend=show_legend,
                legendgroup="p_inv",
            ),
            row=p_market_row,
            col=column,
        )

    fig.add_trace(
        go.Bar(
            x=time,
            y=bids_p,
            name="bid amount P-market",
            marker=dict(color="#003A6F"),
            legendgroup="bids_p",
            showlegend=show_legend,
        ),
        row=p_market_row,
        col=column,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=q_free_amount,
            name="Free amount of Q",
            line=dict(color=COLORS[5], width=LINE_WIDTH),
            legendgroup="q_free",
            showlegend=show_legend,
        ),
        row=q_market_row,
        col=column,
    )

    fig.add_trace(
        go.Bar(
            x=time,
            y=bids_q,
            name="bid amount Q-market",
            marker=dict(color="#6EC5E9"),
            legendgroup="bids_q",
            showlegend=show_legend,
        ),
        row=q_market_row,
        col=column,
    )


if __name__ == "__main__":
    # set path to results files
    file_path = "results/"
    # setup1 = {
    #     "mode": "SEQUENTIAL",  # "SEQUENTIAL",
    #     "clearing": "pay_as_cleared",  # "pay_as_bid",  #
    #     "noise": "0.01",
    #     "trader_setup": "bat_test_4",
    #     "num_traders": 5
    # }
    setup2 = None
    setup1 = {
        "mode": "SEQUENTIAL",  # "SEQUENTIAL",
        "clearing": "pay_as_bid",  # "pay_as_bid",  #
        "noise": "0.1",
        "trader_setup": "many_large",
        "num_traders": 20,
    }
    # setup2 = {
    #     "mode": "SEQUENTIAL",  # "SEQUENTIAL", SIMULTANEOUS
    #     "clearing": "pay_as_cleared",
    #     "noise": "0.1",
    #     "trader_setup": "many_small",
    #     "num_traders": 42
    # }

    # clearing = "pay_as_bid"

    a_types = ["Wind", "PV", "Bat"]
    compare_results(file_path, a_types, setup1, m_setup2=setup2)

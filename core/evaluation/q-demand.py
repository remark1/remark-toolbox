import pandas as pd
import plotly.graph_objects as go


def show_q_demand(f_path, f_name, start_date=None):
    dfs = pd.read_excel(f_path + f_name, sheet_name=None)
    bid_df = dfs['bids']
    # bid_df['time'] = pd.to_datetime(bid_df['time'])
    bids = bid_df.groupby('time')['q_free_amount_mvar'].sum().reset_index()
    bids.set_index('time', inplace=True)
    q_free = bids['q_free_amount_mvar']
    grid_df = dfs['grid_operator'].set_index('time')
    q_demand = grid_df['q_demand_mvar']

    fig = go.Figure()
    fig.add_trace(go.Scatter(y=q_free, x=q_free.index,
                             mode='lines',  line=dict(color='#2E9AFE', width=4),
                             name='q_free'))
    fig.add_trace(go.Scatter(y=q_demand, x=q_demand.index,
                             mode='lines', line=dict(color='#FE9A2E', width=4),
                             name='q_demand'))
    font_size = 20
    x_font_size = 11
    title = f"Q-Free-Amount vs. Q-Demand"
    fig.update_layout(
        margin=dict(t=160),
        template="seaborn",
        title=dict(
            text=title,
            font_size=font_size + 3,
            y=0.93,
            x=0.5,
            xanchor='center',
            yanchor='top',
        ),
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=-0.1,
            # y=1.1,
            xanchor="center",
            x=0.5,
            font={'size': font_size}
        ),
    )
    fig.update_annotations(font_size=font_size)
    fig.show()


# set path to results files
file_path = "../../core/results/"
num_agents = 42
# mode = "NAIVE"
# mode = "SEQUENTIAL"
# clearing = "pay_as_cleared"
# noise = "0.01"
mode = "SIMULTANEOUS"
clearing = "pay_as_bid"
noise = "0.1"
trader_setup = "many_small"

file_name = f"{mode}--{clearing}--{num_agents}-agents--noise-{noise}__{trader_setup}.xlsx"
st_date = "2023-07-02 00:00:00"  # optional
show_q_demand(file_path, file_name, st_date)

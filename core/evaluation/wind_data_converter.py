import pandas as pd

file_path = "C:/Users/sholly/Downloads/stundenwerte_FF_01993_19761201_20231231_hist/"
file_name = "produkt_ff_stunde_19761201_20231231_01993.txt"

# Load the CSV file and specify the delimiter
df = pd.read_csv(file_path + file_name, delimiter=';', skipinitialspace=True)

# Parse the 'MESS_DATUM' column as a datetime object
# Format: YYYYMMDDHH, so we use the format '%Y%m%d%H'
df['MESS_DATUM'] = pd.to_datetime(df['MESS_DATUM'], format='%Y%m%d%H')
df_2023 = df[df['MESS_DATUM'].dt.year == 2023]
# Select the required columns and rename them
df_2023_filtered = df_2023[['MESS_DATUM', 'F']].rename(columns={'MESS_DATUM': 'Date', 'F': 'wind_speed'})

# Create a new dataframe where each row is duplicated
df_duplicated = pd.concat([df_2023_filtered, df_2023_filtered.copy()], ignore_index=True)

# Sort by date to maintain the original order
df_duplicated = df_duplicated.sort_values(by='Date').reset_index(drop=True)

# Add 30 minutes to every other row
df_duplicated.loc[1::2, 'Date'] = df_duplicated.loc[1::2, 'Date'] + pd.Timedelta(minutes=30)

# Save the new data to a CSV file
df_duplicated.to_csv('wind_data_2023_30min_hameln.csv', index=False)





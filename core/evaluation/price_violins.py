import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def show_price_distributions(f_path, f_name):
    dfs = pd.read_excel(f_path + f_name, sheet_name=None)
    bid_df = dfs['bids']
    bid_df_baseline = dfs['bids_baseline']

    bid_df['time'] = pd.to_datetime(bid_df['time'])
    bid_df_baseline['time'] = pd.to_datetime(bid_df_baseline['time'])

    fig = make_subplots(rows=1, cols=2, shared_yaxes=False,
                        subplot_titles=("Preise P-Markt", "Preise Q-Markt"),
                        column_widths=[0.7, 0.3])

    # Violin-Plots für P-Markt (links)
    fig.add_trace(go.Violin(
        y=bid_df['p_award_price_eur_mwh'],
        name="Awarded Prices P-Market",
        legendgroup="p_market",
        box_visible=True,  #
        meanline_visible=True,
        spanmode="hard"
    ), row=1, col=1)

    fig.add_trace(go.Violin(
        y=bid_df_baseline['p_award_price_eur_mwh'],
        name="Awarded Prices Baseline P-Market",
        legendgroup="p_market_baseline",
        box_visible=True,
        meanline_visible=True,
        spanmode="hard"
    ), row=1, col=1)

    # Violin-Plot für Q-Markt (rechts)
    fig.add_trace(go.Violin(
        y=bid_df['q_award_price_eur_mwh'],
        name="Awarded Prices Q-Market",
        legendgroup="q_market",
        box_visible=True,
        meanline_visible=True,
        spanmode="hard"
    ), row=1, col=2)

    # Layout anpassen
    fig.update_layout(
        template="seaborn",
        title="Preisübersicht für P- und Q-Markt",
        yaxis1_title="Preis (EUR/MWh)",
        yaxis2_title="Preis (EUR/MVarh)",
        violinmode='group'  # Gruppierung der Violin-Plots nebeneinander
    )

    fig.show()


if __name__ == "__main__":
    # set path to results files
    file_path = "../../core/results/"
    num_agents = 42
    # mode = "SIMULTANEOUS"
    mode = "SEQUENTIAL"
    clearing = "pay_as_bid"
    # clearing = "pay_as_cleared"
    noise = "0.1"
    trader_setup = "many_small"

    file_name = f"{mode}--{clearing}--{num_agents}-agents--noise-{noise}__{trader_setup}.xlsx"
    show_price_distributions(file_path, file_name)

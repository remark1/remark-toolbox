import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

COLORS = [
    '#6EC5E9',  # light blue
    '#F6A03D',  # orange
    '#6E0592',  # purple
    '#099b9c',  # turquase
    '#9f9e9e',  # grey
    # '#022336',  # black
    # '#149406',  # green
    # '#ce2480', # pink
    '#F4733D',  # dark orange

    '#003A6F',  # dark blue
    '#E39183',
    '#a64027',  # redbrown
    "#d8a909",  # yellow
    "#25A5D0",  # blue
    "#d67410",  # orange
    "#ce2480",  # pink
    "#099b9c",  # green
    "#9f9e9e",  # grey
    '#513FD7'
]
LINE_WIDTH = 3


def show_trader_results(f_path, f_name, asset_types, start_date=None):
    dfs = pd.read_excel(f_path + f_name, sheet_name=None)
    agen_associations = dfs['agent_associations']
    traders = agen_associations['trader']
    filtered_traders = traders[traders.str.contains('|'.join(asset_types), case=False, na=False)]
    for i, trader in enumerate(filtered_traders):
        trader_row = agen_associations[agen_associations['trader'] == trader]
        trader_setup = {
            'trader': trader,
            'pv_agent': trader_row['pv_agent'].values[0],
            'wind_agent': trader_row['wind_agent'].values[0],
            'battery_agent': trader_row['battery_agent'].values[0],
            'pv_asset': trader_row['pv_asset'].values[0],
            'wind_asset': trader_row['wind_asset'].values[0],
            'battery_asset': trader_row['battery_asset'].values[0]
        }
        show_single_trader(dfs, trader_setup, start_date)
        # set limit of traders to be displayed
        # if i > 5:
        #     break


def show_single_trader(dfs, trader_setup, start_date):
    bid_df = dfs['bids']
    gen_forecast_df = dfs['gen_forecast']
    gen_df = dfs['gens']
    if start_date is not None:
        bid_df = bid_df.loc[bid_df['time'] >= start_date]
        gen_forecast_df = gen_forecast_df.loc[gen_forecast_df['time'] >= start_date]
        gen_df = gen_df.loc[gen_df['time'] >= start_date]
        # trader_df = trader_df.loc[start_date: trader_df.index.max()]
        # trader_df_df_hourly = trader_df_df_hourly.loc[start_date: trader_df_df_hourly.index.max()]
    trader = trader_setup['trader']
    trader_df = bid_df[bid_df['src_id'] == trader].set_index('time')
    trader_df_df_hourly = trader_df.resample('h').first()
    revenue_p = trader_df_df_hourly.p_award_amount_mw * trader_df_df_hourly.p_award_price_eur_mwh
    revenue_q = trader_df_df_hourly.q_award_amount_mvar * trader_df_df_hourly.q_award_price_eur_mwh
    sum_p = round(sum(revenue_p), 2)
    sum_q = round(sum(revenue_q), 2)
    rev_title = f'Revenues <br>P-market: {sum_p} € -- Q-market: {sum_q} € -- total: {round(sum_p + sum_q, 2)} €'
    bids_p = trader_df['p_bid_amount_mw']
    bids_q = trader_df['q_bid_amount_mvar']
    price_fc_p = trader_df['p_clear_price_fc_eur_mwh']
    price_fc_q = trader_df['q_clear_price_fc_eur_mvarh']
    q_free_amount = trader_df['q_free_amount_mvar']
    p_bat = trader_df['p_battery_amount_mw']
    wind_gen_forecast_mw = gen_forecast_df[gen_forecast_df['src_id'] == trader_setup['wind_agent']].set_index('time')[
        'p_fc_mw']
    pv_gen_forecast_mw = gen_forecast_df[gen_forecast_df['src_id'] == trader_setup['pv_agent']].set_index('time')[
        'p_fc_mw']
    wind_gen = gen_df[gen_df['src_id'] == trader_setup['wind_asset']].set_index('time')['p_mw']
    pv_gen = gen_df[gen_df['src_id'] == trader_setup['pv_asset']].set_index('time')['p_mw']

    rows = 4
    subplot_titles = ("Price Forecasts", rev_title, "P-Market", "Q-Market")
    row_heights = [0.25, 0.25, 0.25, 0.25]
    p_market_row = 3
    q_market_row = 4
    p_res = trader_df['p_resource_amount_mw']
    p_inv = p_res + p_bat
    bat_included = 'Bat' in trader
    # if not wind_gen_forecast_mw.empty and not (p_bat.empty or (p_bat == 0).all()):
    #     p_inv = wind_gen_forecast_mw.add(p_bat)
    # elif not pv_gen_forecast_mw.empty and not (p_bat.empty or (p_bat == 0).all()):
    #     p_inv = pv_gen_forecast_mw.add(p_bat)
    # else:
    #     bat_included = False
    #     p_inv = pd.Series()
    if bat_included:
        rows = 5
        subplot_titles = ("Price Forecasts", rev_title, "Battery SOC", "P-Market", "Q-Market")
        row_heights = [0.225, 0.225, 0.1, 0.225, 0.225]
        p_market_row = 4
        q_market_row = 5
    time = trader_df.index
    fig = make_subplots(rows=rows, cols=1, subplot_titles=subplot_titles,
                        # shared_xaxes=True,
                        row_heights=row_heights,
                        vertical_spacing=0.1,
                        horizontal_spacing=0.13
                        )

    fig.add_trace(
        go.Scatter(
            x=time,
            y=price_fc_p,
            name='price forecast P-market',
            line=dict(color='#ce2480', width=LINE_WIDTH)
        ), row=1, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=price_fc_q,
            name='price forecast Q-market',
            line=dict(color=COLORS[1], width=LINE_WIDTH)
        ), row=1, col=1)
    fig.add_trace(go.Bar(
        x=trader_df_df_hourly.index,
        y=revenue_p,
        name='revenue P-market',
        # legendgroup='finc q_award_amount_mvar',
        marker_color="#d8a909",  # yellow
    ), row=2, col=1
    )
    fig.add_trace(go.Bar(
        x=trader_df_df_hourly.index,
        y=revenue_q,
        name='revenue Q-market',
        # legendgroup='finc q_award_amount_mvar',
        marker_color="#d67410",  # orange
    ), row=2, col=1
    )
    if bat_included:
        bat_df = dfs['batteries']
        bat_soc = bat_df[bat_df['src_id'] == trader_setup['battery_asset']].set_index('time')['soc_percent']
        fig.add_trace(
            go.Scatter(
                x=time,
                y=bat_soc,
                name='SOC',
                line=dict(color='#513FD7', width=LINE_WIDTH)
            ), row=3, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=pv_gen,
            name='PV generation',
            line=dict(color=COLORS[2], width=LINE_WIDTH)
        ), row=p_market_row, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=wind_gen,
            name='Wind generation',
            line=dict(color=COLORS[2], width=LINE_WIDTH)
        ), row=p_market_row, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=pv_gen_forecast_mw,
            name='PV generation forecast',
            line=dict(color="#d67410",  # orange
                      width=LINE_WIDTH)
        ), row=p_market_row, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=wind_gen_forecast_mw,
            name='Wind generation forecast',
            line=dict(color="#d67410",  # orange
                      width=LINE_WIDTH)
        ), row=p_market_row, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=p_res,
            name='schedule for RES',
            line=dict(color="#ce2480",  # pink
                      width=LINE_WIDTH)
        ), row=p_market_row, col=1)

    if bat_included:
        fig.add_trace(
            go.Scatter(
                x=time,
                y=p_bat,
                name='battery power',
                line=dict(color='#E39183', width=LINE_WIDTH)
            ), row=p_market_row, col=1)
        fig.add_trace(
            go.Scatter(
                x=time,
                y=p_inv,
                name='traded power',
                line=dict(color="#099b9c",  # green
                          width=LINE_WIDTH)
            ), row=p_market_row, col=1)

    fig.add_trace(
        go.Bar(
            x=time,
            y=bids_p,
            name='bid amount P-market',
            marker=dict(color=COLORS[4]),
        ), row=p_market_row, col=1)
    fig.add_trace(
        go.Scatter(
            x=time,
            y=q_free_amount,
            name='Free amount of Q',
            line=dict(color=COLORS[5], width=LINE_WIDTH)
        ), row=q_market_row, col=1)

    fig.add_trace(
        go.Bar(
            x=time,
            y=bids_q,
            name='bid amount Q-market',
            marker=dict(color='#6EC5E9'),
        ), row=q_market_row, col=1)

    font_size = 14
    x_font_size = 11
    title = f"{mode} markets - {clearing} - {num_agents} - noise {noise} <br> {trader}"

    fig.update_layout(
        margin=dict(t=160),
        template="seaborn",
        title=dict(
            text=title,
            font_size=font_size + 3,
            y=0.93,
            x=0.5,
            xanchor='center',
            yanchor='top',
        ),
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=-0.1,
            # y=1.1,
            xanchor="center",
            x=0.5,
            font={'size': font_size}
        ),
        yaxis=dict(tickfont=dict(size=font_size),
                   title=dict(font_size=font_size, text="€/MW - €/MVar")
                   ),
        yaxis2=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="€")),
        xaxis=dict(tickfont=dict(size=x_font_size),  # tickangle=-45
                   ),
        xaxis2=dict(tickfont=dict(size=x_font_size),  # tickangle=-45
                    ),
        xaxis3=dict(tickfont=dict(size=x_font_size),  # tickangle=-45
                    ),
        xaxis4=dict(tickfont=dict(size=x_font_size),  # tickangle=-45
                    ),
    )
    if bat_included:
        fig.update_layout(
            yaxis3=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="SOC %")),
            yaxis4=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MW")),
            yaxis5=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MVar")),
        )
    else:
        fig.update_layout(
            yaxis3=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MW")),
            yaxis4=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MVar")),
        )
    fig.update_annotations(font_size=font_size)

    fig.show()


if __name__ == "__main__":
    # set path to results files
    file_path = "../../core/results/"
    num_agents = 40
    # mode = "SIMULTANEOUS"
    # mode = "SEQUENTIAL"
    mode = "NAIVE"
    clearing = "pay_as_cleared"
    # clearing = "pay_as_bid"
    noise = "0.01"
    # mode = "NAIVE"
    a_types = [
        # 'Wind',
        # 'PV',
         'Bat'
    ]
    trader_setup = 'many_small'
    st_date = "2023-07-02 00:00:00"  # optional
    file_name = f"{mode}--{clearing}--{num_agents}-agents--noise-{noise}__{trader_setup}.xlsx"
    show_trader_results(file_path, file_name, a_types, st_date)

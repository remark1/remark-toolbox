import pandas as pd


def get_dfs_and_agent_associations(f_path, asset_types, setup, limit=5, file_name=None):
    if file_name is None:
        file_name = f"{setup['mode']}--{setup['clearing']}--{setup['num_traders']}-agents--noise-{setup['noise']}__{setup['trader_setup']}.xlsx"
    dfs = pd.read_excel(f_path + file_name, sheet_name=None)
    agent_associations = dfs["agent_associations"]
    traders = agent_associations["trader"]
    if asset_types is not None:
        filtered_traders = traders[
            traders.str.contains("|".join(asset_types), case=False, na=False)
        ]
    else:
        filtered_traders = traders
    if limit is None:
        limit = len(filtered_traders)
    trader_dict = {}
    for i, trader in enumerate(filtered_traders):
        trader_row = agent_associations[agent_associations["trader"] == trader]
        trader_dict[trader] = {
            "trader": trader,
            "pv_agent": trader_row["pv_agent"].values[0],
            "wind_agent": trader_row["wind_agent"].values[0],
            "battery_agent": trader_row["battery_agent"].values[0],
            "pv_asset": trader_row["pv_asset"].values[0],
            "wind_asset": trader_row["wind_asset"].values[0],
            "battery_asset": trader_row["battery_asset"].values[0],
        }
        # set limit of traders to be displayed
        if i >= limit:
            break
    return dfs, trader_dict

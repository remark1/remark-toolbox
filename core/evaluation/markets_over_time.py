import numpy as np
import pandas as pd
from plotly.subplots import make_subplots
import plotly.graph_objs as go
import plotly.colors

LINE_WIDTH = 4
# COLORS = [
#     '#31089d',  # dark blue
#     # '#2a7185',  # blue
#     '#a64027',  # redbrown
#     '#fbdf72',  # light yellow
#     '#60824f',  # dark green
#     '#9cdff0',  # light blue
#     '#022336',  # black
#     '#725ca5',  # blooish purple
#     '#4ed5c7',  # türkis
#     '#f7a67b',  # light orange
#     '#d44865',  # red
#     "#25A5D0",  # blue
#     "#d8a909",  # yellow
#
#     '#b8da8d',  # light green
#     "#9f9e9e",  # grey
#     "#099b9c",  # green
#     "#d67410",  # orange
#     "#ce2480",  # pink
#     "#149406",  # green
#     '#6E0592',  # purple
#     '#2155FB',  # blue
#     '#E39183'
# ]

COLORS = [
    "#1f77b4",  # soft blue
    "#17becf",  # turquoise
    "#2ca02c",  # fresh green
    "#4daf4a",  # medium green
    "#66c2a5",  # teal green
    "#6baed6",  # light blue
    "#3182bd",  # medium blue
    "#31a354",  # vivid green
    "#56b4e9",  # sky blue
    "#2b8cbe",  # ocean blue
    "#41ab5d",  # forest green
    "#99d8c9",  # light turquoise
    "#74c476",  # medium light green
    "#addd8e",  # soft light green
    "#08519c",  # deep blue
    "#4292c6",  # muted blue
    "#9ecae1",  # pale blue
    "#005a32",  # dark green
    "#0072b2",  # rich blue
    "#00441b",  # very dark green
]


def show_market_outcome(f_path, f_name, start_date=None):
    dfs = pd.read_excel(f_path + f_name, sheet_name=None)
    (
        bid_df,
        market_df,
        grid_df_hourly,
        finc_df_hourly,
        prices_p,
        clearing_pr_q,
        efficiency_pr_q,
        traders,
        q_price_min,
        q_price_max,
    ) = prepare_data(start_date, dfs)

    #
    # colorscale = plotly.colors.sequential.Burg
    colorscale = plotly.colors.sequential.Darkmint
    colors = plotly.colors.sample_colorscale(colorscale, len(traders))

    rows = 6
    subplot_titles = (
        "P-Market Prices",
        "P-Market bid amounts",
        "P-market awarded amounts",
        "Q-Market Prices",
        "Q-Market bid amounts",
        "Q-Market awarded amounts",
    )
    time = market_df.index
    fig = make_subplots(
        rows=rows,
        cols=1,
        subplot_titles=subplot_titles,
        # shared_xaxes=True,
        # row_heights=row_heights,
        vertical_spacing=0.1,
        horizontal_spacing=0.13,
    )

    fig.add_trace(
        go.Scatter(
            x=time,
            y=prices_p,
            name="Clearing Prices P-market",
            line=dict(color="#ce2480", width=LINE_WIDTH),
        ),
        row=1,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=clearing_pr_q,
            name="Clearing Prices Q-market",
            line=dict(color="#ce2480", width=LINE_WIDTH),
        ),
        row=4,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=time,
            y=efficiency_pr_q,
            name="Efficiency Prices Q-market",
            line=dict(
                color="#F4733D",  # dark orange
                width=LINE_WIDTH,
            ),
        ),
        row=4,
        col=1,
    )

    for i, trader in enumerate(traders):
        if i == 0:
            show_legend = True
        else:
            show_legend = False
        short_trader_name = trader.replace("Agents.", "").split("-at")[0]
        t_bids = bid_df[bid_df["src_id"] == trader].set_index("time")
        t_bids_hourly = t_bids.resample("h").first()
        fig.add_trace(
            go.Bar(
                x=t_bids_hourly.index,
                # y=t_bids['p_bid_amount_mw'],
                y=t_bids_hourly["p_bid_amount_mw"],
                name="p_bid_amount_mw",
                legendgroup="p_bid_amount_mw",
                showlegend=show_legend,
                marker_color=colors[i],
                hovertext=short_trader_name,
            ),
            row=2,
            col=1,
        )
        fig.add_trace(
            go.Bar(
                x=t_bids_hourly.index,
                y=t_bids_hourly["p_award_amount_mw"],
                name="p_award_amount_mw",
                legendgroup="p_award_amount_mw",
                showlegend=show_legend,
                marker_color=colors[i],
                hovertext=short_trader_name,
            ),
            row=3,
            col=1,
        )
        fig.add_trace(
            go.Bar(
                x=t_bids_hourly.index,
                y=t_bids_hourly["q_bid_amount_mvar"],
                name="q_bid_amount_mvar",
                legendgroup="q_bid_amount_mvar",
                showlegend=show_legend,
                marker_color=colors[i],
                hovertext=short_trader_name,
            ),
            row=5,
            col=1,
        )
        fig.add_trace(
            go.Bar(
                x=t_bids_hourly.index,
                y=t_bids_hourly["q_award_amount_mvar"],
                name="q_award_amount_mvar",
                legendgroup="q_award_amount_mvar",
                showlegend=show_legend,
                marker_color=colors[i],
                hovertext=short_trader_name,
            ),
            row=6,
            col=1,
        )

    fig.add_trace(
        go.Bar(
            x=finc_df_hourly.index,
            y=finc_df_hourly["q_award_amount_mvar"],
            name="finc q_award_amount_mvar",
            legendgroup="finc q_award_amount_mvar",
            marker_color="#d8a909",  # yellow
        ),
        row=6,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=grid_df_hourly.index,
            y=grid_df_hourly["q_demand_mvar"],
            name="Q-demand",
            line=dict(
                color="#F4733D",  # dark orange
                width=LINE_WIDTH,
            ),
        ),
        row=6,
        col=1,
    )

    font_size = 14
    x_font_size = 11
    title = f"{mode} markets - {clearing} - {num_agents} - noise {noise}"
    fig.update_layout(
        margin=dict(t=160),
        template="seaborn",
        title=dict(
            text=title,
            font_size=font_size + 3,
            y=0.93,
            x=0.5,
            xanchor="center",
            yanchor="top",
        ),
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=-0.1,
            # y=1.1,
            xanchor="center",
            x=0.5,
            font={"size": font_size},
        ),
        yaxis=dict(
            tickfont=dict(size=font_size), title=dict(font_size=font_size, text="€/MW")
        ),
        yaxis2=dict(
            tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MW")
        ),
        yaxis3=dict(
            tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MW")
        ),
        yaxis4=dict(
            tickfont=dict(size=font_size),
            title=dict(font_size=font_size, text="€/MVar"),
            range=[q_price_min, q_price_max],
        ),
        yaxis5=dict(
            tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MVar")
        ),
        yaxis6=dict(
            tickfont=dict(size=font_size), title=dict(font_size=font_size, text="MVar")
        ),
        xaxis=dict(
            tickfont=dict(size=x_font_size),  # tickangle=-45
        ),
        xaxis2=dict(
            tickfont=dict(size=x_font_size),  # tickangle=-45
        ),
        xaxis3=dict(
            tickfont=dict(size=x_font_size),  # tickangle=-45
        ),
        xaxis4=dict(
            tickfont=dict(size=x_font_size),  # tickangle=-45
        ),
    )
    fig.update_layout(barmode="stack")
    fig.update_annotations(font_size=font_size)

    fig.show()


def prepare_data(start_date, dfs):
    bid_df = dfs["bids"]
    bid_df["time"] = pd.to_datetime(bid_df["time"])
    market_df = dfs["market"].set_index("time")
    grid_df = dfs["grid_operator"].set_index("time")
    finc_df = dfs["finc"].set_index("time")

    if start_date is not None:
        bid_df = bid_df.loc[bid_df["time"] >= start_date]
        market_df = market_df.loc[start_date : market_df.index.max()]
        grid_df = grid_df.loc[start_date : grid_df.index.max()]
        finc_df = finc_df.loc[start_date : finc_df.index.max()]
    grid_df_hourly = grid_df.resample("h").first()
    finc_df_hourly = finc_df.resample("h").first()
    prices_p = market_df["p_price_eur_mwh"]
    clearing_pr_q = market_df["q_clear_price_eur_mvarh"].ffill()
    efficiency_pr_q = market_df["q_eff_price_eur_mvarh"].ffill()
    agent_associations = dfs["agent_associations"]
    traders = agent_associations["trader"]
    q_price_min = (
        min(
            min(market_df["q_eff_price_eur_mvarh"]),
            min(market_df["q_clear_price_eur_mvarh"]),
        )
        - 2
    )
    q_price_max = (
        max(
            max(market_df["q_eff_price_eur_mvarh"]),
            max(market_df["q_clear_price_eur_mvarh"]),
        )
        + 2
    )
    return (
        bid_df,
        market_df,
        grid_df_hourly,
        finc_df_hourly,
        prices_p,
        clearing_pr_q,
        efficiency_pr_q,
        traders,
        q_price_min,
        q_price_max,
    )


if __name__ == "__main__":
    # set path to results files
    file_path = "./"
    num_agents = 42
    # mode = "NAIVE"
    # mode = "SEQUENTIAL"
    # clearing = "pay_as_cleared"
    # noise = "0.01"
    mode = "SIMULTANEOUS"
    clearing = "pay_as_bid"
    noise = "0.1"
    trader_setup = "many_small"

    file_name = (
        "suite_b9e959795b70487dbfff1671b17af327.xlsx"
    )
    st_date = "2023-07-02 00:00:00"  # optional
    show_market_outcome(file_path, file_name, st_date)

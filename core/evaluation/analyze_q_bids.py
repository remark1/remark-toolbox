import plotly
from plotly.subplots import make_subplots
import plotly.graph_objs as go

from eval_util import get_dfs_and_agent_associations

COLOR_SCALES = [plotly.colors.sequential.Darkmint,
                plotly.colors.sequential.Cividis_r,
                plotly.colors.sequential.Burg,
                plotly.colors.sequential.Viridis_r,
                ]

# apparent power of current asset setups - needs to be updated if this setting is change in the simulation
ASSET_SIZES = {'Bat_large': 1050, 'Bat_small': 210, 'PV_large': 3080, 'PV_large_Bat': 3080, 'PV_small': 1050,
               'PV_small_Bat': 1050, 'Wind_large': 4210, 'Wind_small': 2100, 'Wind_small_Bat': 2100}


def show_q_bidding(f_path, scenarios, asset_types, detailed=False):
    num_cols = 2
    num_rows = 3 * len(scenarios) + 1
    subplot_titles = ['<b>Low Digitalization</b><br>P-Price Forecasts',
                      '<b>High Digitalization</b><br>P-Price Forecasts']
    for setup in scenarios:
        subplot_titles += [f"{setup["mode"]} - {setup['clearing']} - {setup['trader_setup']}: Q-Price Forecasts" for _
                           in range(num_cols)]
        subplot_titles += [f'{setup["mode"]} - {setup['clearing']} - {setup['trader_setup']}: Q-Bid Amounts' for _ in
                           range(num_cols)]
        subplot_titles += [f'{setup["mode"]} - {setup['clearing']} - {setup['trader_setup']}: P-Bid Amounts' for _ in
                           range(num_cols)]

    fig = make_subplots(rows=num_rows, cols=num_cols,
                        subplot_titles=subplot_titles)
    row_counter = 1
    for i, setup in enumerate(scenarios):
        setup_low_digi = setup.copy()
        setup_low_digi["noise"] = 0.1
        setup_high_digi = setup.copy()
        setup_high_digi["noise"] = 0.01
        dfs_low, tr_dict_low = get_dfs_and_agent_associations(f_path, asset_types, setup_low_digi)
        colorscale = COLOR_SCALES[i]
        colors = plotly.colors.sample_colorscale(colorscale, len(tr_dict_low))
        color_asso, smax_colors = assign_color_by_size(list(tr_dict_low.keys()), colors)
        dfs_high, tr_dict_high = get_dfs_and_agent_associations(f_path, asset_types, setup_high_digi)

        if i == 0:
            add_p_price_fc(fig, dfs_low, tr_dict_low, row=row_counter, col=1, tr_colors=color_asso)
            add_p_price_fc(fig, dfs_high, tr_dict_high, row=row_counter, col=2, tr_colors=color_asso,
                           show_legend=False)
            row_counter += 1
        add_q_price_fc(fig, dfs_low, tr_dict_low, setup["clearing"], row=row_counter, col=1,
                       tr_colors=color_asso)
        add_q_price_fc(fig, dfs_high, tr_dict_high, setup["clearing"], row=row_counter, col=2,
                       tr_colors=color_asso,
                       show_legend=False)
        row_counter += 1
        add_bid_amounts(fig, dfs_low, tr_dict_low, 'q_bid_amount_mvar', row=row_counter, col=1,
                        color_cale=COLOR_SCALES[i], tr_colors=color_asso, detailed=detailed)
        add_bid_amounts(fig, dfs_high, tr_dict_high, 'q_bid_amount_mvar', row=row_counter, col=2,
                        color_cale=COLOR_SCALES[i], tr_colors=color_asso,
                        show_legend=False, detailed=detailed)
        row_counter += 1
        add_bid_amounts(fig, dfs_low, tr_dict_low, 'p_bid_amount_mw', row=row_counter, col=1,
                        color_cale=COLOR_SCALES[i], tr_colors=color_asso, detailed=detailed)
        add_bid_amounts(fig, dfs_high, tr_dict_high, 'p_bid_amount_mw', row=row_counter, col=2,
                        color_cale=COLOR_SCALES[i], tr_colors=color_asso, detailed=detailed,
                        show_legend=False)
        # add_dummy_traces(fig, COLOR_SCALES[i], smax_colors, row=row_counter, col=1)
        row_counter += 1
    font_size = 14
    x_font_size = 11
    title = f"{scenarios}"
    fig.update_layout(
        margin=dict(t=160),
        template="seaborn",
        title=dict(
            text=title,
            font_size=font_size + 3,
            y=0.93,
            x=0.5,
            xanchor='center',
            yanchor='top',
        ),
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=-0.1,
            # y=1.1,
            xanchor="center",
            x=0.5,
            font={'size': font_size}
        ),
        yaxis=dict(tickfont=dict(size=font_size),
                   title=dict(font_size=font_size, text="€/MW")
                   ),
        yaxis2=dict(tickfont=dict(size=font_size), title=dict(font_size=font_size, text="€/MW")),
    )
    fig.update_layout(barmode="stack")
    fig.update_annotations(font_size=font_size)
    # Axis labels and grid styling
    for row, y_title in enumerate(['€/MVar', 'MVar', 'MW'] * len(scenarios)):
        for col in range(1, 3):
            fig.update_yaxes(title_text=y_title, title_font=dict(size=font_size), row=row + 2, col=col)

    fig.update_xaxes(matches=f"x1")
    for i in range(0, num_rows):
        col = i * 2 + 1
        fig.update_yaxes(matches=f"y{col}", row=i + 1)
    # for i in range(1, num_cols + 1):
    #     fig.update_xaxes(matches=f"x{i}", col=i)
    fig.show()


def assign_color_by_size(traders, colors):
    # sizes = {}
    # for asset, tr in ASSET_CONFIGS.items():
    #     for tr_name, t_size in tr.items():
    #         a_type = asset.value
    #         tr_size = tr_name.value
    #         if tr_size == 'big':
    #             tr_size = 'large'
    #         asset_name = None
    #         trader_type = None
    #         if a_type == 'pv':
    #             asset_name = 'PV'
    #         elif a_type == 'wind':
    #             asset_name = 'Wind'
    #         elif a_type == 'battery':
    #             asset_name = 'Bat'
    #         if asset_name is not None:
    #             trader_type = f'{asset_name}_{tr_size}'
    #         elif a_type == 'wind-battery':
    #             trader_type = f'Wind_{tr_size}_Bat'
    #         elif a_type == 'pv-battery':
    #             trader_type = f'PV_{tr_size}_Bat'
    #         if trader_type is not None:
    #             sizes[trader_type] = t_size.inverter_S_max_kVA
    # print(sizes)
    sizes = ASSET_SIZES
    trader_dict = {}
    for trader in traders:
        # Extract the type key from the entry (e.g., "PV_small", "PV_small_Bat", etc.)
        type_key = trader.split('-for-')[1].split('-at-')[0]
        # Extract the node value from the entry
        node_value = int(trader.split('-at-')[1])

        # Assign the s_max value based on the type_key, with 0 as default if type_key is not found
        s_max_value = sizes.get(type_key, 0)
        trader_dict[trader] = {'s_max': s_max_value, 'node': node_value}
    sorted_trader_dict = dict(sorted(trader_dict.items(), key=lambda item: (item[1]['s_max'], item[1]['node'])))
    tr_colors = {tr: colors[i] for i, tr in enumerate(sorted_trader_dict.keys())}
    s_max_colors = [(tr['s_max'], colors[i]) for i, tr in enumerate(sorted_trader_dict.values())]
    return tr_colors, s_max_colors


def add_dummy_traces(fig, color_scale, smax_colors, row, col):
    fig.add_trace(
        go.Heatmap(
            z=[[0, 1]],  # Dummy-Daten für den Colorbar
            colorscale=color_scale,
            showscale=True,
            colorbar=dict(
                title="s_max",
                titleside="right",
                tickvals=[smax for smax, c in smax_colors],
                ticktext=[str(v) for v in smax_colors],
                len=0.5,  # Länge des Colorbars anpassen
                x=1.1  # Position des Colorbars rechts vom Plot
            )
        ),
        row=row, col=col
    )


def add_bid_amounts(fig, dfs, tr_dict, trace_name, row, col, tr_colors, color_cale, show_legend=True, detailed=True):
    bid_df = dfs['bids']
    if detailed:
        for tr_name, tr_setup in tr_dict.items():
            trader_df = bid_df[bid_df['src_id'] == tr_name].set_index('time')
            q_bid_am = trader_df[trace_name]
            # fig.add_trace(go.Bar(
            #     x=trader_df.index,
            #     y=q_bid_am,
            #     name='q_bid_amount_mvar',
            #     legendgroup='q_bid_amount_mw',
            #     showlegend=show_legend,
            #     marker_color=tr_colors[tr_name],
            #     # hovertext=short_trader_name
            # ), row=row, col=col
            # )
            fig.add_trace(go.Scatter(
                x=trader_df.index, y=q_bid_am,
                mode='lines',
                name=trace_name,
                legendgroup=trace_name,
                showlegend=show_legend,
                line_color=tr_colors[tr_name],
                stackgroup='one'
                # hovertext=short_trader_name
            ), row=row, col=col
            )
            show_legend = False
    else:
        bids = bid_df.groupby('time')[trace_name].sum().reset_index()
        bids.set_index('time', inplace=True)
        fig.add_trace(go.Bar(
            x=bids.index,
            y=bids[trace_name],
            name=trace_name,
            legendgroup=trace_name,
            showlegend=show_legend,
            marker_color=color_cale[4],
            # hovertext=short_trader_name
        ), row=row, col=col
        )


def add_q_price_fc(fig, dfs, tr_dict, clearing, row, col, tr_colors, show_legend=True):
    bid_df = dfs['bids']
    for tr_name, tr_setup in tr_dict.items():
        trader_df = bid_df[bid_df['src_id'] == tr_name].set_index('time')
        if clearing == "pay_as_cleared":
            price_fc = trader_df["q_clear_price_fc_eur_mvarh"]
            price_name = "Q Clearing Price Forecast"
        else:
            price_fc = trader_df["q_rel_price_fc_eur_mvarh"]
            price_name = "Q Reference Price Forecast"
        fig.add_trace(
            go.Scatter(
                x=trader_df.index,
                y=price_fc,
                name=price_name,
                line=dict(color=tr_colors[tr_name]),
                legendgroup="price_fc_p",
                showlegend=show_legend
            ), row=row, col=col)
        show_legend = False


def add_p_price_fc(fig, dfs, tr_dict, row, col, tr_colors, show_legend=True):
    bid_df = dfs['bids']
    for tr_name, tr_setup in tr_dict.items():
        trader_df = bid_df[bid_df['src_id'] == tr_name].set_index('time')
        price_fc = trader_df["p_clear_price_fc_eur_mwh"]
        fig.add_trace(
            go.Scatter(
                x=trader_df.index,
                y=price_fc,
                name="P Price Forecast",
                line=dict(color=tr_colors[tr_name]),
                legendgroup="price_fc_p",
                showlegend=show_legend
            ), row=row, col=col)
        show_legend = False


if __name__ == "__main__":
    # set path to results files
    file_path = "../../core/results/"
    separate_traders = True  # plot traces of traders individually
    setups = [
        {
            "mode": "SIMULTANEOUS",  # "SEQUENTIAL",
            "clearing": "pay_as_bid",  # "pay_as_bid",  #
            "trader_setup": "many_small",
            "num_traders": 42
        },
        # {
        #     "mode": "SEQUENTIAL",  # "SEQUENTIAL",
        #     "clearing": "pay_as_bid",  # "pay_as_bid",  #
        #     "trader_setup": "many_small",
        #     "num_traders": 42
        # }
    ]
    a_types = [
        'Wind',
        'PV',
        'Bat'
    ]
    show_q_bidding(file_path, setups, a_types, separate_traders)

"""Test changes of different parameters using sensitivity compared to actual changes in those parameters"""

# import used modules and sensitivity.py
from typing import Literal

import pandapower as pp
import pandapower.networks as nw
import pandas as pd
import pytest
import simbench
import util.sensitivity as sensitivity

# define number of external grids (from 1 to 3), change of parameters p_mw or q_mvar (delta_val) tolerance
n_ext_grid = 1
delta_val = -0.1
tolerance = 0.01


# function that creates pandapower initial network
def SimNetwork(n_ext_grid=n_ext_grid):
    case = nw.case39()
    if n_ext_grid > 3 or n_ext_grid < 1:
        raise Exception(
            "The maximal amount of ext_grids is limited to 3 and the minimal amount is set to 1"
        )

    for k in range(n_ext_grid - 1):
        pp.create_ext_grid(case, k)

    pp.runpp(case)

    return case


def run_comparison(
    net: pp.pandapowerNet,
    elem: Literal["load", "sgen", "gen"],
    elem_id: int,
    power_type: Literal["p_mw", "q_mvar"],
    delta: float,
):
    """For the given grid, run the power flow before and after
    adjusting the given element's specified value by ``delta``.
    Return data frames given the actual and sensitivity-predicted
    changes in the Vm values at buses and the Q value at the external
    grid.
    """
    pp.runpp(net)
    pre_bus_vm_pu = net.res_bus["vm_pu"]
    pre_ext_grid_q_mvar = net.res_ext_grid["q_mvar"]
    sens_bus = sensitivity.of_bus_vm_pu_on_element(net, elem, power_type).loc[
        :, elem_id
    ]
    sens_ext_grid = sensitivity.of_ext_grid_q_mvar_on_element(
        net, elem, power_type
    ).loc[:, elem_id]

    net[elem].loc[elem_id, power_type] += delta
    pp.runpp(net)

    post_bus_vm_pu = net.res_bus["vm_pu"]
    post_ext_grid_q_mvar = net.res_ext_grid["q_mvar"]

    real_bus_diff = post_bus_vm_pu - pre_bus_vm_pu
    sens_bus_diff = sens_bus * delta
    real_ext_grid_diff = post_ext_grid_q_mvar - pre_ext_grid_q_mvar
    sens_ext_grid_diff = sens_ext_grid * delta

    bus_df = pd.DataFrame(
        {
            "pre": pre_bus_vm_pu,
            "sens": sens_bus,
            "post": post_bus_vm_pu,
            "real_diff": real_bus_diff,
            "sens_diff": sens_bus_diff,
        }
    )
    ext_grid_df = pd.DataFrame(
        {
            "pre": pre_ext_grid_q_mvar,
            "sens": sens_ext_grid,
            "post": post_ext_grid_q_mvar,
            "real_diff": real_ext_grid_diff,
            "sens_diff": sens_ext_grid_diff,
        }
    )
    return bus_df, ext_grid_df


def test_comparison():
    net: pp.pandapowerNet = nw.case39()  # type: ignore
    net = simbench.get_simbench_net("1-MVLV-semiurb-3.202-1-sw")
    pp.create_sgen(net, net.load.loc[0, "bus"], 0, 0)
    bus_df, ext_grid_df = run_comparison(net, "sgen", 0, "q_mvar", 0.1)
    print(bus_df)
    print(ext_grid_df)
    assert False


# function that gives relative error between sensitivity and actual changes caused by adjusting p of gens
def compare_changes_p_of_gens(delta_val, adjusted_gen):
    # gives network with wanted parameters
    net = SimNetwork()

    # define old network values and external grid values as well as sensitivities and change p_mw parameter
    old_res = net.res_bus
    old_ext_grid = net.res_ext_grid
    sens_vm_bus = sensitivity.of_bus_vm_pu_on_gen_p_mw(net)
    sens_q_ext_grid = sensitivity.of_ext_grid_q_mvar_on_gen_p_mw(net)
    net.gen.loc[adjusted_gen, "p_mw"] += delta_val
    pp.runpp(net)

    # define new network values after change and compare with vm sensitivities and remove all 0 values
    new_res = net.res_bus
    new_ext_grid = net.res_ext_grid
    vm_diff = new_res["vm_pu"] - old_res["vm_pu"]
    vm_diff = vm_diff[abs(vm_diff) >= 1e-14]
    sens_vm_diff = sens_vm_bus.loc[:, adjusted_gen] * delta_val
    sens_vm_diff = sens_vm_diff[abs(sens_vm_diff) >= 1e-14]
    vm_rel_err = abs((sens_vm_diff - vm_diff) / vm_diff)

    # compare with external grid
    diff_list = []
    # loop gives relative error for each external grid for the adjusted load
    for k in range(len(net.ext_grid)):
        ext_grid_sens_diff = (
            sens_q_ext_grid.loc[adjusted_gen, net.ext_grid.bus[k]] * delta_val
        )
        ext_grid_sens_diff = ext_grid_sens_diff[abs(ext_grid_sens_diff) >= 1e-14]
        ext_grid_q_diff = new_ext_grid.loc[k, "q_mvar"] - old_ext_grid.loc[k, "q_mvar"]
        ext_grid_q_diff = ext_grid_q_diff[abs(ext_grid_q_diff) >= 1e-14]
        diff_entry = abs((ext_grid_sens_diff - ext_grid_q_diff) / ext_grid_q_diff)
        diff_list.append(diff_entry)
    ext_grid_diff = pd.Series(diff_list).set_axis(net.ext_grid.bus)
    diff = pd.concat([vm_rel_err, ext_grid_diff], axis=0)
    diff = diff.reset_index()
    return diff


def compare_changes_q_of_loads(delta_val, adjusted_load):
    # gives network with wanted parameters
    net = SimNetwork()

    # define old network values and external grid values as well as sensitivities and change q_mvar parameter
    old_res = net.res_bus
    old_ext_grid = net.res_ext_grid
    sens_vm_bus = sensitivity.of_bus_vm_pu_on_load_q_mvar(net)
    sens_q_ext_grid = sensitivity.of_ext_grid_q_mvar_on_load_q_mvar(net)
    net.load.loc[adjusted_load, "q_mvar"] += delta_val
    pp.runpp(net)

    # define new network values after change and compare with vm sensitivities
    new_res = net.res_bus
    new_ext_grid = net.res_ext_grid
    vm_diff = new_res["vm_pu"] - old_res["vm_pu"]
    vm_diff = vm_diff[abs(vm_diff) >= 1e-14]
    sens_vm_diff = sens_vm_bus.loc[:, adjusted_load] * delta_val
    sens_vm_diff = sens_vm_diff[abs(sens_vm_diff) >= 1e-14]
    vm_rel_err = abs((sens_vm_diff - vm_diff) / vm_diff)

    # compare with external grid

    # create list to which each relative error value is appended later on
    diff_list = []
    # loop gives relative error for each external grid for the adjusted load
    for k in range(len(net.ext_grid)):
        ext_grid_sens_diff = (
            sens_q_ext_grid.loc[adjusted_load, net.ext_grid.bus[k]] * delta_val
        )
        ext_grid_sens_diff = ext_grid_sens_diff[abs(ext_grid_sens_diff) >= 1e-14]
        ext_grid_q_diff = new_ext_grid.loc[k, "q_mvar"] - old_ext_grid.loc[k, "q_mvar"]
        ext_grid_q_diff = ext_grid_q_diff[abs(ext_grid_q_diff) >= 1e-14]
        diff_entry = abs((ext_grid_sens_diff - ext_grid_q_diff) / ext_grid_q_diff)
        diff_list.append(diff_entry)
    ext_grid_diff = pd.Series(diff_list).set_axis(net.ext_grid.bus)
    diff = pd.concat([vm_rel_err, ext_grid_diff], axis=0)
    diff = diff.reset_index()
    return diff


def compare_changes_p_of_loads(delta_val, adjusted_load):
    # gives network with wanted parameters
    net = SimNetwork()

    # define old network values and external grid values as well as sensitivities and change q_mvar parameter
    old_res = net.res_bus
    old_ext_grid = net.res_ext_grid
    sens_vm_bus = sensitivity.of_bus_vm_pu_on_load_p_mw(net)
    sens_p_ext_grid = sensitivity.of_ext_grid_q_mvar_on_load_p_mw(net)
    net.load.loc[adjusted_load, "p_mw"] += delta_val
    pp.runpp(net)

    # define new network values after change and compare with vm sensitivities
    new_res = net.res_bus
    new_ext_grid = net.res_ext_grid
    vm_diff = new_res["vm_pu"] - old_res["vm_pu"]
    vm_diff = vm_diff[abs(vm_diff) >= 1e-14]
    sens_vm_diff = sens_vm_bus.loc[:, adjusted_load] * delta_val
    sens_vm_diff = sens_vm_diff[abs(sens_vm_diff) >= 1e-14]
    vm_rel_err = abs((sens_vm_diff - vm_diff) / vm_diff)

    # compare with external grid

    # create list to which each relative error of ext_grid sensitivity is appended later on
    diff_list = []
    # loop gives relative error for each external grid for the adjusted load
    for k in range(len(net.ext_grid)):
        ext_grid_sens_diff = (
            sens_p_ext_grid.loc[adjusted_load, net.ext_grid.bus[k]] * delta_val
        )
        ext_grid_sens_diff = ext_grid_sens_diff[abs(ext_grid_sens_diff) >= 1e-14]
        ext_grid_q_diff = new_ext_grid.loc[k, "q_mvar"] - old_ext_grid.loc[k, "q_mvar"]
        ext_grid_q_diff = ext_grid_q_diff[abs(ext_grid_q_diff) >= 1e-14]
        diff_entry = abs((ext_grid_sens_diff - ext_grid_q_diff) / ext_grid_q_diff)
        diff_list.append(diff_entry)
    ext_grid_diff = pd.Series(diff_list).set_axis(net.ext_grid.bus)
    diff = pd.concat([vm_rel_err, ext_grid_diff], axis=0)
    diff = diff.reset_index()
    return diff


@pytest.mark.parametrize(
    ("element", "column"), [("load", "q_mvar"), ("load", "p_mw"), ("gen", "p_mw")]
)
def test_sensitivity(element, column):
    delta = 1.0
    abs_tol = 1e-7
    rel_tol = 0.01
    net: pp.pandapowerNet = SimNetwork()
    error_df = pd.DataFrame()

    pre_bus = net.res_bus
    bus_sens_fn = getattr(sensitivity, f"of_bus_vm_pu_on_{element}_{column}")
    bus_sens = bus_sens_fn(net)

    element_df = getattr(net, element)
    for i in element_df.index:
        element_df.at[i, column] += delta
        pp.runpp(net)
        post_bus = net.res_bus
        result = pd.DataFrame()
        result["vm_pu"] = post_bus["vm_pu"]
        result["real_diff"] = post_bus["vm_pu"] - pre_bus["vm_pu"]
        result["sens_diff"] = bus_sens[i] * delta

        result["abs_err"] = result["real_diff"] - result["sens_diff"]
        result["rel_err"] = result["abs_err"] / result["real_diff"]
        result = result.loc[
            (abs(result["abs_err"]) > abs_tol)
            & (abs(result["rel_err"]) > rel_tol)
            & (result["sens_diff"] > abs_tol)
        ]
        result["changed"] = i
        result.index.rename("bus", inplace=True)
        result.set_index("changed", append=True, inplace=True)
        result.index = result.index.swaplevel()
        error_df = pd.concat([error_df, result])

        element_df.at[i, column] -= delta

    if not error_df.empty:
        raise AssertionError(
            f"The errors were above {abs_tol=} and {rel_tol=} in the following cases:\n"
            f"{error_df}"
        )


# functions which will be collected by pytest, compares errors of sensitivites if p of gens is changed
def test_sensitivity_change_p_of_gens(tolerance=tolerance):
    # get network for gen parameters and define DataFrame
    net = SimNetwork()
    ErrorDataFrame = pd.Series()

    # loop that collects all relative errors that are larger than the treshhold into DataFrame with gen and bus
    for j in range(len(net.gen)):
        sens_err = compare_changes_p_of_loads(delta_val, j)
        sens_err = sens_err[sens_err > tolerance]
        sens_err["gen"] = j
        if sens_err.size > 0:
            ErrorDataFrame = pd.concat([ErrorDataFrame, sens_err])
        else:
            continue

    # replace NaN with 0 at bus 0 and remove all rows with NaN, order DataFrame and define assert for pytest
    ErrorDataFrame["index"] = ErrorDataFrame["index"].fillna(0)
    ErrorDataFrame.dropna(inplace=True)
    ErrorDataFrame.reset_index(drop=True, inplace=True)
    ErrorDataFrame.rename(columns={0: "rel_err", "index": "bus"}, inplace=True)
    assert (
        ErrorDataFrame.size == 0
    ), f" Following elements {ErrorDataFrame} exceed tolerance of {tolerance}"
    return None


# functions which will be collected by pytest, compares errors of sensitivites if q of loads is changed
def test_sensitivity_change_q_of_loads(tolerance=tolerance):
    # get network for load parameters and define DataFrame
    net = SimNetwork()
    ErrorDataFrame = pd.Series()

    # loop that collects all relative errors that are larger than the treshhold into DataFrame with load and bus
    for j in range(len(net.load)):
        sens_err = compare_changes_q_of_loads(delta_val, j)
        sens_err = sens_err[sens_err > tolerance]
        sens_err["load"] = j
        if sens_err.size > 0:
            ErrorDataFrame = pd.concat([ErrorDataFrame, sens_err])
        else:
            continue

    # replace NaN with 0 at bus 0 and remove all rows with NaN, order DataFrame and define assert for pytest
    ErrorDataFrame["index"] = ErrorDataFrame["index"].fillna(0)
    ErrorDataFrame.dropna(inplace=True)
    ErrorDataFrame.reset_index(drop=True, inplace=True)
    ErrorDataFrame.rename(columns={0: "rel_err", "index": "bus"}, inplace=True)
    assert (
        ErrorDataFrame.size == 0
    ), f" Following elements {ErrorDataFrame} exceed tolerance of {tolerance}"
    return None


# functions which will be collected by pytest, compares errors of sensitivites if p of loads is changed
def test_sensitivity_change_p_of_loads(tolerance=tolerance):
    # get network for load parameters and define DataFrame
    net = SimNetwork()
    ErrorDataFrame = pd.Series()

    # loop that collects all relative errors that are larger than the treshhold into DataFrame with load and bus
    for j in range(len(net.load)):
        sens_err = compare_changes_p_of_loads(delta_val, j)
        sens_err = sens_err[sens_err > tolerance]
        sens_err["load"] = j
        if sens_err.size > 0:
            ErrorDataFrame = pd.concat([ErrorDataFrame, sens_err])
        else:
            continue

    # replace NaN with 0 at bus 0 and remove all rows with NaN, order DataFrame and define assert for pytest
    ErrorDataFrame["index"] = ErrorDataFrame["index"].fillna(0)
    ErrorDataFrame.dropna(inplace=True)
    ErrorDataFrame.reset_index(drop=True, inplace=True)
    ErrorDataFrame.rename(columns={0: "rel_err", "index": "bus"}, inplace=True)
    assert (
        ErrorDataFrame.size == 0
    ), f" Following elements {ErrorDataFrame} exceed tolerance of {tolerance}"
    return None

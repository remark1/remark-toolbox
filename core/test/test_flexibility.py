from util.flexibility import Flexibility, split_amount


def test_splitting():
    amounts = split_amount({
        'a': Flexibility(min_p=-250, max_p=0),
        'b': Flexibility(min_p=-1, max_p=-1),
    }, -1)
    assert amounts == {'a': 0, 'b': -1}

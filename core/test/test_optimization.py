import itertools
import math
from typing import Literal

import pytest

from mas.strategies.optimization import (
    FlexParams,
    MarketForecastParams,
    MarketParams,
    get_value,
    optimize_p_only,
    optimize_q_market,
    optimize_simultaneous,
)
from pyomo.environ import units as u

u.load_definitions_from_strings(["EUR = [currency]"])
len_test_h = 8
q_volume_tick = 10000.0 * u.W  # 10 kVAR
p_volume_tick = 100000.0 * u.W  # 0.1 MW = 100kW
p_time_tick = 1 * u.h
q_price_tick = 0.0000001 * u.EUR / u.Wh  # 0.1 €/MVAR
p_price_tick = 0.0000001 * u.EUR / u.Wh  # 0.1 €/MWh


def setup_test_params(q_time_tick, clearing_rule: Literal["pay_as_bid", "pay_as_cleared"],
                      plant: Literal["pv", "bat", "pv_and_bat"]):
    q_normalized_price_tick = round(get_value(q_price_tick * q_volume_tick * q_time_tick, u.EUR), 6) * u.EUR
    p_normalized_price_tick = round(get_value(p_price_tick * p_volume_tick * p_time_tick, u.EUR), 6) * u.EUR
    m = MarketParams(
        q_market_clearing=clearing_rule,
        q_volume_tick=q_volume_tick,
        p_volume_tick=p_volume_tick,
        q_time_tick=q_time_tick,
        p_time_tick=1 * u.h,
        q_price_tick=q_normalized_price_tick,
        p_price_tick=p_normalized_price_tick
    )

    fcst = MarketForecastParams(
        p_clear_price=[
            round(get_value((v * u.EUR / u.MWh) * (p_volume_tick * p_time_tick) / p_normalized_price_tick), 6)
            for v in [97, 107, 136, 151, 133, 113, 98, 89]],  # initial price in €/MWh
        q_clear_price=[
            round(get_value((v * u.EUR / u.MWh) * (q_volume_tick * q_time_tick) / q_normalized_price_tick), 6)
            for v in [*[11 for _ in range(int(len_test_h * get_value(1 * u.h / q_time_tick) / 2))],
                      *[30 for _ in range(int(len_test_h * get_value(1 * u.h / q_time_tick) / 2))]]],
        # initial price in €/MVARh
        q_demand=[round(get_value(1 * u.MW / q_volume_tick), 6) for _ in
                  range(int(len_test_h))],  # demand always 1 MVAR
        q_eff_price=[
            round(get_value((v * u.EUR / u.MWh) * (q_volume_tick * q_time_tick) / q_normalized_price_tick), 6)
            for v in [8 for _ in range(int(len_test_h * get_value(1 * u.h / q_time_tick)))]],
        # initial price in €/MVARh
    )

    flex_pv = FlexParams(
        s_max=400000,
        # 300kW PV
        gen_max=[
            p * u.W
            for p in [
                143374,
                109223,
                158660,
                181691,
                119377,
                198867,
                134070,
                194633,
                216881,
                245190,
                241693,
                237246,
                248332,
                215380,
                246540,
                239861,
                220095,
                268656,
                295469,
                256021,
                256362,
                251231,
                224929,
                249646,
                247601,
                231843,
                185471,
                203134,
                242077,
                180701,
                206451,
                242226,
            ]
        ],
        cons_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        # 10 €Cent/kWh in q_price_tick (0.1 €/MWh)
        costs_p=round(
            get_value(
                (0.1 * u.EUR / u.kWh)
                * (p_volume_tick * p_time_tick)
                / p_normalized_price_tick
            ),
            6,
        ),
        # 0.1 €Cent/kCARh in q_price_tick (0.1 €/kVARh)
        costs_q=round(
            get_value(
                (0.01 * u.EUR / u.kWh)
                * (q_volume_tick * q_time_tick)
                / q_normalized_price_tick
            ),
            6,
        ),
        soc=0,
        eff_ch=0.95,
        eff_dis=0.96,
        capa=0 * u.Wh,  # relation 2:1 for 300 kWpeak PV
        p_bat_discharge_max=0 * u.W,
        p_bat_charge_max=0 * u.W,
        fce_max=0
    )
    flex_bat = FlexParams(
        s_max=250000,
        gen_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        cons_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        # 10 €Cent/kWh in q_price_tick (0.1 €/MWh)
        costs_p=round(
            get_value(
                (0.0001 * u.EUR / u.kWh)
                * (p_volume_tick * p_time_tick)
                / p_normalized_price_tick
            ),
            6,
        ),
        # 0.1 €Cent/kCARh in q_price_tick (0.1 €/kVARh)
        costs_q=round(
            get_value(
                (0.01 * u.EUR / u.kWh)
                * (q_volume_tick * q_time_tick)
                / q_normalized_price_tick
            ),
            6,
        ),
        soc=0.5,
        eff_ch=0.95,
        eff_dis=0.96,
        capa=150000000 * u.Wh,  # relation 2:1 for 300 kWpeak PV
        p_bat_discharge_max=300000 * u.W,
        p_bat_charge_max=300000 * u.W,
    )
    flex_pv_and_bat = FlexParams(
        s_max=400000,
        # 300kW PV
        gen_max=[
            p * u.W
            for p in [
                143374,
                109223,
                158660,
                181691,
                119377,
                198867,
                134070,
                194633,
                216881,
                245190,
                241693,
                237246,
                248332,
                215380,
                246540,
                239861,
                220095,
                268656,
                295469,
                256021,
                256362,
                251231,
                224929,
                249646,
                247601,
                231843,
                185471,
                203134,
                242077,
                180701,
                206451,
                242226,
            ]
        ],
        cons_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        # 10 €Cent/kWh in q_price_tick (0.1 €/MWh)
        costs_p=round(
            get_value(
                (0.1 * u.EUR / u.kWh)
                * (p_volume_tick * p_time_tick)
                / p_normalized_price_tick
            ),
            6,
        ),
        # 0.1 €Cent/kCARh in q_price_tick (0.1 €/kVARh)
        costs_q=round(
            get_value(
                (0.01 * u.EUR / u.kWh)
                * (q_volume_tick * q_time_tick)
                / q_normalized_price_tick
            ),
            6,
        ),
        soc=0.5,
        eff_ch=0.95,
        eff_dis=0.96,
        capa=150000 * u.Wh,  # relation 2:1 for 300 kWpeak PV
        p_bat_discharge_max=300000 * u.W,
        p_bat_charge_max=300000 * u.W,
        fce_max=2
    )
    flex = None
    if plant == 'pv':
        flex = flex_pv
    elif plant == 'bat':
        flex = flex_bat
    elif plant == 'pv_and_bat':
        flex = flex_pv_and_bat
    return m, fcst, flex


PARAMS = {
    "market_order": ["sequential", "simultaneous"],
    "clearing": ["pay_as_bid", "pay_as_cleared"],
    "q_time_ticks": [0.25 * u.h, 1 * u.h, 4 * u.h],
    "plant_type": ["bat", "pv", "pv_and_bat"],
    "p_only": [False, True],
    "fixed_active_power": [None, [0, 1, 2, 2, 2, 2, 0, 0]]
}


def setup_tests():
    parameter_combinations = list(itertools.product(*PARAMS.values()))
    filtered_param_combinations = []
    for order, clearing, t_tick, plant, p_only, fixed_p in parameter_combinations:
        if clearing == "pay_as_cleared":
            c = "PAC"
        else:
            c = "PAB"
        if p_only:
            suffix = "only p-market optimization"
            if order == "simultaneous" or c == "PAB":
                continue
        else:
            if order == "sequential":
                if fixed_p is not None:
                    suffix = "q-market optimization"
                else:
                    suffix = "p-market optimization"
            else:
                suffix = "pq-market optimization"
        name = f"{order} - {c} - {plant} - {t_tick} - {suffix}"
        filtered_param_combinations.append([name, *setup_test_params(t_tick, clearing, plant), order, p_only, fixed_p])
    return filtered_param_combinations


test_param_setup = setup_tests()


@pytest.mark.parametrize(
    "name, market, forecast, flex, market_order, p_only, fixed_active_power", test_param_setup
)
def test_optimization(name, market, forecast, flex, market_order, p_only, fixed_active_power):
    ver = True
    print(name)
    model, model_q = None, None
    # market, forecast, flex = setup
    # perform the fitting optimization
    if p_only:
        # active_power, p_bid_prices, p_am_bat, model
        res = optimize_p_only(market, forecast, flex, get_model=True, verbose=ver)
    elif fixed_active_power is not None:
        # reactive_power, q_bid_prices, model_q
        res = optimize_q_market(
            market, forecast, flex, fixed_active_power, get_model=True, verbose=ver
        )
    elif market_order == "sequential":
        # active_power, p_bid_prices, p_am_bat, model =
        res = optimize_p_only(
            market, forecast, flex, get_model=True, verbose=ver
        )
        # reactive_power, q_bid_prices, model_q =
        res2 = optimize_q_market(
            market, forecast, flex, res['p_amount_total_vt'], get_model=True, verbose=ver
        )
        res["q_amount_total_vt"] = res2["q_amount_total_vt"]
        res["q_price_pt"] = res2["q_price_pt"]
        model_q = res2["model"]
    else:
        res = optimize_simultaneous(
            market, forecast, flex, get_model=True, verbose=ver
        )
    if fixed_active_power is not None:
        active_power = fixed_active_power
    else:
        active_power = res["p_amount_total_vt"]
    p_bid_prices = res["p_price_pt"]
    p_am_bat = res["p_amount_bat_vt"]
    p_am_res = res["p_amount_res_vt"]
    reactive_power = res["q_amount_total_vt"]
    q_bid_prices = res["q_price_pt"]
    model = res["model"]
    print(f'{p_am_res=}')
    # check optimization results
    if active_power is not None:
        p_w = [p * get_value(market.p_volume_tick, u.W) for p in active_power]
        p_w_quarter_hour = [p / 4 for p in p_w for _ in range(4)]

        print(f"active power: {p_w} W")
        phi = 0.3175604292915215
        if reactive_power is not None:
            q_var = [q * get_value(market.q_volume_tick, u.W) for q in reactive_power]
            q_var_quarter_hour = [q for q in q_var for _ in range(round(get_value(market.q_time_tick / (0.25 * u.h))))]
            print(f"reactive power: {q_var} Var")
            assert len(p_w) * get_value(market.p_time_tick / market.q_time_tick) == len(q_var)
            apparent_power = [
                math.sqrt((p_w_quarter_hour[t] + math.tan(phi) * p_w_quarter_hour[t]) ** 2 + q_var_quarter_hour[t] ** 2)
                for t in range(len(p_w_quarter_hour))
            ]
            print(f"resulting apparent power: {apparent_power}")
            # check if the apparent power limit is always okay
            # TODO Specify the number of decimal places to be checked
            assert all(
                get_value(flex.s_max, u.W) >= round(apparent_power[t], 7) for t in range(len(p_w))
            )
        # if a battery storage is involved
        if fixed_active_power is None and get_value(flex.capa, u.Wh) != 0 and model is not None:
            p_am_inv = convert_vals(model, 'p_am_inv', market.p_volume_tick, u.kW)
            p_am_gen = convert_vals(model, "p_am_gen", market.p_volume_tick, u.kW)
            p_am_bat_gen = convert_vals(model, "p_am_bat_gen", market.p_volume_tick, u.kW)
            p_am_bat_cons = convert_vals(model, "p_am_bat_cons", market.p_volume_tick, u.kW)
            soc = convert_vals(model, "soc", 1, None)
            print(f"soc: {soc}")
            print(f"p_converter: {p_am_inv} kW")
            print(f"p_gen: {p_am_gen} kW")
            print(f"p_bat: {[p_am_bat_gen[t] - p_am_bat_cons[t] for t in model.T]} kW")
            print(f'{p_am_bat_gen=}')
            print(f'{p_am_bat_cons=}')
            tolerance = 1e-3
            # check if soc is in bounds
            print(f'{soc=}')
            assert all(0 - tolerance <= s <= 1 + tolerance for s in soc)
            # check that charging and discharging of battery are never schedules at the same time
            assert not any(p_am_bat_gen[t] != 0 and p_am_bat_cons[t] != 0 for t in model.T)
            assert all(
                math.isclose(p_am_inv[t], p_am_gen[t] + p_am_bat_gen[t] - p_am_bat_cons[t], rel_tol=1e-7, abs_tol=1e-7)
                for t in model.T)
            # full cycle equivalents
            fce = sum(
                [(abs(p_am_bat[t]) * get_value(market.p_volume_tick, u.W) * 0.25) / (2 * get_value(flex.capa, u.Wh)) for
                 t in model.T])
            print(f'{fce=}')
            assert fce <= flex.fce_max
        else:
            if fixed_active_power is None:
                assert all(get_value(flex.cons_max[i] - flex.p_bat_charge_max, unit=u.W) <= p_w_quarter_hour[i] <=
                           get_value(flex.gen_max[i] + flex.p_bat_discharge_max, u.W) for i in range(len(flex.gen_max)))

        if model is not None:
            assert model.objective() >= 0
        if model_q is not None:
            assert model_q.objective() >= 0


def convert_vals(m, var_name, factor, unit):
    if var := getattr(m, var_name, None):
        if isinstance(var[0], int) or isinstance(var[0], float):
            return [round(get_value(var[t] * factor, unit=unit), 6) for t in range(len(var))]
        return [round(get_value(var[t].value * factor, unit=unit), 6) for t in range(len(var))]

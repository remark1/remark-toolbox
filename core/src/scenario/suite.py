import argparse
import logging
import sys
import traceback
import uuid
from pathlib import Path

import psycopg
import psycopg.rows
from export import export_run
from mas.agents.trader import SequencingMode
from mas.messages import PricingRule
from mas.util import Durations
from scenario.asset_configs import AssetChoice, ASSET_DISTRIBUTIONS
from scenario.db import DB_CONN
from scenario.scenario import run_experiment
from scenario.specification import DEFAULT_GRID, Scenario

MARKET_SETTINGS: list[tuple[SequencingMode, PricingRule]] = [
    (SequencingMode.NAIVE, PricingRule.PAY_AS_CLEARED),
    (SequencingMode.SEQUENTIAL, PricingRule.PAY_AS_CLEARED),
    (SequencingMode.SEQUENTIAL, PricingRule.PAY_AS_BID),
    (SequencingMode.SIMULTANEOUS, PricingRule.PAY_AS_CLEARED),
    (SequencingMode.SIMULTANEOUS, PricingRule.PAY_AS_BID),
]

AGENT_KNOWLEDGE: list[float] = [
    0.01,  # High knowledge
    0.1,  # Low knowledge
]

BUS_COUNT = 120
# TOTAL_AGENT_COUNTS = 80
# TRADER_SETUP = "many_small"
TRADER_SETUP = "many_large"
# TRADER_SETUP = 'bat_test_8'
# TRADER_SETUP = "default_8"


def get_scenarios(*, agents, num_days, q_scale):
    asset_distribution = ASSET_DISTRIBUTIONS[agents]
    num_agents = sum(asset_distribution.values())
    return [
        Scenario(
            name=(
                f"{seq_mode.value}--{pricing.value}--{num_agents}-agents--"
                f"noise-{agent_knowledge}"
            ),
            sequencing_mode=seq_mode,
            grid=DEFAULT_GRID,
            penetrations=ASSET_DISTRIBUTIONS[TRADER_SETUP],
            market_type="day-ahead",
            pricing_rule=pricing,
            q_market_resolution=Durations.ONE_HOUR_s,
            finc_price_EUR_Mvarh=5,
            agent_knowledge=agent_knowledge,
            until=Durations.ONE_DAY_s * (num_days + 1),
            q_demand_scale=q_scale,
        )
        for (seq_mode, pricing) in MARKET_SETTINGS
        for agent_knowledge in AGENT_KNOWLEDGE
    ]


def export(conn, args, run_id, scenario_name, trader_setup=''):
    Path(args.out).mkdir(parents=True, exist_ok=True)
    file_name = args.out + "/" + scenario_name + '__' + trader_setup + ".xlsx"  # TODO: use os.path
    export_run(conn, file_name, run_id)
    return file_name


def create_db_conn():
    return psycopg.connect(
        f"postgres://{DB_CONN['db_user']}:{DB_CONN['db_pass']}"
        f"@{DB_CONN['db_host']}:{DB_CONN['db_port']}/{DB_CONN['db_name']}",
        row_factory=psycopg.rows.dict_row,
    )


if __name__ == "__main__":
    # Turn of task-destroyed messages
    logging.getLogger("asyncio").addFilter(
        lambda record: "Task was destroyed but it is pending!" not in record.msg
    )

    argparser = argparse.ArgumentParser(
        description="Runs the suite of evaluation scenarios, or parts thereof"
    )
    argparser.add_argument(
        "index", type=int, nargs="*", help="indices of the scenarios to run"
    )
    argparser.add_argument(
        "--all",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="run all scenarios",
    )
    argparser.add_argument(
        "--lb",
        type=int,
        help="lowest index of the scenarios to run",
    )
    argparser.add_argument(
        "--ub",
        type=int,
        help="highest index of the scenarios to run"
    )
    # use like -l 1234 2345 3456 4567
    argparser.add_argument(
        '-l', '--idx_list',
        nargs='+', help="list of indices of the scenarios to run")
    argparser.add_argument(
        "--count",
        action="store_true",
        help="print the number of scenarios in the suite",
    )
    argparser.add_argument(
        "--list",
        action="store_true",
        help="list all the scenarios with their index",
    )
    argparser.add_argument(
        "--out",
        type=str,
        default="results",
        help="path to the folder where results should be placed"
    )
    argparser.add_argument(
        "--days",
        type=int,
        default=7,
        help="how many bidding days to calculate"
    )
    argparser.add_argument(
        "--agents",
        choices=ASSET_DISTRIBUTIONS.keys(),
        default=TRADER_SETUP,
        help="the setup of agents to use"
    )
    argparser.add_argument(
        "--q-scale",
        type=float,
        default=1.0,
        help="factor by which to scale the Q-demand from the baseline run",
    )
    argparser.add_argument(
        "--baseline",
        type=str,
        help="set the baseline id (useful for repeated tests)"
    )
    argparser.add_argument(
        "--export",
        type=str,
        help="don't run the simulation, just export the given run's data"
    )
    argparser.add_argument(
        "--mango-port",
        type=int,
        default=5557,
        help="port for the mango simulation"
    )
    args = argparser.parse_args()

    scenarios = get_scenarios(
        agents=args.agents,
        num_days=args.days,
        q_scale=args.q_scale,
    )

    if args.count:
        print(len(scenarios))
        sys.exit(0)

    if args.list:
        for i, scenario in enumerate(scenarios):
            print(f"{i}: {scenario.name}")
        sys.exit(0)

    if args.export:
        with create_db_conn() as conn:
            file_name = export(conn, args, args.export, args.export)
        print(
            f"exported run {args.export} to {file_name}"
        )
        sys.exit(0)

    if args.all:
        scenario_indices = range(0, len(scenarios))
    elif args.idx_list:
        scenario_indices = [int(i) for i in args.idx_list]
    elif args.lb is not None:
        if args.ub is not None:
            scenario_indices = range(args.lb, min(args.ub + 1, len(scenarios)))
        else:
            scenario_indices = range(args.lb, len(scenarios))
    else:
        scenario_indices = args.index

    if not scenario_indices:
        print(
            "must specify at least one scenario to run (by index) or --all\n"
            "run with --help for usage instructions",
            file=sys.stderr,
        )
        sys.exit(1)

    # Connection for recording run in runs table
    with create_db_conn() as conn:
        conn.execute(
            "SELECT set_config('search_path', %s, false);", (DB_CONN["schema_name"],)
        )
        conn.execute("SET timezone TO 'UTC';")

        successes = []
        failures = []
        for scenario_index in scenario_indices:
            run_complete = False
            try:
                run_id = f"suite_{uuid.uuid4().hex}"
                scenario = scenarios[scenario_index]
                run_experiment(conn, scenario, run_id, args.baseline, mango_port=args.mango_port)
                run_complete = True
                file_name = export(conn, args, run_id, scenario.name, TRADER_SETUP)
                successes.append((scenario_index, file_name))
            except Exception as e:
                print(
                    f"\033[31;1mrun for index {scenario_index} with name "
                    f"{scenario.name} failed\033[0m",
                    file=sys.stderr
                )
                traceback.print_exception(e)
                failures.append((scenario_index, run_id, repr(e)))

        print("\n\033[1mSummary\033[0m", file=sys.stderr)

        if successes:
            print(
                f"\033[32;1mThe following {len(successes)} scenarios ran successfully.\033[0m\n"
                f"Their results were written to the specified files.",
                file=sys.stderr,
            )
            for index, file_name in successes:
                print(f"- {index:2>}: {file_name}", file=sys.stderr)

        if failures:
            print(
                f"\033[31;1mThe following {len(failures)} scenario runs failed.\033[0m\n"
                f"Scenario names and run IDs are provided for further investigation.",
                file=sys.stderr,
            )
            for index, run_id, exc in failures:
                print(
                    f"- {index:2>}: had run ID {run_id}, exception: {exc}",
                    file=sys.stderr,
                )
            sys.exit(1)

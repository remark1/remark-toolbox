from dataclasses import dataclass
from typing import ClassVar, TypedDict, Unpack

import mosaik
import mosaik.scenario
from mosaik.scenario import Entity
from pysimmods.buffer.batterysim.presets import battery_preset
from scenario.db import DBSim


class BatteryParams(TypedDict):
    capacity_kWh: float
    max_P_kW: float
    fce_max: float


@dataclass
class BatterySims:
    """Class for creating Battery entities, potentially together with a
    controlling BatteryAgent.
    """

    world: mosaik.World
    agent_sim: mosaik.scenario.ModelFactory
    db_sim: DBSim
    start_date: str

    config: ClassVar[mosaik.SimConfig] = {
        "PySim": {"python": "pysimmods.mosaik.pysim_mosaik:PysimmodsSimulator"},
    }

    def __post_init__(self):
        self.battery_sim = self.world.start(
            "PySim",
            sim_id="PySim",
            start_date=self.start_date,
        )

    def _params_and_inits(self, bat_params: BatteryParams):
        params, inits = battery_preset(
            pn_max_kw=bat_params["max_P_kW"], cap_kwh=bat_params["capacity_kWh"]
        )
        params["sign_convention"] = "active"
        params["fce_max"] = bat_params["fce_max"]
        return params, inits

    def Battery(self, **bat_params: Unpack[BatteryParams]) -> Entity:
        params, inits = self._params_and_inits(bat_params)
        battery = self.battery_sim.Battery(params=params, inits=inits)
        return battery

    def create_controlled_battery(
            self,
            seed: int,
            agent_bus: Entity,
            trader: Entity,
            noise: float,
            **bat_params: Unpack[BatteryParams],
    ) -> tuple[Entity, Entity]:
        params, inits = self._params_and_inits(bat_params)
        battery = self.Battery(**bat_params)
        agent = self.agent_sim.BatteryAgent(
            seed=seed,
            trader=trader.eid,
            efficiency=params["eta_pc"][2] / 100,
            p_discharge_max_kW=params["p_discharge_max_kw"],
            p_charge_max_kW=params["p_charge_max_kw"],
            capacity_kWh=params["cap_kwh"],
            fce_max=params["fce_max"],
            noise_beta=noise,
        )
        self.world.connect(
            battery,
            agent,
            "soc_percent",
            time_shifted=True,
            initial_data={"soc_percent": inits["soc_percent"]},
        )
        self.world.connect(
            battery,
            agent_bus,
            ("p_mw", "P_gen[MW]"),
        )
        self.world.connect(
            agent,
            agent_bus,
            ("q_amount_Mvar", "Q_gen[MVar]"),
        )
        self.world.connect(agent, battery, ("p_set_MW", "p_set_mw"))

        # DB connection
        self.world.connect(battery, self.db_sim.gens, "p_mw", "q_mvar")
        self.world.connect(
            battery,
            self.db_sim.batteries,
            "soc_percent",
            time_shifted=True,
            initial_data={"soc_percent": inits["soc_percent"]},
        )

        return battery, agent

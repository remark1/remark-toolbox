"""
Configurations for the traders in our simulation.
"""

import enum
from dataclasses import dataclass, field

PV_MODEL_PARAMS = {
    # 'scale_factor': 1000,  # multiplies power production, 1 is equal to 1 kW peak power installed
    "lat": 52,
    "lon": 9,
    # 'slope': 0,  # default value,
    # 'azimuth': 0,  # default value,
    # 'optimal_angle': True,  # calculate and use an optimal slope
    # 'optimal_both': False,  # calculate and use an optimal slope and azimuth
    # 'pvtech': 'CIS',  # default value,
    # 'system_loss': 14,  # default value,
    # 'database': 'PVGIS-SARAH3',  # default value,
    "datayear": 2023,  # default value 2016,
}


class AssetChoice(enum.StrEnum):
    """The selection of assets a single trader possesses."""

    PV = "pv"
    WIND = "wind"
    BATTERY = "battery"
    PV_BATTERY = "pv-battery"
    WIND_BATTERY = "wind-battery"


@dataclass
class PVConfig:
    scale_factor: float  # multiplies power production, 1 is equal to 1 kW peak power installed
    lat: float
    lon: float
    datayear: int  # default value,


@dataclass
class WindConfig:
    generation_kWp: float


@dataclass
class BatteryConfig:
    max_P_kW: float
    capacity_kWh: float
    fce_max: float


@dataclass
class AssetConfig:
    inverter_S_max_kVA: float
    pv: PVConfig | None = None
    wind: WindConfig | None = None
    battery: BatteryConfig | None = None
    levelized_cost_in_year_EURcent_kWh: dict[int, tuple[float, float]] = field(
        default_factory=dict
    )


class TraderSize(enum.StrEnum):
    SMALL = enum.auto()
    BIG = enum.auto()


ASSET_CONFIGS: dict[AssetChoice, dict[TraderSize, AssetConfig]] = {
    AssetChoice.PV: {
        TraderSize.SMALL: AssetConfig(
            # Smax is set to enable provision of q-tar iven when peak gen is reached (and rounded up)
            inverter_S_max_kVA=1050,
            pv=PVConfig(scale_factor=1000, **PV_MODEL_PARAMS),
            # TODO: LCOE not used
            levelized_cost_in_year_EURcent_kWh={
                2021: (4.63, 9.78),
                2040: (2.85, 6.02),
            },
        ),
        TraderSize.BIG: AssetConfig(
            inverter_S_max_kVA=3080,
            pv=PVConfig(scale_factor=3000, **PV_MODEL_PARAMS),
            levelized_cost_in_year_EURcent_kWh={
                2021: (3.12, 5.7),
                2040: (1.92, 3.51),
            },
        ),
    },
    AssetChoice.PV_BATTERY: {
        TraderSize.SMALL: AssetConfig(
            inverter_S_max_kVA=1050,
            pv=PVConfig(scale_factor=1000, **PV_MODEL_PARAMS),
            battery=BatteryConfig(capacity_kWh=1200, max_P_kW=300, fce_max=2),
            levelized_cost_in_year_EURcent_kWh={
                2021: (6.58, 14.40),
                2040: (3.44, 8.79),
            },
        ),
        TraderSize.BIG: AssetConfig(
            inverter_S_max_kVA=3080,
            pv=PVConfig(scale_factor=3000, **PV_MODEL_PARAMS),
            battery=BatteryConfig(capacity_kWh=2000, max_P_kW=500, fce_max=2),
            levelized_cost_in_year_EURcent_kWh={
                2021: (5.24, 9.92),
                2040: (2.56, 6.04),
            },
        ),
    },
    AssetChoice.WIND: {
        TraderSize.SMALL: AssetConfig(
            inverter_S_max_kVA=2100,
            wind=WindConfig(generation_kWp=2000),
            levelized_cost_in_year_EURcent_kWh={
                2021: (3.94, 8.29),
                2040: (3.40, 6.97),
            },
        ),
        TraderSize.BIG: AssetConfig(
            inverter_S_max_kVA=4210,
            wind=WindConfig(generation_kWp=4000),
            levelized_cost_in_year_EURcent_kWh={
                2021: (3.94, 8.29),
                2040: (3.40, 6.97),
            },
        ),
    },
    AssetChoice.WIND_BATTERY: {
        TraderSize.SMALL: AssetConfig(
            inverter_S_max_kVA=2100,
            wind=WindConfig(generation_kWp=2000),
            battery=BatteryConfig(capacity_kWh=4000, max_P_kW=1000, fce_max=2),
        ),
    },
    AssetChoice.BATTERY: {
        TraderSize.SMALL: AssetConfig(
            inverter_S_max_kVA=210,
            battery=BatteryConfig(capacity_kWh=500, max_P_kW=200, fce_max=2),
        ),
        TraderSize.BIG: AssetConfig(
            inverter_S_max_kVA=1050,
            battery=BatteryConfig(capacity_kWh=4000, max_P_kW=1000, fce_max=2),
        ),
    },
}

ASSET_DISTRIBUTIONS: dict[str, dict[tuple[AssetChoice, TraderSize], int]] = {
    "bat_test_8": {
        (AssetChoice.PV_BATTERY, TraderSize.SMALL): 1,
        (AssetChoice.PV_BATTERY, TraderSize.BIG): 1,
        (AssetChoice.WIND_BATTERY, TraderSize.SMALL): 1,
        (AssetChoice.BATTERY, TraderSize.SMALL): 1,
        (AssetChoice.BATTERY, TraderSize.BIG): 1,
    },
    "default_8": {
        (AssetChoice.PV, TraderSize.SMALL): 2,
        (AssetChoice.PV_BATTERY, TraderSize.SMALL): 1,
        (AssetChoice.PV, TraderSize.BIG): 1,
        (AssetChoice.PV_BATTERY, TraderSize.BIG): 1,
        (AssetChoice.WIND, TraderSize.SMALL): 1,
        (AssetChoice.BATTERY, TraderSize.SMALL): 1,
    },
    "default_20": {
        (AssetChoice.PV, TraderSize.SMALL): 5,
        (AssetChoice.PV_BATTERY, TraderSize.SMALL): 3,
        (AssetChoice.PV, TraderSize.BIG): 3,
        (AssetChoice.PV_BATTERY, TraderSize.BIG): 2,
        (AssetChoice.WIND, TraderSize.SMALL): 3,
        (AssetChoice.WIND_BATTERY, TraderSize.SMALL): 1,
        (AssetChoice.BATTERY, TraderSize.SMALL): 2,
        (AssetChoice.BATTERY, TraderSize.BIG): 1,
    },
    "many_small": {
        (AssetChoice.PV, TraderSize.SMALL): 17,
        (AssetChoice.PV_BATTERY, TraderSize.SMALL): 13,
        (AssetChoice.PV, TraderSize.BIG): 1,
        (AssetChoice.PV_BATTERY, TraderSize.BIG): 1,
        (AssetChoice.WIND, TraderSize.SMALL): 3,
        (AssetChoice.WIND, TraderSize.BIG): 1,
        (AssetChoice.BATTERY, TraderSize.SMALL): 5,
        (AssetChoice.BATTERY, TraderSize.BIG): 1,
    },
    "many_large": {
        (AssetChoice.PV, TraderSize.SMALL): 3,
        (AssetChoice.PV_BATTERY, TraderSize.SMALL): 2,
        (AssetChoice.PV, TraderSize.BIG): 6,
        (AssetChoice.PV_BATTERY, TraderSize.BIG): 4,
        (AssetChoice.WIND_BATTERY, TraderSize.SMALL): 1,
        (AssetChoice.WIND, TraderSize.BIG): 2,
        (AssetChoice.BATTERY, TraderSize.BIG): 2,
    },
}

AGENT_NAMES: dict[tuple[AssetChoice, TraderSize], str] = {
    (AssetChoice.BATTERY, TraderSize.BIG): "Bat_large",
    (AssetChoice.BATTERY, TraderSize.SMALL): "Bat_small",
    (AssetChoice.PV_BATTERY, TraderSize.BIG): "PV_large_Bat",
    (AssetChoice.PV_BATTERY, TraderSize.SMALL): "PV_small_Bat",
    (AssetChoice.PV, TraderSize.BIG): "PV_large",
    (AssetChoice.PV, TraderSize.SMALL): "PV_small",
    (AssetChoice.WIND_BATTERY, TraderSize.SMALL): "Wind_small_Bat",
    (AssetChoice.WIND, TraderSize.BIG): "Wind_large",
    (AssetChoice.WIND, TraderSize.SMALL): "Wind_small",
}
from dataclasses import dataclass
from pprint import pformat
from typing import Literal

from loguru import logger
from mas.agents.trader import SequencingMode
from mas.messages import PricingRule
from mas.util import Durations, ProductResolution
from scenario.asset_configs import AssetChoice, TraderSize

DEFAULT_GRID = "1-MVLV-semiurb-3.202-1-sw"


@dataclass(kw_only=True)
class Scenario:
    name: str
    """Display name of the scenario"""
    sequencing_mode: SequencingMode
    """Sequencing mode of the markets"""
    grid: str
    """Simbench code of the grid"""
    penetrations: dict[tuple[AssetChoice, TraderSize], int]
    """Number of MV nodes that have a trader with the given unit types.
    """
    market_type: Literal["day-ahead"]
    pricing_rule: PricingRule
    q_market_resolution: ProductResolution
    finc_price_EUR_Mvarh: float = 10
    """maximum price at q-market"""
    agent_knowledge: float
    """This is used as the factor beta when applying noise to the
    agent's predictions according to the formula

        noisy_value = precise_value * exp(beta * N(0,1))
    
    where N(0,1) is a normally distributed random variable.
    """
    q_demand_scale: float = 1.0
    """Factor by which the total Q-demand for the simulation is scaled"""
    until: int
    """Runtime of the scenario. (It will actually run an extra step
    to get the final result.)
    """


def parse_scenario(data) -> Scenario:
    """Parse a scenario JSON as provided by the frontend into our
    internal representation (the `Scenario` dataclass).
    """
    name = data.pop("name")
    try:
        sequencing_mode = SequencingMode(val := data.pop("sequencingMode"))
    except ValueError:
        raise NotImplementedError(
            f"illegal sequencing mode \"{val}\""
        )
    grid = data.pop("grid", "") or DEFAULT_GRID
    penetrations: dict[tuple[AssetChoice, TraderSize], int] = {}
    for asset_choice_str, sizes in data.pop("assetCounts").items():
        try:
            asset_choice = AssetChoice(asset_choice_str)
        except ValueError:
            raise NotImplementedError(
                f"illegal asset choice \"{asset_choice_str}\""
            )
        for size_str, count in sizes.items():
            try:
                trader_size = TraderSize(size_str)
            except ValueError:
                raise NotImplementedError(
                    f"illegal trader size \"{size_str}\""
                )
            penetrations[asset_choice, trader_size] = count

    try:
        pricing_rule = PricingRule(val := data.pop("qClearingRule"))
    except ValueError:
        raise NotImplementedError(
            f"illegal pricing rule \"{val}\""
        )
    days = data.pop("days", 7)
    q_market_resolution_s = data.pop("qTimeResolution_s")

    if data:
        logger.warning(
            f"The following data from the frontend was not used: \n{pformat(data)}"
        )

    return Scenario(
        name=name,
        sequencing_mode=sequencing_mode,
        grid=grid,
        penetrations=penetrations,
        market_type="day-ahead",
        pricing_rule=pricing_rule,
        q_market_resolution=q_market_resolution_s,
        agent_knowledge=0.1,
        until=days * Durations.ONE_DAY_s,
    )
from __future__ import annotations

import argparse
import json
import sys
import uuid
import warnings
from collections import defaultdict
from contextlib import contextmanager
from copy import deepcopy
from dataclasses import asdict
from random import Random
from typing import cast

import mosaik
import psycopg
import psycopg.rows
from loguru import logger
from mosaik.async_scenario import Entity, SimConfig
from mosaik.util import connect_many_to_one
from mosaik.scenario import Entity, SimConfig

from mas.agents.trader import SequencingMode
from scenario.asset_configs import AGENT_NAMES, ASSET_CONFIGS
from scenario.battery import BatterySims
from scenario.db import DB_CONN, DBSim
from scenario.pv import PVSims
from scenario.specification import Scenario, parse_scenario
from scenario.wind import WindSims


def main():
    # Parse command line arguments
    argparser = argparse.ArgumentParser(
        description="Runs the REMARK simulation",
        epilog="The scenario JSON should be provided via standard input.",
    )
    argparser.add_argument(
        "run_id",
        default=f"free_{uuid.uuid4().hex[0:8]}",
        help='the run_id for this scenario (defaults to "free_" plus random number)',
        nargs="?",
    )
    argparser.add_argument(
        "scenario_id", default=None, help="the ID of the scenario", nargs="?"
    )
    argparser.add_argument(
        "-b",
        "--baseline",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="run the simulation in baseline mode (no Q-market at all)",
    )

    args = argparser.parse_args()
    run_id = args.run_id
    scenario_id = args.scenario_id

    # Read and parse scenario from stdin
    if sys.stdin.isatty():
        print(
            "Enter scenario JSON and end with Ctrl-D (EOF); alternatively, provide "
            "the scenario JSON by piping it in:"
        )
    scenario = parse_scenario(json.load(sys.stdin))

    # Connection for recording run in runs table
    with psycopg.connect(
        f"postgres://{DB_CONN['db_user']}:{DB_CONN['db_pass']}"
        f"@{DB_CONN['db_host']}:{DB_CONN['db_port']}/{DB_CONN['db_name']}",
        row_factory=psycopg.rows.dict_row,
    ) as conn:
        conn.execute(
            "SELECT set_config('search_path', %s, false);", (DB_CONN["schema_name"],)
        )

        run_experiment(conn, scenario, run_id, scenario_id=scenario_id)


def run_experiment(
    conn,
    scenario: Scenario,
    run_id: str,
    existing_baseline=None,
    mango_port: int = 5557,
    scenario_id: str | None = None,
):
    """Run an experiment, consisting of a baseline run and a normal run
    for a given scenario."""

    # Remove mosaik's incessant messages about event-based to time-based
    # connections
    try:
        logger.remove(0)
        logger.add(sys.stderr, filter=lambda r: r["function"] != "connect_one")
    except ValueError:
        # If the 0 logger does not exist, this has been run before,
        # so we don't have to do anything.
        pass

    # Print warnings using loguru
    def showwarning(message, category, filename, lineno, file=None, line=None):
        logger.opt(depth=2).warning(f"{category.__name__}: {message}")

    warnings.showwarning = showwarning

    if not existing_baseline:
        logger.info("Running in baseline mode (no Q-market)")
        baseline_id = run_id + "_baseline"
        with record_run(
            conn, baseline_id, baseline_for=run_id, scenario_id=scenario_id
        ):
            run_scenario(
                scenario,
                baseline_id,
                q_demand_Mvar=None,
                conn=conn,
                mango_port=mango_port,
            )
        q_demand_Mvar = get_q_demand_Mvar(
            conn, baseline_id=baseline_id, factor=scenario.q_demand_scale
        )
    elif isinstance(existing_baseline, str):
        logger.info(
            f"Using existing baseline {existing_baseline} (skipping baseline run)"
        )
        q_demand_Mvar = get_q_demand_Mvar(
            conn, baseline_id=existing_baseline, factor=scenario.q_demand_scale
        )
    elif isinstance(existing_baseline, list):
        logger.info("Using supplied Q-demand instead of baseline run")
        q_demand_Mvar = existing_baseline

    logger.info("Running actual simulation")
    with record_run(conn, run_id, scenario_id=scenario_id):
        run_scenario(
            scenario,
            run_id,
            q_demand_Mvar=q_demand_Mvar,
            conn=conn,
            mango_port=mango_port,
        )
    logger.info(f"Completed run {run_id}")


@contextmanager
def record_run(conn, run_id, baseline_for=None, scenario_id=None):
    conn.execute(
        """
        INSERT INTO runs (id, start_time, baseline_for, scenario_id)
        VALUES (%s, NOW(), %s, %s)
        ON CONFLICT (id)
        DO UPDATE SET
            start_time = EXCLUDED.start_time,
            baseline_for = EXCLUDED.baseline_for;
        """,
        (run_id, baseline_for, scenario_id),
    )
    conn.commit()
    successful = True
    try:
        yield
    except:
        successful = False
        raise
    finally:
        conn.execute(
            "UPDATE runs SET end_time = NOW(), successful = %s WHERE id = %s;",
            (successful, run_id),
        )
        conn.commit()


# START_DATE = "1970-01-01 00:00:00Z"
START_DATE = "2023-07-01 00:00:00"
SCENARIO_SEED = 42
WIND_DATA_LOCATIONS = [
    "alsfeld",
    "belm",
    "bergen_ni",
    "braunlage",
    "braunschweig",
    "bremen",
    "diepholz",
    "faßberg",
    "friesoythe",
    "goettingen",
    "hameln",
    "hannover",
    "lingen_ems",
    "lippspringe",
    "osnabrueck",
    "werl",
    "wunstorf",
]


def run_scenario(
    scenario: Scenario,
    run_id: str,
    q_demand_Mvar: list[float] | None,
    conn: psycopg.Connection,
    mango_port: int = 5557,
):
    scenario = deepcopy(scenario)  # Our baseline updates should not affect other runs
    logger.info(f"Starting REMARK scenario {scenario.name} as {run_id}")

    if not q_demand_Mvar:
        # A scenario that does not have a baseline is implicitly a
        # baseline run
        scenario.sequencing_mode = SequencingMode.P_ONLY

    logger.info(f"Scenario config is {scenario}")

    # Sic mundus creatus est

    SIM_CONFIG: SimConfig = {
        "Agents": {"cmd": "%(python)s -m mas.mango_sim %(addr)s"},
        "Functions": {"python": "util.function_sim:Simulator"},
        "Grid": {"python": "mosaik_components.pandapower:Simulator"},
        "CSV": {"python": "mosaik_csv:CSV"},
        "Forecaster": {"python": "util.csv_forecaster:Simulator"},
        "QDemand": {"python": "simulators.q_demand_reader:Simulator"},
        **BatterySims.config,
        **WindSims.config,
        **PVSims.config,
        **DBSim.config,
    }

    world = mosaik.World(SIM_CONFIG, mosaik_config={"start_timeout": 60})

    scenario_random = Random(SCENARIO_SEED)

    # Start simulators

    day_ahead_prices = world.start(
        "Forecaster",
        sim_id="DayAheadPrices",
        sim_start="2023-06-01 00:00:00",
        datafile="data/p_prices_2023.csv",
        horizon=48,
        skip=1,
    ).PPrices()

    agent_sim = world.start(
        "Agents",
        sim_id="Agents",
        # start_date=START_DATE,
        addr=("localhost", mango_port),
        step_size=900,
    )

    db_sim = DBSim(world, run_id, f"{START_DATE}Z")

    with warnings.catch_warnings():
        # Get rid of FutureWarnings from simbench code
        warnings.filterwarnings("ignore", category=FutureWarning)
        warnings.filterwarnings("ignore", category=SyntaxWarning)
        grid = cast(
            Entity,
            world.start("Grid", sim_id="Grid", step_size=900).Grid(
                simbench=scenario.grid
            ),
        )

    wind_sims = WindSims(
        world,
        agent_sim,
        db_sim,
        power_curve_csv="data/power_coeff_curve.csv",
        start_date=START_DATE,
    )

    pv_sims = PVSims(
        world,
        agent_sim,
        db_sim,
        step_size=900,
        start_date=START_DATE,
    )

    battery_sims = BatterySims(
        world,
        agent_sim,
        db_sim,
        start_date=f"{START_DATE}Z",
    )

    # Set up connections

    logger.info("Creating and connecting entities")

    buses = [e for e in grid.children if e.type == "Bus"]
    # Buses are separated by voltage level: high voltage (110 kV),
    # medium voltage (20 kV), and low voltage (400 V).
    # We connect everything to medium voltage buses.
    buses_mv = [e for e in buses if e.extra_info["nominal voltage [kV]"] == 20.0]
    print(f"Grid has {len(buses_mv)} medium-voltage buses")

    connect_many_to_one(
        world,
        buses,
        db_sim.nodes,
        ("P[MW]", "p_mw"),
        ("Q[MVar]", "q_mvar"),
        ("Vm[pu]", "vm_pu"),
        ("Va[deg]", "va_deg"),
    )
    connect_many_to_one(
        world,
        [e for e in grid.children if e.type == "Line"],
        db_sim.lines,
        ("I[kA]", "i_ka"),
        ("loading[%]", "loading_percent"),
    )

    grid_operator = agent_sim.GridOperator(
        id="GridOperator",
        seed=SCENARIO_SEED,
        net=scenario.grid,
        q_market_resolution=scenario.q_market_resolution,
        q_demand_Mvar=q_demand_Mvar,
    )
    market = agent_sim.Market(
        id="Market",
        seed=SCENARIO_SEED,
        grid_operator=grid_operator.eid,
        pricing_rule=scenario.pricing_rule.value,
        sequencing_mode=scenario.sequencing_mode.value,
        q_market_resolution=scenario.q_market_resolution,
    )
    world.connect(day_ahead_prices, market, ("Price[EUR/MWh]", "p_price"))

    trader_index = 1
    # each agent gets a random seed for reproducibility
    agent_seeds = scenario_random.choices(
        range(1, 100), k=sum(scenario.penetrations.values())
    )
    for (asset_choice, trader_size), num_agents in scenario.penetrations.items():
        a_name = AGENT_NAMES[(asset_choice, trader_size)]
        logger.info(f"agents with {a_name}: {num_agents}")
        agent_buses = scenario_random.sample(buses_mv, num_agents)
        for agent_bus in agent_buses:
            a_seed = agent_seeds[trader_index - 1]
            node = agent_bus.extra_info["index"]
            asset_config = ASSET_CONFIGS[asset_choice][trader_size]
            trader: Entity = agent_sim.Trader(
                id=f"Trader-{trader_index}-for-{a_name}-at-{node}",
                seed=a_seed,
                market=market.eid,
                node=node,
                sequencing_mode=scenario.sequencing_mode.value,
                s_max_VA=asset_config.inverter_S_max_kVA * 1_000,
                noise_beta=scenario.agent_knowledge,
                q_demand_Mvar=q_demand_Mvar,
            )
            world.connect(
                trader,
                db_sim.bids,
                ("p_bid_price_EUR_MWh", "p_bid_price_eur_mwh"),
                ("p_bid_amount_MW", "p_bid_amount_mw"),
                ("q_bid_price_EUR_Mvarh", "q_bid_price_eur_mvarh"),
                ("q_bid_amount_Mvar", "q_bid_amount_mvar"),
                ("p_award_amount_MW", "p_award_amount_mw"),
                ("q_award_amount_Mvar", "q_award_amount_mvar"),
                ("p_award_price_EUR_MWh", "p_award_price_eur_mwh"),
                ("q_award_price_EUR_Mvarh", "q_award_price_eur_mwh"),
                ("q_free_amount_Mvar", "q_free_amount_mvar"),
                ("p_clear_price_fc_EUR_MWh", "p_clear_price_fc_eur_mwh"),
                ("q_demand_fc_Mvar", "q_demand_fc_mvar"),
                ("q_clear_price_fc_EUR_Mvarh", "q_clear_price_fc_eur_mvarh"),
                ("q_eff_price_fc_EUR_Mvarh", "q_eff_price_fc_eur_mvarh"),
                ("p_battery_amount_MW", "p_battery_amount_mw"),
                ("p_resource_amount_MW", "p_resource_amount_mw"),
                ("q_rel_price_fc_EUR_Mvarh", "q_rel_price_fc_eur_mvarh"),
            )
            world.connect(day_ahead_prices, trader, ("Price[EUR/MWh]", "p_price"))

            agent_associations = defaultdict(
                lambda: None,
                run_id=run_id,
                trader=trader.full_id,
            )

            if battery_config := asset_config.battery:
                battery_asset, battery_agent = battery_sims.create_controlled_battery(
                    seed=a_seed,
                    agent_bus=agent_bus,
                    trader=trader,
                    capacity_kWh=battery_config.capacity_kWh,
                    max_P_kW=battery_config.max_P_kW,
                    fce_max=battery_config.fce_max,
                    noise=scenario.agent_knowledge,
                )
                agent_associations["battery_agent"] = battery_agent.full_id
                agent_associations["battery_asset"] = battery_asset.full_id

            if wind_config := asset_config.wind:
                wind_asset, wind_agent = wind_sims.create_controlled_turbine(
                    seed=a_seed,
                    wind_csv=f"data/wind/wind_data_2023_30min_{scenario_random.choice(WIND_DATA_LOCATIONS)}.csv",
                    agent_bus=agent_bus,
                    trader=trader,
                    generation_kWp=wind_config.generation_kWp,
                    noise=scenario.agent_knowledge,
                )
                agent_associations["wind_agent"] = wind_agent.full_id
                agent_associations["wind_asset"] = wind_asset.full_id

            if pv_config := asset_config.pv:
                pv_asset, pv_agent = pv_sims.create_controlled_system(
                    seed=a_seed,
                    agent_bus=agent_bus,
                    trader=trader,
                    pv_params=asdict(pv_config),
                    noise=scenario.agent_knowledge,
                )
                agent_associations["pv_agent"] = pv_agent.full_id
                agent_associations["pv_asset"] = pv_asset.full_id

            conn.execute(
                """
                INSERT INTO agent_associations
                    (run_id, trader, pv_agent, wind_agent, battery_agent, pv_asset, wind_asset, battery_asset)
                VALUES (
                    %(run_id)s, %(trader)s,
                    %(pv_agent)s, %(wind_agent)s, %(battery_agent)s,
                    %(pv_asset)s, %(wind_asset)s, %(battery_asset)s
                );
                """,
                params=agent_associations,
            )

            trader_index += 1

    _finc_trader = agent_sim.FincTrader(
        id="FincTrader",
        seed=SCENARIO_SEED,
        market=market.eid,
        node=2,
        cost_EUR_Mvarh=scenario.finc_price_EUR_Mvarh,
    )  # TODO: pick right node
    world.connect(
        _finc_trader,
        db_sim.finc,
        ("q_award_amount_Mvar", "q_award_amount_mvar"),
        ("q_award_price_EUR_Mvarh", "q_award_price_eur_mvarh"),
    )
    world.connect(
        grid_operator,
        db_sim.grid_operator,
        ("q_demand_MVar", "q_demand_mvar"),
    )
    world.connect(
        market,
        db_sim.market,
        ("p_price_EUR_MWh", "p_price_eur_mwh"),
        ("marginal_q_price_EUR_Mvarh", "q_clear_price_eur_mvarh"),
        ("efficiency_q_price_EUR_Mvarh", "q_eff_price_eur_mvarh"),
    )

    try:
        world.run(until=scenario.until + 1, print_progress=False)  # "individual")
    finally:
        world.shutdown()


def get_q_demand_Mvar(
    conn: psycopg.Connection, baseline_id: str, factor: float
) -> list[float]:
    q_demand_rows = conn.execute(
        """
        SELECT time, coalesce(sum(q_free_amount_mvar), 0) AS q_free_total_mvar
        FROM bids
        WHERE run_id = %(baseline_id)s
        GROUP BY time
        ORDER BY time;
        """,
        params={
            "baseline_id": baseline_id,
        },
    ).fetchall()
    logger.info(f"First Q-demand is for {q_demand_rows[0]["time"]}")
    q_demand_Mvar = [row["q_free_total_mvar"] * factor for row in q_demand_rows]
    return q_demand_Mvar + [0] * 97  # Add dummy demands for the final day

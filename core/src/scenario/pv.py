from dataclasses import dataclass
from typing import ClassVar

import mosaik.scenario
from mosaik.scenario import Entity
from scenario.db import DBSim

PVSIM_PARAMS = {
    "cache_dir": None,  # it caches PVGIS API requests
    "verbose": False,  # don't print PVGIS parameters and requests
}


@dataclass
class PVSims:
    world: mosaik.World
    agent_sim: mosaik.scenario.ModelFactory
    db_sim: DBSim
    # sun_csv: str
    step_size: int
    start_date: str

    config: ClassVar[mosaik.SimConfig] = {
        "PVSim": {"python": "mosaik_components.pv.pvgis_simulator:PVGISSimulator"}
    }

    def __post_init__(self):
        pv_sim_params = PVSIM_PARAMS
        pv_sim_params["start_date"] = self.start_date
        self.pv_sim = self.world.start(
            "PVSim",
            step_size=self.step_size,
            sim_params=pv_sim_params,
        )

    def create_controlled_system(
        self,
        seed: int,
        agent_bus: Entity,
        trader: Entity,
        noise: float,
        pv_params: dict,
    ) -> tuple[Entity, Entity]:
        """Create a PV System and an asset agent controlling it."""
        agent = self.agent_sim.PVGenAgent(
            seed=seed,
            trader=trader.eid,
            pv_params=pv_params,
            start_date=self.start_date,
            noise_beta=noise,
        )
        pv_model = self.pv_sim.PVSim(**pv_params)
        self.world.connect(pv_model, agent, ("P[MW]", "pv_p_MW"))
        # We take the outputs from the agent instead of the unit
        # simulator because the unit simulators are not controllable.
        # This should be changed in the future: The agents sends
        # setpoints to the unit, and the unit then provides its
        # measurements to the grid.
        # Cf. the `WindSims` class as well.
        self.world.connect(
            agent,
            agent_bus,
            ("p_amount_MW", "P_gen[MW]"),
            ("q_amount_Mvar", "Q_gen[MVar]"),
        )
        # DB connection
        self.world.connect(agent, self.db_sim.gen_forecast, ("p_MW_fc", "p_fc_mw"))
        self.world.connect(pv_model, self.db_sim.gens, ("P[MW]", "p_mw"))
        return pv_model, agent

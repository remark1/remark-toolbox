# The field names are as required by the TimescaleDB simulator
import os
from dataclasses import dataclass
from typing import ClassVar

import mosaik
from mosaik.scenario import Entity

DB_CONN = {
    "db_name": os.environ.get("REMARK_PG_DB", "remark"),
    "db_user": os.environ.get("REMARK_PG_USER", "remark"),
    "db_pass": os.environ.get("REMARK_PG_PASSWORD", "remark"),
    "schema_name": os.environ.get("REMARK_PG_SCHEMA", "remark"),
    "db_host": os.environ.get("REMARK_PG_HOST", "localhost"),
    "db_port": int(os.environ.get("REMARK_PG_PORT", 5432)),
}


@dataclass
class DBSim:
    """Class for managing the DB simulator. The database's tables can be
    accessed as attributes (e.g. `db_sim.gens` for the *gens* table).
    The table entities will be created on the first access and cached
    for future accesses.
    """
    world: mosaik.World
    run_id: str
    start_date: str

    config: ClassVar[mosaik.SimConfig] = {
        "TimescaleDB": {
            "python": "mosaik_components.timescaledb.custom_writer:CustomWriter"
        },
    }

    def __post_init__(self):
        self._tables = {}
        self.sim = self.world.start(
            "TimescaleDB",
            sim_id="DB",
            **DB_CONN,
            start_date=self.start_date,
            run_id=self.run_id,
            src_col="src_id",
            step_size=900,
        )

    def __getattr__(self, name: str) -> Entity:
        """Return the table entity for the table named like this
        attribute. (The entity is only created once per table and cached
        for repeated calls.)
        """
        table = self._tables.get(name)
        if table is None:
            table = getattr(self.sim, name)()
            self._tables[name] = table
        return table

from dataclasses import dataclass
from typing import ClassVar

import mosaik
import mosaik.scenario
from mosaik.scenario import Entity
from scenario.db import DBSim


@dataclass
class WindSims:
    world: mosaik.World
    agent_sim: mosaik.scenario.ModelFactory
    db_sim: DBSim
    power_curve_csv: str
    start_date: str

    config: ClassVar[mosaik.SimConfig] = {
        "Wind": {"python": "mosaik_components.wind:Simulator"},
        "WindCSV": {"python": "mosaik_csv:CSV"},
        "WindForecaster": {"python": "util.csv_forecaster:Simulator"},
    }

    def __post_init__(self):
        self.turbine_sim = self.world.start(
            "Wind",
            power_curve_csv=self.power_curve_csv,
            step_size=3600,
        )

    def Turbine(self, generation_kWp: float, csv: str) -> Entity:
        wind: Entity = self.world.start(
            "WindCSV",
            # sim_id="WindData",
            sim_start=self.start_date,
            datafile=csv,
        ).Wind()
        turbine = self.turbine_sim.WT(max_power=generation_kWp / 1000)
        self.world.connect(wind, turbine, "wind_speed")
        return turbine

    def create_controlled_turbine(
            self,
            seed: int,
            wind_csv: str,
            agent_bus: Entity,
            trader: Entity,
            generation_kWp: float,
            noise: float,
    ) -> tuple[Entity, Entity]:
        """Create a wind turbine and an asset agent controlling it."""
        print(f"agent on bus {agent_bus} got {wind_csv}")
        forecast: Entity = self.world.start(
            "WindForecaster",
            # sim_id="WindForecast",
            sim_start=self.start_date,
            datafile=wind_csv,
            horizon=96,  # file has half-hour resolution, we want hourly for two days
            skip=2,
        ).Wind()

        agent = self.agent_sim.WindGenAgent(
            seed=seed,
            trader=trader.eid,
            power_curve_path=self.power_curve_csv,
            max_power_kWp=generation_kWp,
            noise_beta=noise,
        )
        self.world.connect(forecast, agent, ("wind_speed", "wind_speed_forecast"))
        turbine = self.Turbine(generation_kWp, wind_csv)
        # We take the outputs from the agent instead of the unit
        # simulator because the unit simulators are not controllable.
        # This should be changed in the future: The agents sends
        # setpoints to the unit, and the unit then provides its
        # measurements to the grid.
        # Cf. the `PVSims` class as well.
        self.world.connect(
            agent,
            agent_bus,
            ("p_amount_MW", "P_gen[MW]"),
            ("q_amount_Mvar", "Q_gen[MVar]"),
        )
        self.world.connect(turbine, self.db_sim.gens, ("P_gen", "p_mw"))

        self.world.connect(agent, self.db_sim.gen_forecast, ("p_MW_fc", "p_fc_mw"))
        return turbine, agent

import math
from typing import TypeVar, override

from loguru import logger
from mas.auctions.base import Auction, AuctionResult
from mas.messages import AwardMessage, MarketProduct, PowerType, PricingRule
from mas.util import Durations, ProductResolution, format_time
from util.time_storage import TimeStorage

Trader = TypeVar("Trader")


class SensitivityAuction(Auction[Trader]):
    sensitivities: TimeStorage[tuple[float, dict[int, float]]]
    product_duration_s: ProductResolution
    pricing_rule: PricingRule

    def __init__(
            self,
            *,
            pricing_rule: PricingRule,
            closing_time,
            product_duration_s: ProductResolution,
    ):
        super().__init__()
        self.sensitivities = TimeStorage(
            step_length=Durations.ONE_HOUR_s,
            steps=24,
        )
        self.pricing_rule = pricing_rule
        self.closing_time = closing_time
        self.product_duration_s = product_duration_s

    @property
    @override
    def product_description(self) -> MarketProduct:
        return MarketProduct(
            id="Q-market",
            power_type=PowerType.REACTIVE_CAPACITIVE,
            one_VA_vt=100.0,  # 0.1 kvar
            pt_in_EUR_vttt=0.0001,  # steps of 0.1 EUR per 0.100 Mvarh
            minimum_price_pt=0,
            maximum_price_pt=30_000,
            maximum_volume_vt=1_000_000,
            market_interval_s=Durations.ONE_DAY_s,
            closing_time_s=self.closing_time,
            # NOTE: Market result delta for Q-market should be larger
            # than for P-market, so they get cleared in the right order
            # even if they close simultaneously.
            market_result_delta_s=30 * Durations.MINUTE_s,
            # first supply start at following midnight
            supply_start_delta_s=Durations.ONE_DAY_s - self.closing_time,
            product_duration_s=self.product_duration_s,
            sensitivity_based_clearing=True,
            pricing_rule=self.pricing_rule,
            may_send_multiple_bids=True,
            supports_all_or_nothing=False,
        )

    def set_sensitivities(
            self, supply_time: int, target_MVar: float, sensitivities: dict[int, float]
    ):
        self.sensitivities[supply_time] = (target_MVar, sensitivities)

    @override
    def clear(self, supply_time: int) -> AuctionResult[Trader]:
        target_MVar, sensitivities = self.sensitivities[supply_time]
        target_var_vt = target_MVar * 1e6 / self.product_description.one_VA_vt
        all_bids = self.bids.pop(supply_time)
        # Only take the non-empty bids here
        sorted_bids = sorted(
            [bid for bid in all_bids if bid[1].amount_vt > 0],
            key=lambda bid: bid[1].price_pt * sensitivities[bid[1].location],
        )
        # Seed the award messages with the empty bids, so that they
        # don't disturb the efficiency price calculations later
        award_messages: list[tuple[Trader, AwardMessage]] = [
            (bidder, AwardMessage.from_bid(bid, 0, 0))
            for (bidder, bid) in all_bids
            if bid.amount_vt == 0
        ]
        marginal_price = 0
        for bidder, bid in sorted_bids:
            sensitivity = sensitivities[bid.location]
            if sensitivity < 0.0:
                raise ValueError(
                    f"cannot clear with negative sensitivity (is {sensitivity}) at "
                    f"node {bid.location}"
                )
            if target_var_vt <= 0.0:
                # No more bids are accepted (we indicate this by
                # returning AwardMessages with awarded_amount=0)
                award_messages.append(
                    (bidder, AwardMessage.from_bid(bid, 0, marginal_price))
                )
            elif (eff_amount := bid.amount_vt * sensitivity) < target_var_vt:
                # Bid can be accepted completely
                award_messages.append(
                    (bidder, AwardMessage.from_bid(bid, bid.amount_vt, bid.price_pt))
                )
                # TODO: Do we need to use the sensitivity-based price
                # for the marginal price here?
                marginal_price = bid.price_pt
                target_var_vt -= eff_amount
            else:
                # Marginal bid (needs to be split)
                award_amount = math.ceil(target_var_vt / sensitivity)
                award_messages.append(
                    (bidder, AwardMessage.from_bid(bid, award_amount, bid.price_pt))
                )
                marginal_price = bid.price_pt
                target_var_vt -= eff_amount
        if self.product_description.pricing_rule == PricingRule.PAY_AS_CLEARED:
            efficiency_price = None
            for _, award_message in award_messages:
                award_message.awarded_price = marginal_price
                award_message.marginal_price_pt = marginal_price
        else:
            logger.info(
                f"efficiency price for Q-auction for day {format_time(supply_time)} "
                f"is determined by {sorted_bids[0]}"
            )
            efficiency_price = sorted_bids[0][1].price_pt
            if efficiency_price is None:
                efficiency_price = 0
            for _, award_message in award_messages:
                award_message.efficiency_price_pt = efficiency_price
                award_message.marginal_price_pt = marginal_price
        if target_var_vt > 0.0:
            problem = (f"Q-demand at {format_time(supply_time)} was not met, "
                       f"{target_var_vt * self.product_description.one_VA_vt / 1e3} kVar remaining.")
            if self.product_description.pricing_rule == PricingRule.PAY_AS_CLEARED:
                # Pay-as-cleared does not produce sensible prices if the
                # demand is not met
                raise Exception(problem)
            else:
                logger.warning(problem)
        return AuctionResult(
            marginal_price_pt=marginal_price,
            efficiency_price_pt=efficiency_price,
            award_messages=award_messages,
        )



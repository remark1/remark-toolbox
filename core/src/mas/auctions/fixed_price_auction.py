from typing import Callable, TypeVar, override

from mas.auctions.base import Auction, AuctionResult
from mas.messages import AwardMessage, MarketProduct, PowerType, PricingRule
from mas.util import Durations

Trader = TypeVar("Trader")


class FixedPriceAuction(Auction[Trader]):
    price_provider: Callable[[int], int]

    def __init__(self, price_provider: Callable[[int], int]):
        super().__init__()
        self.price_provider = price_provider

    @property
    @override
    def product_description(self) -> MarketProduct:
        CLOSING_TIME_s = 15 * Durations.ONE_HOUR_s  # 15:00 each day
        return MarketProduct(
            id="P-market",
            power_type=PowerType.ACTIVE_SUPPLY,
            one_VA_vt=1e5,  # 0.1 MW
            pt_in_EUR_vttt=0.01,  # (steps of 10 ct/MWh)
            minimum_price_pt=-50_000,  # -0.5 EUR / kWh
            maximum_price_pt=300_000,  # 3 EUR / kWh
            maximum_volume_vt=1_000,
            market_interval_s=Durations.ONE_DAY_s,
            closing_time_s=CLOSING_TIME_s,
            market_result_delta_s=2 * Durations.QUARTER_HOUR_s,
            supply_start_delta_s=Durations.ONE_DAY_s - CLOSING_TIME_s,
            product_duration_s=Durations.ONE_HOUR_s,
            sensitivity_based_clearing=False,
            pricing_rule=PricingRule.PAY_AS_CLEARED,
            may_send_multiple_bids=True,
            supports_all_or_nothing=False,
        )

    @override
    def clear(self, supply_time: int) -> AuctionResult[Trader]:
        price = self.price_provider(supply_time)
        auction_result = AuctionResult(
            marginal_price_pt=price,
            award_messages=[
                (
                    sender,
                    AwardMessage.from_bid(
                        bid,
                        awarded_amount=award_amount(bid.amount_vt, bid.price_pt, price),
                        awarded_price=price,
                    ),
                )
                for (sender, bid) in self.bids[supply_time]
            ],
        )
        del self.bids[supply_time]
        return auction_result


def award_amount(bid_amount, bid_price, market_price):
    # Trader wants to sell
    if bid_amount >= 0 and bid_price <= market_price:
        return bid_amount
    # Trader wants to buy
    if bid_amount <= 0 and market_price <= bid_price:
        return bid_amount

    return 0

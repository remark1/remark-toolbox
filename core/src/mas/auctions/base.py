from abc import ABC, abstractmethod
from collections import defaultdict
from collections.abc import Collection
from dataclasses import dataclass
from typing import Generic, TypeVar

from mas.messages import AwardMessage, PlaceBid, MarketProduct

Trader = TypeVar("Trader")


@dataclass
class AuctionResult(Generic[Trader]):
    marginal_price_pt: int
    award_messages: Collection[tuple[Trader, AwardMessage]]
    efficiency_price_pt: int | None = None


class Auction(ABC, Generic[Trader]):
    bids: dict[int, list[tuple[Trader, PlaceBid]]]

    def __init__(self):
        self.bids = defaultdict(list)

    @abstractmethod
    def clear(self, supply_time: int) -> AuctionResult[Trader]: ...

    @property
    @abstractmethod
    def product_description(self) -> MarketProduct: ...

    def validate_bid(self, trader: Trader, bid: PlaceBid):
        desc = self.product_description
        assert bid.product_id == desc.id
        # TODO: Further checks

    def add_bid(self, trader: Trader, bid: PlaceBid):
        self.validate_bid(trader, bid)
        self.bids[bid.supply_start].append((trader, bid))

from __future__ import annotations

import enum
from dataclasses import dataclass
from typing import Any, List, Literal

import mango.messages.codecs as codecs
from mango.messages.codecs import json_serializable
from mas.util import Durations, ProductResolution

codec = codecs.JSON()


def message(cls):
    """Decorator for dataclasses to make them json_serializable and add
    them to our codec."""
    global codec
    adapted_cls = json_serializable(cls, repr=False)
    codec.add_serializer(*adapted_cls.__serializer__())
    return adapted_cls


def recreate_int_keys(str_dict: dict[Any, Any]) -> dict[int, Any]:
    """Due to intermediate serialization as JSON, some of our dicts
    will have their ``int`` keys turned into ``str``. This function
    undoes that.
    """
    return {int(key): value for key, value in str_dict.items()}


# Market-participant-market-operator interface


@message
class PowerType(enum.StrEnum):
    """The type of power being traded."""

    ACTIVE_DEMAND = "active_demand"
    """The market participant buys active power."""
    ACTIVE_SUPPLY = "active_supply"
    """The market participant sells active power."""
    REACTIVE_INDUCTIVE = "reactive_inductive"
    """The market participant sells inductive reactive power."""
    REACTIVE_CAPACITIVE = "reactive_capacitive"
    """The market participant sells capacitive reactive power."""


#@message
class PricingRule(enum.StrEnum):
    """Rules for deciding the awarded price."""

    PAY_AS_BID = "pay_as_bid"
    """The agent gets payed/pays the amount that they bid."""
    PAY_AS_CLEARED = "pay_as_cleared"
    """The agent gets payed/pays the marginal price of the auction."""


@message
@dataclass
class MarketProduct:
    """Description of a single market product."""

    id: Any
    """Identifier for the market product chosen by the market operator.
    Market participants must use this unchanged when creating bids.
    """
    power_type: PowerType
    """The type of power traded with this product."""
    one_VA_vt: float
    """The tick size for volume for this product. This is given as a
    multiple of the physical unit for this product (Wh for active
    power, VArh for reactive power; we use VAh in the variable name to
    encompass both of them). All other places use integers to specify a
    multiple of this tick size.
    """
    pt_in_EUR_vttt: float
    """The price tick size for this product, given in EUR per volume
    tick per price tick.
    """
    minimum_price_pt: int
    """The minimum price for this product (in ticks); can be negative.
    """
    maximum_price_pt: int
    """The maximum price for this product (in ticks)."""
    maximum_volume_vt: int
    """The maximum volume for this product (in ticks)."""

    market_interval_s: int
    """The length of a market interval (i.e. how often the market runs)
    in mosaik time steps. This number will be used as a modulus for
    recurring events.
    """
    closing_time_s: int
    """The closing time for this product, given modulo the market
    interval. In other words, the market closes at (the end of) each
    mosaik time step `time` such that
        time % market_interval == closing_time
    """
    market_result_delta_s: int
    """The amount of time between market closing and publication of
    the market results (in mosaik time steps).
    """
    supply_start_delta_s: int
    """The amount of time between market closing and the first supply
    start for this market interval (in mosaik time steps).
    """
    product_duration_s: ProductResolution
    """The duration of each supply interval. This should be a divisor
    of the market_interval. Starting from the time t_0 specified by
    supply_start_delta, the supply intervals will be
       t_0, t_0 + product_duration, t_0 + 2 * product_duration, ...
    until the supply start of the next market interval.
    """

    sensitivity_based_clearing: bool
    """Whether auctions for this product are cleared based on
    sensitivity or not.
    """
    pricing_rule: PricingRule
    """The pricing rule for this product."""
    may_send_multiple_bids: bool
    """Whether an agent may send multiple bids for the same supply
    interval. (This allows for the creation of piecewise constant
    price functions.)
    """
    supports_all_or_nothing: bool
    """Whether an agent may mark bids as all-or-nothing."""


@message
@dataclass
class RegisterTrader:
    """Sent by a market participant agent to inform a market that the
    agents wants to participate in that market. The market should
    respond with a MarketRulesMessage.

    - *Frequency*: Once per simulation.
    - *Expected answer*: `MarketRulesMessage`.
    """

    name: str
    """The name of the market participant. (Might be used for
    debugging purposes.)
    """


@message
@dataclass
class MarketRulesMessage:
    """Sent by a market agent in response to a RegistrationMessage to
    inform the registrating agent about the available products.

    - *Frequency*: Once per simulation.
    - *Expected answer*: none.
    """

    market_products: List[MarketProduct]
    """The list of products traded on this market."""


@message
@dataclass
class PlaceBid:
    """Sent by a market participant to place a bid in an auction. The
    market operator will reply with an `AwardMessage` in a future step
    (cf. `market_result_delta` in `MarketProduct`).
    
    - *Frequency*: Up to once per market clearing.
    - *Expected answer*: `AwardMessage` (potentially in a later step)
    """

    id: Any
    """An identifier chosen by the market participant for this bid. The
    market will send its AwardMessage with this id.
    """
    product_id: Any
    """The id of the `MarketProduct` this bid is for."""
    supply_start: int
    """The supply start for this bid. This distinguishes the different
    auctions for this product.
    """
    price_pt: int
    """The price for this bid in ticks (cf. the product's
    `MarketProduct`). Must lie between the `MarketProduct`'s
    `minimum_price` and `maximum_price`.
    """
    amount_vt: int
    """The amount in ticks (cf. the product's `MarketProduct`). Must be
    positive and less than the `MarketProduct`'s `maximum_amount`.
    """
    is_all_or_nothing: bool
    """Whether this bid may be split (i.e. awarding only some of the
    amount.)
    """
    location: int
    """The ID of the pandapower node for this bid. (This should be the
    ID that the network operator uses for it's calculations.)
    """


@message
@dataclass(kw_only=True)
class AwardMessage:
    """Response to a :cls:`BidMessage` informing the bidding agent whether
    (or to what degree) its bid was accepted.

    - *Frequency*: Once per clearing of relevant market.
    - *Expected answer*: none.
    """

    bid_id: Any
    """The `id` of the `BidMessage` this responds to."""
    product_id: Any
    supply_start: int
    location: int
    awarded_amount_vt: int
    """The amount that was awarded. This will lie between 0 and the
    bid's `amount`. (An awarded amount of 0 indicactes that the bid
    was rejected.)
    """
    awarded_price: int
    """The actual price awarded for this bid. In case of a rejected bid,
    this will be (an approximation of) the marginal price for this
    auction.
    """

    marginal_price_pt: int | None = None
    efficiency_price_pt: int | None = None

    @classmethod
    def from_bid(
        cls, bid: PlaceBid, awarded_amount: int, awarded_price: int
    ) -> AwardMessage:
        """Create an AwardMessage by copying over basic information from
        the given bid.
        """
        return AwardMessage(
            bid_id=bid.id,
            product_id=bid.product_id,
            supply_start=bid.supply_start,
            location=bid.location,
            awarded_amount_vt=awarded_amount,
            awarded_price=awarded_price,
        )


# Market operator to grid operator communication


@message
@dataclass(kw_only=True)
class ImpartMarketSummary:
    """Sent from Market to GridOperator to inform about the result of
    the P-market an request resulting sensitivities.
    
    - *Frequency*: Once per P-market clearing.
    - *Expected answer*: `ImpartQSensitivitiesAndDemand`.
    """
    supply_time: int
    p_MW_by_bus: dict[int, float]
    """Mapping from load index to p of that load"""


@message
@dataclass
class ImpartQSensitivitiesAndDemand:
    """Sent from GridOperator to Market to inform about the 
    sensitivities and demand resulting from the P-market
    
    - *Frequency*: Once per P-market clearing.
    - *Expected answer*: none.
    """
    supply_time: int
    target: float
    sensitivities: dict[int, float]
    """Mapping from load index to sensitivity"""


# Other interfaces


@message
@dataclass
class RegisterUnit:
    """Sent from UnitAgent to Trader to register it as the trader's
    asset.
    
    - *Frequency*: Once per simulation.
    - *Expected answer*: none.
    """
    pass


@message
@dataclass
class RegisterBattery:
    """Sent from BatteryAgent to Trader to register it as the trader's
    battery.
    
    - *Frequency*: Once per simulation.
    - *Expected answer*: none.
    """
    pass


@message
@dataclass(kw_only=True)
class RequestUnitFlexibility:
    """Sent from Trader to unit agent to request the agent's
    flexibility.
    
    - *Frequency*: Once per day.
    - *Expected answer*: `ImpartUnitFlexibility`.
    """
    id: Any
    time: int
    resolution_s: Literal[
        Durations.QUARTER_HOUR_s,
        Durations.ONE_HOUR_s,
        Durations.FOUR_HOURS_s,
    ]


@message
@dataclass(kw_only=True)
class RequestBatteryFlexibility:
    """Sent from Trader to BatteryAgent to request the agent's
    flexibility.
    
    - *Frequency*: Once per day.
    - *Expected answer*: `ImpartBatteryFlexibility`.
    """
    id: Any
    time: int


@message
@dataclass(kw_only=True)
class ImpartUnitFlexibility:
    """Sent from UnitAgent for a generating unit (PV or
    wind) to Trader, imparting the unit's flexibility over the next 24h
    to the trader.
    
    - *Frequency*: Once per day.
    - *Expected answer*: none.
    """
    id: Any
    time: int
    p_gen_max_W: list[float]
    """(resolution as requested by RequestUnitFlexibility)"""
    p_cost_EUR_Wh: float
    q_cost_EUR_varh: float


@message
@dataclass(kw_only=True)
class ImpartBatteryFlexibility:
    """Sent from BatteryAgent to Trader, informing the
    latter about the batteries parameters and state of charge.
    
    - *Frequency*: Once per day.
    - *Expected answer*: none.

    TODO: Maybe only send the parameters once?
    """
    id: Any
    time: int
    soc: float
    eff_ch: float
    eff_dis: float
    capa_Wh: float
    p_charge_max_W: float
    p_discharge_max_W: float
    fce_max: float


@message
@dataclass(kw_only=True)
class ObligationMessage:
    """Sent from Trader to unit agent to inform it
    about the obligations resulting from the market.
    
    - *Frequency*: Once per time step.
    - *Expected answer*: none.
    """
    supply_start: int
    p_amount_W: float
    q_amount_var: float

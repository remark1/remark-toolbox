from __future__ import annotations

import enum
import math
from random import Random
from typing import TYPE_CHECKING, Literal, TypeAlias

if TYPE_CHECKING:
    from mas.messages import MarketProduct


class Durations(enum.IntEnum):
    MINUTE_s = 60
    QUARTER_HOUR_s = MINUTE_s * 15
    ONE_HOUR_s = MINUTE_s * 60
    FOUR_HOURS_s = ONE_HOUR_s * 4
    ONE_DAY_s = ONE_HOUR_s * 24


ProductResolution: TypeAlias = Literal[
    Durations.QUARTER_HOUR_s,
    Durations.ONE_HOUR_s,
    Durations.FOUR_HOURS_s,
]


STEP_SIZE_s: int = Durations.QUARTER_HOUR_s
DAILY_STEPS: int = Durations.ONE_DAY_s // STEP_SIZE_s


def EUR_MVAh_to_pt(price, product: MarketProduct):
    return (
        price
        * 1e-6
        * product.one_VA_vt
        * product.product_duration_s
        / 3600
        / product.pt_in_EUR_vttt
    )


def pt_to_EUR_MVAh(price, product: MarketProduct):
    return (
        price
        * 1e6
        * 3600
        * product.pt_in_EUR_vttt
        / product.one_VA_vt
        / product.product_duration_s
    )


def format_time(time: int, print_seconds: bool = False) -> str:
    day, time = divmod(time, 24 * 60 * 60)
    hour, time = divmod(time, 60 * 60)
    minute, second = divmod(time, 60)
    if print_seconds:
        return f"{day}, {hour:0>2}:{minute:0>2}:{second:0>2}"
    else:
        assert second == 0
        return f"{day}, {hour:0>2}:{minute:0>2}"


def noisify(precise_value: float, beta: float, agent_random: Random) -> float:
    """Add some Gaussian noise to `precise_value`. More specifically,
    `precise_value` is multiplied by exp(beta * N(0,1)) where N(0,1) is
    a normally distributed random variable."""
    return precise_value * math.exp(beta * agent_random.gauss())
"""This module implements a simply naive bidding strategies for placing
bids on an active and a reactive power market.

The strategy is based on optimizing the total profit accross both
markets given projections for the prices on those markets and the costs
for producing the two types of power.

The parameters required for the optimization are given in the
``BidParams`` typed dict, but they are given as keyword arguments to the
optimization function ``optimize_bid``.
"""
from functools import partial
from typing import TypedDict

import numpy as np
from scipy.optimize import minimize, Bounds, NonlinearConstraint
from typing_extensions import Unpack


class BidParams(TypedDict):
    price_P: float
    """The projected price on the active power market (per unit)"""
    cost_P: float
    """The cost (per unit) for providing active power"""
    market_size_Q: float
    """The projected requested amount on the reactive power market"""
    min_bid_Q: float
    """The minimum bid on the reactive power market"""
    efficient_price_Q: float
    """The projected efficient price (per unit) on the reactive power
    market. This is the cheapest price offered.
    """
    marginal_price_Q: float
    """The projected marginal price (per unit) on the reactive power
    market. This is the highest price still accepted.
    """
    cost_Q: float
    """The cost (per unit) for providing reactive power"""
    cap_amount: float
    """The joint capacity for active and reactive power. Currently,
    the Pythagorean sum sqrt(P^2 + Q^2) must lie within the inverval
    [0, cap_amount].
    """


# The actual function performing the optimization based on the given
# parameters.


def optimize_bid(**params: Unpack[BidParams]):
    """Find the optimal bid according to the naive strategy, given the
    price projections and costs given in the ``params``.

    :return: A tuple consisting of
        - the expected profit
        - an np.array consisting of the P-amount, Q-amount and Q-price
          (in the units used in the ``params`` data structure)
        - ``"accept_Q"`` or ``"reject_Q"`` depending on whether the
          projected profit based on a bid where acceptance of the bid
          on the Q-market is excepted
    """
    initial_bid = np.zeros(3)
    # Optimize the Q-accept and Q-reject case separately. Later, we will
    # choose the better of the two results.
    res_accepted = minimize(
        partial(neg_profit_accepted, **params),
        initial_bid,
        method="trust-constr",
        jac=partial(jac_profit_accepted, **params),
        hessp=partial(hess_profit_accepted, **params),
        bounds=Bounds(0, np.inf),
        constraints=[
            amount_constraint(**params),
            accept_Q_constraint(**params),
        ],
    )
    res_rejected = minimize(
        partial(neg_profit_rejected, **params),
        initial_bid,
        method="trust-constr",
        jac=partial(jac_profit_rejected, **params),
        hessp=partial(hess_profit_rejected, **params),
        bounds=Bounds(0, np.inf),
        constraints=[
            amount_constraint(**params),
            reject_Q_constraint(**params),
        ],
    )
    if res_accepted.fun <= res_rejected.fun:
        return (-res_accepted.fun, res_accepted.x, "accept_Q")
    else:
        return (-res_rejected.fun, res_rejected.x, "reject_Q")


P, Q = slice(0, 1), slice(1, 3)  # for splitting the bid np.array into P and Q part
Am, Pr = 0, 1  # amount, price (for accessing the bid np.array)

# These functions give the negative profit on the P market for the given
# bid. Currently, the bid only consists of the amount, as the optimal
# bid price on the PAC market is the cost. (The sign is negative as
# we want to maximize the profit but scipy only provides a minimize
# function.)
# (The jac_ and hess_ versions give the Jacobian and Hessian of the
# function, respectively. They are needed by the optimization
# algorithms.)


def neg_profit_P(bid, price_P, cost_P, **other_params):
    return -bid[Am] * (price_P - cost_P)


def jac_profit_P(bid, price_P, cost_P, **other_params):
    return np.array(
        [
            -(price_P - cost_P),
        ]
    )


def hess_profit_P(bid, p, **other_params):
    return np.zeros(1)


# These functions give the negative profit on the Q market.


def neg_profit_Q_accepted(bid, cost_Q, **other_params):
    return -bid[Am] * (bid[Pr] - cost_Q)


def jac_profit_Q_accepted(bid, cost_Q, **other_params):
    return np.array(
        [
            -(bid[Pr] - cost_Q),
            -bid[Am],
        ]
    )


def hess_profit_Q_accepted(bid, p, **params):
    return np.array(
        [
            -p[Pr],
            -p[Am],
        ]
    )


# These functions give the total profit provided that the Q-market bid
# is accepted. Splitting up the accept and reject cases is necessary
# because the optimization will try to get as close as possible to the
# "edge" and then usually fall off. As the reject area has its own
# local optima, only those will be found.
# The solution is to encode the edge as a constraint instead and to
# optimize both cases separately. Then, the best solution among the
# two resulting candidates can be chosen.


def neg_profit_accepted(bid, **params):
    return neg_profit_P(bid[P], **params) + neg_profit_Q_accepted(bid[Q], **params)


def jac_profit_accepted(bid, **params):
    return np.concatenate(
        (
            jac_profit_P(bid[P], **params),
            jac_profit_Q_accepted(bid[Q], **params),
        )
    )


def hess_profit_accepted(bid, p, **params):
    return np.concatenate(
        (
            hess_profit_P(bid[P], p[P], **params),
            hess_profit_Q_accepted(bid[Q], p[Q], **params),
        )
    )


# These functions give the total profit provided that the Q-market bid
# is rejected.


def neg_profit_rejected(bid, **params):
    return neg_profit_P(bid[P], **params)


def jac_profit_rejected(bid, **params):
    return np.concatenate((jac_profit_P(bid[P], **params), np.zeros_like(bid[Q])))


def hess_profit_rejected(bid, p, **params):
    return np.concatenate(
        (hess_profit_P(bid[P], p[P], **params), np.zeros_like(bid[Q]))
    )


# These are the constraints for the the total capacity. We assume for
# now that the capacity can be split arbitrarily provided that the
# Pythagorean sum of both parts is below the bound.
# As a small optimization, we consider remove the square root from the
# sum calculation and square the total capacity instead.


def func_amount(bid, **other_params):
    return [bid[P][Am] ** 2 + bid[Q][Am] ** 2]


def jac_amount(bid, **other_params):
    return [[2 * bid[P][Am], 2 * bid[Q][Am], 0]]


def hess_amount(bid, v, **other_params):
    return np.array(
        [
            [2 * v[0], 0, 0],
            [0, 2 * v[0], 0],
            [0, 0, 0],
        ]
    )


def amount_constraint(cap_amount, **other_params) -> NonlinearConstraint:
    return NonlinearConstraint(
        partial(func_amount, **other_params),
        0,
        cap_amount**2,
        jac=partial(jac_amount, **other_params),
        hess=partial(hess_amount, **other_params),
    )


# This is the constraint encoding the edge between the accept and
# reject areas for the Q-market bid.


def effective_price_Q(
    bid, market_size_Q, min_bid_Q, efficient_price_Q, marginal_price_Q, **other_params
):
    market_share = (market_size_Q - bid[Am]) / (market_size_Q - min_bid_Q)
    return efficient_price_Q + (marginal_price_Q - efficient_price_Q) * market_share


def func_accept(bid, **params):
    return [effective_price_Q(bid[Q], **params) - bid[Q][Pr]]


def jac_accept(
    bid, marginal_price_Q, efficient_price_Q, market_size_Q, min_bid_Q, **other_params
):
    return [
        [0, -(marginal_price_Q - efficient_price_Q) / (market_size_Q - min_bid_Q), -1]
    ]


def hess_accept(bid, v, **params):
    return np.zeros((3, 3))


def accept_Q_constraint(**params) -> NonlinearConstraint:
    return NonlinearConstraint(
        partial(func_accept, **params),
        0,
        np.inf,
        jac=partial(jac_accept, **params),
        hess=partial(hess_accept, **params),
    )


def reject_Q_constraint(**params) -> NonlinearConstraint:
    return NonlinearConstraint(
        partial(func_accept, **params),
        -np.inf,
        0,
        jac=partial(jac_accept, **params),
        hess=partial(hess_accept, **params),
    )

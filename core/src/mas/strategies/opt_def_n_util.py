import math
from dataclasses import dataclass, field, fields
from typing import Literal

from loguru import logger

from pyomo.core import Constraint, value
from pyomo.core.expr import evaluate_expression
from pyomo.environ import units as u
from pyomo.environ import Var

# cos phi that specifies the obligatory amount of Q that has to be provided according to VDE technical connection rules
phi = math.acos(0.95)


@dataclass
class MarketParams:
    q_market_clearing: Literal["pay_as_bid", "pay_as_cleared"]
    """Q-market pricing is either PAC or PAB"""
    q_volume_tick: float
    """Tick size of allowed bid volumes at Q-market, in VAR.
    This corresponds also to the minimum bid."""
    q_price_tick: float
    """Tick size for prices, in €/(q volume tick * q time tick)"""
    q_maximal_price: int | None = None
    """Maximal price allowed at the Q-market (in price ticks)"""
    q_time_tick: float = field(default_factory=lambda: 1.0 * u.h)
    """How long each market interval is, in h"""
    p_time_tick: float = field(default_factory=lambda: 1.0 * u.h)
    """How long each market interval is, in h"""
    p_price_tick: float = field(default_factory=lambda: 0.01 * u.EUR)
    """Tick size for prices, in €/(p volume tick * p time tick)"""
    p_volume_tick: float = field(default_factory=lambda: 100000 * u.W)  # <-0.1 MW
    """Tick size of allowed bid volumes at P-market, in W
    This corresponds also to the minimum bid."""


@dataclass
class MarketForecastParams:
    p_clear_price: list[float]
    """expected prices at the P-market for each time step in (in p_price_tick) - (resolution p_time_ticks)"""
    q_demand: list[int] | None = None
    """expected demand of reactive power by the grid operator (in q_volume_tick)- (resolution p_time_ticks)"""
    q_clear_price: list[float] | None = None
    """expected maximum accepted bids on Q-market (clearing prices) (in q_price_tick) - (resolution q_time_ticks)"""
    q_eff_price: list[float] | None = None
    """expected minimum accepted bids on Q-market (efficiency prices) (in q_price_tick) - (resolution q_time_ticks)"""


@dataclass
class FlexParams:
    s_max: float
    """Maximum allowed apparent power at the inverter (in VA)"""
    gen_max: list[float]
    """maximum generation of active power in each time step (in W) - (resolution 0.25h)"""
    cons_max: list[float]
    """maximum consumption of active power in each time step (in W) - (resolution 0.25h)"""
    costs_p: float
    """cost for active power(in p_price_tick)"""
    costs_q: float
    """cost for reactive power (in q_price_tick)"""
    soc: float | None = 0
    """state of charge of a battery storage in [0,1]"""
    eff_ch: float | None = 0
    """charging efficiency of a battery storage in [0,1]"""
    eff_dis: float | None = 0
    """discharging efficiency of a battery storage in [0,1]"""
    capa: float = field(default_factory=lambda: 0 * u.Wh)
    """energy capacity of a battery storage in Wh"""
    p_bat_discharge_max: float = field(default_factory=lambda: 0 * u.W)
    """maximal charging power of a battery storage in W"""
    p_bat_charge_max: float = field(default_factory=lambda: 0 * u.W)
    """maximal discharging power of a battery storage in W"""
    fce_max: float | None = 0
    """maximum allowed full cycle equivalents per day"""


def get_value(x, unit=None):
    if isinstance(x, float) or isinstance(x, int):
        return x  # Short-cut, so units don't spread everywhere
    return evaluate_expression(u.convert(x, to_units=unit))


def is_feasible(model, solver):
    """Checks to see if the algebraic model is feasible in its current state.

    Checks variable bounds and active constraints. Not for use with
    untransformed GDP models.

    """
    constraint_tolerance = solver.options["numerics/feastol"]
    variable_tolerance = solver.options["numerics/epsilon"]
    logger.debug('Checking if model is feasible.')
    for constr in model.component_data_objects(
            ctype=Constraint, active=True, descend_into=True
    ):
        # Check constraint lower bound
        if constr.lower is not None and (
                value(constr.lower) - value(constr.body) >= constraint_tolerance
        ):
            logger.info(
                '%s: body %s < LB %s'
                % (constr.name, value(constr.body), value(constr.lower))
            )
            return False
        # check constraint upper bound
        if constr.upper is not None and (
                value(constr.body) - value(constr.upper) >= constraint_tolerance
        ):
            logger.info(
                '%s: body %s > UB %s'
                % (constr.name, value(constr.body), value(constr.upper))
            )
            return False
    for var in model.component_data_objects(ctype=Var, descend_into=True):
        # Check variable lower bound
        if var.has_lb() and value(var.lb) - value(var) >= variable_tolerance:
            logger.info(
                '%s: value %s < LB %s' % (var.name, value(var), value(var.lb))
            )
            return False
        # Check variable upper bound
        if var.has_ub() and value(var) - value(var.ub) >= variable_tolerance:
            logger.info(
                '%s: value %s > UB %s' % (var.name, value(var), value(var.ub))
            )
            return False
    logger.info('Model is feasible.')
    return True


def get_feasible_p(p_w, s_max):
    p = get_value(p_w, u.W)
    potential_s = p + math.tan(phi) * p
    bound = min(potential_s, get_value(s_max, u.W))
    if potential_s != 0:
        feasible_percentage = bound / potential_s
    else:
        feasible_percentage = 1
    return feasible_percentage * p


def get_p_inv_max(s_max):
    """
    get the maximal p that can be reached at the converter according to s_max while also providing q_tar
    """
    return s_max * math.cos(phi)


def get_q_tar_max(s_max):
    """
    Calculate the maximal possible q_tar that could be reached if s_max is completely used for p and q_tar
    Args:
        s_max ():
    """
    return s_max * math.sin(phi)


def get_feasible_free_p(gen_max, s_max, p_volume_tick, p_tick_factor):
    p_gen_max = [int((get_feasible_p(p, s_max)) / get_value(p_volume_tick, u.W)) for p in
                 gen_max]
    return get_max_p_for_product_duration(p_gen_max, p_tick_factor)


def get_free_q(p_w, s_max):
    # p_gen_max_w = [get_feasible_p(p, phi, s_max) for p in
    #                gen_max]
    total_q = math.sqrt(get_value(s_max, u.W) ** 2 - p_w ** 2)
    q_tar = abs(p_w) * math.tan(phi)
    return total_q - q_tar


def get_feasible_free_q(p_w, s_max, q_volume_tick, q_tick_factor):
    free_q = [int(get_free_q(p, s_max) / get_value(q_volume_tick, u.W)) for p in p_w]
    return get_max_p_for_product_duration(free_q, q_tick_factor)


def get_max_p_for_product_duration(p, ticks):
    i = 0
    curtailed_p = []
    while i <= len(p) - ticks:
        max_p = min(p[i:i + ticks])
        curtailed_p += [max_p for _ in range(ticks)]
        i += ticks
    return curtailed_p

import copy
import io
import math
import sys
import time
from dataclasses import dataclass, field, fields
from enum import Enum, Flag, auto
from io import TextIOBase
from pprint import pformat, pprint
from typing import Any, NotRequired, TypedDict

from loguru import logger
from pyomo.core import Constraint
from pyomo.core.expr import NPV_ProductExpression
from pyomo.environ import (
    Binary,  # type: ignore
    ConcreteModel,
    Integers,  # type: ignore
    NonNegativeReals,  # type: ignore
    NonNegativeIntegers,  # type: ignore
    Objective,
    Param,
    RangeSet,
    Reals,  # type: ignore
    SolverFactory,
    SolverStatus,
    TerminationCondition,
    Var,
    maximize,
    value
)
from pyomo.environ import units as u
from pyomo.util.check_units import assert_units_consistent
from mas.strategies.opt_def_n_util import (
    FlexParams,
    MarketForecastParams,
    MarketParams,
    get_value,
    is_feasible,
    get_feasible_free_p,
    get_feasible_free_q,
    get_p_inv_max,
    get_q_tar_max
)

u.load_definitions_from_strings(["EUR = [currency]"])

# cos phi that specifies the obligatory amount of Q that has to be provided according to VDE technical connection rules
phi = math.acos(0.95)


class Markets(Flag):
    P = auto()
    Q = auto()


class PricingRule(Enum):
    PAC = auto()
    PAB = auto()


def create_model(
        optimized_markets: Markets,
        market: MarketParams,
        flex: FlexParams,
        forecast: MarketForecastParams,
        num_intervals: int,
        fixed_active_power: list = None
):
    model = ConcreteModel()
    # Upper bound has - 1 as both bounds are inclusive for RangeSet
    model.T = RangeSet(0, num_intervals - 1)
    model.s_max = Param(domain=Reals, units=u.W, initialize=flex.s_max)
    # calculate initialization values that market all renewable generation to p and the free q amount to q

    p_tick_factor = round(get_value(market.p_time_tick / (0.25 * u.h)))
    if fixed_active_power is not None:
        initial_p = fixed_active_power
        initial_q_tar = [(p * get_value(market.p_volume_tick, u.W)) * math.tan(phi) * u.W for p in initial_p]
    else:
        initial_p = get_feasible_free_p(flex.gen_max, flex.s_max, market.p_volume_tick, p_tick_factor)
        initial_q_tar = [(p * get_value(market.p_volume_tick, u.W)) * math.tan(phi) * u.W for p in initial_p]
    if Markets.P in optimized_markets:
        model.p_clear_pr = Param(model.T, domain=Reals, initialize=forecast.p_clear_price)
        model.p_cost = Param(domain=Reals, initialize=flex.costs_p)
        p_inv_bounds = math.floor(get_p_inv_max(flex.s_max) / get_value(market.p_volume_tick, u.W))
        # p_amount at inverter in  volume ticks
        model.p_am_inv = Var(model.T, domain=Integers, initialize=initial_p, bounds=(-p_inv_bounds, p_inv_bounds))

        # p_amount of generating unit in volume ticks
        model.p_am_gen = Var(model.T, domain=NonNegativeReals, initialize=initial_p,
                             bounds=([(0, p) for p in initial_p]))
        # p_amount from discharging of battery storage in volume ticks
        model.p_am_bat_gen = Var(
            model.T,
            within=NonNegativeReals,
            bounds=(0, get_value(flex.p_bat_discharge_max / market.p_volume_tick)),
            initialize=[0 for _ in range(num_intervals)],
        )
        # p_amount from charging of battery storage in volume ticks
        model.p_am_bat_cons = Var(
            model.T,
            within=NonNegativeReals,
            bounds=(0, get_value(flex.p_bat_charge_max / market.p_volume_tick)),
            initialize=[0 for _ in range(num_intervals)],
        )
        model.is_charging = Var(model.T, within=Binary, initialize=[0 for _ in range(num_intervals)])
        model.soc = Var(model.T, within=NonNegativeReals, bounds=(0, 1),
                        initialize=[flex.soc for _ in range(num_intervals)]
                        )
        model.p_pr = Param(model.T, domain=Integers, mutable=True, initialize=flex.costs_p)
        model.p_gen_max = Param(model.T, domain=Reals, initialize=[f / market.p_volume_tick for f in flex.gen_max])

        # model.p_gen_max = Param(model.T, domain=Reals, units=u.W, initialize=flex.gen_max)

        # model.p_cons_max = Param(model.T, domain=Reals, mutable=True, units=u.W, initialize=flex.cons_max)

        @model.Constraint(model.T)
        def p_upper_constraint(m, t):
            return m.p_am_gen[t] <= m.p_gen_max[t]  # / market.p_volume_tick

        # @model.Constraint(model.T)
        # def p_lower_constraint(m, t):
        #     return m.p_am_gen[t] >= -m.p_cons_max[t] / market.p_volume_tick

        @model.Constraint(model.T)
        def p_converter_constraint(m, t):
            return m.p_am_inv[t] == m.p_am_gen[t] + m.p_am_bat_gen[t] * (1 - m.is_charging[t]) - m.p_am_bat_cons[t] * \
                m.is_charging[t]

        @model.Constraint(model.T)
        def p_tick_rule(m, t):
            if (t + 1) % p_tick_factor != 0 and t <= len(m.T) - 1:
                return m.p_am_inv[t] == m.p_am_inv[t + 1]
            return Constraint.Skip

        if get_value(flex.capa, u.Wh) != 0:

            @model.Constraint(model.T)
            def soc_constraint(m, t):
                if t == 0:
                    return m.soc[t] == flex.soc + ((m.p_am_bat_cons[t] * flex.eff_ch * m.is_charging[t]) - (
                            m.p_am_bat_gen[t] / flex.eff_dis) * (1 - m.is_charging[t])) * (
                            (market.p_volume_tick * (0.25 * u.h)) / flex.capa)
                return (m.soc[t] == m.soc[t - 1] + ((m.p_am_bat_cons[t] * flex.eff_ch * m.is_charging[t]) - (
                        m.p_am_bat_gen[t] / flex.eff_dis) * (1 - m.is_charging[t])) * (
                                (market.p_volume_tick * (0.25 * u.h)) / flex.capa))

            @model.Constraint(model.T)
            def max_power_rule(m, t):
                return m.p_am_bat_gen[t] <= get_value(
                    flex.p_bat_discharge_max / market.p_volume_tick
                ) * (1 - m.is_charging[t])

            @model.Constraint(model.T)
            def min_power_rule(m, t):
                return (
                        m.p_am_bat_cons[t]
                        <= get_value(flex.p_bat_charge_max / market.p_volume_tick)
                        * m.is_charging[t]
                )

            @model.Constraint()
            def cycle_constraint(m):
                return sum([(
                                    abs(m.p_am_bat_gen[t] * (1 - m.is_charging[t])
                                        - m.p_am_bat_cons[t] * m.is_charging[t]) *
                                    (market.p_volume_tick * (0.25 * u.h))) / (2 * flex.capa) for t in
                            m.T]) <= flex.fce_max

        model.q_tar = Var(model.T, domain=Reals, initialize=initial_q_tar, units=u.W,
                          bounds=(0, get_q_tar_max(flex.s_max)))

        @model.Constraint(model.T)
        def tar_constraint(m, t):
            return m.q_tar[t] == math.tan(phi) * (abs(m.p_am_inv[t]) * market.p_volume_tick)
    else:
        model.p_am_inv = Param(model.T, domain=Integers, initialize=initial_p)
        model.q_tar = Param(model.T, domain=Reals, units=u.W, initialize=initial_q_tar)
    if Markets.Q not in optimized_markets:
        model.q_am = Param(model.T, domain=NonNegativeIntegers, initialize=0)
        # bounds=(0, int(flex.s_max / get_value(market.q_volume_tick, u.W))))
    else:
        q_tick_factor = round(get_value(market.q_time_tick / (0.25 * u.h)))
        # don't tell a battery to do only q
        if all(p == 0 for p in initial_p):
            initial_q = [0 for _ in model.T]
        else:
            initial_q = get_feasible_free_q([p * get_value(market.p_volume_tick, u.W) for p in initial_p], flex.s_max,
                                            market.q_volume_tick, q_tick_factor)
            # don't aim to bid more than the total demand at the market
            initial_q = [min(forecast.q_demand[t], initial_q[t]) for t in model.T]
        model.q_am = Var(model.T, domain=NonNegativeIntegers, initialize=initial_q,
                         bounds=(0, math.floor(flex.s_max / get_value(market.q_volume_tick, u.W))))

    @model.Constraint(model.T)
    def s_max_constraint(m, t):
        return (
                (m.p_am_inv[t] * market.p_volume_tick) ** 2
                + (m.q_am[t] * market.q_volume_tick + m.q_tar[t]) ** 2
        ) <= m.s_max ** 2

    if Markets.Q in optimized_markets:
        model.q_cost = Param(domain=Reals, initialize=flex.costs_q)
        model.q_clear_pr = Param(model.T, domain=Reals, initialize=forecast.q_clear_price)

        @model.Constraint(model.T)
        def q_tick_rule_am(m, t):
            if (t + 1) % q_tick_factor != 0 and t <= len(m.T) - 1:
                return m.q_am[t] == m.q_am[t + 1]
            return Constraint.Skip

        if market.q_market_clearing == "pay_as_bid":
            model.q_pr = Var(model.T, domain=NonNegativeIntegers, initialize=0, bounds=(0, 1e6))
            model.q_demand = Param(model.T, domain=Integers, initialize=forecast.q_demand)
            model.q_eff_pr = Param(model.T, domain=Reals, initialize=forecast.q_eff_price)
            initial_k = [(forecast.q_demand[t] - initial_q[t]) / (forecast.q_demand[t] - 1) for t in model.T]
            k_bounds_1 = [(forecast.q_demand[t]) / (forecast.q_demand[t] - 1) for t in model.T]
            k_bounds_2 = [(forecast.q_demand[t] - flex.s_max / get_value(market.q_volume_tick, u.W)) / (
                    forecast.q_demand[t] - 1) for t in model.T]
            k_bounds = [(min(k_bounds_1[t], k_bounds_2[t], 0), max(k_bounds_1[t], k_bounds_2[t])) for t in model.T]
            # Expected market share of a Q bid
            model.k = Var(model.T, domain=Reals, initialize=initial_k, bounds=k_bounds)

            @model.Constraint(model.T)
            def k_definition(m, t):
                if m.q_demand[t] == 0 or m.q_demand[t] == 1:
                    return m.k[t] == 0
                return m.k[t] == (m.q_demand[t] - m.q_am[t]) / (m.q_demand[t] - 1)

            initial_rel_q_price = [
                forecast.q_eff_price[t] + (forecast.q_clear_price[t] - forecast.q_eff_price[t]) * initial_k[t] for t in
                model.T]
            # rel_q_price_bounds = [
            #     (forecast.q_eff_price[t],
            #      math.ceil(
            #          forecast.q_eff_price[t] + (forecast.q_clear_price[t] - forecast.q_eff_price[t]) * k_bounds[t][1]))
            #     for t
            #     in
            #     model.T]
            # in_bounds = [rel_q_price_bounds[t][0] <= initial_rel_q_price[t] <= rel_q_price_bounds[t][1] for t in
            #              model.T]
            model.q_rel_pr = Var(model.T, domain=Reals, initialize=initial_rel_q_price,  # bounds=rel_q_price_bounds
                                 )

            @model.Constraint(model.T)
            def q_rel_pr_definition(m, t):
                return (
                        m.q_rel_pr[t]
                        == m.q_eff_pr[t] + (m.q_clear_pr[t] - m.q_eff_pr[t]) * m.k[t]
                )

            @model.Constraint(model.T)
            def q_pr_pab_constraint(m, t):
                return m.q_pr[t] <= m.q_rel_pr[t]

            @model.Constraint(model.T)
            def q_tick_rule_pr(m, t):
                if (t + 1) % q_tick_factor != 0 and t <= len(m.T) - 1:
                    return m.q_pr[t] == m.q_pr[t + 1]
                return Constraint.Skip

        else:
            model.q_pr = Param(model.T, domain=NonNegativeIntegers, initialize=flex.costs_q)  # , bounds=(0, 1e6))

            # @model.Constraint(model.T)
            # def q_pr_pac_constraint(m, t):
            #     return m.q_pr[t] == m.q_cost

    return model


def add_objective(
        model,
        optimized_markets: Markets,
        market: MarketParams,
        flex: FlexParams,
        avg_p_price: float
):
    """add the fitting objective"""

    def p_profit(m):
        """profit on p-market"""
        return sum(
            (m.p_clear_pr[t] - m.p_cost) * market.p_price_tick * m.p_am_inv[t] / (market.p_time_tick / (0.25 * u.h)) -
            m.q_tar[t] * 0.0000000000000001 * u.EUR / u.W
            for t in m.T
        )

    def q_profit_pac(m):
        """profit on q-market (PAC)"""
        return sum(
            (m.q_clear_pr[t] - m.q_cost) * market.q_price_tick * m.q_am[t] / (market.q_time_tick / (0.25 * u.h))
            for t in m.T
        )

    def q_profit_pab(m):
        """profit on q-market (PAB)"""
        return sum(
            (m.q_pr[t] - m.q_cost) * market.q_price_tick * m.q_am[t] / (market.q_time_tick / (0.25 * u.h))
            for t in m.T
        )

    def value_soc(m):
        """Assign a value to the current soc aka contents of the battery storage"""
        return (m.soc[len(m.T) - 1] - flex.soc) * flex.capa * avg_p_price * market.p_price_tick / (
                market.p_time_tick * market.p_volume_tick)

    if Markets.P in optimized_markets:
        model.objective = Objective(expr=p_profit(model), sense=maximize)
        if get_value(flex.capa, u.Wh) != 0:
            model.objective.expr += value_soc(model)
    else:
        model.objective = Objective(expr=0, sense=maximize)

    if Markets.Q in optimized_markets:
        if market.q_market_clearing == "pay_as_bid":
            model.objective.expr += q_profit_pab(model)
        else:
            model.objective.expr += q_profit_pac(model)


def solve(model):
    """
    solver parameters:
    - limits/gap : solving stops, if the relative gap is below the given value (for convergence)
    - limits/time: maximal time in seconds to run (default: 1e+20)
    - numerics/feastol: feasibility tolerance for constraints (default: 1e-06)
    - numerics/epsilon: absolute values smaller than this are considered zero (default: 1e-09)
    - branching/random/priority: priority of branching rule <random> (range: [-536870912,536870911], default: -100000)
    - display/verblevel: verbosity level of output (range: [0,5], default: 4)
    Args:
        model ():

    Returns:

    """
    solver = SolverFactory("scip")
    solver.options["limits/gap"] = 0.01  # allows 1 % gap
    # solver.options["limits/time"] = 30
    solver.options['numerics/epsilon'] = 1e-09
    solver.options['numerics/feastol'] = 1e-05
    solver.options['display/verblevel'] = 3
    best_solved_model = None
    best_objective = 0
    solver_parameters = [
        # default
        # {'numerics/feastol': 1e-06},
        # # more tolerance for constraints
        {"limits/time": 60},
        # {'numerics/feastol': 1e-04, "limits/time": 90},
        # {'numerics/feastol': 1e-03},
        # add some more randomness to branching to diversify search and give it another try
        # {'numerics/feastol': 1e-05, 'numerics/epsilon': 1e-08, "limits/time": 100, "branching/random/priority": 1000},
        # {'numerics/feastol': 1e-03, 'numerics/epsilon': 1e-08, "limits/time": 180},
        {'numerics/feastol': 1e-03, 'numerics/epsilon': 1e-08, "limits/time": 180},
        # {'numerics/feastol': 1e-03, 'numerics/epsilon': 1e-08, "limits/time": 100, "limits/gap": 0.01},
    ]
    optimization_time = 0
    for i, params in enumerate(solver_parameters):
        for key, value in params.items():
            solver.options[key] = value
        logger.info(f"optimizing with {params}")
        # if not is_feasible(model, solver):
        # logger.info(f"model not feasible with {params}")
        # if i == len(solver_parameters) - 1:
        #     logger.info("optimization is attempted with the last parameter set.")
        # else:
        #     continue
        try:
            start = time.time()
            results = solver.solve(model,  # tee=True,  # logfile='scip_log.txt'
                                   )
            end = time.time()
            optimization_time = end - start
            gap = results.solver.Gap
            obj_val = model.objective()
            if results.solver.termination_condition == TerminationCondition.infeasible:
                logger.info('infeasible')
                solver.options['numerics/feastol'] = min(solver.options['numerics/feastol'] * 10, 1e-03)
                results = solver.solve(model)
                gap = results.solver.Gap
                obj_val = model.objective()
            if best_solved_model is None or best_objective < obj_val:
                best_objective = obj_val
                best_solved_model = copy.deepcopy(model)
                logger.info(f'found new {best_objective=}')
        except Exception as e:
            logger.info(e)
            if i == len(solver_parameters) - 1:
                logger.info("retrying optimization")
                solver.options['numerics/feastol'] = min(solver.options['numerics/feastol'] * 10, 1e-03)
                solver.options["limits/time"] = 65
                results = solver.solve(model)
                gap = results.solver.Gap
                obj_val = model.objective()
                if best_solved_model is None or best_objective < obj_val:
                    best_objective = obj_val
                    best_solved_model = copy.deepcopy(model)
                    logger.info(f'found new {best_objective=}')
            # except Exception as e2:
            #     logger.info(e2)
            #     logger.info("retrying optimization one last time")
            #     solver.options["limits/time"] = 100
            #     solver.options['numerics/feastol'] = min(solver.options['numerics/feastol'], 1e-03)
            #     results = solver.solve(model)
        if results.solver.termination_condition in (TerminationCondition.optimal, TerminationCondition.other):
            break
        else:
            logger.info(
                f"solver not successful with termination_condition "
                f"{results.solver.termination_condition} "
                f"with {gap=}%, {obj_val=} in {optimization_time} seconds")
        # elif i == len(solver_parameters) - 1 and results.solver.termination_condition == TerminationCondition.other:
        #     break

    logger.info(
        f"solver exiting with termination_condition "
        f"{results.solver.termination_condition} "
        f"with {gap=}%,  {obj_val=}, in {optimization_time} seconds")

    if results.solver.status == SolverStatus.ok and (
            results.solver.termination_condition
            in (TerminationCondition.optimal, TerminationCondition.other, TerminationCondition.maxTimeLimit)
    ):
        logger.info(f'returning model with objective {best_solved_model.objective()} €')
        return best_solved_model
    else:
        raise Exception(
            f"No solution found, status={results.solver.status}, "
            f"termination_condition={results.solver.termination_condition}"
        )


def write_results(stream: TextIOBase, model, market: MarketParams, flex: FlexParams):
    stream.write(f"profit = {model.objective()} €\n")
    for var_name, factor, unit in [
        ("p_am_inv", market.p_volume_tick, u.kW),
        ("p_am_gen", market.p_volume_tick, u.kW),
        ("p_am_bat_gen", market.p_volume_tick, u.kW),
        ("p_am_bat_cons", market.p_volume_tick, u.kW),
        ("soc", 1, None),
        ("is_charging", 1, None),
        ("p_pr", market.p_price_tick / (market.p_volume_tick * market.p_time_tick), u.EUR / u.MWh),
        ("p_clear_pr", market.p_price_tick / (market.p_volume_tick * market.p_time_tick), u.EUR / u.MWh),
        ("q_am", market.q_volume_tick, u.kW),
        ("q_tar", u.W, u.kW),
        ("q_pr", market.q_price_tick / (market.q_volume_tick * market.q_time_tick), u.EUR / u.MWh),
        ("q_clear_pr", market.q_price_tick / (market.q_volume_tick * market.q_time_tick), u.EUR / u.MWh),
        ("q_rel_pr", market.q_price_tick / (market.q_volume_tick * market.q_time_tick), u.EUR / u.MWh),
        ("k", 1, None),
    ]:
        if var := getattr(model, var_name, None):
            if isinstance(var[0], float) or isinstance(var[0], int):
                stream.write(
                    f"{var_name} = {[round(get_value(var[t] * factor, unit=unit), 6) for t in range(len(var))]} {str(unit)}\n"
                )
            else:
                stream.write(
                    f"{var_name} = {[round(get_value(var[t].value * factor, unit=unit), 6) for t in range(len(var))]} {str(unit)}\n"
                )

    stream.write(
        f"costs_p = {round(get_value(flex.costs_p * market.p_price_tick / (market.p_volume_tick * market.p_time_tick),
                                     unit=u.EUR / u.MWh), 6)} {str(u.EUR / u.MWh)}\n")
    stream.write(
        f"costs_q = {round(get_value(flex.costs_q * market.q_price_tick / (market.q_volume_tick * market.q_time_tick),
                                     unit=u.EUR / u.MWh), 6)} {str(u.EUR / u.MWh)}\n")

    # if hasattr(model, "p_am_inv") and hasattr(model, "q_am"):
    # if isinstance(model.p_am_inv[0],int):
    # stream.write(
    #     f"s = {
    #     [get_value((
    #                        (model.p_am_inv[t] * market.p_volume_tick) ** 2
    #                        + (model.q_am[t].value * market.q_volume_tick
    #                           + model.q_tar[t].value * u.W
    #                           ) ** 2
    #                ) ** 0.5, unit=u.kW)
    #      for t in model.T]
    #     } kVA\n"
    # )
    # else:
    # stream.write(
    #     f"s = {
    #     [get_value((
    #                        (model.p_am_inv[t].value * market.p_volume_tick) ** 2
    #                        + (model.q_am[t].value * market.q_volume_tick
    #                           + model.q_tar[t].value * u.W
    #                           ) ** 2
    #                ) ** 0.5, unit=u.kW)
    #      for t in model.T]
    #     } kVA\n"
    # )
    return stream


def harmonize_time_series(
        market: MarketParams,
        forecast: MarketForecastParams,
        flex: FlexParams,
        active_power=None
) -> tuple[MarketForecastParams, int, list[float]] | tuple[MarketForecastParams, int]:
    """harmonize the length of all time series if p and q-market products have different resolutions"""
    # first check if all input time series cover the same time horizon
    check_input_length(market, forecast, flex)
    # cale everything to 15 Minute resolution
    res = 0.25 * u.h
    p_clear_price = [v for v in forecast.p_clear_price for _ in range(int(get_value(market.p_time_tick / res)))]
    q_demand = [v for v in forecast.q_demand for _ in range(int(get_value((1 * u.h) / res)))]
    q_clear_price = [v for v in forecast.q_clear_price for _ in range(int(get_value(market.q_time_tick / res)))]
    q_eff_price = [v for v in forecast.q_eff_price for _ in range(int(get_value(market.q_time_tick / res)))]
    fc_scaled = MarketForecastParams(
        p_clear_price=p_clear_price,
        q_clear_price=q_clear_price,
        q_demand=q_demand,
        q_eff_price=q_eff_price
    )

    num_intervals = check_input_length(market, fc_scaled, flex, tick=res)

    if active_power is not None:
        a_power = [v for v in active_power for _ in range(int(get_value(market.p_time_tick / res)))]
        return fc_scaled, num_intervals, a_power
    return fc_scaled, num_intervals


def check_input_length(market: MarketParams, forecast: MarketForecastParams, flex: FlexParams, tick=None):
    """
    Check if the length of all input time series matches
    Args:
        market ():
        forecast ():
        flex ():
        tick: if given, this tick is used for all time series
    Returns: return either the time horizon in h or the number of intervals if tick is given
    """

    if tick is not None:
        p_tt = tick
        q_tt = tick
    else:
        p_tt = market.p_time_tick
        q_tt = market.q_time_tick

    len_p_market = len(forecast.p_clear_price) * p_tt
    len_gen = len(flex.gen_max) * 0.25 * u.h
    len_cons = len(flex.cons_max) * 0.25 * u.h
    len_q_market = len(forecast.q_clear_price) * q_tt
    len_q_eff = len(forecast.q_eff_price) * q_tt
    len_q_demand = len(forecast.q_demand) * p_tt

    if not get_value(len_p_market, u.h) == get_value(len_q_market, u.h) == get_value(len_q_eff, u.h) == get_value(
            len_q_demand, u.h) == get_value(len_gen, u.h) == get_value(len_cons, u.h):
        raise Exception(f'Length of input data does not match. \n'
                        f'Length P-Market: {len_p_market} \n'
                        f'Length Q-Market: {len_q_market} \n'
                        f'Length Q-Market efficiency prices: {len_q_eff} \n'
                        f'Length Q-Demand: {len_q_demand} \n'
                        f'Length generation data: {len_gen} \n'
                        f'Length consumption data: {len_cons}')
    else:

        if tick is not None:
            return len(forecast.p_clear_price)
        return len_p_market


class OptimizerOutput(TypedDict):
    p_amount_total_vt: list[int]
    p_price_pt: list[int]
    p_amount_bat_vt: list[float]
    p_amount_res_vt: list[float]
    q_amount_total_vt: list[int]
    q_price_pt: list[int]
    q_rel_price_pt: list[float]
    model: NotRequired[Any]


def rescale_outputs(
        model, market: MarketParams, get_p=True, get_q=True
) -> OptimizerOutput:
    """
    rescale the outputs of the optimization to original time scales
    Args:
        model ():
        market ():
        get_p: if true return amounts and prices fpr P-market
        get_q: if true return amounts and prices fpr Q-market
    Returns:

    """
    res = OptimizerOutput(
        p_amount_total_vt=[],
        p_price_pt=[],
        p_amount_bat_vt=[],
        p_amount_res_vt=[],
        q_amount_total_vt=[],
        q_price_pt=[],
        q_rel_price_pt=None,  # type: ignore
    )
    if hasattr(model, "p_am_gen"):
        res["p_amount_res_vt"] = [model.p_am_gen[t].value for t in model.T]
    else:
        res["p_amount_res_vt"] = [0 for _ in model.T]
    if hasattr(model, "p_am_bat_gen"):
        # in p_volume_tick
        res["p_amount_bat_vt"] = [
            model.p_am_bat_gen[t].value * (1 - model.is_charging[t].value) - model.p_am_bat_cons[t].value *
            model.is_charging[t].value for t in model.T]
    else:
        res["p_amount_bat_vt"] = [0 for _ in model.T]

    if get_p:
        p_tick_factor = round(get_value(market.p_time_tick / (0.25 * u.h)))
        i = 0
        while i < len(model.p_am_inv):
            if all(model.p_am_inv[i].value == model.p_am_inv[i + j].value for j in range(1, p_tick_factor)):
                res["p_amount_total_vt"].append(round(model.p_am_inv[i].value))
            if all(model.p_pr[i].value == model.p_pr[i + j].value for j in range(1, p_tick_factor)):
                res["p_price_pt"].append(round(model.p_pr[i].value))
            else:
                raise Exception(f'Error while rescaling optimization results for P-market \n'
                                f'Not all values in a time tick are equal')
            i += p_tick_factor
    else:
        res["p_amount_total_vt"] = None  # type: ignore
        res["p_price_pt"] = None  # type: ignore
    if get_q:
        q_tick_factor = round(get_value(market.q_time_tick / (0.25 * u.h)))
        i = 0
        while i < len(model.q_am):
            if all(value(model.q_am[i]) == value(model.q_am[i + j]) for j in range(1, q_tick_factor)):
                res["q_amount_total_vt"].append(round(value(model.q_am[i])))
            if all(value(model.q_pr[i]) == value(model.q_pr[i + j]) for j in range(1, q_tick_factor)):
                res["q_price_pt"].append(round(value(model.q_pr[i])))
            else:
                raise Exception(f'Error while rescaling optimization results for Q-market \n'
                                f'Not all values in a time tick are equal')
            i += q_tick_factor
        if not all(q >= 0 for q in res["q_amount_total_vt"]):
            logger.warning(f"not all q-amounts are positive: {res["q_amount_total_vt"]} (in q-volume-steps)")
        if hasattr(model, "q_rel_pr"):
            res["q_rel_price_pt"] = [model.q_rel_pr[t].value for t in model.T]
    else:
        res["q_amount_total_vt"] = None  # type: ignore
        res["q_price_pt"] = None  # type: ignore

    return res


def run_optimizer(model, market: MarketParams, forecast: MarketForecastParams, flex: FlexParams, verbose=False,
                  get_model=False, get_p=True, get_q=True) -> OptimizerOutput:
    try:
        assert_units_consistent(model)
        solved_model = solve(model)
        make_data_class_printable(flex)
        if verbose:
            output = write_results(io.StringIO(), solved_model, market, flex)
            print(output.getvalue())
        res = rescale_outputs(solved_model, market, get_p=get_p, get_q=get_q)
        if not get_model:
            return res
        else:
            res['model'] = model
            return res
    except:
        model.display()
        logger.warning(
            f"{pformat(make_data_class_printable(market))}\n{pformat(forecast)}\n{pformat(make_data_class_printable(flex))}")
        raise


def make_data_class_printable(f):
    flex = {}
    for field in fields(f):
        value = getattr(f, field.name)
        if isinstance(value, NPV_ProductExpression):
            flex[field.name] = get_value(value, value.args[1])
        else:
            flex[field.name] = value
    return flex


def optimize_p_only(
        market: MarketParams, forecast: MarketForecastParams, flex: FlexParams, verbose=False, get_model=False
) -> OptimizerOutput:
    """
    For naive bidding: optimize only the bid on the p-market without considering q-market at all
    Args:
        market ():
        forecast ():
        flex ():
        verbose: if true print results
        get_model: if true also return the model
    Returns:

    """
    forecast_h, num_intervals = harmonize_time_series(market, forecast, flex)
    avg_p_price = sum(forecast_h.p_clear_price) / (len(forecast_h.p_clear_price))
    model = create_model(Markets.P, market, flex, forecast_h, num_intervals)
    add_objective(model, Markets.P, market, flex, avg_p_price)
    return run_optimizer(model, market, forecast, flex, verbose, get_model, get_q=False)


def optimize_q_market(
        market: MarketParams,
        forecast: MarketForecastParams,
        flex: FlexParams,
        active_power, verbose=False, get_model=False
) -> OptimizerOutput:
    """
    Optimize bidding for q-market only but with results from p-market given
    (for sequential bidding)
    Args:
        market ():
        forecast ():
        flex ():
        active_power (): time series of active power values resulting from p-market
        verbose: if true print results
        get_model: if true also return the model
    Returns:

    """
    forecast_h, num_intervals, active_power_h = harmonize_time_series(market, forecast, flex, active_power)
    avg_p_price = sum(forecast_h.p_clear_price) / (len(forecast_h.p_clear_price))
    model = create_model(Markets.Q, market, flex, forecast_h, num_intervals, active_power_h)
    add_objective(model, Markets.Q, market, flex, avg_p_price)
    return run_optimizer(model, market, forecast, flex, verbose, get_model, get_p=False)


def optimize_simultaneous(
        market: MarketParams, forecast: MarketForecastParams, flex: FlexParams, verbose=False, get_model=False
) -> OptimizerOutput:
    """
    optimize bids for both markets simultaneously
    Args:
        market ():
        forecast ():
        flex ():
        verbose: if true print results
        get_model: if true also return the model
    Returns:

    """
    forecast_h, num_intervals = harmonize_time_series(market, forecast, flex)
    avg_p_price = sum(forecast_h.p_clear_price) / (len(forecast_h.p_clear_price))
    model = create_model(Markets.P | Markets.Q, market, flex, forecast_h, num_intervals)
    add_objective(model, Markets.P | Markets.Q, market, flex, avg_p_price)
    return run_optimizer(model, market, forecast, flex, verbose, get_model)


if __name__ == "__main__":
    verbose = True
    len_test_h = 8
    q_volume_tick = 10000.0 * u.W  # 10 kVAR
    p_volume_tick = 100000.0 * u.W  # 0.1 MW = 100kW
    q_time_tick = 1 * u.h
    p_time_tick = 1 * u.h
    q_price_tick = 0.0000001 * u.EUR / u.Wh  # 0.1€/MVARh
    # 0.1 €/MVARh in €/(q_volume_tick*q_time_tick)
    q_normalized_price_tick = (
            round(get_value(q_price_tick * q_volume_tick * q_time_tick, u.EUR), 6) * u.EUR
    )

    p_price_tick = 0.0000001 * u.EUR / u.Wh  # 0.1€/MWh
    p_normalized_price_tick = (
            round(get_value(p_price_tick * p_volume_tick * p_time_tick, u.EUR), 6) * u.EUR
    )

    market1 = MarketParams(
        q_market_clearing="pay_as_bid",
        q_volume_tick=q_volume_tick,
        p_volume_tick=p_volume_tick,
        q_time_tick=q_time_tick,
        q_price_tick=q_normalized_price_tick,
        p_price_tick=p_normalized_price_tick,
    )
    forecast1 = MarketForecastParams(
        p_clear_price=[
            round(get_value((v * u.EUR / u.MWh) * (p_volume_tick * p_time_tick) / p_normalized_price_tick), 6)
            for v in [97, 107, 136, 151, 133, 113, 98, 89]],  # initial price in €/MWh
        q_clear_price=[
            round(get_value((v * u.EUR / u.MWh) * (q_volume_tick * q_time_tick) / q_normalized_price_tick), 6)
            for v in [*[11 for _ in range(int(len_test_h * get_value(1 * u.h / q_time_tick) / 2))],
                      *[30 for _ in range(int(len_test_h * get_value(1 * u.h / q_time_tick) / 2))]]],
        # initial price in €/MVARh
        q_demand=[round(get_value(1 * u.MW / q_volume_tick), 6) for _ in
                  range(int(len_test_h))],  # demand always 1 MVARh
        q_eff_price=[
            round(get_value((v * u.EUR / u.MWh) * (q_volume_tick * q_time_tick) / q_normalized_price_tick), 6)
            for v in [8 for _ in range(int(len_test_h * get_value(1 * u.h / q_time_tick)))]],
        # initial price in €/MVARh
    )
    # import random
    #
    # random_gen = [random.randint(100000, 300000) for _ in range(len_test_h)]
    flex_pv = FlexParams(
        s_max=400000 * u.W,
        gen_max=[p * u.W for p in
                 [143374, 109223, 158660, 181691, 119377, 198867, 134070, 194633, 216881, 245190, 241693, 237246,
                  248332, 215380, 246540, 239861, 220095, 268656, 295469, 256021, 256362, 251231, 224929, 249646,
                  247601, 231843, 185471, 203134, 242077, 180701, 206451, 242226]],
        # 300kW PV
        cons_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        # 10 €Cent/kWh in q_price_tick (0.1 €/MWh)
        costs_p=round(
            get_value(
                (0.1 * u.EUR / u.kWh)
                * (p_volume_tick * p_time_tick)
                / p_normalized_price_tick
            ),
            6,
        ),
        # 0.1 €Cent/kCARh in q_price_tick (0.1 €/kVARh)
        costs_q=round(
            get_value(
                (0.01 * u.EUR / u.kWh)
                * (q_volume_tick * q_time_tick)
                / q_normalized_price_tick
            ),
            6,
        ),
    )

    flex_pv_and_bat = FlexParams(
        s_max=400000 * u.W,
        # 300kW PV
        gen_max=[
            p * u.W
            for p in [
                143374,
                109223,
                158660,
                181691,
                119377,
                198867,
                134070,
                194633,
                216881,
                245190,
                241693,
                237246,
                248332,
                215380,
                246540,
                239861,
                220095,
                268656,
                295469,
                256021,
                256362,
                251231,
                224929,
                249646,
                247601,
                231843,
                185471,
                203134,
                242077,
                180701,
                206451,
                242226,
            ]
        ],
        cons_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        # 10 €Cent/kWh in q_price_tick (0.1 €/MWh)
        costs_p=round(
            get_value(
                (0.1 * u.EUR / u.kWh)
                * (p_volume_tick * p_time_tick)
                / p_normalized_price_tick
            ),
            6,
        ),
        # 0.1 €Cent/kCARh in q_price_tick (0.1 €/kVARh)
        costs_q=round(
            get_value(
                (0.01 * u.EUR / u.kWh)
                * (q_volume_tick * q_time_tick)
                / q_normalized_price_tick
            ),
            6,
        ),
        soc=0.5,
        eff_ch=0.95,
        eff_dis=0.96,
        capa=150000 * u.Wh,  # relation 2:1 for 300 kWpeak PV
        p_bat_discharge_max=300000 * u.W,
        p_bat_charge_max=300000 * u.W,
    )
    flex_bat = FlexParams(
        s_max=250000 * u.W,
        gen_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        cons_max=[p * u.W for p in [0 for _ in range(int(len_test_h * 4))]],
        # 10 €Cent/kWh in q_price_tick (0.1 €/MWh)
        costs_p=round(
            get_value(
                (0.0001 * u.EUR / u.kWh)
                * (p_volume_tick * p_time_tick)
                / p_normalized_price_tick
            ),
            6,
        ),
        # 0.1 €Cent/kCARh in q_price_tick (0.1 €/kVARh)
        costs_q=round(
            get_value(
                (0.01 * u.EUR / u.kWh)
                * (q_volume_tick * q_time_tick)
                / q_normalized_price_tick
            ),
            6,
        ),
        soc=0.5,
        eff_ch=0.95,
        eff_dis=0.96,
        capa=1500000 * u.Wh,  # relation 2:1 for 300 kWpeak PV
        p_bat_discharge_max=300000 * u.W,
        p_bat_charge_max=300000 * u.W,
    )

    print('optimize simultaneous')
    print(optimize_simultaneous(market1, forecast1, flex_pv_and_bat, verbose=verbose))
    print()
    print('optimize p only')
    p_am, p_pr, p_am_bat = optimize_p_only(market1, forecast1, flex_bat, verbose=verbose)
    print((p_am, p_pr))
    # p_am = [0, 1, 2, 2, 2, 2, 0, 0]
    # print()
    print('optimize q based on results from p')
    print(optimize_q_market(market1, forecast1, flex_bat, p_am, verbose=verbose))

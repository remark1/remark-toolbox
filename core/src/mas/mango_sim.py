"""
Gateway between mosaik and the MAS.

The class :class:`MosaikAPI` implements the `low-level mosaik API`_.

The MosaikAPI also manages the root main_container for the MAS.  It starts
a :class:`MosaikAgent` agent within that main_container.
The MosaikAgent serves as a gateway between the Mo/Mp agents
and mosaik.

The Mo/Mp agents do not run within the root main_container but in a separate
containers in sub processes. These subprocesses are as well managed by the MosaikAPI.

The entry point for the MAS is the function :func:`main()`.  It parses the
command line arguments and starts the :func:`run()` coroutine which runs until
mosaik sends a *stop* message to the MAS.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html

"""

from __future__ import annotations

import dataclasses
import json
import logging  # to set up redirection to loguru
import random
import sys
import time
import warnings
from sqlite3 import connect

import loguru
import mosaik_api_v3
from loguru import logger
from mas.agents.base import UnknownMessageWarning
from mas.agents.battery import BatteryAgent
from mas.agents.finc import FincTrader
from mas.agents.grid_operator import GridOperatorAgent
from mas.agents.load import LoadAgent
from mas.agents.market import MarketAgent
from mas.agents.pv_gen import PVGenAgent
from mas.agents.trader import TraderAgent
from mas.agents.wind_gen import WindGenAgent
from mas.messages import codec
from mas.util import format_time
from mosaik_components.mango import AgentSpec, MangoSimulator

mosaik_logger = logger.bind(context="mosaik")


# From loguru's documentation
class InterceptHandler(logging.Handler):
    def emit(self, record):
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno
        frame, depth = sys._getframe(6), 6
        while frame and frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1
        (
            logger.opt(depth=depth, exception=record.exc_info).log(
                level, record.getMessage()
            )
        )


def showwarning(message, category, filename, lineno, file=None, line=None):
    logger.opt(depth=2).warning(f"{category.__name__}: {message}")


warnings.showwarning = showwarning


def main() -> int:
    warnings.filterwarnings("once", category=UserWarning)
    warnings.filterwarnings("error", category=UnknownMessageWarning)
    logging.basicConfig(handlers=[InterceptHandler()], level=0, force=True)
    logger.remove()
    logger.add(sys.stderr, level="INFO")
    time_stamp = time.time()
    logger.add(f"logs/mas_{time_stamp}.log", mode="a", level="INFO")
    logger.add(
        "logs/messages.log",
        mode="w",
        level="TRACE",
        filter=lambda r: r["extra"].get("context") == "message",
        format=lambda r: f"Day {format_time(r['extra']['time']):>9} | "
        "{extra[direction]:7} | "
        "{extra[sender]:>14} -> {extra[receiver]:14} | "
        "{extra[message]} [{module}:{function}:{line}]\n",
    )

    # log_db = connect("logs/log.sqlite3", autocommit=True)
    # log_db.execute("DROP TABLE IF EXISTS messages;")
    # log_db.execute(
    #     """CREATE TABLE messages (
    #         step INTEGER,
    #         time TEXT,
    #         is_send BOOL,
    #         sender TEXT,
    #         receiver TEXT,
    #         message TEXT,
    #         args JSON
    #     );"""
    # )

    # def log_message(msg: loguru.Message) -> None:
    #     extra = msg.record["extra"]
    #     log_db.execute(
    #         "INSERT INTO messages VALUES (?, ?, ?, ?, ?, ?, ?);",
    #         (
    #             extra["time"],
    #             format_time(extra["time"]),
    #             extra["direction"] == "Send",
    #             extra["sender"],
    #             extra["receiver"],
    #             type(extra["message"]).__name__,
    #             json.dumps(dataclasses.asdict(extra["message"])),
    #         ),
    #     )

    # logger.add(
    #     sink=log_message,
    #     level="TRACE",
    #     filter=lambda r: r["extra"].get("context") == "message",
    # )

    greetings = [
        "Wolle Rose kaufen?",
        "How happy you make me, oh Mandy.",
        "I hope you're having a beautiful day!",
    ]
    logger.warning(random.choice(greetings))

    logger.disable("numba")
    logger.disable("simbench")
    logger.disable("mango")
    logger.disable("mosaik_api_v3")

    mango_sim = MangoSimulator(*AGENT_SETUP, codec=codec)
    return mosaik_api_v3.start_simulation(mango_sim, configure_logging=False)


AGENT_SETUP = [
    AgentSpec(GridOperatorAgent, model="GridOperator"),
    AgentSpec(MarketAgent, model="Market"),
    AgentSpec(TraderAgent, model="Trader"),
    BatteryAgent,
    LoadAgent,
    WindGenAgent,
    PVGenAgent,
    FincTrader,
]


if __name__ == "__main__":
    main()

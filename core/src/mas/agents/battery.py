from __future__ import annotations

import math
import warnings
from math import nan
from typing import TypedDict, override

import mas.messages as msg
from loguru import logger
from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.trader import TraderAgent
from mas.util import DAILY_STEPS, STEP_SIZE_s
from mosaik_components.mango import AgentAddress
from util.time_storage import TimeStorage

logger = logger.bind(context="mas")


class BatteryAgent(MaMoAgent):
    trader: AgentAddress
    current_soc: float
    p_obligations_W: TimeStorage[float]
    q_obligations_var: TimeStorage[float]
    capacity_kWh: float
    charge_efficiency: float
    discharge_efficiency: float
    p_discharge_max_kW: float
    p_charge_max_kW: float
    noise_beta: float

    def __init__(
            self,
            suggested_aid: str,
            container: Container,
            seed: int,
            trader: AgentAddress[TraderAgent],
            efficiency: float,
            p_charge_max_kW: float,
            p_discharge_max_kW: float,
            capacity_kWh: float,
            fce_max: float,
            noise_beta: float,
    ):
        super().__init__(suggested_aid, container, seed)
        self.trader = trader
        # TODO: Get these values from the simulation’s configuration
        self.capacity_kWh = capacity_kWh
        self.p_discharge_max_kW = p_discharge_max_kW
        self.p_charge_max_kW = p_charge_max_kW
        self.charge_efficiency = efficiency
        self.discharge_efficiency = efficiency
        self.fce_max = fce_max
        self.noise_beta = noise_beta
        self.p_obligations_W = TimeStorage(
            default_factory=lambda _: 0.0, step_length=STEP_SIZE_s, steps=DAILY_STEPS * 2
        )
        self.q_obligations_var = TimeStorage(
            default_factory=lambda _: 0.0, step_length=STEP_SIZE_s, steps=DAILY_STEPS * 2
        )
        self.current_soc = nan

    @override
    def handle_parsed_msg(self, message, sender):
        match message:
            case msg.RequestBatteryFlexibility(id=id, time=time):
                charge = self.projected_soc(time)
                self.send_message(
                    msg.ImpartBatteryFlexibility(
                        id=id,
                        time=time,
                        soc=charge,
                        eff_ch=self.charge_efficiency,
                        eff_dis=self.discharge_efficiency,
                        capa_Wh=self.capacity_kWh * 1000,
                        p_charge_max_W=self.p_charge_max_kW * 1000,
                        p_discharge_max_W=self.p_discharge_max_kW * 1000,
                        fce_max=self.fce_max
                    ),
                    to=self.trader,
                )

            case msg.ObligationMessage():
                self.p_obligations_W[message.supply_start] = message.p_amount_W
                self.q_obligations_var[message.supply_start] = message.q_amount_var

            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def setup_done(self):
        self.send_message(
            msg.RegisterBattery(),
            to=self.trader,
        )

    def start_mosaik_step(self, soc_percent: float):
        # logger.info(f"{self.aid} received {soc_percent=}")
        self.current_soc = soc_percent / 100

    def get_mosaik_output(self) -> BatteryOutput:
        return BatteryOutput(
            p_set_MW=self.p_obligations_W[self.time] * 1e-6,
            q_amount_Mvar=self.q_obligations_var[self.time] * 1e-6,
        )

    def projected_soc(self, time):
        charge_kWh = self.capacity_kWh * self.current_soc
        for t in range(self.time, time, 900):
            if (obligation_kW := self.p_obligations_W[t] / 1000) > 0:  # Discharge
                charge_kWh -= 0.25 * obligation_kW / self.discharge_efficiency  # 0.25 hours
            else:
                charge_kWh -= 0.25 * obligation_kW * self.charge_efficiency

        result_soc = charge_kWh / self.capacity_kWh
        clipped_soc = max(0, min(1, result_soc))
        if result_soc != clipped_soc:
            logger.warning(
                f"{self.aid} for projected SOC of {result_soc} outside (0, 1); "
                "clipping to (0, 1)"
            )
        return clipped_soc


class BatteryOutput(TypedDict):
    p_set_MW: float
    q_amount_Mvar: float

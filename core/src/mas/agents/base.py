from abc import ABC, abstractmethod
from asyncio import Task
from typing import Any, Dict
from random import Random

from loguru import logger
from mango.container.core import Container
from mango.messages.message import Performatives
from mosaik_components.mango import AgentAddress, MosaikAgent

mosaik_logger = logger.bind(context="mamo_agent")
message_logger = logger.bind(context="message")


class MaMoAgent(MosaikAgent, ABC):
    """
    A MaMoAgent is the general class (e.g. of market operators and market participants),
    which run in mosaik.

    Implements message counting and communication with an observer to detect termination
    of message exchange between agents for the current (Mosaik) time step.
    """

    def __init__(
            self,
            suggested_aid: str,
            container: Container,
            seed: int
    ):
        super().__init__(container, suggested_aid=suggested_aid)
        self.agent_random = Random(seed)
        # print(f'{suggested_aid} got seed {seed}')

    def handle_message(self, content, meta: Dict[str, Any]):
        """
        This is Mango's message entry point. MaMoAgents should usually overwrite
        `handle_parsed_msg` instead, unless they're directly concerned with the
        interaction between Mango and Mosaik.
        """
        message_logger.trace(
            "{direction} {message} from {sender} at {receiver} (time {time})",
            direction="Receive",
            message=content,
            sender=meta["sender_id"],
            receiver=self.aid,
            time=self.time,
        )
        try:
            sender_addr: tuple[str, int] = tuple(meta["sender_addr"])  # type: ignore
            self.handle_parsed_msg(
                content,
                sender=AgentAddress(sender_addr, meta["sender_id"]),
            )
        except Exception as e:
            logger.exception(e)
            raise e

    @abstractmethod
    def handle_parsed_msg(
            self,
            message,
            sender: AgentAddress,
    ):
        """
        Handle an incoming message after it has been counted for termination
        detection, checked for timeliness, and parsed into an agent_messages.py
        dataclass.

        This method should be overridden instead of `handle_msg`.
        """
        raise NotImplementedError()

    def send_message(
            self,
            message: Any,
            /,
            to: AgentAddress,
            *,
            performative: Performatives = Performatives.inform,
    ) -> Task[Any]:
        """
        Send a (counted) message to receiver.

        "Counted" means counted for our termination detection, so this method
        should *not* be used to send messages to the observer.

        :param message: The message to be sent. Must be a dataclass.
        :param receiver: The receiver of the message as a (addr, id) pair.
        :param performative: The ACL performative for this message.
        :param converstation_id: The conversation ID for this message.
        :param include_in_count: (default `True`) Whether to include this message
        in the count for termination detection. This needs to be set to `False` for
        some messages that concern the termination detection itself.
        """
        message_logger.opt(depth=1).trace(
            "{direction} {message} from {sender} to {receiver} (time {time})",
            direction="Send",
            message=message,
            sender=self.aid,
            receiver=to.aid,
            time=self.time,
        )
        future: Task[Any] = self.schedule_instant_task(
            self.send_acl_message(
                receiver_addr=to.container_addr,
                receiver_id=to.aid,
                content=message,
                acl_metadata={
                    "performative": performative,
                    "sender_id": self.aid,
                },
            )
        )
        return future


class UnknownMessageWarning(UserWarning):
    def __init__(self, agent_id, message):
        self.agent_id = agent_id
        self.message_type = type(message)

    def __str__(self):
        return f"{self.agent_id} received {self.message_type.__name__}"

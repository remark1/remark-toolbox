from __future__ import annotations

import typing
import warnings

from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.trader import TraderAgent
from mas.messages import (
    ImpartUnitFlexibility,
    RegisterUnit,
    RequestUnitFlexibility,
)
from mosaik_components.mango import AgentAddress
from util.flexibility import Flexibility
from util.time_storage import TimeStorage


class LoadAgent(MaMoAgent):
    p_forecast_MW: TimeStorage[float]

    def __init__(
            self,
            suggested_aid: str,
            container: Container,
            seed: int,
            trader: AgentAddress[TraderAgent],
    ):
        super().__init__(suggested_aid, container, seed)
        self.mp = trader
        self.p_forecast_MW = TimeStorage(step_length=900, steps=8)

    def handle_parsed_msg(self, message, sender):
        match message:
            case RequestUnitFlexibility(time):
                self.send_message(
                    ImpartUnitFlexibility(
                        Flexibility(
                            min_p=-self.p_forecast_MW[time] * 1000,
                            max_p=-self.p_forecast_MW[time] * 1000,
                        )
                    ),
                    to=self.mp,
                )
            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def setup_done(self):
        self.send_message(
            RegisterUnit(),
            to=self.mp,
        )

    def start_mosaik_step(self, p_forecast_mw: list[float]):
        # TODO: Make nicer
        if not isinstance(p_forecast_mw, list):
            p_forecast_mw = [p_forecast_mw] * 8
        for i, p_mw in enumerate(p_forecast_mw):
            self.p_forecast_MW[self.time + i * 900] = p_mw

    def get_mosaik_output(self) -> typing.NoReturn:
        assert False, "load agent provides no output"

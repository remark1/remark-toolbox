from __future__ import annotations

import typing
import warnings
from typing import TypedDict, override

from loguru import logger
from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.trader import TraderAgent
from mas.messages import (
    ImpartUnitFlexibility,
    ObligationMessage,
    RegisterUnit,
    RequestUnitFlexibility,
)
from mas.util import DAILY_STEPS, Durations, STEP_SIZE_s, noisify
from mosaik_components.mango import AgentAddress
from mosaik_components.wind.WindTurbine import WindTurbine
from util.time_storage import TimeStorage

WF_STEP_SIZE_s: int = Durations.ONE_HOUR_s
"""Step size of the wind forecast (which differs from the global step
size).
"""
WF_DAILY_STEPS: int = Durations.ONE_DAY_s // WF_STEP_SIZE_s
"""Steps per day for the wind forecast."""


class WindGenAgent(MaMoAgent):
    wind_forecast: TimeStorage[float]
    trader: AgentAddress[TraderAgent]
    turbine: WindTurbine
    p_obligations_W: TimeStorage[float]
    q_obligations_var: TimeStorage[float]
    noise_beta: float

    def __init__(
        self,
        suggested_aid: str,
        container: Container,
        seed: int,
        trader: AgentAddress[TraderAgent],
        power_curve_path: str,
        max_power_kWp: float,
        noise_beta: float,
    ):
        super().__init__(suggested_aid, container, seed)
        self.trader = trader
        self.noise_beta = noise_beta
        self.turbine = WindTurbine(
            max_power=max_power_kWp / 1000, path=power_curve_path
        )
        self.wind_forecast = TimeStorage(
            steps=WF_DAILY_STEPS * 2, step_length=WF_STEP_SIZE_s
        )
        self.p_obligations_W = TimeStorage(
            default_factory=lambda _: 0.0, steps=DAILY_STEPS * 2, step_length=STEP_SIZE_s
        )
        self.q_obligations_var = TimeStorage(
            default_factory=lambda _: 0.0, steps=DAILY_STEPS * 2, step_length=STEP_SIZE_s
        )

    @override
    def handle_parsed_msg(self, message, sender):
        match message:
            case RequestUnitFlexibility():
                answer = ImpartUnitFlexibility(
                    id=message.id,
                    time=message.time,
                    p_gen_max_W=[
                        self.turbine.power_out(
                            wind_speed=self.wind_forecast[t]
                        ).tolist()
                        * 1e6
                        for t in range(message.time, message.time + Durations.ONE_DAY_s, message.resolution_s)
                    ],
                    p_cost_EUR_Wh=0.0,
                    q_cost_EUR_varh=0.0,
                )
                self.send_message(answer, to=self.trader)

            case ObligationMessage(
                supply_start=time, p_amount_W=p_amount_W, q_amount_var=q_amount_var
            ):
                # Save obligation to pass on to turbines
                self.p_obligations_W[time] = p_amount_W
                self.q_obligations_var[time] = q_amount_var

            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def setup_done(self):
        self.send_message(
            RegisterUnit(),
            to=self.trader,
        )

    def start_mosaik_step(self, wind_speed_forecast: list[float]):
        if self.time % 3600 == 0:
            for i, wind in enumerate(wind_speed_forecast):
                self.wind_forecast[self.time + 3600 * i] = noisify(
                    wind, self.noise_beta, self.agent_random
                )

    def get_mosaik_output(self) -> WindGenAgentOutput:
        return WindGenAgentOutput(
            p_amount_MW=self.p_obligations_W[self.time] * 1e-6,
            q_amount_Mvar=self.q_obligations_var[self.time] * 1e-6,
            p_MW_fc=self.turbine.power_out(self.wind_forecast[self.time]).tolist(),
        )


class WindGenAgentOutput(TypedDict):
    p_amount_MW: float
    q_amount_Mvar: float
    p_MW_fc: float

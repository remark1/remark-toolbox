import sys
import typing
import warnings
from typing import TypedDict, override

from mas.util import DAILY_STEPS, Durations
import pandapower as pp
import pandas as pd
import simbench
from loguru import logger
from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.messages import (
    ImpartMarketSummary,
    ImpartQSensitivitiesAndDemand,
    recreate_int_keys,
)
from mas.util import ProductResolution
from mosaik_components.mango import AgentAddress
from util import sensitivity
from util.time_storage import TimeStorage

warnings.filterwarnings("ignore", category=FutureWarning, module="simbench")


class GridOutput(TypedDict, total=False):
    q_demand_MVar: float


class GridOperatorAgent(MaMoAgent):
    net: pp.pandapowerNet
    q_demand_Mvar: TimeStorage[float]
    q_market_resolution_s: ProductResolution
    extra_sgens: pd.Series

    def __init__(
            self,
            suggested_aid: str,
            container: Container,
            seed: int,
            net: str,
            q_market_resolution: ProductResolution,
            q_demand_Mvar: list[float] | None,
    ):
        super().__init__(suggested_aid, container, seed)
        self.net = simbench.get_simbench_net(net)
        self._profiles = simbench.get_absolute_values(
            self.net, profiles_instead_of_study_cases=True
        )
        self._profiles = {
            (elem, col): df
            for (elem, col), df in self._profiles.items()
            if not self.net[elem].empty
        }
        bus_index = self.net.bus.index
        sgen_indices = pp.create_sgens(self.net, bus_index, 0.0)
        self.extra_sgens = pd.Series(bus_index, index=sgen_indices)
        # TODO: Get these values live from the DB and don't presume
        # 8 simulation days
        if q_demand_Mvar:
            self.q_demand_Mvar = TimeStorage(
                steps=DAILY_STEPS * 12, step_length=Durations.QUARTER_HOUR_s
            )
            self.q_demand_Mvar.set_from(0, q_demand_Mvar)
        else:
            self.q_demand_Mvar = TimeStorage(
                default_factory=lambda _: None,
                steps=DAILY_STEPS * 12,
                step_length=Durations.QUARTER_HOUR_s,
            )  # type: ignore
        self.q_market_resolution_s = q_market_resolution

    def apply_profiles(self, time: int):
        for (elm, param), series in self._profiles.items():
            self.net[elm].update(series.loc[time // 900].rename(param))

    @override
    def handle_parsed_msg(self, message, sender: AgentAddress):
        match message:
            case ImpartMarketSummary():
                try:
                    self.apply_profiles(message.supply_time)
                    p_MW_by_bus = recreate_int_keys(message.p_MW_by_bus)
                    p_MW_by_bus = pd.Series(p_MW_by_bus)
                    p_MW_by_extra_sgen = p_MW_by_bus.reindex(
                        self.extra_sgens, fill_value=0.0
                    ).set_axis(self.extra_sgens.index)
                    self.net.sgen.loc[
                        p_MW_by_extra_sgen.index, "p_mw"
                    ] = p_MW_by_extra_sgen
                    pp.runpp(self.net)
                    sens = -1 * (
                        sensitivity.of_ext_grid_q_mvar_on_element(
                            self.net, "sgen", "q_mvar"
                        )
                        .loc[0, :]
                        .reindex(self.extra_sgens.index)
                        .set_axis(self.extra_sgens, axis=0)
                    )
                    # TODO: This ignores parts of the market results of
                    # the P-market for 4h Q-markets.
                    # if message.supply_time % self._q_market_resolution_s:
                    self.send_message(
                        ImpartQSensitivitiesAndDemand(
                            supply_time=message.supply_time,
                            target=self.q_demand_Mvar[message.supply_time],
                            sensitivities=dict(sens),  # type: ignore
                        ),
                        to=sender,
                    )
                except Exception as e:
                    logger.exception(e)
                    sys.exit()

            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    @override
    def start_mosaik_step(self):
        pass

    @override
    def get_mosaik_output(self) -> GridOutput:
        output: GridOutput = {"q_demand_MVar": self.q_demand_Mvar[self.time]}
        return output

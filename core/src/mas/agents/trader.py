from __future__ import annotations

import math
import warnings
from enum import StrEnum
from random import Random
from typing import Any, Generic, Iterable, TypedDict, TypeVar, override

from loguru import logger
from mango.container.core import Container
from mosaik_components.mango import AgentAddress

import mas.messages as msg
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.market import MarketAgent
from mas.strategies.optimization import (
    FlexParams,
    MarketForecastParams,
    MarketParams,
    optimize_p_only,
    optimize_q_market,
    optimize_simultaneous,
    u,
)
from mas.util import (
    DAILY_STEPS,
    Durations,
    EUR_MVAh_to_pt,
    STEP_SIZE_s,
    format_time,
    noisify,
)
from util.time_storage import TimeStorage

mosaik_logger = logger.bind(context="mp")


class SequencingMode(StrEnum):
    """How the agent sequences its optimizations for the market."""

    P_ONLY = "P_ONLY"
    """Only bid on the P-market."""
    NAIVE = "NAIVE"
    """Bid as much as possible on the P-market and only bid what
    happens to remain on the Q-market."""
    # We distinguish between simultaneous and sequential here. The
    # difference between PAC and PAB is handled by the optimizer,
    # instead.
    SEQUENTIAL = "SEQUENTIAL"
    """Bid on the P-market while taking the Q-market into account, but
    the Q-market is run by a separate optimization run with the P-market
    results."""
    SIMULTANEOUS = "SIMULTANEOUS"
    """Create bids for both markets at the same time."""


class TraderAgent(MaMoAgent):
    market: AgentAddress
    unit_agent: AgentAddress | None
    battery_agent: AgentAddress | None

    next_flex: FlexParams
    unit_received: bool = True
    """This is set to `False` whenever we request flexibility info
    from the unit, and set to `True` once we receive it. If this and
    `battery_received` are both `True` after receiving flexibility info,
    we are ready to bid.
    """
    battery_received: bool = True
    """This is set to `False` whenever we request flexibility info
    from the battery, and set to `True` once we receive it. If this and
    `unit_received` are both `True` after receiving flexibility info,
    we are ready to bid.
    """

    node: int
    """The node at which this agent's assets are connected."""

    noise_beta: float
    """The factor used for noise by this agent."""

    p_auction: msg.MarketProduct
    q_auction: msg.MarketProduct

    sequencing_mode: SequencingMode = SequencingMode.P_ONLY
    s_max_VA: float

    p_award_amounts_vt: TimeStorage[int]
    q_award_amounts_vt: TimeStorage[int]
    p_award_price_pt: TimeStorage[int]
    q_award_price_pt: TimeStorage[int]
    q_free_amounts_var: TimeStorage[float]
    """Amount of reactive energy that could be provided "for free", i.e.
    without restricting the use of real power.
    """
    q_total_amounts_var: TimeStorage[float]
    bid_amounts_vt: dict[Any, TimeStorage[int]]
    bid_prices_pt: dict[Any, TimeStorage[int]]
    p_res_am_vt: TimeStorage[float]
    p_bat_am_vt: TimeStorage[float]

    finc_probabilities: TimeStorage[float]
    finc_price_pt: int = 10
    p_forecast_pt: TimeStorage[float]
    q_efficiency_prices_pt: TimeStorage[int | None]
    q_relevant_prices_pt: TimeStorage[float | None]
    q_demand_fc_var: TimeStorage[float]

    def __init__(
        self,
        container: Container,
        suggested_aid: str,
        seed: int,
        node: int,
        market: AgentAddress[MarketAgent],
        sequencing_mode: str,
        s_max_VA: float,
        noise_beta: float,
        q_demand_Mvar: list[float] | None,
    ):
        super().__init__(suggested_aid=suggested_aid, container=container, seed=seed)
        self.seed = seed
        self.market = market
        self.node = node
        self.sequencing_mode = SequencingMode(sequencing_mode)
        self.s_max_VA = s_max_VA
        self.noise_beta = noise_beta
        self.q_demand_fc_var = TimeStorage(
            step_length=STEP_SIZE_s,
            steps=DAILY_STEPS * 12,
        )
        if q_demand_Mvar:
            q_demand_var = [
                noisify(q_Mvar, noise_beta * 2, self.agent_random) * 1e6
                for q_Mvar in q_demand_Mvar
            ]
            self.q_demand_fc_var.set_from(0, q_demand_var)
        else:
            # We need the Q-demand, except for P_ONLY runs
            assert sequencing_mode == SequencingMode.P_ONLY
            self.q_demand_fc_var.set_from(0, [None] * (DAILY_STEPS * 8))  # type: ignore

        self.unit_agent = None
        self.battery_agent = None
        self.next_flex = FlexParams(
            s_max=s_max_VA,
            gen_max=[0.0] * DAILY_STEPS,
            cons_max=[0.0] * DAILY_STEPS,
            costs_p=0.0,
            costs_q=0.0,
        )

        self.p_award_amounts_vt = None  # type: ignore
        self.q_award_amounts_vt = None  # type: ignore
        self.p_award_price_pt = None  # type: ignore
        self.q_award_price_pt = None  # type: ignore
        self.bid_amounts_vt = {}
        self.bid_prices_pt = {}
        self.q_free_amounts_var = TimeStorage(
            # if we only have a battery, we never update this, meaning
            # we consider the full capacity of the inverter to be free
            default_factory=lambda _: s_max_VA,
            step_length=STEP_SIZE_s,
            steps=DAILY_STEPS * 2,
        )
        self.q_free_amounts_var.set_from(0, [None] * DAILY_STEPS)  # type: ignore
        self.q_total_amounts_var = TimeStorage(
            step_length=STEP_SIZE_s, steps=DAILY_STEPS * 2
        )
        self.p_res_am_vt = TimeStorage(
            default_factory=lambda _: 0,
            step_length=STEP_SIZE_s,
            steps=DAILY_STEPS * 2,
        )
        self.p_bat_am_vt = TimeStorage(
            default_factory=lambda _: 0,
            step_length=STEP_SIZE_s,
            steps=DAILY_STEPS * 2,
        )

        self.p_forecast_pt = TimeStorage(step_length=Durations.ONE_HOUR_s, steps=48)
        self.p_forecast_pt.set_from(0, [0] * 24)

        # These need a horizon of 3 days as we already write data
        # for the day after tomorrow when we receive the market
        # result today, but then we still want to output today's
        # data after that.
        self.finc_probabilities = TimeStorage(
            default_factory=lambda _: None,
            step_length=Durations.ONE_HOUR_s,
            steps=3 * 24,
        )
        self.finc_probabilities.set_from(0, [0.5] * 48)

        self.q_efficiency_prices_pt = TimeStorage(
            default_factory=lambda _: None,
            step_length=Durations.ONE_HOUR_s,
            steps=3 * 24,
        )
        self.q_relevant_prices_pt = TimeStorage(
            default_factory=lambda _: None,
            step_length=Durations.QUARTER_HOUR_s,
            steps=2 * DAILY_STEPS,
        )

    @property
    def market_forecast(self) -> MarketForecastParams:
        next_supply_start = self.next_supply_start
        forecast_range = range(
            next_supply_start,
            next_supply_start + Durations.ONE_DAY_s,
            Durations.ONE_HOUR_s,
        )
        num_q_ticks = Durations.ONE_DAY_s // self.q_auction.product_duration_s
        if self.sequencing_mode == SequencingMode.P_ONLY:
            return MarketForecastParams(
                p_clear_price=[self.p_forecast_pt[time] for time in forecast_range],
                q_demand=[None] * num_q_ticks,
                q_clear_price=[None] * num_q_ticks,
                q_eff_price=[None] * num_q_ticks,
            )
        return MarketForecastParams(
            p_clear_price=[self.p_forecast_pt[time] for time in forecast_range],
            q_demand=[
                round(self.q_demand_fc_var[time] / self.q_auction.one_VA_vt)
                for time in range(
                    next_supply_start,
                    next_supply_start + Durations.ONE_DAY_s,
                    Durations.ONE_HOUR_s,
                )
            ],
            q_clear_price=[
                self.finc_probabilities[time] * self.finc_price_pt
                for time in forecast_range
            ],
            q_eff_price=[self.q_efficiency_prices_pt[time] for time in forecast_range],
        )

    @property
    def next_supply_start(self):
        return (self.time // Durations.ONE_DAY_s + 1) * Durations.ONE_DAY_s

    @override
    def handle_parsed_msg(self, message, sender):
        match message:
            case msg.RegisterUnit():
                if self.unit_agent:
                    raise ValueError(
                        f"while trying to register {sender.aid} at trader {self.aid}: "
                        f"unit agent already registered: {self.unit_agent.aid}"
                    )
                self.unit_agent = sender

            case msg.RegisterBattery():
                if self.battery_agent:
                    raise ValueError(
                        f"while trying to register {sender.aid} at trader {self.aid}: "
                        f"battery agent already registered: {self.battery_agent.aid}"
                    )
                self.battery_agent = sender

            case msg.MarketRulesMessage(products):
                for product in products:
                    # We need to set up a bunch of `TimeStorage`s with step length and
                    # step count based on the product's rules
                    daily_values = Durations.ONE_DAY_s // product.product_duration_s
                    time_storage_config = {
                        "step_length": product.product_duration_s,
                        "steps": 2 * daily_values,
                    }
                    if (
                        product.power_type == msg.PowerType.REACTIVE_CAPACITIVE
                        and self.sequencing_mode == SequencingMode.P_ONLY
                    ):
                        time_storage_config["default_factory"] = lambda _: None
                    self.bid_amounts_vt[product.id] = TimeStorage(**time_storage_config)
                    self.bid_amounts_vt[product.id].set_from(0, [0] * daily_values)
                    self.bid_prices_pt[product.id] = TimeStorage(**time_storage_config)
                    self.bid_prices_pt[product.id].set_from(0, [0] * daily_values)
                    if product.power_type == msg.PowerType.ACTIVE_SUPPLY:
                        self.p_auction = product
                        self.p_award_amounts_vt = TimeStorage(**time_storage_config)
                        self.p_award_amounts_vt.set_from(0, [0] * daily_values)
                        self.p_award_price_pt = TimeStorage(**time_storage_config)
                        self.p_award_price_pt.set_from(0, [0] * daily_values)
                    elif product.power_type == msg.PowerType.REACTIVE_CAPACITIVE:
                        self.q_auction = product
                        self.q_award_amounts_vt = TimeStorage(**time_storage_config)
                        self.q_award_amounts_vt.set_from(0, [0] * daily_values)
                        self.q_award_price_pt = TimeStorage(**time_storage_config)
                        self.q_award_price_pt.set_from(0, [0] * daily_values)
                        if product.pricing_rule == msg.PricingRule.PAY_AS_BID:
                            self.q_efficiency_prices_pt.set_from(0, [0] * 48)
                self.market_params = MarketParams(
                    q_market_clearing=self.q_auction.pricing_rule,
                    q_volume_tick=self.q_auction.one_VA_vt * u.W,
                    p_volume_tick=self.p_auction.one_VA_vt * u.W,
                    q_price_tick=self.q_auction.pt_in_EUR_vttt * u.EUR,
                    q_time_tick=self.q_auction.product_duration_s / 3600 * u.h,
                )

            case msg.ImpartUnitFlexibility():
                self.q_free_amounts_var.set_from(
                    message.time,
                    [
                        math.sqrt(self.s_max_VA**2 - p_W**2)
                        for p_W in message.p_gen_max_W
                    ],
                )

                self.next_flex.gen_max = message.p_gen_max_W
                self.next_flex.cons_max = [0.0] * DAILY_STEPS
                self.next_flex.costs_p = message.p_cost_EUR_Wh
                self.next_flex.costs_q = message.q_cost_EUR_varh
                self.unit_received = True
                self.bid_if_ready(message.id)

            case msg.ImpartBatteryFlexibility():
                self.next_flex.soc = message.soc
                self.next_flex.eff_ch = message.eff_ch
                self.next_flex.eff_dis = message.eff_dis
                self.next_flex.capa = message.capa_Wh * u.Wh
                self.next_flex.p_bat_discharge_max = message.p_discharge_max_W * u.W
                self.next_flex.p_bat_charge_max = message.p_charge_max_W * u.W
                self.next_flex.fce_max = message.fce_max
                self.battery_received = True
                self.bid_if_ready(message.id)

            case msg.AwardMessage():
                self.handle_auction_results_msg(message)

            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def bid_if_ready(self, auction_id: str):
        """Checks that all flexibility information has arrived, and
        performs bidding if that is the case.

        This should be called after receiving each `Impart*Flexibility`
        message and setting the appropriate `self.*_received` variable
        to `True`.
        """
        if not (self.unit_received and self.battery_received):
            return

        try:
            if auction_id == self.p_auction.id:
                self.perform_p_bidding(self.next_flex)
            elif auction_id == self.q_auction.id:
                self.perform_q_bidding(self.next_flex)
            else:
                raise ValueError(f"Received flexibility for unexpected id {auction_id}")

        except Exception:
            raise Exception(
                f"{self.aid}, trading for {self.unit_agent} and {self.battery_agent}, "
                "could not perform its optimization"
            )

    def setup_done(self):
        self.send_message(
            msg.RegisterTrader(name=self.aid),
            to=self.market,
        )

    def start_mosaik_step(self, p_price: list[float]):
        if not self.unit_agent and not self.battery_agent:
            warnings.warn(f"{self.aid} has no connected asset agents. Doing nothing.")
            return

        if (
            self.time % self.p_auction.market_interval_s
            == self.p_auction.closing_time_s
        ):
            price_forecast_EUR_MWh = p_price
            start_h = self.p_auction.supply_start_delta_s // Durations.ONE_HOUR_s
            # workaround for reproducibility
            new_random = Random(self.seed)
            self.p_forecast_pt.set_from(
                self.time + self.p_auction.supply_start_delta_s,
                [
                    noisify(
                        EUR_MVAh_to_pt(price_EUR_MWh, self.p_auction),
                        self.noise_beta,
                        new_random,
                    )
                    for price_EUR_MWh in price_forecast_EUR_MWh[start_h : start_h + 24]
                ],
            )

        for auction in [self.p_auction, self.q_auction]:
            if self.time % auction.market_interval_s == auction.closing_time_s:
                if self.unit_agent:
                    self.unit_received = False
                    self.send_message(
                        msg.RequestUnitFlexibility(
                            id=auction.id,
                            time=self.time + auction.supply_start_delta_s,
                            # optimization always expects these in
                            # quarter-hour resolution
                            resolution_s=Durations.QUARTER_HOUR_s,
                        ),
                        to=self.unit_agent,
                    )
                if self.battery_agent:
                    self.battery_received = False
                    self.send_message(
                        msg.RequestBatteryFlexibility(
                            id=auction.id,
                            time=self.time + auction.supply_start_delta_s,
                        ),
                        to=self.battery_agent,
                    )

    def send_bids(
        self,
        product: msg.MarketProduct,
        amounts_vt: Iterable[int],
        prices_pt: Iterable[int],
    ):
        for i, (am_vt, pr_pt) in enumerate(zip(amounts_vt, prices_pt)):
            time = (
                self.time
                + product.supply_start_delta_s
                + i * product.product_duration_s
            )
            self.bid_amounts_vt[product.id][time] = am_vt
            self.bid_prices_pt[product.id][time] = pr_pt
            self.send_message(
                msg.PlaceBid(
                    id="",
                    product_id=product.id,
                    supply_start=time,
                    amount_vt=am_vt,
                    price_pt=pr_pt,
                    is_all_or_nothing=False,
                    location=self.node,
                ),
                to=self.market,
            )

    def perform_p_bidding(self, flex: FlexParams):
        product = self.p_auction
        match self.sequencing_mode:
            case SequencingMode.NAIVE | SequencingMode.P_ONLY:
                optimizer_result = optimize_p_only(
                    self.market_params, self.market_forecast, flex
                )
            case SequencingMode.SEQUENTIAL:
                optimizer_result = optimize_simultaneous(
                    self.market_params, self.market_forecast, flex
                )
            case SequencingMode.SIMULTANEOUS:
                optimizer_result = optimize_simultaneous(
                    self.market_params, self.market_forecast, flex
                )

                self.cached_optimizer_result = optimizer_result
        if self.p_res_am_vt is not None:
            self.p_res_am_vt.set_from(
                self.next_supply_start, optimizer_result["p_amount_res_vt"]
            )
        if self.p_bat_am_vt is not None:
            self.p_bat_am_vt.set_from(
                self.next_supply_start, optimizer_result["p_amount_bat_vt"]
            )
        self.send_bids(
            product,
            optimizer_result["p_amount_total_vt"],
            optimizer_result["p_price_pt"],
        )

    def perform_q_bidding(self, flex: FlexParams):
        product = self.q_auction
        match self.sequencing_mode:
            case SequencingMode.P_ONLY:
                return
            case SequencingMode.NAIVE | SequencingMode.SEQUENTIAL:
                supply_times = range(
                    self.time + product.supply_start_delta_s,
                    self.time
                    + product.supply_start_delta_s
                    + product.market_interval_s,
                    product.product_duration_s,
                )
                p_am = [self.p_award_amounts_vt[time] for time in supply_times]
                optimizer_result = optimize_q_market(
                    self.market_params, self.market_forecast, flex, p_am
                )
            case SequencingMode.SIMULTANEOUS:
                # Simply reuse values from optimization for P-bidding
                optimizer_result = self.cached_optimizer_result

        if any(pr < 0 for pr in optimizer_result["q_price_pt"]):
            logger.warning(
                "Got negative prices from optimization!\n"
                f"{self.market_params=}\n{self.market_forecast=}\n{flex=}\n"
                f"Q-amounts: {optimizer_result["q_amount_total_vt"]}\n"
                f"Q-prices: {optimizer_result["q_price_pt"]}"
            )
        if optimizer_result["q_rel_price_pt"] is not None:
            self.q_relevant_prices_pt.set_from(
                self.time + product.supply_start_delta_s,
                optimizer_result["q_rel_price_pt"],
            )
        self.send_bids(
            product,
            optimizer_result["q_amount_total_vt"],
            optimizer_result["q_price_pt"],
        )

    def handle_auction_results_msg(self, message: msg.AwardMessage):
        if message.product_id == self.p_auction.id:
            self.p_award_amounts_vt[message.supply_start] = message.awarded_amount_vt
            self.p_award_price_pt[message.supply_start] = message.awarded_price
            for t in range(
                message.supply_start,
                message.supply_start + self.p_auction.product_duration_s,
                self.q_auction.product_duration_s,
            ):
                self.q_total_amounts_var[t] = (
                    message.awarded_amount_vt
                    * self.p_auction.one_VA_vt
                    * math.tan(math.acos(0.95))
                )
            if self.sequencing_mode == SequencingMode.P_ONLY:
                self.send_obligation_message(message.supply_start)
        else:
            self.q_award_amounts_vt[message.supply_start] = message.awarded_amount_vt
            self.q_award_price_pt[message.supply_start] = message.awarded_price
            self.q_total_amounts_var[message.supply_start] = (
                self.p_award_amounts_vt[message.supply_start]
                * self.p_auction.one_VA_vt
                * math.tan(math.acos(0.95))
                + message.awarded_amount_vt * self.q_auction.one_VA_vt
            )
            self.send_obligation_message(message.supply_start)

            # Update FINC probabilities and efficiency price forecast
            # for the next day
            if message.marginal_price_pt == self.finc_price_pt:
                factor = 1.25
            else:
                factor = 0.8
            self.finc_probabilities[message.supply_start + Durations.ONE_DAY_s] = (
                prob_update(factor, self.finc_probabilities[message.supply_start])
            )
            self.q_efficiency_prices_pt[message.supply_start + Durations.ONE_DAY_s] = (
                message.efficiency_price_pt
            )

    def send_obligation_message(self, time: int):
        if self.unit_agent:
            self.send_message(
                msg.ObligationMessage(
                    supply_start=time,
                    p_amount_W=(
                        (self.p_award_amounts_vt[time] - self.p_bat_am_vt[time])
                        * self.p_auction.one_VA_vt
                    ),
                    q_amount_var=self.q_total_amounts_var[time],
                ),
                to=self.unit_agent,
            )
        if self.battery_agent:
            self.send_message(
                msg.ObligationMessage(
                    supply_start=time,
                    p_amount_W=self.p_bat_am_vt[time] * self.p_auction.one_VA_vt,
                    q_amount_var=self.q_total_amounts_var[time],
                ),
                to=self.battery_agent,
            )

    def get_mosaik_output(self) -> TraderOutput[float]:
        if not (self.unit_received and self.battery_received):
            raise Exception(
                f"at day {format_time(self.time)}, {self.aid} never received "
                "flexibility info from one of its agents:\n"
                f"{self.unit_agent=}, {self.unit_received=};\n"
                f"{self.battery_agent=}, {self.battery_received=}"
            )

        output: TraderOutput[float] = {}

        p_auct = self.p_auction
        q_auct = self.q_auction
        output_sources: TraderOutput[tuple[TimeStorage | None, float]] = TraderOutput(
            p_award_amount_MW=(self.p_award_amounts_vt, p_auct.one_VA_vt * 1e-6),
            q_award_amount_Mvar=(self.q_award_amounts_vt, q_auct.one_VA_vt * 1e-6),
            p_award_price_EUR_MWh=(
                self.p_award_price_pt,
                p_auct.pt_in_EUR_vttt / p_auct.one_VA_vt * 1e6,
            ),
            q_award_price_EUR_Mvarh=(
                self.q_award_price_pt,
                q_auct.pt_in_EUR_vttt / q_auct.one_VA_vt * 1e6,
            ),
            p_bid_amount_MW=(self.bid_amounts_vt[p_auct.id], p_auct.one_VA_vt * 1e-6),
            p_battery_amount_MW=(self.p_bat_am_vt, p_auct.one_VA_vt * 1e-6),
            p_resource_amount_MW=(self.p_res_am_vt, p_auct.one_VA_vt * 1e-6),
            p_bid_price_EUR_MWh=(
                self.bid_prices_pt[p_auct.id],
                p_auct.pt_in_EUR_vttt / p_auct.one_VA_vt * 1e6,
            ),
            q_bid_amount_Mvar=(self.bid_amounts_vt[q_auct.id], q_auct.one_VA_vt * 1e-6),
            q_bid_price_EUR_Mvarh=(
                self.bid_prices_pt[q_auct.id],
                q_auct.pt_in_EUR_vttt / q_auct.one_VA_vt * 1e6,
            ),
            q_free_amount_Mvar=(self.q_free_amounts_var, 1e-6),
            p_clear_price_fc_EUR_MWh=(
                self.p_forecast_pt,
                p_auct.pt_in_EUR_vttt / p_auct.one_VA_vt * 1e6,
            ),
            q_demand_fc_Mvar=(self.q_demand_fc_var, 1e-6),
            q_clear_price_fc_EUR_Mvarh=(
                self.finc_probabilities,
                self.finc_price_pt * q_auct.pt_in_EUR_vttt / q_auct.one_VA_vt * 1e6,
            ),
            q_eff_price_fc_EUR_Mvarh=(
                self.q_efficiency_prices_pt,
                q_auct.pt_in_EUR_vttt / q_auct.one_VA_vt * 1e6,
            ),
            q_rel_price_fc_EUR_Mvarh=(
                self.q_relevant_prices_pt,
                q_auct.pt_in_EUR_vttt / q_auct.one_VA_vt * 1e6,
            ),
        )

        for key, (storage, factor) in output_sources.items():
            try:
                if storage is not None and (value := storage[self.time]) is not None:
                    output[key] = value * factor
            except Exception as e:
                raise Exception(f"preparing {key} output for {self.aid}") from e
        return output


T = TypeVar("T")


class TraderOutput(Generic[T], TypedDict, total=False):
    p_award_amount_MW: T
    q_award_amount_Mvar: T
    p_award_price_EUR_MWh: T
    q_award_price_EUR_Mvarh: T
    p_bid_amount_MW: T
    p_battery_amount_MW: T
    p_resource_amount_MW: T
    p_bid_price_EUR_MWh: T
    q_bid_amount_Mvar: T
    q_bid_price_EUR_Mvarh: T
    q_free_amount_Mvar: T
    p_clear_price_fc_EUR_MWh: T
    q_demand_fc_Mvar: T
    q_clear_price_fc_EUR_Mvarh: T
    q_eff_price_fc_EUR_Mvarh: T
    q_rel_price_fc_EUR_Mvarh: T


def prob_update(factor, prob):
    """This function "updates" a probability using the factor `factor`.
    It does this by transforming the probability to odds (using the
    formula p -> p / (1 - p)), multiplying the odds by the factor, and
    transforming the new odds back to a probability.

    *Odds* are an alternative representation of probabilities which
    express them in the interval [0, infinity]. The advantage is that
    multiplication of odds with non-negative numbers is sensible and
    always results in new odds.
    """
    # This formula is the result of algebraically simplifying the
    # procedure "transform to odds - multiply - transform back to
    # probability"
    result = factor * prob / (1 - prob + factor * prob)
    assert result >= 0.0, f"{factor=}, {prob=}"
    return result

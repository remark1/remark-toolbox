from __future__ import annotations

import typing
import warnings

import arrow
import numpy as np
import pandas as pd
from loguru import logger  # noqa: F401
from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.trader import TraderAgent
from mas.messages import (
    ImpartUnitFlexibility,
    ObligationMessage,
    RegisterUnit,
    RequestUnitFlexibility,
)
from mas.util import DAILY_STEPS, Durations, STEP_SIZE_s, noisify
from mosaik_components.mango import AgentAddress
from mosaik_components.pv.pvgis import PVGIS
from util.time_storage import TimeStorage

MAX_POWER_MW = 0.1

# needed to instantiate second PVGIS Model for getting forecast Data
PVSIM_PARAMS = {
    # 'start_date' : START,
    "local_cache_dir": None,  # it caches PVGIS API requests
    "verbose": True,  # don't print PVGIS parameters and requests
}
DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"


class PVGenAgent(MaMoAgent):
    trader: AgentAddress[TraderAgent]
    # pv_forecast_w: TimeStorage[float]
    pv_forecast_W: TimeStorage[float]
    p_obligations_W: TimeStorage[float]
    q_obligations_var: TimeStorage[float]
    noise_beta: float

    def __init__(
        self,
        suggested_aid: str,
        container: Container,
        seed: int,
        trader: AgentAddress[TraderAgent],
        pv_params: dict,
        start_date: str,
        noise_beta: float,
    ):
        super().__init__(suggested_aid, container, seed)
        self.trader = trader
        self.noise_beta = noise_beta
        self.pv_params = pv_params
        # add some variety to PV location
        self.pv_params["lat"] = pv_params["lat"] + self.agent_random.uniform(0, 1)
        self.pv_params["lon"] = pv_params["lon"] + self.agent_random.uniform(0, 1)
        self.pv_forecast_W = TimeStorage(steps=2 * DAILY_STEPS, step_length=STEP_SIZE_s)
        try:
            logger.info(f"Trying to start PV-GIS model in {self.aid}")
            self.pv_forecast_model = PVForecastModel(start_date, pv_params, noise_beta)
        except Exception as e:
            logger.exception(e)
            raise
        self.set_day_forecast(0)
        self.p_obligations_W = TimeStorage(
            default_factory=lambda _: 0.0,
            steps=2 * DAILY_STEPS,
            step_length=STEP_SIZE_s,
        )
        self.q_obligations_var = TimeStorage(
            default_factory=lambda _: 0.0,
            steps=2 * DAILY_STEPS,
            step_length=STEP_SIZE_s,
        )

    def handle_parsed_msg(self, message, sender):
        match message:
            case RequestUnitFlexibility(id=id, time=time, resolution_s=resolution_s):
                self.send_message(
                    ImpartUnitFlexibility(
                        id=id,
                        time=time,
                        p_gen_max_W=[
                            self.pv_forecast_W[t]
                            for t in range(
                                time,
                                time + Durations.ONE_DAY_s,
                                resolution_s,
                            )
                        ],
                        p_cost_EUR_Wh=0.0,
                        q_cost_EUR_varh=0.0,
                    ),
                    to=self.trader,
                )
            case ObligationMessage(
                supply_start=time, p_amount_W=p_amount_W, q_amount_var=q_amount_var
            ):
                # Save obligation to pass on to panels
                self.p_obligations_W[time] = p_amount_W
                self.q_obligations_var[time] = q_amount_var
            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def setup_done(self):
        self.send_message(
            RegisterUnit(),
            to=self.trader,
        )

    def start_mosaik_step(self, pv_p_MW: float):
        if self.time % Durations.ONE_DAY_s == 0:
            # set the next day's forecast
            self.set_day_forecast(self.time + Durations.ONE_DAY_s)

    def set_day_forecast(self, day_start: int):
        self.pv_forecast_W.set_from(
            day_start, self.pv_forecast_model.get_forecast(day_start, self.agent_random)
        )

    def get_mosaik_output(self) -> PVGenAgentOutput:
        return {
            "p_amount_MW": self.p_obligations_W[self.time] * 1e-6,
            "q_amount_Mvar": self.q_obligations_var[self.time] * 1e-6,
            "p_MW_fc": self.pv_forecast_W[self.time] * 1e-6,
        }


class PVGenAgentOutput(typing.TypedDict):
    p_amount_MW: float
    q_amount_Mvar: float
    p_MW_fc: float


class PVForecastModel:
    """
    Convenience class for wrapping the PVGIS model, which generates the output with applied noise as a PV forecast
    """

    def __init__(self, start_date, pv_params, noise):
        self.pv_params = pv_params
        self.start_date = arrow.get(start_date, DATE_FORMAT)
        self.noise = noise
        self.pv_gis = PVGIS(**PVSIM_PARAMS)
        self.yearly_forecast = self.get_forecast_year()

    def get_forecast_year(self):
        # get PV data for one year in hourly resolution
        production, info = self.pv_gis.get_production_timeserie(**self.pv_params)
        # change history year to current one
        production.index = pd.to_datetime(
            production.index, utc=True
        ) + pd.offsets.DateOffset(years=self.start_date.year - production.index[0].year)
        old_index = production.index.copy()
        # change to quarter hour resolution
        new_step_size = pd.Timedelta(900, unit="seconds")
        production = production.fillna(0).resample(new_step_size).sum()
        new_index = sorted(production.index.get_indexer(old_index, method="ffill"))
        for i in range(0, len(new_index) - 1):  # rescaling with new step size
            production.iloc[new_index[i] : new_index[i + 1]] = production.iloc[
                new_index[i] : new_index[i + 1]
            ].mean()
        return production

    def get_forecast(self, time_step, agent_random):
        """Returns the forecast from mosaik step `time_step` for one day
        in quarter-hourly resolution.
        """
        current_time = self.start_date.shift(seconds=+time_step)
        start_last_hour = current_time.replace(minute=0, second=0)
        current_index = self.yearly_forecast.index.get_indexer(
            [start_last_hour.datetime], method="ffill"
        )[0]
        # get values from current hour and duration of one day
        p_vals = self.yearly_forecast.iloc[current_index : current_index + 96].values
        pv_forecast_w = noisify(p_vals, self.noise, agent_random)
        # pv_forecast_w = TimeStorage(lambda _: 0.0, steps=2 * 24, step_length=Durations.ONE_HOUR_s)
        # for i, p in enumerate(p_vals):
        #     pv_forecast_w[time_step + 3600 * i] = noisify(p, self.noise)
        return pv_forecast_w.tolist()

from __future__ import annotations

import warnings
from collections import defaultdict
from typing import Awaitable, Callable, TypedDict, override

from loguru import logger
from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.grid_operator import GridOperatorAgent
from mas.auctions.base import Auction, AuctionResult
from mas.auctions.fixed_price_auction import FixedPriceAuction
from mas.auctions.q_auction import SensitivityAuction
from mas.messages import (
    ImpartMarketSummary,
    ImpartQSensitivitiesAndDemand,
    MarketRulesMessage,
    PlaceBid,
    PricingRule,
    RegisterTrader,
    recreate_int_keys,
)
from mas.util import (
    Durations,
    EUR_MVAh_to_pt,
    ProductResolution,
    format_time,
    pt_to_EUR_MVAh,
)
from mosaik_components.mango import AgentAddress
from util.time_storage import TimeStorage

logger = logger.bind(context="mo")


class MarketAgent(MaMoAgent):
    grid_operator: AgentAddress[GridOperatorAgent]
    traders: dict[AgentAddress, str]
    auctions: dict[str, Auction[AgentAddress]]
    p_auction: FixedPriceAuction[AgentAddress]
    q_auction: SensitivityAuction[AgentAddress]

    p_prices_EUR_MWh: TimeStorage[float]

    marginal_q_prices_pt: dict[int, int]
    efficiency_q_prices_pt: dict[int, int]

    def __init__(
            self,
            suggested_aid: str,
            container: Container,
            seed: int,
            grid_operator: AgentAddress[GridOperatorAgent],
            q_market_resolution: ProductResolution,
            sequencing_mode: str,
            pricing_rule: PricingRule,
    ):
        super().__init__(suggested_aid=suggested_aid, container=container, seed=seed)

        self.traders = {}
        self.grid_operator = grid_operator
        self.p_auction = FixedPriceAuction(self.p_price)
        self.sequencing_mode = sequencing_mode
        match sequencing_mode:
            case "SIMULTANEOUS":
                closing_time = 15 * Durations.ONE_HOUR_s + Durations.QUARTER_HOUR_s
            case _:
                closing_time = 16 * Durations.ONE_HOUR_s
        self.q_auction = SensitivityAuction(
            pricing_rule=pricing_rule,
            closing_time=closing_time,
            product_duration_s=q_market_resolution,
        )
        self.q_market_resolution = q_market_resolution
        self.auctions = {
            auction.product_description.id: auction
            for auction in [self.p_auction, self.q_auction]
        }

        self.p_prices_EUR_MWh = TimeStorage(steps=48, step_length=3600)
        self.marginal_q_prices_pt = {}
        self.efficiency_q_prices_pt = {}

    @override
    def handle_parsed_msg(self, message, sender):
        match message:
            case RegisterTrader(name):
                self.traders[sender] = name
                self.send_message(
                    MarketRulesMessage(
                        [
                            auction.product_description
                            for auction in self.auctions.values()
                        ]
                    ),
                    to=sender,
                )

            case PlaceBid(product_id=product_id):
                self.auctions[product_id].add_bid(sender, message)

            case ImpartQSensitivitiesAndDemand(supply_time, target_MVar, sensivitities):
                self.q_auction.set_sensitivities(
                    supply_time, target_MVar, recreate_int_keys(sensivitities)
                )

            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def p_price(self, time):
        price_EUR_MWh = self.p_prices_EUR_MWh[time]
        return int(EUR_MVAh_to_pt(price_EUR_MWh, self.p_auction.product_description))

    @override
    def start_mosaik_step(self, p_price: list[float]) -> None:
        if self.time % 3600 == 0:
            self.p_prices_EUR_MWh.set_from(self.time, p_price)
        self.schedule_clearing(self.p_auction, self.post_p_clear)
        if self.sequencing_mode != "P_ONLY":
            self.schedule_clearing(self.q_auction, self.post_q_clear)

    def schedule_clearing(
            self,
            auction: Auction[AgentAddress],
            post_clear: Callable[[int, AuctionResult[AgentAddress]], Awaitable[None]],
    ):
        product = auction.product_description
        if self.time % product.market_interval_s == product.closing_time_s:
            clearing_time = self.time + product.market_result_delta_s
            supply_start_time = self.time + product.supply_start_delta_s
            supply_range = range(
                supply_start_time,
                supply_start_time + product.market_interval_s,
                product.product_duration_s,
            )
            logger.info(
                f"scheduling clearing of {product.id}:\n"
                f"current time:  day {format_time(self.time)} (= closing time)\n"
                f"clearing time: day {format_time(clearing_time)}\n"
                f"supply starts: [day {format_time(supply_range.start)};  "
                f"day {format_time(supply_range.start + supply_range.step)};  ...;  "
                f"day {format_time(supply_range.stop)})"
            )
            for supply_time in supply_range:
                self.schedule_timestamp_task(
                    self.clear_auction(auction, post_clear, supply_time), clearing_time
                )

    async def clear_auction(
            self,
            auction: Auction[AgentAddress],
            post_clear: Callable[[int, AuctionResult[AgentAddress]], Awaitable[None]],
            supply_time: int,
    ):
        results = auction.clear(supply_time)
        for bidder, award_message in results.award_messages:
            self.send_message(award_message, to=bidder)

        await post_clear(supply_time, results)

    async def post_p_clear(
            self, supply_time: int, results: AuctionResult[AgentAddress]
    ):
        # Inform grid operator
        p_MW_by_node: dict[int, float] = defaultdict(float)
        one_W_vt = self.p_auction.product_description.one_VA_vt
        for _, award_message in results.award_messages:
            p_MW_by_node[award_message.location] = (
                    award_message.awarded_amount_vt * one_W_vt * 1e-6
            )
        self.send_message(
            ImpartMarketSummary(
                supply_time=supply_time,
                p_MW_by_bus=p_MW_by_node,
            ),
            to=self.grid_operator,
        )

    async def post_q_clear(
            self, supply_time: int, results: AuctionResult[AgentAddress]
    ):
        self.marginal_q_prices_pt[supply_time] = results.marginal_price_pt
        self.efficiency_q_prices_pt[supply_time] = results.efficiency_price_pt

    @override
    def get_mosaik_output(self) -> MarketOutput:
        output: MarketOutput = {}
        output["p_price_EUR_MWh"] = pt_to_EUR_MVAh(
            self.p_price(self.time), self.p_auction.product_description
        )
        if (q_price_pt := self.marginal_q_prices_pt.get(self.time)) is not None:
            output["marginal_q_price_EUR_Mvarh"] = pt_to_EUR_MVAh(
                q_price_pt, self.q_auction.product_description
            )
        if (q_price_pt := self.efficiency_q_prices_pt.get(self.time)) is not None:
            output["efficiency_q_price_EUR_Mvarh"] = pt_to_EUR_MVAh(
                q_price_pt, self.q_auction.product_description
            )
        return output


class MarketOutput(TypedDict, total=False):
    p_price_EUR_MWh: float
    marginal_q_price_EUR_Mvarh: float
    efficiency_q_price_EUR_Mvarh: float

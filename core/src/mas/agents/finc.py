import typing
from typing import override, Any
import warnings

from loguru import logger  # noqa: F401
from mango.container.core import Container
from mas.agents.base import MaMoAgent, UnknownMessageWarning
from mas.agents.market import MarketAgent
from mas.messages import (
    AwardMessage,
    PlaceBid,
    MarketProduct,
    MarketRulesMessage,
    RegisterTrader,
)
from mas.util import Durations, EUR_MVAh_to_pt
from mosaik_components.mango import AgentAddress

from util.time_storage import TimeStorage


class FincOutput(typing.TypedDict, total=False):
    q_award_amount_Mvar: float
    q_award_price_EUR_Mvarh: float


class FincTrader(MaMoAgent):
    market: AgentAddress
    q_auction: MarketProduct
    node: int
    cost_EUR_Mvarh: float
    cost_pt: int
    q_award_amounts_vt: TimeStorage[int]
    q_award_price_pt: TimeStorage[int]

    def __init__(
            self,
            suggested_aid: str,
            container: Container,
            seed: int,
            market: AgentAddress[MarketAgent],
            node: int,
            cost_EUR_Mvarh: float,

    ):
        super().__init__(suggested_aid, container, seed)
        self.market = market
        self.node = node
        self.cost_EUR_Mvarh = cost_EUR_Mvarh
        self.q_award_amounts_vt = None  # type: ignore
        self.q_award_price_pt = None  # type: ignore
        self.bid_amounts_vt = {}

    @override
    def handle_parsed_msg(self, message, sender: AgentAddress):
        match message:
            case MarketRulesMessage(products):
                for product in products:
                    # We need to set up a bunch of `TimeStorage`s with step length and
                    # step count based on the product's rules

                    if product.power_type == "reactive_capacitive":
                        daily_values = Durations.ONE_DAY_s // product.product_duration_s
                        time_storage_config = {
                            "step_length": product.product_duration_s,
                            "default_factory": lambda _: None,
                            "steps": 2 * daily_values,
                        }
                        self.q_auction = product
                        self.cost_pt = round(EUR_MVAh_to_pt(self.cost_EUR_Mvarh, product))
                        self.q_award_amounts_vt = TimeStorage(**time_storage_config)
                        self.q_award_amounts_vt.set_from(0, [0] * daily_values)
                        self.q_award_price_pt = TimeStorage(**time_storage_config)
                        self.q_award_price_pt.set_from(0, [0] * daily_values)
            case AwardMessage():
                self.handle_auction_results_msg(message)
            case _:
                warnings.warn(UnknownMessageWarning(self.aid, message))

    def handle_auction_results_msg(self, message: AwardMessage):
        if message.product_id == self.q_auction.id:
            self.q_award_amounts_vt[message.supply_start] = message.awarded_amount_vt
            self.q_award_price_pt[message.supply_start] = message.awarded_price

    @override
    def setup_done(self):
        self.send_message(
            RegisterTrader(name=self.aid),
            to=self.market,
        )

    def start_mosaik_step(self) -> None:
        if (
                self.time % self.q_auction.market_interval_s
                == self.q_auction.closing_time_s
        ):
            supply_times = range(
                self.time + self.q_auction.supply_start_delta_s,
                self.time
                + self.q_auction.supply_start_delta_s
                + self.q_auction.market_interval_s,
                self.q_auction.product_duration_s,
            )
            for supply_time in supply_times:
                self.send_message(
                    PlaceBid(
                        id=None,
                        product_id=self.q_auction.id,
                        supply_start=supply_time,
                        amount_vt=100_000_000,
                        price_pt=self.cost_pt,
                        is_all_or_nothing=False,
                        location=self.node,
                    ),
                    to=self.market,
                )

    @override
    def get_mosaik_output(self) -> FincOutput:
        output: FincOutput = {}
        q_auct = self.q_auction
        if self.q_award_amounts_vt is not None and self.q_award_amounts_vt[self.time] is not None:
            output['q_award_amount_Mvar'] = self.q_award_amounts_vt[self.time] * q_auct.one_VA_vt * 1e-6
            output['q_award_price_EUR_Mvarh'] = self.q_award_price_pt[
                                                    self.time] * q_auct.pt_in_EUR_vttt / q_auct.one_VA_vt * 1e6
        return output

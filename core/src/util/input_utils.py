from typing import Dict, Iterable, TypeVar

T = TypeVar("T")


def get_single(it: Iterable[T]) -> T:
    iterator = iter(it)
    val = next(iterator)
    try:
        next(iterator)
    except StopIteration:
        return val

    raise ValueError("Iterable is not a singleton")


def get_value(d: Dict[str, T]) -> T:
    return get_single(d.values())

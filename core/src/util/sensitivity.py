"""
This module calculates the sensitivity matrix of a power-flow setup in
pandapower terms. pandapower differs from the "normal" load-flow setup
by having load and gen grid elements instead of PQ and PV buses.

All sensitivities indicate the linearized change of a physical quantity in the numerator
(e.g. voltage magnitude in pu) if the physical quantity in the denominator is changed by 1 unit
(e.g. reactive power changed by 1 MVar).

This module should be imported qualified, so that it's methods read
sensitivity.of_bus_vm_pu_on_load_q_mvar and similar.
"""

from typing import Literal

import numpy as np
import pandapower as pp
import pandas as pd
from pandapower.pypower.idx_bus import VA, VM


def of_ext_grid_q_mvar_on_element(
    net: pp.pandapowerNet,
    elem: Literal["load", "gen", "sgen"],
    power_type: Literal["p_mw", "q_mvar"],
) -> pd.DataFrame:
    source_buses = get_source_buses(elem, power_type)
    internal_sens = internal_sensitivity(net)
    element_internal_buses = net._pd2ppc_lookups["bus"][net[elem].bus]

    sens = (
        of_ext_grid_q_pu_on_vm_pu_and_va_deg(net)
        @ internal_sens.loc[:, source_buses]
        .reindex(columns=element_internal_buses, fill_value=0.0)
        .set_axis(net[elem].index, axis=1)
    ) * net[elem].scaling

    # TODO: Check whether a sign factor is needed
    sens.index = net.ext_grid.index
    return sens


def of_ext_grid_q_mvar_on_sgen_p_mw(net: pp.pandapowerNet) -> pd.DataFrame:
    return of_ext_grid_q_mvar_on_element(net, "sgen", "p_mw")


def of_ext_grid_q_mvar_on_sgen_q_mvar(net: pp.pandapowerNet) -> pd.DataFrame:
    return of_ext_grid_q_mvar_on_element(net, "sgen", "q_mvar")


def of_ext_grid_q_mvar_on_gen_p_mw(net: pp.pandapowerNet) -> pd.DataFrame:
    return of_ext_grid_q_mvar_on_element(net, "gen", "p_mw")


def of_ext_grid_q_mvar_on_load_p_mw(net: pp.pandapowerNet) -> pd.DataFrame:
    return of_ext_grid_q_mvar_on_element(net, "load", "p_mw")


def of_ext_grid_q_mvar_on_load_q_mvar(net: pp.pandapowerNet) -> pd.DataFrame:
    return of_ext_grid_q_mvar_on_element(net, "load", "q_mvar")


def of_bus_vm_pu_on_element(
    net: pp.pandapowerNet,
    elem: Literal["load", "gen", "sgen"],
    power_type: Literal["p_mw", "q_mvar"],
) -> pd.DataFrame:
    """Calculate the sensitivity of the voltage magnitude [pu] at the
    buses on the power (active [MW] or reactive [MVAr]) at the given
    elements.

    :param net: A ``pandapowerNet``.
    :return: A pandas DataFrame with rows corresponding to the buses
        and columns corresponding to the elements of the given
        ``element_type``.
    """

    # To translate the sensitivities to the elements, we just need to
    # pick the right values of the general sensitivity matrix and
    # re-index the result to use the element's index.
    # (For loads, we also flip the sign.)
    source_buses = get_source_buses(elem, power_type)
    match elem:
        case "load":
            sign_factor = -1.0
        case _:
            sign_factor = 1.0

    internal_sens = internal_sensitivity(net).loc["vm_pu_at_pq", source_buses]

    # Lookup for reindexing according to the buses and elements
    lookup = net._pd2ppc_lookups["bus"]
    bus_lookup = lookup[net.bus.index]
    elem_lookup = lookup[net[elem].bus]

    expanded_sens = internal_sens.reindex(
        index=bus_lookup,
        columns=elem_lookup,
        # missing values correspond to PV buses, where the voltage
        # magnitude is unaffected by changes to power values
        fill_value=0.0,
    )
    # Change internal indices to pandapower indices
    sens = expanded_sens.set_axis(net.bus.index, axis="index").set_axis(
        net[elem].index, axis="columns"
    )
    return sign_factor * sens * net[elem].scaling / net.sn_mva


def get_source_buses(
    elem: Literal["load", "gen", "sgen"],
    power_type: Literal["p_mw", "q_mvar"],
):
    match (elem, power_type):
        case ("gen", "q_mvar"):
            raise ValueError(
                "Reactive power is not controllable for gens, so sensitivities don't "
                "exist."
            )
        case ("gen", "p_mw"):
            return "p_pu_at_pv"
        case (_, "p_mw"):
            return "p_pu_at_pq"
        case (_, "q_mvar"):
            return "q_pu_at_pq"


def of_bus_vm_pu_on_sgen_p_mw(net: pp.pandapowerNet) -> pd.DataFrame:
    """Calculate the sensitivity of the voltage magnitude [pu] at the
    buses on real power P [MW] of the static generators (sgens).

    :param net: A ``pandapowerNet``
    :return: A pandas DataFrame with rows corresponing to the buses
        and columns corresponding to the sgens.
    """
    return of_bus_vm_pu_on_element(net, "sgen", "p_mw")


def of_bus_vm_pu_on_sgen_q_mvar(net: pp.pandapowerNet) -> pd.DataFrame:
    """Calculate the sensitivity of the voltage magnitude [pu] at the
    buses on reactive power P [MVAr] of the static generators (sgens).

    :param net: A ``pandapowerNet``
    :return: A pandas DataFrame with rows corresponing to the buses
        and columns corresponding to the sgens.
    """
    return of_bus_vm_pu_on_element(net, "sgen", "q_mvar")


def of_bus_vm_pu_on_gen_p_mw(net: pp.pandapowerNet) -> pd.DataFrame:
    """Calculate the sensitivity of the voltage magnitude [pu] at the
    buses on real power P [MW] of the generators (gens).

    :param net: A ``pandapowerNet``
    :return: A pandas DataFrame with rows corresponing to the buses
        and columns corresponding to the gens.
    """
    return of_bus_vm_pu_on_element(net, "gen", "p_mw")


def of_bus_vm_pu_on_load_p_mw(net: pp.pandapowerNet) -> pd.DataFrame:
    """Calculate the sensitivity of the voltage magnitude [pu] at the
    buses on real power P [MW] of the loads.

    :param net: A ``pandapowerNet``
    :return: A pandas DataFrame with rows corresponing to the buses
        and columns corresponding to the loads.
    """
    return of_bus_vm_pu_on_element(net, "load", "p_mw")


def of_bus_vm_pu_on_load_q_mvar(net: pp.pandapowerNet) -> pd.DataFrame:
    """Calculate the sensitivity of the voltage magnitude [pu] at the
    buses on reactive power Q [MVAr] of the loads.

    :param net: A ``pandapowerNet``
    :return: A pandas DataFrame with rows corresponing to the buses
        and columns corresponding to the loads.
    """
    return of_bus_vm_pu_on_element(net, "load", "q_mvar")


def of_ext_grid_q_pu_on_vm_pu_and_va_deg(net: pp.pandapowerNet) -> pd.DataFrame:
    """Calculate the sensitivity of the reactive power [pu] at the external grids
        on the voltage magnitude [pu] and voltage angle [deg] at the PQ and PV buses.

    :param net: Grid created through pandapower API. These grids may also be synthetic or benchamarks networks
        provided by the pandapower networks module.
    :type net: pp.pandapowerNet
    :return: Pandas DataFrame containing all sensitivities of q [pu] at the respective external grid
        (multiple external grids are possible) with respect to vm [pu] and va [deg] at the PQ and PV buses.
        Rows are indexed according to the PQ and PV buses, collumns are indexed with respect to external grid buses
    :rtype: pd.DataFrame
    """

    # create DataFrame
    va_vm_df = pd.DataFrame()

    # define index, va and vm
    va = pd.Series(net._ppc["internal"]["bus"][:, VA], name="va_deg")
    vm = pd.Series(net._ppc["internal"]["bus"][:, VM], name="vm_pu")

    # define pv and pq buses as indicies for reindexing
    pv_buses = net._ppc["internal"]["pv"]
    pq_buses = net._ppc["internal"]["pq"]
    pvpq_buses = np.hstack((pv_buses, pq_buses))

    # call function for index of to be returned DataFrame and create Pandas Series to use .reindex_like()
    index = va_vm_index(net)
    pvpq_index = pd.Series(index=pvpq_buses, dtype=float)
    pq_index = pd.Series(index=pq_buses, dtype=float)

    # loop that adds a column of sensitivity of q, vm and va for each external grid
    for k in range(len(net.ext_grid)):
        slack_bus = net._pd2ppc_lookups["bus"][net.ext_grid.bus[k]]
        Ybus_row = pd.Series(
            np.asarray(net._ppc["internal"]["Ybus"].todense()[slack_bus, :])[0]
        )
        G = np.real(Ybus_row)
        B = np.imag(Ybus_row)
        va_diff = va.at[slack_bus] - va
        va_diff_cos = np.cos(np.deg2rad(va_diff))
        va_diff_sin = np.sin(np.deg2rad(va_diff))
        va_deg_at_pvpq = (
            -vm.at[slack_bus] * vm * (G * va_diff_cos + B * va_diff_sin)
        ).reindex_like(pvpq_index)
        vm_pu_at_pq = (
            vm.at[slack_bus] * (G * va_diff_sin - B * va_diff_cos)
        ).reindex_like(pq_index)
        va_vm_col = pd.concat([va_deg_at_pvpq, vm_pu_at_pq], axis=0).set_axis(
            index, axis=0
        )
        va_vm_df = pd.concat([va_vm_df, va_vm_col], axis=1)

    # set external grid buses as column index
    va_vm_df.set_axis(net.ext_grid.bus, axis=1)

    return va_vm_df.transpose()


def va_vm_index(net):
    """Gives indicies of the voltage angles and non-constant voltage magnitudes at the (internal) PV and PQ buses.

    :param net: Grid created through pandapower API. These grids may also be synthetic or benchamarks networks
        provided by the pandapower networks module.
    :type net: pp.pandapowerNet
    :return: Indicies of voltage angles and non-constant voltage magnitudes at the (internal) PV and PQ buses
    :rtype: <class 'pandas.core.indexes.multi.MultiIndex'>
    """
    pv_buses = net._ppc["internal"]["pv"]
    pq_buses = net._ppc["internal"]["pq"]
    return pd.MultiIndex.from_tuples(
        [
            *(("va_deg_at_pv", bus) for bus in pv_buses),
            *(("va_deg_at_pq", bus) for bus in pq_buses),
            *(("vm_pu_at_pq", bus) for bus in pq_buses),
        ]
    )


def p_q_index(net):
    """Gives indicies of the real and non-constant reactive powers at the (internal) PV and PQ buses.

    :param net: Grid created through pandapower API. These grids may also be synthetic or benchamarks networks
        provided by the pandapower networks module.
    :type net: pp.pandapowerNet
    :return: Indicies of real and non-constant reactive powers at the (internal) PV and PQ buses
    :rtype: <class 'pandas.core.indexes.multi.MultiIndex'>
    """
    pv_buses = net._ppc["internal"]["pv"]
    pq_buses = net._ppc["internal"]["pq"]
    return pd.MultiIndex.from_tuples(
        [
            *(("p_pu_at_pv", bus) for bus in pv_buses),
            *(("p_pu_at_pq", bus) for bus in pq_buses),
            *(("q_pu_at_pq", bus) for bus in pq_buses),
        ]
    )


def internal_jacobian(net):
    """Generate internal jacobian matrix

    :param net: Grid created through pandapower API. These grids may also be synthetic or benchamarks networks
        provided by the pandapower networks module.
    :type net: pp.pandapowerNet
    :return: Pandas DataFrame containing the internal jacobian matrix with partial derivatives
        of all p [pu] and q [pu] with respect to vm [pu] and va [deg] relations
    :rtype: pd.DataFrame
    """
    if net._ppc is None:
        raise Exception(
            "The pandapower power flow has to be performed before using this module"
        )

    return pd.DataFrame(
        net._ppc["internal"]["J"].todense(),
        columns=va_vm_index(net),
        index=p_q_index(net),
    )


def internal_sensitivity(net) -> pd.DataFrame:
    """Invert internal jacobain matrix to generate internal sensitivity matrix

    :param net: Grid created through pandapower API. These grids may also be synthetic or benchamarks networks
        provided by the pandapower networks module.
    :type net: pp.pandapowerNet
    :return: Pandas DataFrame containing internal sensitivity matrix with partial derivatives
        of all vm [pu] and va [deg] with respect to p [pu] and q [pu] relations
    :rtype: pd.DataFrame
    """

    # Return cached result if it exists and was computed for the same
    # res_bus.
    if hasattr(net, "_sensitivity_data") and pp.dataframes_equal(
        net.res_bus, net._res_bus_copy
    ):
        return net._sensitivity_data

    jac = internal_jacobian(net)
    sens = pd.DataFrame(np.linalg.inv(jac.values), index=jac.columns, columns=jac.index)
    net._sensitivity_data = sens
    net._res_bus_copy = net.res_bus
    return sens

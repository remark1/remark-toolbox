from typing import Any, Callable, Dict, List
import mosaik_api_v3
from mosaik_api_v3.types import (
    CreateResult,
    InputData,
    Meta,
    ModelName,
    OutputData,
    OutputRequest,
    SimId,
    Time,
)

META: Meta = {
    "api_version": "3.0",
    "type": "time-based",
    "models": {
        "Function": {
            "public": True,
            "params": ["function"],
            "attrs": ["value"],
            "any_inputs": False,
        }
    },
    "extra_methods": [],
}


class Simulator(mosaik_api_v3.Simulator):
    step_size: int
    functions: Dict[str, Callable[[Time], Any]]

    def __init__(self):
        super().__init__(META)

    def init(self, sid: SimId, time_resolution: float = 1, step_size=900) -> Meta:
        self.step_size = step_size
        self.functions = {}
        self.time = 0
        return self.meta

    def create(
        self, num: int, model: ModelName, function: Callable[[Time], Any]
    ) -> List[CreateResult]:
        new_entities = []
        for i in range(len(self.functions), len(self.functions) + num):
            eid = f"Function-{i}"
            self.functions[eid] = function
            new_entities.append(
                {
                    "eid": eid,
                    "type": model,
                }
            )
        return new_entities

    def step(self, time: Time, inputs: InputData, max_advance: Time) -> Time | None:
        assert inputs == {}
        self.time = time
        return time + self.step_size

    def get_data(self, outputs: OutputRequest) -> OutputData:
        return {eid: {"value": self.functions[eid](self.time)} for eid in outputs}

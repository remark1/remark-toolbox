from typing import Callable, Collection, Generic, List, Never, TypeVar

from mas.util import format_time

V = TypeVar("V")


_SENTINEL: Never = object()  # type: ignore


def fail(x: int) -> Never:
    return _SENTINEL  # type: ignore


class TimeStorage(Generic[V]):
    """
    Representation of a fixed-length "window" of values moving forward in time.

    For example, if we have the sequence of values

        0  10  20  30  40  50  60  70  ...

    and a `steps` is 4, we would be able to access the values 0, 10, 20, 30
    at the indices 0, 1, 2, 3 at first. Once the window moves forward, accessing
    index 0 becomes invalid and we can access the values 10, 20, 30, 40 at the
    indices 1, 2, 3, 4 instead, and so on.

    In this implementation, the window automatically moves forward whenever a
    value in the future is accessed (read or written); trying to access values
    in the past raises an exception.

    In addition to the moving window, the stepping size of the indices can be
    chosen. For example, you might have values at times 0, 900, 1800, 2700, ...
    instead of 0, 1, 2, 3, ...
    """

    _store: List[V]
    """The store for the values. This is organized as a ring buffer, i.e. the
    value at index i should be accessed via
        self._store[i % len(self._store)]
    """
    _start: int
    _shift: int
    _step_length: int
    _default_factory: Callable[[int], V]

    def __init__(
        self,
        *,
        steps: int,
        step_length: int,
        default_factory: Callable[[int], V] = fail,
        shift: int = 0,
    ) -> None:
        super().__init__()
        self._default_factory = default_factory
        self._store = [default_factory(i * step_length + shift) for i in range(steps)]
        self._step_length = step_length
        self._start = 0
        self._shift = shift

    def _interval(self, time: int) -> int:
        return (time - self._shift) // self._step_length

    def __contains__(self, time) -> bool:
        return self._start <= self._interval(time) < self._start + len(self._store)

    def __getitem__(self, time: int) -> V:
        interval = self._interval(time)
        if interval < self._start:
            raise KeyError(f"Time {time} lies too far in the past.")
        self.advance_to(interval + 1)
        value = self._store[interval % len(self._store)]
        if value is _SENTINEL:
            raise KeyError(
                f"no default factory provided, but value requested for {time} "
                f"(day {format_time(time)})"
            )
        return value

    def __setitem__(self, time: int, value: V) -> None:
        if not (time - self._shift) % self._step_length == 0:
            raise KeyError(
                f"Time {time} does not fall onto interval start when using "
                f"interval size {self._step_length} and shift {self._shift}."
            )
        interval = self._interval(time)
        if interval < self._start:
            raise KeyError("Cannot set value in the past.")
        else:
            self.advance_to(interval + 1)
            self._store[interval % len(self._store)] = value

    def set_from(self, time: int, values: Collection[V]) -> None:
        if not (time - self._shift) % self._step_length == 0:
            raise KeyError(
                f"Time {time} does not fall onto interval start when using "
                f"interval size {self._step_length} and shift {self._shift}."
            )
        interval = self._interval(time)
        if interval < self._start:
            raise KeyError("Cannot set values in the past")

        self.advance_to(interval + len(values))
        for i, value in enumerate(values):
            self._store[(interval + i) % len(self._store)] = value

    def advance_to(self, new_end: int):
        """
        Move the window so that it ends at `new_end` (exclusive).

        Calls the default factory to produce necessary intermediate values
        but avoids calls for values that would be overwritten anyways.
        """
        for i in range(
            max(new_end - len(self._store), self._start + len(self._store)),
            new_end,
        ):
            self._store[i % len(self._store)] = self._default_factory(
                i * self._step_length + self._shift
            )
        self._start = max(self._start, new_end - len(self._store))

    def __repr__(self):
        store_contents = (
            self._store[self._start % len(self._store) :]
            + self._store[: self._start % len(self._store)]
        )
        return (
            f"<TimeStorage from {self._start * self._step_length + self._shift}, "
            f"{(self._start + 1) * self._step_length + self._shift}, ..., is "
            f"{store_contents}>"
        )

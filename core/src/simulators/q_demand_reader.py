import datetime
import math
from random import Random
import mosaik_api_v3
import psycopg
import psycopg.rows
from mosaik_api_v3.types import SimId, Time
from mosaik_api_v3.datetime import Converter

META: mosaik_api_v3.Meta = {
    "api_version": "3.0",
    "type": "time-based",
    "models": {
        "QDemand": {
            "public": True,
            "params": [],
            "attrs": ["q"],
        }
    },
}


class Simulator(mosaik_api_v3.Simulator):
    def __init__(self):
        super().__init__(META)

    def init(
            self,
            sid: str,
            start_date: str,
            baseline_id: str,
            time_resolution: float = 1,
            beta: float = 0.2,
            extra_demand: float = 0.0,
            step_size=900,
            **db,
    ) -> mosaik_api_v3.Meta:
        self.created = False
        self.time_converter = Converter(
            start_date=start_date,
            time_resolution=time_resolution,
        )
        self.baseline_id = baseline_id
        self.conn = psycopg.connect(
            f"postgres://{db['db_user']}:{db['db_pass']}"
            f"@{db['db_host']}:{db['db_port']}/{db['db_name']}",
            row_factory=psycopg.rows.dict_row,
        )
        self.beta = beta
        self.extra_demand = extra_demand
        self.step_size = step_size
        self.conn.execute(
            "SELECT set_config('search_path', %s, false);", (db["schema_name"],)
        )
        self.q_demand = 0.0
        return self.meta

    def create(self, num: int, model: SimId) -> list[mosaik_api_v3.CreateResult]:
        assert num == 1
        assert model == "QDemand"
        assert not self.created
        self.created = True
        self.random = Random(73)
        return [
            {"eid": "QDemand", "type": "QDemand"},
        ]

    def step(self, time: int, inputs, max_advance: int) -> int | None:
        free_q = self.conn.execute(
            """
            SELECT coalesce(sum(q_free_amount), 0) as total_free_q
            FROM bids
            WHERE run_id = %(baseline_id)s
            AND time = %(time)s;
            """,
            params={
                "baseline_id": self.baseline_id,
                "time": self.time_converter.datetime_from_step(time),
            },
        ).fetchone()["total_free_q"]
        self.q_demand = free_q * math.exp(self.beta * self.random.gauss()) + self.extra_demand
        return time + self.step_size

    def get_data(self, outputs) -> dict[str, dict[str, float]]:
        return {
            "QDemand": {
                "q": self.q_demand
            }
        }

    def finalize(self):
        self.conn.close()
